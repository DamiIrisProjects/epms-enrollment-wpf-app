﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.Net;
using System.Net.Security;
using PassportEnrolmentContract;

namespace PassportEnrolmentProxy
{
    public class Proxy
    {
        public IContract Channel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public Proxy()
        {
            //Get from config
            ServiceBaseAddress = ConfigurationManager.AppSettings["ServiceUrl"] + "/Service1.svc";

            var binding = new WSHttpBinding();
            binding.MaxReceivedMessageSize = 4194304;//4 mb
            binding.Security.Mode = SecurityMode.None;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.SendTimeout = TimeSpan.FromMinutes(60);

            binding.CloseTimeout = TimeSpan.FromMinutes(60);
            binding.OpenTimeout = TimeSpan.FromMinutes(60);

            binding.ReceiveTimeout = TimeSpan.FromMinutes(60);
            binding.MaxReceivedMessageSize = int.MaxValue;


            binding.ReaderQuotas.MaxArrayLength = 4194304;

            // Until live
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true;
            };

            var cf = new ChannelFactory<IContract>(binding, ServiceBaseAddress);
            Channel = cf.CreateChannel();
        }
    }
}
