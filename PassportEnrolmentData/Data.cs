﻿using Afisteam.Biometrics;
using PassportEnrolment;
using PassportEnrolmentEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace PassportEnrolmentData
{
    public class Data : DataProvider
    {
        #region Operator Functions

        public string CreateOperator(Operator opr)
        {
            var query = string.Empty;
            var dset = new DataSet();

            // Verify User doesn't exist
            query = "select username from operators where lower(username) = @username";
            IDataParameter[] parama =
                {   
                    new SqlParameter("username",  opr.Username.ToLower())
                };

            RunProcedure(query, parama, dset);

            if (dset.Tables[0].Rows.Count != 0)
                return "1";

            var bioid = 0;

            if (opr.Biometrics != null)
            {
                // Verify Biometrics doesn't already exit
                if (opr.Biometrics.Fingers != null)
                {
                    var operators = VerifyOperatorFingerprint(opr.Biometrics.Fingers.Fingers[FingerIndex.LeftThumb].BestFingerprint.Template, true);

                    if (operators.Count != 0)
                        return operators[0].Username;
                }

                // Save Biometrics
                CreateTransactionCommand();
                query = "insert into biometrics (photo, photo_cv, fingerprint, fingerprint_image) values (@photo, @photo_cv, @fingerprint, @fingerprint_image)";

                IDataParameter[] paramb =
                {   
                    new SqlParameter("photo",  opr.Biometrics.PictureColor),
                    new SqlParameter("photo_cv",  opr.Biometrics.PictureBw),
                    new SqlParameter("fingerprint_image",  opr.Biometrics.FingerprintArray),
                    new SqlParameter("fingerprint",  opr.Biometrics.Fingers !=  null ? opr.Biometrics.Fingers.Fingers[FingerIndex.LeftThumb].BestFingerprint.Template : null)
                };

                command.Parameters.AddRange(paramb);
                command.CommandText = query;
                command.ExecuteNonQuery();

                bioid = GetIdNumber(command);
                command.Transaction.Commit();
            }

            // Create Operator
            var sh = SaltedHash.Create(opr.Password);

            query = "insert into operators (username, pwd_hash, pwd_salt, firstname, surname, biometrics_id, last_action, date_registered, is_active, is_disabled, is_logged) values (@username, @pwd_hash, @pwd_salt, @firstname, @surname, @biometrics_id, @last_action, @date_registered, 0, 0, 0)";
            IDataParameter[] param =
                {   
                    new SqlParameter("username",  opr.Username),
                    new SqlParameter("pwd_hash",  sh.Hash),
                    new SqlParameter("pwd_salt",  sh.Salt),
                    new SqlParameter("firstname",  opr.FirstName),
                    new SqlParameter("last_action",  DateTime.Now),
                    new SqlParameter("surname",  opr.Surname),
                    new SqlParameter("date_registered",  DateTime.Now),
                    new SqlParameter("biometrics_id",  bioid.ToString() == "0" ? null : bioid.ToString()),
                };

            ExecuteQuery(query, param);

            return string.Empty;
        }

        public Operator GetOperatorDetails(string variable, bool useId)
        {
            
            var query = string.Empty;
            var dset = new DataSet();

            if (useId)
            {
                query = "select operator_id, firstname, surname, username, pwd_hash, pwd_salt, current_login_attempts, is_active from operators where operator_id = @operator_id";
                IDataParameter[] param =
                {   
                    new SqlParameter("operator_id",  variable)
                };

                RunProcedure(query, param, dset);
            }
            else
            {
                query = "select operator_id, firstname, surname, username, pwd_hash, pwd_salt, current_login_attempts, is_active from operators where username = @username";
                IDataParameter[] param =
                {   
                    new SqlParameter("username",  variable)
                };

                RunProcedure(query, param, dset);
            }

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];
                var opr = new Operator()
                {
                    FirstName = row["firstname"].ToString(),
                    Surname = row["surname"].ToString(),
                    Username = row["username"].ToString(),
                    Hash = row["pwd_hash"].ToString(),
                    Salt = row["pwd_salt"].ToString(),
                    OperatorId = int.Parse(row["operator_id"].ToString()),
                    LoginAttempts = int.Parse(row["current_login_attempts"].ToString()),
                    IsActive = int.Parse(row["is_active"].ToString())
                };

                AddOperatorRoles(opr);
                return opr;
            }

            return null;
        }

        public int LoginOperator(ref Operator opr, bool useFinger)
        {
            var password = opr.Password;

            if (useFinger)
            {
                var matchedPeople = new List<Operator>();
                var bytes = opr.Biometrics.Fingers.Fingers[FingerIndex.LeftThumb].BestFingerprint.Template;
                var operators = VerifyOperatorFingerprint(bytes, true);

                if (bytes != null)
                {
                    var template1_1 = bytes;

                    // get biometrics of everyone
                    var people = GetAllOperators();
                    var matchedBins = new List<string>();

                    foreach (var person in people)
                    {
                        using (Afisteam.Biometrics.Math.IMatcher matcher = new Afisteam.Biometrics.Math.Innovatrics.IdkitMatcher())
                        {
                            matcher.clearTemplateSet();

                            // add user with unique id
                            try
                            {
                                matcher.AddUser(person.Biometrics.Fingers, person.OperatorId.ToString());

                                // Search all fingers for match
                                var Matches = matcher.Identify(template1_1, FingerIndex.LeftThumb);

                                if (Matches.Count != 0)
                                {
                                    foreach (Match match in Matches)
                                    {
                                        matchedBins.Add(match.PersonId);
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                // save error
                            }
                        }
                    }

                    if (matchedBins.Count != 0)
                    {
                        foreach (var pid in matchedBins)
                        {
                            matchedPeople.Add(people.FirstOrDefault(m => m.OperatorId.ToString() == pid));
                        }
                    }
                }
                if (operators.Count != 0)
                {
                    opr = operators[0];
                    return 0;
                }

                return 1;
            }
            else
            {
                // Get hash and salt from database
                opr = GetOperatorDetails(opr.Username, false);
                if (opr != null)
                {
                    // Verify
                    var sh = SaltedHash.Create(opr.Salt, opr.Hash);
                    var value = sh.Verify(password);

                    if (value)
                        return 0;
                    else
                        return 1;
                }

                return 2;
            }
        }

        private List<Operator> VerifyOperatorFingerprint(byte[] bytes, bool p)
        {
            throw new NotImplementedException();
        }

        public List<Operator> GetAllOperators()
        {
            
            var query = string.Empty;
            var dset = new DataSet();
            var operators = new List<Operator>();

            query = @"select operator_id, firstname, is_disabled, surname, username, is_active, fingerprint, fingerprint_image from operators o 
                    left join biometrics b on b.biometrics_id = o.biometrics_id";

            RunProcedure(query, null, dset);

            for (var i = 0; i < dset.Tables[0].Rows.Count; i++)
            {
                var row = dset.Tables[0].Rows[i];
                var p = new Operator
                {
                    OperatorId = int.Parse(row["operator_id"].ToString()),
                    FirstName = row["firstname"].ToString(),
                    Surname = row["surname"].ToString(),
                    Username = row["username"].ToString(),
                    IsActive = int.Parse(row["is_active"].ToString()),
                    Biometrics = new PersonBiometrics {FingerprintArray = row["fingerprint_image"] as byte[]}
                };

                var person = new Afisteam.Biometrics.Person();
                person.Fingers.Add(new Finger(FingerIndex.LeftThumb, row["fingerprint"] as byte[]));
                p.Biometrics.Fingers = person;
                AddOperatorRoles(p);
                operators.Add(p);
            }

            return operators;
        }

        private void AddOperatorRoles(Operator person)
        {
            
            var query = string.Empty;
            var dset = new DataSet();
            var operators = new List<Operator>();

            query = @"select r.Role_name, priv.Opr_privilege_name, r.Opr_role_id from Opr_role_person_link role_opr
					left join Opr_Roles r on role_opr.Role_id = r.Opr_role_id
                    left join Opr_privilege_role_link priv_link on role_opr.Role_id = priv_link.Opr_role_id
                    left join Opr_privileges priv on priv_link.Opr_privi_id = priv.Opr_privilege_id
                    where role_opr.Operator_id = @Operator_id and role_opr.Is_active = 1";

            IDataParameter[] param =
                {   
                    new SqlParameter("Operator_id",  person.OperatorId)
                };

            RunProcedure(query, param, dset);

            var roles = new List<Role>();
            var role = new Role();
            var currentRole = -1;

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                if (row["Opr_role_id"].ToString() != currentRole.ToString())
                {
                    role = new Role();
                    role.RoleName = row["Role_name"].ToString();
                    role.Privileges = new List<string>();
                    role.Privileges.Add(row["Opr_privilege_name"].ToString());
                    role.RoleId = int.Parse(row["Opr_role_id"].ToString());
                    currentRole = int.Parse(row["Opr_role_id"].ToString());
                    roles.Add(role);
                }
                else
                {
                    role.Privileges.Add(row["Opr_privilege_name"].ToString());
                }
            }

            person.OperatorRoles = roles;

            // modify permission names
            for (var i = 0; i < person.OperatorRoles.Count; i++)
            {
                for (var j = 0; j < person.OperatorRoles[i].Privileges.Count; j++)
                {
                    var privi = person.OperatorRoles[i].Privileges[j];
                    if (privi != "user_administrator")
                        person.OperatorRoles[i].Privileges[j] = privi;
                }
            }

            operators.Add(person);
        }

        public List<Role> GetAllRoles()
        {
            
            var dset = new DataSet();
            var role = new Role();

            var query = @"select r.opr_role_id, r.Role_name, priv.Opr_privilege_name from opr_roles r left join Opr_privilege_role_link priv_link on r.Opr_role_id = priv_link.Opr_role_id
                      left join Opr_privileges priv on priv.Opr_privilege_id = priv_link.Opr_privi_id order by r.Opr_role_id";

            RunProcedure(query, null, dset);

            var roles = new List<Role>();
            foreach (DataRow row in dset.Tables[0].Rows)
            {
                var createNew = true;
                foreach (var x in roles)
                {
                    createNew = x.RoleId.ToString() != row["opr_role_id"].ToString();
                }

                if (createNew)
                {
                    role = new Role();
                    role.RoleName = row["Role_name"].ToString();
                    role.Privileges = new List<string>();
                    role.Privileges.Add(row["Opr_privilege_name"].ToString());
                    role.RoleId = int.Parse(row["opr_role_id"].ToString());
                    roles.Add(role);
                }
                else
                {
                    role.Privileges.Add(row["Opr_privilege_name"].ToString());
                }
            }

            return roles;
        }

        public void CreateRole(Role role)
        {
            
            CreateTransactionCommand();
            var query = "insert into opr_roles (role_name) values (@role_name)";
            command.Parameters.Add(new SqlParameter("role_name", role.RoleName));
            command.CommandText = query;
            command.ExecuteNonQuery();

            var oprRoleId = GetIdNumber(command);
            command.Transaction.Commit();

            // add privileges
            foreach (var priv in role.Privileges)
            {
                var dset = new DataSet();
                query = "select Opr_privilege_id from opr_privileges where opr_privilege_name = @opr_privilege_name";
                IDataParameter[] paramb =
                {   
                    new SqlParameter("opr_privilege_name",  priv)
                };

                RunProcedure(query, paramb, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    query = "insert into Opr_privilege_role_link (Opr_role_id, Opr_privi_id) values (@Opr_role_id, @Opr_privi_id)";
                    IDataParameter[] paramc =
                    {   
                        new SqlParameter("Opr_role_id",  oprRoleId),
                        new SqlParameter("Opr_privi_id",  dset.Tables[0].Rows[0][0])
                    };

                    ExecuteQuery(query, paramc);
                }
            }
        }

        public void ResetOperatorPassword(Operator currentOperator)
        {
            var sh = SaltedHash.Create("newpassword");

            var query = "update operators set pwd_hash = #pwd_hash and pwd_salt = #pwd_hash where username = @username";
            IDataParameter[] param =
                {   
                    new SqlParameter("pwd_hash",  sh.Hash),
                    new SqlParameter("pwd_salt",  sh.Salt),
                    new SqlParameter("username",  currentOperator.Username)
                };

            ExecuteQuery(query, param);
        }

        public void UnlockAccount(Operator currentOperator)
        {
            var query = "update operators set is_disabled = 0, Current_login_attempts = 0 where operator_id = @operator_id";
            IDataParameter[] param =
                {   
                    new SqlParameter("operator_id",  currentOperator.OperatorId)
                };

            ExecuteQuery(query, param);
        }

        public void LockAccount(Operator currentOperator)
        {
            var query = "update operators set is_disabled = 1 where operator_id = @operator_id";
            IDataParameter[] param =
                {   
                    new SqlParameter("operator_id",  currentOperator.OperatorId)
                };

            ExecuteQuery(query, param);
        }

        public List<Operator> GetOperatorSeekingActivation()
        {
            return GetAllOperators().Where(x => x.IsActive == 0).ToList();
        }

        public void ActivateOperator(Operator currentAwaitingOperator)
        {
            
            var query = "update operators set is_active = 1 where operator_id = @operator_id";
            IDataParameter[] param =
                {   
                    new SqlParameter("operator_id",  currentAwaitingOperator.OperatorId)
                };

            ExecuteQuery(query, param);
        }

        public string ChangePassword(string username, string newpassword, string oldpassword)
        {
            // Verify password is correct
            var message = string.Empty;

            // Get hash and salt from database
            var opr = GetOperatorDetails(username, false);

            if (opr != null)
            {
                // Verify
                var sh = SaltedHash.Create(opr.Salt, opr.Hash);
                var value = sh.Verify(oldpassword);

                if (value)
                {
                    
                    var query = "update operators set pwd_hash = #pwd_hash and pwd_salt = #pwd_hash where username = @username";
                    IDataParameter[] param =
                    {   
                        new SqlParameter("pwd_hash",  sh.Hash),
                        new SqlParameter("pwd_salt",  sh.Salt),
                        new SqlParameter("username",  username)
                    };

                    ExecuteQuery(query, param);
                }
                else
                    // Are we going to lock accounts if this is done too many times?
                    message = "Incorrect password entered. Please check and try again";
            }
            else
            {
                message = "No user found matching this username.";
            }

            return message;
        }

        public void SetNumberOfPassword(int number)
        {

        }

        public void AddRole(Operator currentOperator, Role role)
        {
            
            var query = "insert into Opr_role_person_link (Operator_id, Role_id, Is_active) values (@Operator_id, @Role_id, @Is_active)";
            IDataParameter[] param =
                {   
                    new SqlParameter("Operator_id",  currentOperator.OperatorId),
                    new SqlParameter("Role_id",  role.RoleId),
                    new SqlParameter("Is_active",  1)
                };

            ExecuteQuery(query, param);
        }

        public void RemoveRole(int roleId, int oprId)
        {
            
            var query = "update Opr_role_person_link set is_active = 0 where role_id = @role_id and person_id = @person_id and is_active = 1";
            IDataParameter[] param =
                {   
                    new SqlParameter("role_id",  roleId),
                    new SqlParameter("person_id",  oprId)
                };

            ExecuteQuery(query, param);
        }

        #endregion
    }
}
