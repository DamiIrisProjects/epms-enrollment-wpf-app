﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Data.OracleClient;

namespace PassportEnrolment
{
    public class DataProvider
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        private string connectionType = ConfigurationManager.ConnectionStrings["ConnectionType"].ToString();
        DbConnection connection;
        public DbCommand command;

        public void CreateConnection()
        {
            if (connection == null)
            {
                try
                {
                    if (connectionType == "MySQL")
                        connection = new MySqlConnection(connectionString);
                    if (connectionType == "MSSQL")
                        connection = new SqlConnection(connectionString);
                    if (connectionType == "Oracle")
                        connection = new OracleConnection(connectionString);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void CreateTransactionCommand()
        {
            if (command != null && command.Transaction != null && command.Transaction.Connection != null && command.Transaction.Connection.State == ConnectionState.Open)
                command.Transaction.Connection.Close();

            if (connection == null)
                CreateConnection();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            DbTransaction transaction = connection.BeginTransaction();

            command = connection.CreateCommand();
            command.Transaction = transaction;
        }

        public int ExecuteQuery(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected = -1;

            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                try
                {
                    con.Open();

                    using (DbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = strQuery;
                        if (parameters != null && parameters.Length > 0)
                        {
                            for (int i = 0; i < parameters.Length; i++)
                            {
                                cmd.Parameters.Add(parameters[i]);
                            }
                        }

                        cmd.Connection = con;
                        iRowsAffected = cmd.ExecuteNonQuery();
                    }
                }
                catch
                { throw; }
            }

            return iRowsAffected;
        }

        public void RunProcedure(string strQuery, IDataParameter[] parameters, DataSet dataSet)
        {
            DbConnection con = GetNewConnection();                       

            try
            {
                using (DbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    DbDataAdapter MySqlDA = new MySqlDataAdapter();
                    MySqlDA.SelectCommand = cmd;
                    MySqlDA.Fill(dataSet);

                    con.Close();
                    con.Dispose();
                }
            }
            catch (Exception)
            {
                con.Close();
                con.Dispose();
                throw;
            }
        }

        private DbConnection GetNewConnection()
        {
            DbConnection con = null;

            switch (connectionType)
            {
                case "MySQL":
                    {
                        con = new MySqlConnection(connectionString); break;
                    }

                case "MSSQL":
                    {
                        con = new SqlConnection(connectionString); break;
                    }

                case "Oracle":
                    {
                        con = new OracleConnection(connectionString); break;
                    }
            }

            return con;
        }

        public int GetIdNumber(DbCommand cmd)
        {
            DataSet dset = new DataSet();
            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                switch (connectionType)
                {
                    case "MySQL":
                        {
                            cmd.CommandText = "select last_insert_id();"; break;
                        }

                    case "MSSQL":
                        {
                            break;
                        }

                    case "Oracle":
                        {
                            break;
                        }
                }
               
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CloseQuery()
        {
            connection.Close();
        }
    }
}
