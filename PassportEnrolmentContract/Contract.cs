﻿using PassportEnrolmentEntities;
using System.Collections.Generic;
using System.ServiceModel;

namespace PassportEnrolmentContract
{
    [ServiceContract]
    public interface IContract
    {
        #region Operator Functions

        [OperationContract]
        string CreateOperator(Operator opr);

        [OperationContract]
        Operator GetOperatorDetails(string variable, bool useId);

        [OperationContract]
        int LoginOperator(ref Operator opr, bool userFinger);

        [OperationContract]
        List<Role> GetAllRoles();

        [OperationContract]
        void CreateRole(Role role);

        [OperationContract]
        List<Operator> GetAllOperators();

        [OperationContract]
        void ResetOperatorPassword(Operator currentOperator);

        [OperationContract]
        void UnlockAccount(Operator currentOperator);

        [OperationContract]
        void LockAccount(Operator currentOperator);

        [OperationContract]
        List<Operator> GetOperatorSeekingActivation();

        [OperationContract]
        void ActivateOperator(Operator currentAwaitingOperator);

        [OperationContract]
        string ChangePassword(string username, string newpassword, string oldpassword);

        [OperationContract]
        void SetNumberOfPassword(int p);

        [OperationContract]
        void AddRole(Operator currentOperator, Role role);

        [OperationContract]
        void RemoveRole(int roleId, int oprId);

        #endregion
    }
}
