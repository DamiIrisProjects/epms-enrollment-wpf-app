﻿using PassportEnrolmentContract;
using PassportEnrolmentData;
using PassportEnrolmentEntities;
using System.Collections.Generic;

namespace PassportEnrolmentService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IContract
    {
        #region Operator Functions

        public string CreateOperator(Operator opr)
        {
            Data data = new Data();
            return data.CreateOperator(opr);
        }

        public Operator GetOperatorDetails(string variable, bool useId)
        {
            Data data = new Data();
            return data.GetOperatorDetails(variable, useId);
        }

        public int LoginOperator(ref Operator opr, bool userFinger)
        {
            Data data = new Data();
            return data.LoginOperator(ref opr, userFinger);
        }

        public List<Role> GetAllRoles()
        {
            Data data = new Data();
            return data.GetAllRoles();
        }

        public void CreateRole(Role role)
        {
            Data data = new Data();
            data.CreateRole(role);
        }

        public List<Operator> GetAllOperators()
        {
            Data data = new Data();
            return data.GetAllOperators();
        }

        public void ResetOperatorPassword(Operator currentOperator)
        {
            Data data = new Data();
            data.ResetOperatorPassword(currentOperator);
        }

        public void UnlockAccount(Operator currentOperator)
        {
            Data data = new Data();
            data.UnlockAccount(currentOperator);
        }

        public void LockAccount(Operator currentOperator)
        {
            Data data = new Data();
            data.LockAccount(currentOperator);
        }

        public List<Operator> GetOperatorSeekingActivation()
        {
            Data data = new Data();
            return data.GetOperatorSeekingActivation();
        }

        public void ActivateOperator(Operator currentAwaitingOperator)
        {
            Data data = new Data();
            data.ActivateOperator(currentAwaitingOperator);
        }

        public string ChangePassword(string username, string newpassword, string oldpassword)
        {
            Data data = new Data();
            return data.ChangePassword(username, newpassword, oldpassword);
        }

        public void SetNumberOfPassword(int number)
        {
            Data data = new Data();
            data.SetNumberOfPassword(number);
        }

        public void AddRole(Operator currentOperator, Role role)
        {
            Data data = new Data();
            data.AddRole(currentOperator, role);
        }

        public void RemoveRole(int roleId, int oprId)
        {
            Data data = new Data();
            data.RemoveRole(roleId, oprId);
        }

        #endregion
    }
}
