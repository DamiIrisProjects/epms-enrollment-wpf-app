﻿using Afisteam.Biometrics.Gui;
using PassportEnrolment.Helpers;
using PassportEnrolmentEntities;
using System;
using System.Windows;
using System.Windows.Input;

namespace PassportEnrolment.Windows
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen
    {
        #region Variables

        private Cursor _def;

        #endregion

        #region Constructor

        public LoginScreen()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Password;
            string message = string.Empty;
            var operatr = new Operator();
            bool allow = false;

            if (username != "admin" || password != "admin")
            {
                using (var webClient = new System.Net.WebClient())
                {
                    string url = "http://localhost:49140/umaservice/Login/57/test@test.com/damilola";
                    webClient.DownloadString(url);
                    //JObject jObj = (JObject)JsonConvert.DeserializeObject(json);
                }
            }
            else
                allow = true;

            if (message == string.Empty || allow)
            {
                DoLogin(operatr, allow);
            }
            else
            {
                MessageBox.Show(message, "Login Failed", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }           

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtUsername.Focus();
        }

        private void TextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            _def = Cursor;
            Cursor = Cursors.Hand;
        }

        private void TextBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = _def;
        }

        private void btnChangePassoword_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var pwdWin = new ChangePassword();
            pwdWin.ShowDialog();
        }

        private void btnRegister_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var registerWin = new RegisterUser();
            registerWin.ShowDialog();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void loginWithfinger_Click(object sender, RoutedEventArgs e)
        {
            byte[] fingerToTest = null;

            try
            {
                // Get a fingerprint
                Helper.SetDefaultFingerprintScannerSettings();

                var finger = new Afisteam.Biometrics.Finger(Afisteam.Biometrics.FingerIndex.LeftThumb);
                var fingerScanForm = new FingerScanForm(finger);
                var showDialog = fingerScanForm.ShowDialog();
                if (showDialog != null && showDialog.Value)
                {
                    fingerToTest = finger.BestFingerprint.Template;
                }

                // Check if its an operator
                var opr = VerifyOperator();

                if (opr == null)
                {
                    MessageBox.Show("Could not find an operator matching this fingerprint", "No Match Found", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Fingerprint verified: " + opr.FirstName + " " + opr.Surname, "Login Successful", MessageBoxButton.OK, MessageBoxImage.Information);
                    DoLogin(opr, false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Initializing Fingerprint", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }               

        #endregion

        #region Operations

        private void DoLogin(Operator operatr, bool allow)
        {
            PiggyBank.CurrentOperator = operatr;

            if (allow)
            {
                SetCurrentRole();
            }
            else
                PiggyBank.CurrentRoles = operatr.OperatorRoles;


            var window = new MainWindow();
            //window.SetRoleItems();

            window.Show();
            Close();
        }

        private static Operator VerifyOperator()
        {
            throw new NotImplementedException();
        }

        private void SetCurrentRole()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
