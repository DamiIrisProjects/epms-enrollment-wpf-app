﻿using System;
using System.Windows;

namespace PassportEnrolment.Windows
{
    /// <summary>
    /// Interaction logic for RegisterUser.xaml
    /// </summary>
    public partial class RegisterUser
    {
        public RegisterUser()
        {
            InitializeComponent();
        }

        #region Operations

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!OperatorDetails.ValidateInfo())
            {
                TabControl.SelectedIndex = 0;
                return;
            }


            if (!LoginDetails.ValidateUsernamePW())
            {
                TabControl.SelectedIndex = 1;
                return;
            }

            // Send for approval
            //Operator op = OperatorDetails.GetUserDetails();
            //op.Biometrics = PersonBiometrics.Biometrics;

            try
            {
                // First verify user doesn't already exist
                string exists = ""; // DataLink.VerifyOperatorExists(op, LoginDetails.txtUsername.Text, LoginDetails.txtPassword.Password);

                if (exists == string.Empty)
                {
                    // Then add User
                    //DataLink.AddOperator(op, LoginDetails.txtUsername.Text, LoginDetails.txtPassword.Password);
                    MessageBox.Show("Operator created and pending approval", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
                else
                {
                    if (exists == "1")
                    {
                        MessageBox.Show("Username already in use.", "Validation Failed", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("A record your fingerprint already exists as Username : '" + exists + "'", "Validation Failed", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Could not save operator", "Error Saving Operator", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion
    }
}
