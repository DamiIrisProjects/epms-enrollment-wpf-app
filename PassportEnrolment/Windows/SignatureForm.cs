﻿// Notes:
// There are three coordinate spaces to deal with that are named:
//   tablet: the raw tablet coordinate
//   screen: the tablet LCD screen
//   client: the Form window client area

using PassportEnrolment.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PassportEnrolment.Windows
{
    public partial class SignatureForm : Form
  {
    private wgssSTU.Tablet       _mTablet;
    private readonly wgssSTU.ICapability  _mCapability;
    private readonly wgssSTU.IInformation _mInformation;
    public Bitmap Signature;


    // In order to simulate buttons, we have our own Button class that stores the bounds and event handler.
    // Using an array of these makes it easy to add or remove buttons as desired.
    private delegate void ButtonClick();
    private struct Button
    {
      public Rectangle Bounds; // in Screen coordinates
      public String Text;
      public EventHandler Click;

      public void PerformClick()
      {
        Click(this, null);
      }
    };

    private Pen _mPenInk;  // cached object.
    
    // The isDown flag is used like this:
    // 0 = up
    // +ve = down, pressed on button number
    // -1 = down, inking
    // -2 = down, ignoring
    private int _mIsDown;

    private List<wgssSTU.IPenData> _mPenData; // Array of data being stored. This can be subsequently used as desired.

    private readonly Button[] _mBtns; // The array of buttons that we are emulating.

    private readonly wgssSTU.encodingMode _mEncodingMode; // How we send the bitmap to the device.
    private readonly byte[] _mBitmapData; // This is the flattened data of the bitmap that we send to the device.

    // As per the file comment, there are three coordinate systems to deal with.
    // To help understand, we have left the calculations in place rather than optimise them.

    private PointF TabletToClient(wgssSTU.IPenData penData)
    {
      // Client means the Windows Form coordinates.
      return new PointF((float)penData.x * ClientSize.Width / _mCapability.tabletMaxX, (float)penData.y * ClientSize.Height / _mCapability.tabletMaxY);
    }

    private Point TabletToScreen(wgssSTU.IPenData penData)
    {
      // Screen means LCD screen of the tablet.
      return Point.Round(new PointF((float)penData.x * _mCapability.screenWidth / _mCapability.tabletMaxX, (float)penData.y * _mCapability.screenHeight / _mCapability.tabletMaxY));
    }
    
    private Point ClientToScreen(Point pt)
    {
      // client (window) coordinates to LCD screen coordinates. 
      // This is needed for converting mouse coordinates into LCD bitmap coordinates as that's
      // what this application uses as the coordinate space for buttons.
      return Point.Round(new PointF((float)pt.X * _mCapability.screenWidth / ClientSize.Width, (float)pt.Y * _mCapability.screenHeight / ClientSize.Height));
    }

    private void ClearScreen()
    {
      // note: There is no need to clear the tablet screen prior to writing an image.
      _mTablet.writeImage((byte)_mEncodingMode, _mBitmapData);

      _mPenData.Clear();
      _mIsDown = 0;
      Invalidate();
    }


    private void btnOk_Click(object sender, EventArgs e)
    {
      // You probably want to add additional processing here.
              if (_mPenData.Count > 0) {
                  DialogResult = DialogResult.OK;
                  Close();
              }
    }


    private void btnCancel_Click(object sender, EventArgs e)
    {
      // You probably want to add additional processing here.
      _mPenData.Clear();
      DialogResult = DialogResult.Cancel;
      Close();
    }

    
    private void btnClear_Click(object sender, EventArgs e)
    {
      if (_mPenData.Count != 0)
      {
        ClearScreen();
      }
    }

    
    // Pass in the device you want to connect to!
    public SignatureForm(BiometricsControl parent, wgssSTU.IUsbDevice usbDevice)
    {
        // This is a DPI aware application, so ensure you understand how .NET client coordinates work.
      // Testing using a Windows installation set to a high DPI is recommended to understand how
      // values get scaled or not.

      AutoScaleDimensions = new SizeF(96F, 96F);
      AutoScaleMode = AutoScaleMode.Dpi;

      InitializeComponent();

      _mPenData = new List<wgssSTU.IPenData>();

      _mTablet = new wgssSTU.Tablet();
      wgssSTU.ProtocolHelper protocolHelper = new wgssSTU.ProtocolHelper();

      // A more sophisticated applications should cycle for a few times as the connection may only be
      // temporarily unavailable for a second or so. 
      // For example, if a background process such as Wacom STU Display
      // is running, this periodically updates a slideshow of images to the device.

      wgssSTU.IErrorCode ec = _mTablet.usbConnect(usbDevice, false);
      if (ec.value == 0)
      {
        _mCapability = _mTablet.getCapability();
        _mInformation = _mTablet.getInformation();
      }
      else
      {
        throw new Exception(ec.message);
      }

      SuspendLayout();
      AutoScaleDimensions = new SizeF(96F, 96F);
      AutoScaleMode = AutoScaleMode.Dpi;

      // Set the size of the client window to be actual size, 
      // based on the reported DPI of the monitor.

      Size clientSize = new Size((int)(_mCapability.tabletMaxX / 2540F * 96F), (int)(_mCapability.tabletMaxY / 2540F * 96F));
      ClientSize = clientSize;
      ResumeLayout();

      _mBtns = new Button[3];
      if (usbDevice.idProduct != 0x00a2)
      {
        // Place the buttons across the bottom of the screen.

        int w2 = _mCapability.screenWidth / 2;
        //int w3 = m_capability.screenWidth / 3;
        //int w1 = m_capability.screenWidth - w2 - w3;
        int w1 = _mCapability.screenWidth / 2;
        int y = _mCapability.screenHeight * 6 / 7;
        int h = _mCapability.screenHeight - y;

        _mBtns[0].Bounds = new Rectangle(0, y, w1, h);
        _mBtns[1].Bounds = new Rectangle(w1, y, w2, h);
        //m_btns[2].Bounds = new Rectangle(w1 + w2, y, w3, h);
      }
      else
      {
        // The STU-300 is very shallow, so it is better to utilise
        // the buttons to the side of the display instead.

        int x = _mCapability.screenWidth * 3 / 4;
        int w = _mCapability.screenWidth - x;

        int h2 = _mCapability.screenHeight / 2;
        int h1 = _mCapability.screenHeight / 2;
        //int h1 = m_capability.screenHeight - h2 - h3;

        _mBtns[0].Bounds = new Rectangle(x, 0, w, h1);
        _mBtns[1].Bounds = new Rectangle(x, h1, w, h2);
        //m_btns[2].Bounds = new Rectangle(x, h1 + h2, w, h3);
      }
      _mBtns[0].Text = "OK";
      _mBtns[1].Text = "Clear";
      //m_btns[2].Text = "Cancel";
      _mBtns[0].Click = btnOk_Click;
      _mBtns[1].Click = btnClear_Click;
      //m_btns[2].Click = new EventHandler(btnCancel_Click);


      // Disable color if the STU-520 bulk driver isn't installed.
      // This isn't necessary, but uploading colour images with out the driver
      // is very slow.

      // Calculate the encodingMode that will be used to update the image

    ushort idP = _mTablet.getProductId();
    wgssSTU.encodingFlag encodingFlag = (wgssSTU.encodingFlag)protocolHelper.simulateEncodingFlag(idP);
    bool useColor = false;
    if ((encodingFlag & (wgssSTU.encodingFlag.EncodingFlag_16bit | wgssSTU.encodingFlag.EncodingFlag_24bit)) != 0)
    {
        if (_mTablet.supportsWrite())
            useColor = true;
    }
    if ((encodingFlag & wgssSTU.encodingFlag.EncodingFlag_24bit) != 0)
    {
        _mEncodingMode = _mTablet.supportsWrite() ? wgssSTU.encodingMode.EncodingMode_24bit_Bulk : wgssSTU.encodingMode.EncodingMode_24bit;
    }
    else if ((encodingFlag & wgssSTU.encodingFlag.EncodingFlag_16bit) != 0)
    {
        _mEncodingMode = _mTablet.supportsWrite() ? wgssSTU.encodingMode.EncodingMode_16bit_Bulk : wgssSTU.encodingMode.EncodingMode_16bit;
    }
    else
    {
        // assumes 1bit is available
        _mEncodingMode = wgssSTU.encodingMode.EncodingMode_1bit;
    }

 

      // Size the bitmap to the size of the LCD screen.
      // This application uses the same bitmap for both the screen and client (window).
      // However, at high DPI, this bitmap will be stretch and it would be better to 
      // create individual bitmaps for screen and client at native resolutions.
      var mBitmap = new Bitmap(_mCapability.screenWidth, _mCapability.screenHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
      {
        Graphics gfx = Graphics.FromImage(mBitmap);
        gfx.Clear(Color.White);

        // Uses pixels for units as DPI won't be accurate for tablet LCD.
        Font font = new Font(FontFamily.GenericSansSerif, _mBtns[0].Bounds.Height / 2F, GraphicsUnit.Pixel);
        StringFormat sf = new StringFormat();
        sf.Alignment = StringAlignment.Center;
        sf.LineAlignment = StringAlignment.Center;

        if (useColor)
        {
          gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
        }
        else
        {
          // Anti-aliasing should be turned off for monochrome devices.
          gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixel;
        }

        // Draw the buttons
        for (int i = 0; i < _mBtns.Length; ++i)
        {
          if (useColor)
          {
            gfx.FillRectangle(Brushes.LightGray, _mBtns[i].Bounds);
          }
          gfx.DrawRectangle(Pens.Black, _mBtns[i].Bounds);
          gfx.DrawString(_mBtns[i].Text, font, Brushes.Black, _mBtns[i].Bounds, sf);
        }

        gfx.Dispose();
        font.Dispose();

        // Finally, use this bitmap for the window background.
        BackgroundImage = mBitmap;
        BackgroundImageLayout = ImageLayout.Stretch;
      }

      // Now the bitmap has been created, it needs to be converted to device-native
      // format.
      {

        // Unfortunately it is not possible for the native COM component to
        // understand .NET bitmaps. We have therefore convert the .NET bitmap
        // into a memory blob that will be understood by COM.

        System.IO.MemoryStream stream = new System.IO.MemoryStream();
        mBitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
        _mBitmapData = (byte[])protocolHelper.resizeAndFlatten(stream.ToArray(), 0, 0, (uint)mBitmap.Width, (uint)mBitmap.Height, _mCapability.screenWidth, _mCapability.screenHeight, (byte)_mEncodingMode, wgssSTU.Scale.Scale_Fit, 0, 0);
        protocolHelper = null;
        stream.Dispose();
      }

      // If you wish to further optimize image transfer, you can compress the image using 
      // the Zlib algorithm.
      
      bool useZlibCompression = false;
      if (!useColor && useZlibCompression)
      {
        // m_bitmapData = compress_using_zlib(m_bitmapData); // insert compression here!
        _mEncodingMode |= wgssSTU.encodingMode.EncodingMode_Zlib;
      }

      // Calculate the size and cache the inking pen.
      
      SizeF s = AutoScaleDimensions;
      float inkWidthMm = 0.7F;
      _mPenInk = new Pen(Color.DarkBlue, inkWidthMm / 25.4F * ((s.Width + s.Height) / 2F));
      _mPenInk.StartCap = _mPenInk.EndCap = System.Drawing.Drawing2D.LineCap.Round;
      _mPenInk.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;

      
      // Add the delagate that receives pen data.
      _mTablet.onPenData += new wgssSTU.ITabletEvents2_onPenDataEventHandler(OnPenData);
      _mTablet.onGetReportException += new wgssSTU.ITabletEvents2_onGetReportExceptionEventHandler(OnGetReportException);


      // Initialize the screen
      ClearScreen();

      // Enable the pen data on the screen (if not already)
      _mTablet.setInkingMode(0x01);
    }



    private void Form2_FormClosed(object sender, FormClosedEventArgs e)
    {
      // Ensure that you correctly disconnect from the tablet, otherwise you are 
      // likely to get errors when wanting to connect a second time.
      if (_mTablet != null)
      {
        _mTablet.onPenData -= new wgssSTU.ITabletEvents2_onPenDataEventHandler(OnPenData);
        _mTablet.onGetReportException -= new wgssSTU.ITabletEvents2_onGetReportExceptionEventHandler(OnGetReportException);
        
        _mTablet.setInkingMode(0x00);
        _mTablet.setClearScreen();
        _mTablet.disconnect();
      }

      _mPenInk.Dispose();
    }

    private void OnGetReportException(wgssSTU.ITabletEventsException tabletEventsException)
    {
      try
      {
        tabletEventsException.getException();
      }
      catch (Exception e)
      {
        MessageBox.Show("Error: " + e.Message);
        _mTablet.disconnect();
        _mTablet = null;
        _mPenData = null;
        Close();
      }
    }

    private void OnPenData(wgssSTU.IPenData penData) // Process incoming pen data
    {
      Point pt = TabletToScreen(penData);

      int btn = 0; // will be +ve if the pen is over a button.
      {        
        for (int i = 0; i < _mBtns.Length; ++i)
        {
          if (_mBtns[i].Bounds.Contains(pt))
          {
            btn = i+1;
            break;
          }          
        }
      }

      bool isDown = (penData.sw != 0);

      // This code uses a model of four states the pen can be in:
      // down or up, and whether this is the first sample of that state.

      if (isDown)
      {
        if (_mIsDown == 0)
        {
          // transition to down
          if (btn > 0)
          {
            // We have put the pen down on a button.
            // Track the pen without inking on the client.

            _mIsDown = btn; 
          }
          else
          {
            // We have put the pen down somewhere else.
            // Treat it as part of the signature.

            _mIsDown = -1;
          }
        }
        else
        {
          // already down, keep doing what we're doing!
        }

        // draw
        if (_mPenData.Count != 0 && _mIsDown == -1)
        {
          // Draw a line from the previous down point to this down point.
          // This is the simplist thing you can do; a more sophisticated program
          // can perform higher quality rendering than this!
          
          Graphics gfx = CreateGraphics();
          gfx.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
          gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
          gfx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
          gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
          
          wgssSTU.IPenData prevPenData = _mPenData[_mPenData.Count - 1];

          PointF prev = TabletToClient(prevPenData);

          gfx.DrawLine(_mPenInk, prev, TabletToClient(penData));
          gfx.Dispose();
        }

        // The pen is down, store it for use later.
        if (_mIsDown == -1)
          _mPenData.Add(penData);
      }
      else
      {
        if (_mIsDown != 0)
        {
          // transition to up
          if (btn > 0)
          {
            // The pen is over a button

            if (btn == _mIsDown)
            {
              // The pen was pressed down over the same button as is was lifted now. 
              // Consider that as a click!
              _mBtns[btn - 1].PerformClick();
            }
          }
          _mIsDown = 0;
        }
        else
        {
           // still up
        }

        // Add up data once we have collected some down data.
        if (_mPenData.Count != 0)
          _mPenData.Add(penData);
      }
    }

       


    private void Form2_Paint(object sender, PaintEventArgs e)
    {
      if (_mPenData.Count != 0)
      {
        // Redraw all the pen data up until now!

        Graphics gfx = e.Graphics;
        gfx.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
        gfx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
        gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        bool isDown = false;
        PointF prev = new PointF();
        for (int i = 0; i < _mPenData.Count; ++i)
        {
          if (_mPenData[i].sw != 0)
          {
            if (!isDown)
            {
              isDown = true;
              prev = TabletToClient(_mPenData[i]);
            }
            else
            {
              PointF curr = TabletToClient(_mPenData[i]);
              gfx.DrawLine(_mPenInk, prev, curr);
              prev = curr;
            }
          }
          else
          {
            if (isDown)
            {
              isDown = false;
            }
          }
        }
      }
          
    }

    private void Form2_MouseClick(object sender, MouseEventArgs e)
    {      
      // Enable the mouse to click on the simulated buttons that we have displayed.
      
      // Note that this can add some tricky logic into processing pen data
      // if the pen was down at the time of this click, especially if the pen was logically
      // also 'pressing' a button! This demo however ignores any that.

      Point pt = ClientToScreen(e.Location);
      foreach (Button btn in _mBtns)
      {
        if (btn.Bounds.Contains(pt))
        {
          btn.PerformClick();
          break;
        }
      }
    }
   
    public List<wgssSTU.IPenData> GetPenData()
    {
      return _mPenData;
    }

    public wgssSTU.ICapability GetCapability()
    {
      return _mPenData != null ? _mCapability : null;
    }

    public wgssSTU.IInformation GetInformation()
    {
      return _mPenData != null ? _mInformation : null;
    }

    public Bitmap GetSigImage() {

        Bitmap bitmap;
        SolidBrush brush;
        Point p1, p2;

        Rectangle rect = new Rectangle(0, 0,
            _mCapability.screenWidth, _mCapability.screenHeight);

        try
        {
            bitmap = new Bitmap(rect.Width, rect.Height);
            Graphics gfx = Graphics.FromImage(bitmap);
            SizeF s = AutoScaleDimensions;
            //            Dim inkWidthMM = 0.7F
            Single inkWidthMm = 1.0F;
            _mPenInk = new Pen(Color.DarkBlue, inkWidthMm / 25.4F * ((s.Width + s.Height) / 2.0F));
            _mPenInk.StartCap = _mPenInk.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            _mPenInk.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;

            gfx.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            gfx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            gfx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            brush = new SolidBrush(Color.White);
            gfx.FillRectangle(brush, 0, 0, rect.Width, rect.Height);

            for( int i=1; i<_mPenData.Count; ++i)
            {
                p1 = TabletToScreen(_mPenData[i - 1]);
                p2 = TabletToScreen(_mPenData[i]);

                if (_mPenData[i-1].sw > 0 || _mPenData[i].sw > 0)
                {
                    gfx.DrawLine(_mPenInk, p1, p2);
                }
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show("Exception: " + ex.Message);
            bitmap = null;
        }
        return bitmap;
      }
  }
}
