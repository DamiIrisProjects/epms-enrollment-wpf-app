﻿using CameraControllerTest;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class CloseSessionCommand : Command
    {
        // Command IDs
        public const int errr = 1;
       

        public CloseSessionCommand(CameraModel inModel)
            : base(inModel)
        {
        }


        //// Execute a command.
        public override bool execute()
        {
            int err = EDSDKErrors.EDS_ERR_OK;

            //// Open session with remote camera.
            err = EDSDK.EdsCloseSession(base.model.getCameraObject());


            //// Notify Error.
            if (err != EDSDKErrors.EDS_ERR_OK)
            {
                base.model.notifyObservers(errr, err);
            }
            return true;

        }


    }

}