﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class SaveSettingCommand : Command
    {
        // Command IDs
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;

        private CameraControllerTest.EDSDKTypes.EdsSaveTo _saveTo;
        public SaveSettingCommand(CameraModel model, CameraControllerTest.EDSDKTypes.EdsSaveTo saveTo)
            : base(model)
        {
            _saveTo = saveTo;
        }


        //// Execute a command.
        public override bool execute()
        {

            int err = (int)EDSDKErrors.EDS_ERR_OK;
            bool @lock = false;

            //// You should do UILock when you send a command to camera models elder than EOS30D.

            if (base.model.isLegacy())
            {
                uint test = (uint)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UILock;
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), 0, 0);


                if (err == (int)EDSDKErrors.EDS_ERR_OK)
                {
                    @lock = true;

                }
            }

            ////Set destination of file save.

            if (err == (int)EDSDKErrors.EDS_ERR_OK)
            {

                IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(this._saveTo));
                Marshal.StructureToPtr(this._saveTo, ptr, false);

                err = EDSDK.EdsSetPropertyData(base.model.getCameraObject(), EDSDKTypes.kEdsPropID_SaveTo, 0, Marshal.SizeOf(this._saveTo), this._saveTo);

                Marshal.FreeHGlobal(ptr);
            }


            if (@lock == true)
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UIUnLock, 0);

            }

            //// Notify Error.

            if (err != EDSDKErrors.EDS_ERR_OK)
            {
                //// Retry when the camera replys deviceBusy.
                if (err == EDSDKErrors.EDS_ERR_DEVICE_BUSY)
                {
                    base.model.notifyObservers(warn, err);
                    System.Threading.Thread.Sleep(500);
                    return false;

                }

                base.model.notifyObservers(errr, err);

            }

            return true;

        }

    }
}