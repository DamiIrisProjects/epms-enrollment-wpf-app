﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{

    public class OpenSessionCommand : Command
    {
        // Command IDs
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;

        public OpenSessionCommand(CameraModel model)
            : base(model)
        {
        }

        //// Execute a command.	
        public override bool execute()
        {

            //Dim err As Integer = EDSDKErrors.EDS_ERR_OK

            //// Open session with remote camera.
            //err = EdsOpenSession(MyBase.model.getCameraObject())


            ////Notify Error
            //If err <> EDSDKErrors.EDS_ERR_OK Then

            //MyBase.model.notifyObservers(errr, err)

            //End If

            //Return True


            int err = EDSDKErrors.EDS_ERR_OK;
            bool locked = false;

            //// Open session with remote camera.
            err = EDSDK.EdsOpenSession(base.model.getCameraObject());



            if (base.model.isLegacy())
            {
                //Preservation ahead is set to PC

                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    int saveTo = (int)EDSDKTypes.EdsSaveTo.kEdsSaveTo_Host;

                    err = EDSDK.EdsSetPropertyData(base.model.getCameraObject(), EDSDKTypes.kEdsPropID_SaveTo, 0, Marshal.SizeOf(saveTo), saveTo);

                }

                GetPropertyCommand getPropertyCommand = default(GetPropertyCommand);
                getPropertyCommand = new GetPropertyCommand(base.model, EDSDKTypes.kEdsPropID_Unknown);
                getPropertyCommand.execute();

                GetPropertyDescCommand getPropertyDescCommand = default(GetPropertyDescCommand);
                getPropertyDescCommand = new GetPropertyDescCommand(base.model, EDSDKTypes.kEdsPropID_Unknown);
                getPropertyDescCommand.execute();


            }
            else
            {
                //Preservation ahead is set to PC

                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    int saveTo = (int)EDSDKTypes.EdsSaveTo.kEdsSaveTo_Host;
                    err = EDSDK.EdsSetPropertyData(base.model.getCameraObject(), EDSDKTypes.kEdsPropID_SaveTo, 0, Marshal.SizeOf(saveTo), saveTo);

                }


                //UI lock

                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UILock, 0);
                }


                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    locked = true;

                }



                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    EDSDKTypes.EdsCapacity capacity = default(EDSDKTypes.EdsCapacity);
                    capacity.numberOfFreeClusters = 0x7fffffff;
                    capacity.bytesPerSector = 0x1000;
                    capacity.reset = true;

                    err = EDSDK.EdsSetCapacity(base.model.getCameraObject(), capacity);

                }

                //It releases it when locked

                if (locked)
                {
                    EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UIUnLock, 0);

                }

            }


            //Notification of error

            if (err < EDSDKErrors.EDS_ERR_OK)
            {
                //CameraEvent e("error", &err);
                base.model.notifyObservers(errr, err);

            }


            return true;
        }

    }

}