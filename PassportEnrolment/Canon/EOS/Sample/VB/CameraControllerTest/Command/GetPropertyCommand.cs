﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class GetPropertyCommand : Command
    {
        // Command IDs
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;

        private int propertyID;

        public GetPropertyCommand(CameraModel model, int propertyID)
            : base(model)
        {
            this.propertyID = propertyID;
        }

        //// Execute a command.	
        public override bool execute()
        {

            int err = EDSDKErrors.EDS_ERR_OK;
            bool locked = false;

            //// You should do UILock when you send a command to camera models elder than EOS30D.

            if (base.model.isLegacy())
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UILock, 0);


                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    locked = true;

                }

            }

            ////Get a property.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = getProperty(this.propertyID);

            }


            if (locked)
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UIUnLock, 0);

            }

            //// Notify Error.

            if (err != EDSDKErrors.EDS_ERR_OK)
            {
                //// Retry when the camera replys deviceBusy.

                if ((err + EDSDKErrors.EDS_ERRORID_MASK) == EDSDKErrors.EDS_ERR_DEVICE_BUSY)
                {
                    base.model.notifyObservers(warn, err);

                    return false;

                }

                base.model.notifyObservers(errr, err);

            }

            return true;

        }


        private int getProperty(int id)
        {
            int err = EDSDKErrors.EDS_ERR_OK;
            EDSDKTypes.EdsDataType dataType = EDSDKTypes.EdsDataType.kEdsDataType_Unknown;
            int dataSize = 0;


            if (id == EDSDKTypes.kEdsPropID_Unknown)
            {
                //// If the propertyID is invalidID,
                //// you should retry to get properties.
                //// InvalidID is able to be published for the models elder than EOS30D.

                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getProperty(EDSDKTypes.kEdsPropID_AEMode);
                }
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getProperty(EDSDKTypes.kEdsPropID_Tv);
                }
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getProperty(EDSDKTypes.kEdsPropID_Av);
                }
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getProperty(EDSDKTypes.kEdsPropID_ISOSpeed);
                }
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getProperty(EDSDKTypes.kEdsPropID_ImageQuality);
                }

                return err;
            }

            //// Get propertysize.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsGetPropertySize(base.model.getCameraObject(), id, 0, ref dataType, ref dataSize);

            }


            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                int data = 0;
                if (dataType == EDSDKTypes.EdsDataType.kEdsDataType_UInt32)
                {
                    //// Get a property.
                    IntPtr ptr = Marshal.AllocHGlobal(dataSize);

                    err = EDSDK.EdsGetPropertyData(base.model.getCameraObject(), id, 0, dataSize, ptr);

                    data = (int)Marshal.PtrToStructure(ptr, typeof(int));
                    Marshal.FreeHGlobal(ptr);


                    if (err == EDSDKErrors.EDS_ERR_OK)
                    {
                        base.model.setPropertyUInt32(id, data);

                    }
                }


                if (dataType == EDSDKTypes.EdsDataType.kEdsDataType_String)
                {
                    string str = null;
                    //char[EDS_MAX_NAME]
                    IntPtr ptr = Marshal.AllocHGlobal(EDSDKTypes.EDS_MAX_NAME);

                    //// Get a property.
                    err = EDSDK.EdsGetPropertyData(base.model.getCameraObject(), id, 0, dataSize, ptr);

                    str = Marshal.PtrToStringAnsi(ptr);
                    Marshal.FreeHGlobal(ptr);

                    //// Stock the property .

                    if (err == EDSDKErrors.EDS_ERR_OK)
                    {
                        base.model.setPropertyString(id, str);

                    }
                }

            }


            //// Notify updating.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                base.model.notifyObservers(updt, id);

            }

            return err;

        }

    }

}