﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public interface Observer
    {
        void update(Observable @from, int msg, int data);
    }

    public class Observable
    {

        private int m_numof_ob;

        private Observer[] m_observers;

        public Observable()
        {
            m_numof_ob = -1;
            // ArraySize = 0
        }

        ~Observable()
        {
            deleteObservers();
        }

        //// Add an observer.

        public void addObserver(object ob)
        {
            m_numof_ob = m_numof_ob + 1;
            Array.Resize(ref m_observers, m_numof_ob + 1);
            m_observers[m_numof_ob] = (Observer)ob;

        }

        //// Delete an observer .

        public void deleteObserver(Observer ob)
        {
            int iCnt = 0;


            if (m_numof_ob <= 0)
            {
                m_observers = new Observer[-1 + 1];
                m_numof_ob = -1;

            }
            else
            {
                for (iCnt = 0; iCnt <= m_numof_ob; iCnt++)
                {
                    if (object.ReferenceEquals(m_observers[iCnt], ob))
                    {
                        do
                        {
                            m_observers[iCnt] = m_observers[iCnt + 1];
                            iCnt = iCnt + 1;
                        } while (!(iCnt == m_numof_ob));
                    }
                }

                m_observers[m_numof_ob] = null;
                m_numof_ob = m_numof_ob - 1;
                Array.Resize(ref m_observers, m_numof_ob + 1);

            }

        }


        //// Notify to observers.

        public void notifyObservers(int msg, int data = 0)
        {
            int iCnt = 0;


            for (iCnt = m_numof_ob; iCnt >= 0; iCnt += -1)
            {
                m_observers[iCnt].update(this, msg, data);
                iCnt = iCnt - 1;

            }

        }


        public void deleteObservers()
        {
            int icnt = 0;

            for (icnt = 0; icnt <= m_numof_ob; icnt++)
            {
                m_observers[icnt] = null;
            }

            // Set the number of observer as 0.
            m_numof_ob = 0;

        }


        public int countObservers()
        {
            return m_numof_ob;

        }

    }
}