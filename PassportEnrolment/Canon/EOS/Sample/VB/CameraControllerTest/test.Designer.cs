﻿namespace CameraControllerTest
{
    partial class EOSLib
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label7 = new System.Windows.Forms.Label();
            this.ImageQualityCmb = new System.Windows.Forms.ComboBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.InfoTextBox = new System.Windows.Forms.TextBox();
            this.ExposureCompCmb = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.MeteringModeCmb = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TvCmb = new System.Windows.Forms.ComboBox();
            this.AvCmb = new System.Windows.Forms.ComboBox();
            this.ISOSpeedCmb = new System.Windows.Forms.ComboBox();
            this.AEModeCmb = new System.Windows.Forms.ComboBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.TakeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label7.Location = new System.Drawing.Point(23, 287);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(71, 13);
            this.Label7.TabIndex = 59;
            this.Label7.Text = "ImageQuality:";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ImageQualityCmb
            // 
            this.ImageQualityCmb.Location = new System.Drawing.Point(126, 284);
            this.ImageQualityCmb.Name = "ImageQualityCmb";
            this.ImageQualityCmb.Size = new System.Drawing.Size(166, 21);
            this.ImageQualityCmb.TabIndex = 58;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(308, 288);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(96, 17);
            this.progressBar.TabIndex = 57;
            // 
            // InfoTextBox
            // 
            this.InfoTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.InfoTextBox.Location = new System.Drawing.Point(23, 322);
            this.InfoTextBox.Name = "InfoTextBox";
            this.InfoTextBox.Size = new System.Drawing.Size(381, 20);
            this.InfoTextBox.TabIndex = 56;
            this.InfoTextBox.Text = "Info";
            // 
            // ExposureCompCmb
            // 
            this.ExposureCompCmb.Location = new System.Drawing.Point(126, 246);
            this.ExposureCompCmb.Name = "ExposureCompCmb";
            this.ExposureCompCmb.Size = new System.Drawing.Size(166, 21);
            this.ExposureCompCmb.TabIndex = 55;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label4.Location = new System.Drawing.Point(23, 249);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(84, 13);
            this.Label4.TabIndex = 54;
            this.Label4.Text = "Exposure Comp:";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeteringModeCmb
            // 
            this.MeteringModeCmb.Location = new System.Drawing.Point(126, 210);
            this.MeteringModeCmb.Name = "MeteringModeCmb";
            this.MeteringModeCmb.Size = new System.Drawing.Size(166, 21);
            this.MeteringModeCmb.TabIndex = 53;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(23, 213);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(81, 13);
            this.Label1.TabIndex = 52;
            this.Label1.Text = "Metering Mode:";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TvCmb
            // 
            this.TvCmb.Location = new System.Drawing.Point(126, 107);
            this.TvCmb.Name = "TvCmb";
            this.TvCmb.Size = new System.Drawing.Size(166, 21);
            this.TvCmb.TabIndex = 51;
            // 
            // AvCmb
            // 
            this.AvCmb.Location = new System.Drawing.Point(126, 142);
            this.AvCmb.Name = "AvCmb";
            this.AvCmb.Size = new System.Drawing.Size(166, 21);
            this.AvCmb.TabIndex = 50;
            // 
            // ISOSpeedCmb
            // 
            this.ISOSpeedCmb.Location = new System.Drawing.Point(126, 176);
            this.ISOSpeedCmb.Name = "ISOSpeedCmb";
            this.ISOSpeedCmb.Size = new System.Drawing.Size(166, 21);
            this.ISOSpeedCmb.TabIndex = 49;
            // 
            // AEModeCmb
            // 
            this.AEModeCmb.Location = new System.Drawing.Point(126, 72);
            this.AEModeCmb.Name = "AEModeCmb";
            this.AEModeCmb.Size = new System.Drawing.Size(166, 21);
            this.AEModeCmb.TabIndex = 48;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label6.Location = new System.Drawing.Point(23, 110);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(23, 13);
            this.Label6.TabIndex = 47;
            this.Label6.Text = "Tv:";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label5.Location = new System.Drawing.Point(23, 145);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(23, 13);
            this.Label5.TabIndex = 46;
            this.Label5.Text = "Av:";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(23, 180);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(28, 13);
            this.Label3.TabIndex = 45;
            this.Label3.Text = "ISO:";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(23, 76);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(54, 13);
            this.Label2.TabIndex = 44;
            this.Label2.Text = "AE Mode:";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ExitBtn
            // 
            this.ExitBtn.Location = new System.Drawing.Point(324, 72);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(80, 35);
            this.ExitBtn.TabIndex = 43;
            this.ExitBtn.Text = "Quit";
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // TakeBtn
            // 
            this.TakeBtn.Location = new System.Drawing.Point(308, 215);
            this.TakeBtn.Name = "TakeBtn";
            this.TakeBtn.Size = new System.Drawing.Size(96, 52);
            this.TakeBtn.TabIndex = 42;
            this.TakeBtn.Text = "Take Picture";
            this.TakeBtn.Click += new System.EventHandler(this.TakeBtn_Click);
            // 
            // test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 415);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.ImageQualityCmb);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.InfoTextBox);
            this.Controls.Add(this.ExposureCompCmb);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.MeteringModeCmb);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.TvCmb);
            this.Controls.Add(this.AvCmb);
            this.Controls.Add(this.ISOSpeedCmb);
            this.Controls.Add(this.AEModeCmb);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.TakeBtn);
            this.Name = "test";
            this.Text = "test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VBSample_FormClosing);
            this.Load += new System.EventHandler(this.VBSample_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.ComboBox ImageQualityCmb;
        internal System.Windows.Forms.ProgressBar progressBar;
        internal System.Windows.Forms.TextBox InfoTextBox;
        internal System.Windows.Forms.ComboBox ExposureCompCmb;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.ComboBox MeteringModeCmb;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ComboBox TvCmb;
        internal System.Windows.Forms.ComboBox AvCmb;
        internal System.Windows.Forms.ComboBox ISOSpeedCmb;
        private System.Windows.Forms.ComboBox AEModeCmb;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button ExitBtn;
        internal System.Windows.Forms.Button TakeBtn;
    }
}