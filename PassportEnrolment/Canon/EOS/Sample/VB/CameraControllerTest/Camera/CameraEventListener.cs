﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class CameraEventListener
    {

        public static long handleObjectEvent(int inEvent, IntPtr inRef, IntPtr inContext)
        {

            long rtn = 0;

            switch (inEvent)
            {
                case EDSDKTypes.kEdsObjectEvent_DirItemRequestTransfer:
                    EOSLib.controller.actionPerformed("download", inRef);

                    break;
                default:
                    ////Release unnecessary objects.
                    if ((inRef == null) == false)
                    {
                        EDSDK.EdsRelease(inRef);
                    }

                    break;
            }

            rtn = Convert.ToInt64(EDSDKErrors.EDS_ERR_OK);
            return rtn;

        }


        public static long handlePropertyEvent(int inEvent, int inPropertyID, int inParam, IntPtr inContext)
        {

            long rtn = 0;

            switch (inEvent)
            {
                case EDSDKTypes.kEdsPropertyEvent_PropertyChanged:
                    EOSLib.controller.actionPerformed("get", inPropertyID);
                    break;
                case EDSDKTypes.kEdsPropertyEvent_PropertyDescChanged:
                    EOSLib.controller.actionPerformed("getlist", inPropertyID);
                    break;
            }


            rtn = Convert.ToInt64(EDSDKErrors.EDS_ERR_OK);
            return rtn;

        }


        public static long handleStateEvent(int inEvent, int inParam, IntPtr inContext)
        {

            long rtn = 0;

            switch (inEvent)
            {
                case EDSDKTypes.kEdsStateEvent_Shutdown:
                    EOSLib.controller.actionPerformed("clse");

                    break;
            }


            rtn = Convert.ToInt64(EDSDKErrors.EDS_ERR_OK);
            return rtn;

        }
    }
}
