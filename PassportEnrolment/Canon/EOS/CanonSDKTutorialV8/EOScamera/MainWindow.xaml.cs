﻿using EDSDKLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Luxand;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Threading;
using Image = System.Drawing.Image;

namespace EOScamera
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Variables

        SDKHandler CameraHandler;
        List<int> AvList;
        List<int> TvList;
        List<int> ISOList;
        List<Camera> CamList;
        bool IsInit = false;
        int BulbTime = 30;
        int whichCamera = 0;
        int USBcamcount = 0;
        bool takephoto = false;
        ImageSource EvfImage;
        JpegBitmapDecoder dec;
        ImageBrush bgbrush = new ImageBrush();
        ThreadStart SetImageAction;
        string[] cameraList;

        // Face Recognition
        String cameraName;
        Thread CameraThread;
        int tracker = 0;
        int cameraHandle = 0;
        int needClose;
        bool faceFound = false;
        System.Drawing.Image frameImage;
        FSDKCam.VideoFormatInfo[] formatList = null;
        public delegate void UpdateImageDel(BitmapSource src);

        public bool DontUseFacialRecognition { get; set; }

        public string Filelocation { get; set; }

        public Bitmap CroppedImage { get; set; }

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();

            // Confirm there is a camera plugged
            FSDKCam.GetCameraList(out cameraList, out USBcamcount);
            CameraHandler = new SDKHandler();
            CamList = CameraHandler.GetCameraList();

            if (USBcamcount != 0 || CamList.Count == 0)
            {
                cameraName = cameraList[cameraList.Length - 1]; // choose the highest camera

                if (!DontUseFacialRecognition)
                {  
                    // Check that face recog SDK is valid
                    if (FSDK.FSDKE_OK != FSDK.ActivateLibrary("NxLq2hTLcLbL7xD11GUIOcRezUQKSIx4Y0cPOJaztQdnCYV52csmELrQJ41w1L33z25oSvb5CMEjSqRlWm2Fod9BmhJzq4PV2nM9lUqhMNgTBez1oVWRlcV2H4Ezg7fWFZCqF7m+ekBxE7Wc5sQpS2L69PxwvzZ+SpEfrRl4gzM="))
                    {
                        MessageBox.Show("Please run the License Key Wizard (Start - Luxand - FaceSDK - License Key Wizard)");
                    }

                    FSDK.InitializeLibrary();
                    FSDKCam.InitializeCapturing();

                    FSDKCam.GetVideoFormatList(ref cameraName, out formatList, out USBcamcount);
                }

                if (CamList.Count == 0)
                {
                    whichCamera = 0;

                    if (formatList != null)
                    {
                        int VideoFormat = 0; // choose a video format
                        camerabox.Width = formatList[VideoFormat].Width;
                        camerabox.Height = formatList[VideoFormat].Height;
                        Width = formatList[VideoFormat].Width + 48;
                        Height = formatList[VideoFormat].Height + 96;
                    }
                }
                else
                {
                    whichCamera = 1;
                    CameraHandler.CameraAdded += new SDKHandler.CameraAddedHandler(SDK_CameraAdded);
                    CameraHandler.LiveViewUpdated += new SDKHandler.StreamUpdate(SDK_LiveViewUpdated);
                    CameraHandler.CameraHasShutdown += SDK_CameraHasShutdown;
                    SetImageAction = new ThreadStart(delegate { bgbrush.ImageSource = EvfImage; });
                    RefreshCamera();
                    IsInit = true;
                }

                // Set default location
                if (string.IsNullOrEmpty(Filelocation))
                {
                    string picturefolder = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                    string folder = Path.Combine(picturefolder, "Cannon");
                    Directory.CreateDirectory(folder);
                    Filelocation = folder;
                }
            }
            else
            {
                MessageBox.Show("Please attach a camera");
            }
        } 

        #endregion

        #region Face Recognition

        private void StartVideo()
        {
            try
            {
                int width = 0;
                int height = 0;
                int left = 0;
                int top = 0;
                FSDK.CImage crop = null;
                cameraHandle = 0;

                int r = FSDKCam.OpenVideoCamera(ref cameraName, ref cameraHandle);

                tracker = 0;
                FSDK.CreateTracker(ref tracker);

                int err = 0; // set realtime face detection parameters
                FSDK.SetTrackerMultipleParameters(tracker, "RecognizeFaces=false; HandleArbitraryRotations=false; DetermineFaceRotationAngle=false; InternalResizeWidth=100; FaceDetectionThreshold=1;", ref err);

                while (true)
                {
                    Int32 imageHandle = 0;
                    Int32 cropHandle = 0;

                    if (FSDK.FSDKE_OK != FSDKCam.GrabFrame(cameraHandle, ref imageHandle)) // grab the current frame from the camera
                    {
                        DoEvents();
                        continue;
                    }

                    FSDK.CImage image = new FSDK.CImage(imageHandle);
                    crop = new FSDK.CImage();
                    cropHandle = crop.ImageHandle;
                    FSDK.SetJpegCompressionQuality(90);

                    long[] IDs;
                    long faceCount = 0;
                    
                    FSDK.FeedFrame(tracker, 0, image.ImageHandle, ref faceCount, out IDs, sizeof(long) * 256); // maximum 256 faces detected
                    Array.Resize(ref IDs, (int)faceCount);

                    frameImage = image.ToCLRImage();
                    Graphics gr = Graphics.FromImage(frameImage);

                    if (IDs.Length == 0)
                    {
                        faceFound = false;
                    }
                    else
                    {
                        faceFound = true;

                        for (int i = 0; i < IDs.Length; ++i)
                        {
                            FSDK.TFacePosition facePosition = new FSDK.TFacePosition();
                            FSDK.GetTrackerFacePosition(tracker, 0, IDs[i], ref facePosition);

                            left = facePosition.xc - (int)(facePosition.w * 0.68);
                            top = facePosition.yc - (int)(facePosition.w * 0.75);
                            width = (int)(facePosition.w * 1.32);
                            height = (int)(facePosition.w * 1.76);

                            if (needClose == 0)
                            {
                                // draw rectangle
                                gr.DrawRectangle(Pens.LightGreen, left, top, width, height);
                            }
                            else
                                needClose = 2;
                        }
                    }

                    Dispatcher.Invoke((Action)(() =>
                    {
                        using (MemoryStream memory = new MemoryStream())
                        {
                            frameImage.Save(memory, ImageFormat.Png);
                            memory.Position = 0;
                            BitmapImage bitmapImage = new BitmapImage();
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = memory;
                            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                            bitmapImage.EndInit();

                            camerabox.Source = bitmapImage;
                        }
                    }));

                    GC.Collect(); // collect the garbage

                    // make UI controls accessible
                    DoEvents();

                    if (needClose == 2 && faceFound)
                        break;
                }

                CroppedImage = CropImage(frameImage, left, top, width, height);
                CroppedImage.Save(Path.Combine(Filelocation, "USBImgCrop.png"));

                if (CroppedImage.Width > 400)
                {
                    Save(CroppedImage, 400, 2000, 80, Path.Combine(Filelocation, "USBImgCrop80.jpg"));
                    Save(CroppedImage, 400, 2000, 100, Path.Combine(Filelocation, "USBImgCrop100.jpg"));
                }
                else
                {
                    Save(CroppedImage, CroppedImage.Width, 2000, 80, Path.Combine(Filelocation, "USBImgCrop80.jpg"));
                    Save(CroppedImage, CroppedImage.Width, 2000, 100, Path.Combine(Filelocation, "USBImgCrop100.jpg"));
                }

                Dispatcher.Invoke((Action)(() =>
                {
                    Close();
                }));
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                Dispatcher.Invoke((Action)(() =>
                {
                    Close();
                }));
            }
        }

        private void UpdateImage(BitmapSource src)
        {
            camerabox.Source = src;
        }

        private static void DoEvents()
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                                                  new Action(delegate { }));
        }

        #endregion

        #region SDK Events

        private void SDK_LiveViewUpdated(Stream img)
        {
            if (CameraHandler.IsLiveViewOn)
            {
                try
                {
                    int tracker = 0;
                    Int32 cropHandle = 0;
                    FSDK.CreateTracker(ref tracker);
                    System.Drawing.Image frameImage = Image.FromStream(img);

                    int err = 0; // set realtime face detection parameters
                    FSDK.SetTrackerMultipleParameters(tracker, "RecognizeFaces=false; HandleArbitraryRotations=false; DetermineFaceRotationAngle=false; InternalResizeWidth=100; FaceDetectionThreshold=2;", ref err);

                    Graphics gr = Graphics.FromImage(frameImage);
                    FSDK.CImage crop = new FSDK.CImage();
                    FSDK.CImage image = new FSDK.CImage(frameImage);
                    cropHandle = crop.ImageHandle;
                    FSDK.SetJpegCompressionQuality(100);

                    long[] IDs;
                    long faceCount = 0;
                    int width = 0;
                    int height = 0;
                    int left = 0;
                    int top = 0;
                    FSDK.FeedFrame(tracker, 0, image.ImageHandle, ref faceCount, out IDs, sizeof(long) * 256); // maximum 256 faces detected
                    Array.Resize(ref IDs, (int)faceCount);

                    for (int i = 0; i < IDs.Length; ++i)
                    {
                        FSDK.TFacePosition facePosition = new FSDK.TFacePosition();
                        FSDK.GetTrackerFacePosition(tracker, 0, IDs[i], ref facePosition);

                        left = facePosition.xc - (int)(facePosition.w * 0.612);
                        top = facePosition.yc - (int)(facePosition.w * 0.75);
                        width = (int)(facePosition.w * 1.188);
                        height = (int)(facePosition.w * 1.584);

                        // draw rectangle
                        System.Drawing.Pen greenPen = new System.Drawing.Pen(System.Drawing.Color.LightGreen, 2);
                        gr.DrawRectangle(greenPen, left, top, width, height);

                        if (takephoto)
                        {
                            takephoto = false;
                            TakePhotograph();
                        }
                    }

                    img = new MemoryStream();
                    frameImage.Save(img, ImageFormat.Jpeg);

                    img.Position = 0;
                    dec = new JpegBitmapDecoder(img, BitmapCreateOptions.None, BitmapCacheOption.None);
                    EvfImage = dec.Frames[0];

                    Application.Current.Dispatcher.Invoke(SetImageAction);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception");
                }
            }
        }

        private void SDK_CameraAdded()
        {
            RefreshCamera();
        }

        private void SDK_CameraHasShutdown(object sender, EventArgs e)
        {
            CloseSession();
            Close();
        }

        #endregion

        #region Session

        private void OpenSessionButton_Click(object sender, RoutedEventArgs e)
        {
            if (CameraHandler.CameraSessionOpen)
            {
                LVCanvas.Background = System.Windows.Media.Brushes.LightGray;
                CameraHandler.CloseSession();
                SessionButton.Content = "Open Session";
            }
            else
            {
                int result = OpenSession();
                if (result == 1)
                {
                    LVCanvas.Background = bgbrush;
                    CameraHandler.StartLiveView();
                }
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshCamera();
        }

        #endregion

        #region Canon Settings

        private void AvCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_Av, CameraValues.AV((string)AvCoBox.SelectedItem));
        }

        private void TvCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_Tv, CameraValues.TV((string)TvCoBox.SelectedItem));
            if ((string)TvCoBox.SelectedItem == "Bulb")
            {
                BulbBox.IsEnabled = true;
                BulbSlider.IsEnabled = true;
            }
            else
            {
                BulbBox.IsEnabled = false;
                BulbSlider.IsEnabled = false;
            }
        }

        private void ISOCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_ISOSpeed, CameraValues.ISO((string)ISOCoBox.SelectedItem));
        }

        private void WBCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (WBCoBox.SelectedIndex)
            {
                case 0: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Auto); break;
                case 1: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Daylight); break;
                case 2: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Cloudy); break;
                case 3: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Tangsten); break;
                case 4: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Fluorescent); break;
                case 5: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Strobe); break;
                case 6: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_WhitePaper); break;
                case 7: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Shade); break;
            }
        }

        private void BulbSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IsInit) BulbBox.Text = BulbSlider.Value.ToString();
        }

        private void BulbBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInit)
            {
                int b;
                if (int.TryParse(BulbBox.Text, out b) && b != BulbTime)
                {
                    BulbTime = b;
                    BulbSlider.Value = BulbTime;
                }
                else BulbBox.Text = BulbTime.ToString();
            }
        }
        
        #endregion

        #region Canon Live view

        private void LVCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CameraHandler.IsLiveViewOn && CameraHandler.IsCoordSystemSet)
            {
                System.Windows.Point p = e.GetPosition(this);
                ushort x = (ushort)((p.X / LVCanvas.Width) * CameraHandler.Evf_CoordinateSystem.width);
                ushort y = (ushort)((p.Y / LVCanvas.Height) * CameraHandler.Evf_CoordinateSystem.height);
                CameraHandler.SetManualWBEvf(x, y);
            }
        }

        private void FocusNear3Button_Click(object sender, RoutedEventArgs e)
        {
            CameraHandler.SetFocus(EDSDK.EvfDriveLens_Near3);
        }

        private void FocusFar3Button_Click(object sender, RoutedEventArgs e)
        {
            CameraHandler.SetFocus(EDSDK.EvfDriveLens_Far3);
        }

        #endregion

        #region Canon Subroutines

        private void TakePhotograph()
        {
            Directory.CreateDirectory(Filelocation);
            CameraHandler.ImageSaveDirectory = Filelocation;
            CameraHandler.TakePhoto();

            CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Camera);
            CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Host);
        }

        private void CloseSession()
        {
            CameraHandler.CloseSession();
            SessionButton.Content = "Open Session";
        }

        private void RefreshCamera()
        {
            CloseSession();
            CamList = CameraHandler.GetCameraList();
        }

        private int OpenSession()
        {
            if (CamList.Count > 0)
            {
                RefreshCamera();
                CameraHandler.OpenSession(CamList[0]);
                SessionButton.Content = "Close Session";
                string cameraname = CameraHandler.MainCamera.Info.szDeviceDescription;
                if (CameraHandler.GetSetting(EDSDK.PropID_AEMode) != EDSDK.AEMode_Manual) MessageBox.Show("Camera is not in manual mode. Some features might not work!");
                AvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Av);
                TvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Tv);
                ISOList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_ISOSpeed);
                int wbidx = (int)CameraHandler.GetSetting((uint)EDSDK.PropID_WhiteBalance);

                return 1;
            }
            else
            {
                MessageBox.Show("No camera found");
                return 0;
            }
        }

        #endregion

        #region Window Events

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (whichCamera == 1)
                {
                    int result = OpenSession();
                    if (result == 1)
                    {
                        LVCanvas.Background = bgbrush;
                        CameraHandler.StartLiveView();
                        btnTakePicture.IsEnabled = true;
                    }
                }
                else
                {
                    LVCanvas.Visibility = Visibility.Collapsed;
                    AdvancedOptions.Visibility = Visibility.Collapsed;
                    camerabox.Visibility = Visibility.Visible;
                    btnTakePicture.IsEnabled = true;

                    CameraThread = new Thread(new ThreadStart(StartVideo));
                    CameraThread.Start();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Could not start video capture");
            }
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FSDK.SetJpegCompressionQuality(85);
                FSDK.CImage image = new FSDK.CImage(Path.Combine(Filelocation, "tempfile.jpg"));
                FSDK.CImage crop = new FSDK.CImage();
                int cropHandle = crop.ImageHandle;

                System.Drawing.Image frameImage = image.ToCLRImage();
                Graphics gr = Graphics.FromImage(frameImage);

                FSDK.TFacePosition facePosition = image.DetectFace();
                if (0 == facePosition.w)
                    MessageBox.Show("No faces detected", "Face Detection");
                else
                {
                    int left = facePosition.xc - (int)(facePosition.w * 0.68);
                    int top = facePosition.yc - (int)(facePosition.w * 0.75);
                    int width = (int)(facePosition.w * 1.32);
                    int height = (int)(facePosition.w * 1.76);

                    // get rectangle
                    FSDK.CreateEmptyImage(ref cropHandle);
                    CroppedImage = (Bitmap)ResizeImageFixedWidth(CropImage(frameImage, left, top, width, height), 400);
                    CroppedImage.Save(Path.Combine(Filelocation, "ImgCrop.png"));
                    Save(CroppedImage, 400, 2000, 80, Path.Combine(Filelocation, "ImgCrop80.jpg"));
                    Save(CroppedImage, 400, 2000, 100, Path.Combine(Filelocation, "ImgCrop100.jpg"));

                    gr.Flush();
                }

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void TakePhotoButton_Click(object sender, RoutedEventArgs e)
        {
            if (whichCamera == 1)
            {
                takephoto = true;
                btnDone.IsEnabled = true;
            }
            else
            {
                needClose = 1;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (whichCamera == 1)
            {
                if (CameraHandler.CameraSessionOpen)
                {
                    LVCanvas.Background = System.Windows.Media.Brushes.LightGray;
                    CameraHandler.CloseSession();
                    SessionButton.Content = "Open Session";
                }

                CameraHandler.Dispose();
            }
            else
            {
                FSDK.FreeTracker(tracker);

                FSDKCam.CloseVideoCamera(cameraHandle);
                FSDKCam.FinalizeCapturing();
            }
        }


        #endregion

        #region functions

        public Bitmap CropImage(System.Drawing.Image source, int x, int y, int width, int height)
        {
            Rectangle crop = new Rectangle(x, y, width, height);

            var bmp = new Bitmap(crop.Width, crop.Height);
            using (var gr = Graphics.FromImage(bmp))
            {
                gr.DrawImage(source, new Rectangle(0, 0, bmp.Width, bmp.Height), crop, GraphicsUnit.Pixel);
            }
            return bmp;
        }

        public System.Drawing.Image ResizeImageFixedWidth(System.Drawing.Image imgToResize, int width)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = ((float)width / (float)sourceWidth);

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (System.Drawing.Image)b;
        }

        public void Save(Bitmap image, int maxWidth, int maxHeight, int quality, string filePath)
        {
            // Get the image's original width and height
            int originalWidth = image.Width;
            int originalHeight = image.Height;

            // To preserve the aspect ratio
            float ratioX = (float)maxWidth / (float)originalWidth;
            float ratioY = (float)maxHeight / (float)originalHeight;
            float ratio = Math.Min(ratioX, ratioY);

            // New width and height based on aspect ratio
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);

            // Convert other formats (including CMYK) to RGB.
            Bitmap newImage = new Bitmap(newWidth, newHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            // Draws the image in the specified size with quality mode set to HighQuality
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            // Get an ImageCodecInfo object that represents the JPEG codec.
            ImageCodecInfo imageCodecInfo = GetEncoderInfo(ImageFormat.Jpeg);

            // Create an Encoder object for the Quality parameter.
            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;

            // Create an EncoderParameters object. 
            EncoderParameters encoderParameters = new EncoderParameters(1);

            // Save the image as a JPEG file with quality level.
            EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
            encoderParameters.Param[0] = encoderParameter;
            newImage.Save(filePath, imageCodecInfo, encoderParameters);
        }

        /// <summary>
        /// Method to get encoder infor for given image format.
        /// </summary>
        /// <param name="format">Image format</param>
        /// <returns>image codec info.</returns>
        private ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }

        #endregion
    }
}
