﻿using PassportEnrolment.Helpers;
using PassportEnrolment.Windows;
using PassportEnrolmentEntities;
using System.Windows;

namespace PassportEnrolment
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Helper.MainWindow = this;
            mainTabControl.Height = SystemParameters.VirtualScreenHeight - 150;
        }

        public void SetRoleItems()
        {
            if (PiggyBank.CurrentRoles != null && PiggyBank.CurrentRoles.Count != 0)
            {
                if (PiggyBank.hasPrivilege("Is_User_administrator"))
                {
                    
                }
            }
            else
            {
                DefaultText.Visibility = Visibility.Visible;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            var login = new LoginScreen();

            login.Show();
            Close();
        }
    }
}
