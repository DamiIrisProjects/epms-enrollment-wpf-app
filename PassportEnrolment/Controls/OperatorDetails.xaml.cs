﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using PassportEnrolmentEntities;

namespace PassportEnrolment.Controls
{
    /// <summary>
    /// Interaction logic for PersonDetailsInfo.xaml
    /// </summary>
    public partial class OperatorDetails : UserControl
    {
        public OperatorDetails()
        {
            InitializeComponent();
        }

        private void txtPhonenum_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !Helper.NoTextAllowed(e.Text);
        }

        public bool ValidateInfo()
        {
            bool isValid = true;

            isValid = Helper.CheckTextBox(txtFname);
            isValid = Helper.CheckTextBox(txtLname) && isValid;
            isValid = Helper.CheckTextBox(txtPhonenum) && isValid;
            isValid = Helper.CheckDatePicker(datePicker) && isValid;
            isValid = Helper.CheckRadioBox(new List<RadioButton>(){ rdMiss, rdMr, rdMrs}) && isValid;

            return isValid;
        }

        public Operator GetUserDetails()
        {
            Operator person = new Operator();
            person.FirstName = txtFname.Text;
            person.Surname = txtLname.Text;
            person.Middlename = txtMname.Text;
            person.DateOfBirth = datePicker.SelectedDate ?? DateTime.MinValue;
            person.EmailAddress = txtEmail.Text;
            person.PhoneNumber = txtPhonenum.Text;

            if (rdMr.IsChecked == true)
                person.Title = "Mr";
            if (rdMrs.IsChecked == true)
                person.Title = "Mrs";
            if (rdMiss.IsChecked == true)
                person.Title = "Miss";

            return person;
        }


    }
}
