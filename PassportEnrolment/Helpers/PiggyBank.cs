﻿using System.Collections.Generic;
using PassportEnrolmentEntities;

namespace PassportEnrolment.Helpers
{
    public static class PiggyBank
    {
        public static List<Role> CurrentRoles { get; set; }

        public static List<Role> AllRoles { get; set; }

        public static Operator CurrentOperator { get; set; }

        public static List<Person> OperatorsList { get; set; }

        public static List<string> PrivilegesList { get; set; }

        public static bool hasPrivilege(string privilege)
        {
            foreach (Role r in CurrentRoles)
            {
                foreach (string privi in r.Privileges)
                {
                    if (privi.ToLower() == privilege.ToLower())
                        return true;
                }
            }

            return false;
        }

        
    }
}
