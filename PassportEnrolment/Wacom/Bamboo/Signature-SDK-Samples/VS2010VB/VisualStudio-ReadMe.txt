Using the signature components in Visual Studio and .NET

The components need to be registered in the Global Assembly Cache to use the Florentis.InteropAX dlls.
A sample script file GAC-Registration.bat is included in VS2010 to demonstrate one way to achieve this.
The script file may require different paths depending on the development environment:
  location of gacutil.exe (e.g. "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin\NETFX 4.0 Tools")
  location of installed signature components (e.g. "C:\Program Files (x86)\Common Files\Florentis"

Start a Dos command As Administrator then run:
  GAC-Registration.bat

Once the components have been registered in this way the sample VS projects can be built and run.
