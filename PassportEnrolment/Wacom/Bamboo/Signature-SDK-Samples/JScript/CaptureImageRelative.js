/*
  CaptureImageRelative
  Captures a signature and creates an image file with the signature positioned as it was signed on the device
  The image must be created using the DPI option (negative dimensionX dimensionY)
*/
function print( txt ) {
  WScript.Echo(txt);
}
main();
function main() {
  filename = "sig-relative.png";
  args = WScript.Arguments;
  if(args.Count() > 0 )
    filename=args(0);
  sigCtl = new ActiveXObject("Florentis.SigCtl");
  dynCapt = new ActiveXObject("Florentis.DynamicCapture");
  rc = dynCapt.Capture(sigCtl,"Who","Why");
  if( rc == 0 ) {
    sigCtl.Signature.ExtraData("AdditionalData") = "CaptureImage.js Additional Data";
    flags = 0x2000000 | 0x1000 |  0x80000 | 0x400000; //SigObj.renderRelative | SigObj.outputFilename | SigObj.color32BPP | SigObj.encodeData
    rc = sigCtl.Signature.RenderBitmap(filename, -160, -160, "image/bmp", 0.5, 0xff0000, 0xffffff, 0.0, 0.0, flags );
    print("Created Relative Signature image file: " + filename);
    filename="sig.png";
    flags = 0x1000 | 0x80000 | 0x400000; //SigObj.outputFilename | SigObj.color32BPP | SigObj.encodeData
    rc = sigCtl.Signature.RenderBitmap(filename, 300, 150, "image/png", 0.5, 0xff0000, 0xffffff, 0.0, 0.0, flags );
    print("Created standard Signature image file: " + filename);
  }
  else {
    print("Capture returned: " + rc);
    switch(rc) {
      case 1:   print("Cancelled");
                break;
      case 100: print("Signature tablet not found");
                break;
      case 103: print("Capture not licensed");
                break;
    }
  }
}
