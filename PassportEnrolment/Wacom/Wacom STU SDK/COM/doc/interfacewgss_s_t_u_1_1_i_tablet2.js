var interfacewgss_s_t_u_1_1_i_tablet2 =
[
    [ "getProductId", "interfacewgss_s_t_u_1_1_i_tablet2.html#a348877591e96f9a7c4a138a3cc26d14f", null ],
    [ "getCapability", "interfacewgss_s_t_u_1_1_i_tablet2.html#af4bd4001328110605c1434471d8901c8", null ],
    [ "getUid2", "interfacewgss_s_t_u_1_1_i_tablet2.html#a9ff0e4bfde35a0f43f9915ca9765914c", null ],
    [ "setClearScreenArea", "interfacewgss_s_t_u_1_1_i_tablet2.html#a5864b5712b387ddef68dc3f6dfc1f515", null ],
    [ "writeImageArea", "interfacewgss_s_t_u_1_1_i_tablet2.html#a0adc11ee17f1ce7c76657a2ef47de1d9", null ],
    [ "getHandwritingThicknessColor24", "interfacewgss_s_t_u_1_1_i_tablet2.html#ac49d43c87edcc742a0c1b0d4bca35266", null ],
    [ "setHandwritingThicknessColor24", "interfacewgss_s_t_u_1_1_i_tablet2.html#ac124e75d2999d14ef00a6e518b54212b", null ],
    [ "getBackgroundColor24", "interfacewgss_s_t_u_1_1_i_tablet2.html#a772406429c0f29e430a3ba2bb1ebdf9d", null ],
    [ "setBackgroundColor24", "interfacewgss_s_t_u_1_1_i_tablet2.html#ad26e97e68e4288f2c41468a2b943c93f", null ],
    [ "getScreenContrast", "interfacewgss_s_t_u_1_1_i_tablet2.html#afca17ec73097f4c005b6f9f5a556a7e1", null ],
    [ "setScreenContrast", "interfacewgss_s_t_u_1_1_i_tablet2.html#a530f432b7db581f084a27b30bce61e43", null ],
    [ "getEncryptionStatus", "interfacewgss_s_t_u_1_1_i_tablet2.html#ab6ebe0c9c649e1774177af935314718b", null ],
    [ "encryptionHandler2", "interfacewgss_s_t_u_1_1_i_tablet2.html#a22f755a5e4753063b825be0bbd25a6d3", null ],
    [ "Protocol", "interfacewgss_s_t_u_1_1_i_tablet2.html#a9cfb42a5933ea4f4cacf1f5ddab55515", null ]
];