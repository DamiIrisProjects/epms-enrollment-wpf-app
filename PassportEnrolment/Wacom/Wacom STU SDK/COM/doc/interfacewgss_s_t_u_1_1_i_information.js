var interfacewgss_s_t_u_1_1_i_information =
[
    [ "modelName", "interfacewgss_s_t_u_1_1_i_information.html#af6aab56f9e8487506ea0bc88f1bbaa56", null ],
    [ "firmwareMajorVersion", "interfacewgss_s_t_u_1_1_i_information.html#a53f2d5e4c0a296e44e049bce51d02e62", null ],
    [ "firmwareMinorVersion", "interfacewgss_s_t_u_1_1_i_information.html#a0e3cc3e0554e06c4db1e878494e065c5", null ],
    [ "secureIc", "interfacewgss_s_t_u_1_1_i_information.html#a4e0e4a218c03b226fe44af9b630d023f", null ],
    [ "secureIcVersion", "interfacewgss_s_t_u_1_1_i_information.html#a1187e3b66fa997411f116af230b1fd93", null ]
];