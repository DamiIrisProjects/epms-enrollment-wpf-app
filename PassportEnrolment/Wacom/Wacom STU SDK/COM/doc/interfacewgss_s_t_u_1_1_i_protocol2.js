var interfacewgss_s_t_u_1_1_i_protocol2 =
[
    [ "getCapability", "interfacewgss_s_t_u_1_1_i_protocol2.html#af4bd4001328110605c1434471d8901c8", null ],
    [ "getUid2", "interfacewgss_s_t_u_1_1_i_protocol2.html#a9ff0e4bfde35a0f43f9915ca9765914c", null ],
    [ "setClearScreenArea", "interfacewgss_s_t_u_1_1_i_protocol2.html#a5864b5712b387ddef68dc3f6dfc1f515", null ],
    [ "setStartImageDataArea", "interfacewgss_s_t_u_1_1_i_protocol2.html#ab4750fd9954f7e12da7e333555360443", null ],
    [ "setEndImageData", "interfacewgss_s_t_u_1_1_i_protocol2.html#a781dacec50e8765c02d8f5bb53269c5d", null ],
    [ "getHandwritingThicknessColor24", "interfacewgss_s_t_u_1_1_i_protocol2.html#ac49d43c87edcc742a0c1b0d4bca35266", null ],
    [ "setHandwritingThicknessColor24", "interfacewgss_s_t_u_1_1_i_protocol2.html#ac124e75d2999d14ef00a6e518b54212b", null ],
    [ "getBackgroundColor24", "interfacewgss_s_t_u_1_1_i_protocol2.html#a772406429c0f29e430a3ba2bb1ebdf9d", null ],
    [ "setBackgroundColor24", "interfacewgss_s_t_u_1_1_i_protocol2.html#ad26e97e68e4288f2c41468a2b943c93f", null ],
    [ "getScreenContrast", "interfacewgss_s_t_u_1_1_i_protocol2.html#afca17ec73097f4c005b6f9f5a556a7e1", null ],
    [ "setScreenContrast", "interfacewgss_s_t_u_1_1_i_protocol2.html#a530f432b7db581f084a27b30bce61e43", null ],
    [ "getEncryptionStatus", "interfacewgss_s_t_u_1_1_i_protocol2.html#ab6ebe0c9c649e1774177af935314718b", null ],
    [ "getEncryptionCommand", "interfacewgss_s_t_u_1_1_i_protocol2.html#a1735bd65147c62f3b52ca6ef9c1f0f03", null ],
    [ "setEncryptionCommand", "interfacewgss_s_t_u_1_1_i_protocol2.html#ac7d063879a849dd9684dace2ec914008", null ],
    [ "Interface", "interfacewgss_s_t_u_1_1_i_protocol2.html#aa3b798bb505065b9ecd82b591b6a010f", null ]
];