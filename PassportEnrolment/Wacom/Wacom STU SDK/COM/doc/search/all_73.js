var searchData=
[
  ['scale',['Scale',['../namespacewgss_s_t_u.html#a8e8d3da3ac3510ba6f5aa1a4c8c82eae',1,'wgssSTU']]],
  ['scale_5fclip',['Scale_Clip',['../namespacewgss_s_t_u.html#a8e8d3da3ac3510ba6f5aa1a4c8c82eaeac664bdc021b297c3d0cbff67f99fd2da',1,'wgssSTU']]],
  ['scale_5ffit',['Scale_Fit',['../namespacewgss_s_t_u.html#a8e8d3da3ac3510ba6f5aa1a4c8c82eaeaf80f5dbbd9f49a673cde3d451db1afc9',1,'wgssSTU']]],
  ['scale_5fstretch',['Scale_Stretch',['../namespacewgss_s_t_u.html#a8e8d3da3ac3510ba6f5aa1a4c8c82eaea44e4cc5d179ba2e263e813f2431f5d07',1,'wgssSTU']]],
  ['screenheight',['screenHeight',['../interfacewgss_s_t_u_1_1_i_capability.html#adbcefc38fd5b508fa5b9e9318c32b2eb',1,'wgssSTU::ICapability']]],
  ['screenwidth',['screenWidth',['../interfacewgss_s_t_u_1_1_i_capability.html#a59332a8c1ca8ef20e96a644a76f1bda9',1,'wgssSTU::ICapability']]],
  ['secureic',['secureIc',['../interfacewgss_s_t_u_1_1_i_information.html#a4e0e4a218c03b226fe44af9b630d023f',1,'wgssSTU::IInformation']]],
  ['secureicversion',['secureIcVersion',['../interfacewgss_s_t_u_1_1_i_information.html#a1187e3b66fa997411f116af230b1fd93',1,'wgssSTU::IInformation']]],
  ['sequence',['sequence',['../interfacewgss_s_t_u_1_1_i_pen_data_time_count_sequence.html#a69d94e8c75eb6ca0044390880080fbda',1,'wgssSTU::IPenDataTimeCountSequence']]],
  ['serialconnect',['serialConnect',['../interfacewgss_s_t_u_1_1_i_tablet.html#a91a56c28294679a2de753cc7f9890eaf',1,'wgssSTU::ITablet']]],
  ['serialinterface',['SerialInterface',['../namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_serial_interface',1,'wgssSTU']]],
  ['sessionid',['sessionId',['../interfacewgss_s_t_u_1_1_i_pen_data_encrypted.html#a832438703b9d5e21f2b2fc6c28219a8e',1,'wgssSTU::IPenDataEncrypted::sessionId()'],['../interfacewgss_s_t_u_1_1_i_pen_data_time_count_sequence_encrypted.html#a832438703b9d5e21f2b2fc6c28219a8e',1,'wgssSTU::IPenDataTimeCountSequenceEncrypted::sessionId()']]],
  ['set',['set',['../interfacewgss_s_t_u_1_1_i_interface.html#aeb9ac0ba18b4e7e9eb92e528e31ee916',1,'wgssSTU::IInterface']]],
  ['setbackgroundcolor',['setBackgroundColor',['../interfacewgss_s_t_u_1_1_i_protocol.html#af5945799ae19d1931c1159f94c7d32d5',1,'wgssSTU::IProtocol::setBackgroundColor()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#af5945799ae19d1931c1159f94c7d32d5',1,'wgssSTU::ITablet::setBackgroundColor()']]],
  ['setbackgroundcolor24',['setBackgroundColor24',['../interfacewgss_s_t_u_1_1_i_protocol2.html#ad26e97e68e4288f2c41468a2b943c93f',1,'wgssSTU::IProtocol2::setBackgroundColor24()'],['../interfacewgss_s_t_u_1_1_i_tablet2.html#ad26e97e68e4288f2c41468a2b943c93f',1,'wgssSTU::ITablet2::setBackgroundColor24()']]],
  ['setbacklightbrightness',['setBacklightBrightness',['../interfacewgss_s_t_u_1_1_i_protocol.html#a7c7f39ff7e5419bc332be3e56e55d7b6',1,'wgssSTU::IProtocol::setBacklightBrightness()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#a7c7f39ff7e5419bc332be3e56e55d7b6',1,'wgssSTU::ITablet::setBacklightBrightness()']]],
  ['setclearscreen',['setClearScreen',['../interfacewgss_s_t_u_1_1_i_protocol.html#ad2fe61fa5837e93dc63df3bf154bde75',1,'wgssSTU::IProtocol::setClearScreen()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#ad2fe61fa5837e93dc63df3bf154bde75',1,'wgssSTU::ITablet::setClearScreen()']]],
  ['setclearscreenarea',['setClearScreenArea',['../interfacewgss_s_t_u_1_1_i_protocol2.html#a5864b5712b387ddef68dc3f6dfc1f515',1,'wgssSTU::IProtocol2::setClearScreenArea()'],['../interfacewgss_s_t_u_1_1_i_tablet2.html#a5864b5712b387ddef68dc3f6dfc1f515',1,'wgssSTU::ITablet2::setClearScreenArea()']]],
  ['setdh',['setDH',['../interfacewgss_s_t_u_1_1_i_tablet_encryption_handler.html#a5c0aa641da0731b8f168b775b936b2b6',1,'wgssSTU::ITabletEncryptionHandler']]],
  ['setdhbase',['setDHbase',['../interfacewgss_s_t_u_1_1_i_protocol.html#a4d1d87de2759118e631181eca13ba669',1,'wgssSTU::IProtocol::setDHbase()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#a4d1d87de2759118e631181eca13ba669',1,'wgssSTU::ITablet::setDHbase()']]],
  ['setdhprime',['setDHprime',['../interfacewgss_s_t_u_1_1_i_protocol.html#ad67b11928d7a17b226254e42a607c2cc',1,'wgssSTU::IProtocol::setDHprime()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#ad67b11928d7a17b226254e42a607c2cc',1,'wgssSTU::ITablet::setDHprime()']]],
  ['setencryptioncommand',['setEncryptionCommand',['../interfacewgss_s_t_u_1_1_i_protocol2.html#ac7d063879a849dd9684dace2ec914008',1,'wgssSTU::IProtocol2']]],
  ['setendcapture',['setEndCapture',['../interfacewgss_s_t_u_1_1_i_protocol.html#a38fd8b7e9ccd572edf8003d235d64a0b',1,'wgssSTU::IProtocol']]],
  ['setendimagedata',['setEndImageData',['../interfacewgss_s_t_u_1_1_i_protocol.html#ab7270984a2ae1dff872fc14904da9d1c',1,'wgssSTU::IProtocol::setEndImageData()'],['../interfacewgss_s_t_u_1_1_i_protocol2.html#a781dacec50e8765c02d8f5bb53269c5d',1,'wgssSTU::IProtocol2::setEndImageData()']]],
  ['sethandwritingdisplayarea',['setHandwritingDisplayArea',['../interfacewgss_s_t_u_1_1_i_protocol.html#ab3fe19e3cf93a09cea900b1f24748b24',1,'wgssSTU::IProtocol::setHandwritingDisplayArea()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#ab3fe19e3cf93a09cea900b1f24748b24',1,'wgssSTU::ITablet::setHandwritingDisplayArea()']]],
  ['sethandwritingthicknesscolor',['setHandwritingThicknessColor',['../interfacewgss_s_t_u_1_1_i_protocol.html#aced2567a94e088751e97b390d9ae71cf',1,'wgssSTU::IProtocol::setHandwritingThicknessColor()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#a73a7982e8c8526e1acc7c9976017276a',1,'wgssSTU::ITablet::setHandwritingThicknessColor()']]],
  ['sethandwritingthicknesscolor24',['setHandwritingThicknessColor24',['../interfacewgss_s_t_u_1_1_i_protocol2.html#ac124e75d2999d14ef00a6e518b54212b',1,'wgssSTU::IProtocol2::setHandwritingThicknessColor24()'],['../interfacewgss_s_t_u_1_1_i_tablet2.html#ac124e75d2999d14ef00a6e518b54212b',1,'wgssSTU::ITablet2::setHandwritingThicknessColor24()']]],
  ['sethostpublickey',['setHostPublicKey',['../interfacewgss_s_t_u_1_1_i_protocol.html#af0849f16a9e2f243756a184c85c1edf8',1,'wgssSTU::IProtocol']]],
  ['sethostpublickeyandpollfordevicepublickey',['setHostPublicKeyAndPollForDevicePublicKey',['../interfacewgss_s_t_u_1_1_i_protocol_helper.html#aaaad784e2d7228dd9b2976183ecb88e0',1,'wgssSTU::IProtocolHelper']]],
  ['setimagedatablock',['setImageDataBlock',['../interfacewgss_s_t_u_1_1_i_protocol.html#a5b7070c7ec4cf753c2a3f8f29c17e2b3',1,'wgssSTU::IProtocol']]],
  ['setinkingmode',['setInkingMode',['../interfacewgss_s_t_u_1_1_i_protocol.html#ac604d6917e240deea09b5cbe099ac84e',1,'wgssSTU::IProtocol::setInkingMode()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#ac604d6917e240deea09b5cbe099ac84e',1,'wgssSTU::ITablet::setInkingMode()']]],
  ['setinkthreshold',['setInkThreshold',['../interfacewgss_s_t_u_1_1_i_protocol.html#af35788f6507b3606e2eeb1a1e9e4d439',1,'wgssSTU::IProtocol::setInkThreshold()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#af35788f6507b3606e2eeb1a1e9e4d439',1,'wgssSTU::ITablet::setInkThreshold()']]],
  ['setpendataoptionmode',['setPenDataOptionMode',['../interfacewgss_s_t_u_1_1_i_protocol.html#a01d7e2032d5ca8c0555c9e40d5477bce',1,'wgssSTU::IProtocol::setPenDataOptionMode()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#a01d7e2032d5ca8c0555c9e40d5477bce',1,'wgssSTU::ITablet::setPenDataOptionMode()']]],
  ['setproperty',['setProperty',['../interfacewgss_s_t_u_1_1_i_component.html#a9cd2f4ff9b1fb26aaba85b2c89136c7c',1,'wgssSTU::IComponent']]],
  ['setreset',['setReset',['../interfacewgss_s_t_u_1_1_i_protocol.html#abda2a221fd6cac634d5c890650ff649f',1,'wgssSTU::IProtocol']]],
  ['setscreencontrast',['setScreenContrast',['../interfacewgss_s_t_u_1_1_i_protocol2.html#a530f432b7db581f084a27b30bce61e43',1,'wgssSTU::IProtocol2::setScreenContrast()'],['../interfacewgss_s_t_u_1_1_i_tablet2.html#a530f432b7db581f084a27b30bce61e43',1,'wgssSTU::ITablet2::setScreenContrast()']]],
  ['setstartcapture',['setStartCapture',['../interfacewgss_s_t_u_1_1_i_protocol.html#a01c84fced3cc67e57e00f16e13d35897',1,'wgssSTU::IProtocol']]],
  ['setstartimagedata',['setStartImageData',['../interfacewgss_s_t_u_1_1_i_protocol.html#a5a07c56f07253c598582ccf4ae0c9611',1,'wgssSTU::IProtocol']]],
  ['setstartimagedataarea',['setStartImageDataArea',['../interfacewgss_s_t_u_1_1_i_protocol2.html#ab4750fd9954f7e12da7e333555360443',1,'wgssSTU::IProtocol2']]],
  ['setuid',['setUid',['../interfacewgss_s_t_u_1_1_i_protocol.html#a792c6d185485f5b09f40074588b0449c',1,'wgssSTU::IProtocol::setUid()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#a792c6d185485f5b09f40074588b0449c',1,'wgssSTU::ITablet::setUid()']]],
  ['simulateencodingflag',['simulateEncodingFlag',['../interfacewgss_s_t_u_1_1_i_protocol_helper2.html#ac1ccc5f00b8ccb568defcb9be21da726',1,'wgssSTU::IProtocolHelper2']]],
  ['startcapture',['startCapture',['../interfacewgss_s_t_u_1_1_i_tablet.html#a68941800ca488a718fecc206565aac2b',1,'wgssSTU::ITablet']]],
  ['statuscansend',['statusCanSend',['../interfacewgss_s_t_u_1_1_i_protocol_helper.html#a21f0901a65931af83807fcfc589bdb51',1,'wgssSTU::IProtocolHelper']]],
  ['statuscode',['statusCode',['../interfacewgss_s_t_u_1_1_i_status.html#a5a0e7d28126633cd094dd265095f9fe6',1,'wgssSTU::IStatus::statusCode()'],['../namespacewgss_s_t_u.html#ae98a46f4ea1a43ca48acaf15d2eb7113',1,'wgssSTU::StatusCode()']]],
  ['statuscode_5fcalculation',['StatusCode_Calculation',['../namespacewgss_s_t_u.html#ae98a46f4ea1a43ca48acaf15d2eb7113a67c2d7cdcfbe206c7ce63f08c11857ce',1,'wgssSTU']]],
  ['statuscode_5fcapture',['StatusCode_Capture',['../namespacewgss_s_t_u.html#ae98a46f4ea1a43ca48acaf15d2eb7113a704bc28d009f337e8bfdd4da27ba5e49',1,'wgssSTU']]],
  ['statuscode_5fimage',['StatusCode_Image',['../namespacewgss_s_t_u.html#ae98a46f4ea1a43ca48acaf15d2eb7113afd8dca3e253f1c9383315870392ee142',1,'wgssSTU']]],
  ['statuscode_5fimage_5fboot',['StatusCode_Image_Boot',['../namespacewgss_s_t_u.html#ae98a46f4ea1a43ca48acaf15d2eb7113a82f5420af1d11a397c443944559664c2',1,'wgssSTU']]],
  ['statuscode_5fready',['StatusCode_Ready',['../namespacewgss_s_t_u.html#ae98a46f4ea1a43ca48acaf15d2eb7113a8a1341497aad745ff6fb98af4d8fb4e9',1,'wgssSTU']]],
  ['statuscode_5fsystemreset',['StatusCode_SystemReset',['../namespacewgss_s_t_u.html#ae98a46f4ea1a43ca48acaf15d2eb7113ac9dbe167ce4143b00ca07a4774103538',1,'wgssSTU']]],
  ['statuscodersa',['StatusCodeRSA',['../namespacewgss_s_t_u.html#a9e33e51d31438c5bc26934ed14322e48',1,'wgssSTU']]],
  ['statuscodersa_5fcalculating',['StatusCodeRSA_Calculating',['../namespacewgss_s_t_u.html#a9e33e51d31438c5bc26934ed14322e48a97e527c134d28052e6318d4d96e0852e',1,'wgssSTU']]],
  ['statuscodersa_5feven',['StatusCodeRSA_Even',['../namespacewgss_s_t_u.html#a9e33e51d31438c5bc26934ed14322e48a238de267af86d75e1470c0bd937ce7ca',1,'wgssSTU']]],
  ['statuscodersa_5finvalid',['StatusCodeRSA_Invalid',['../namespacewgss_s_t_u.html#a9e33e51d31438c5bc26934ed14322e48a8d62db28ae2f36f0cf790265aea08fb5',1,'wgssSTU']]],
  ['statuscodersa_5flong',['StatusCodeRSA_Long',['../namespacewgss_s_t_u.html#a9e33e51d31438c5bc26934ed14322e48aa8451eccb5b2d3e4107e9d08b10b8b28',1,'wgssSTU']]],
  ['statuscodersa_5fnotready',['StatusCodeRSA_NotReady',['../namespacewgss_s_t_u.html#a9e33e51d31438c5bc26934ed14322e48abce54983fb44cc45548a191e281abce4',1,'wgssSTU']]],
  ['statuscodersa_5fready',['StatusCodeRSA_Ready',['../namespacewgss_s_t_u.html#a9e33e51d31438c5bc26934ed14322e48a0841a4f3249ab65910a25d4342c1db99',1,'wgssSTU']]],
  ['statuscodersa_5fshort',['StatusCodeRSA_Short',['../namespacewgss_s_t_u.html#a9e33e51d31438c5bc26934ed14322e48a968a170163e7e386f05bcf6d750cc7c9',1,'wgssSTU']]],
  ['statuscodersac',['statusCodeRSAc',['../interfacewgss_s_t_u_1_1_i_encryption_status.html#a45411faa710fc1c803781fffe336302f',1,'wgssSTU::IEncryptionStatus']]],
  ['statuscodersae',['statusCodeRSAe',['../interfacewgss_s_t_u_1_1_i_encryption_status.html#a0776211b409112d4fbbd99a9b1c71293',1,'wgssSTU::IEncryptionStatus']]],
  ['statuscodersan',['statusCodeRSAn',['../interfacewgss_s_t_u_1_1_i_encryption_status.html#addce7d3808de8246a418f927b85f766d',1,'wgssSTU::IEncryptionStatus']]],
  ['statusword',['statusWord',['../interfacewgss_s_t_u_1_1_i_status.html#a71692adf5ee0851c8a22decd1ebf1268',1,'wgssSTU::IStatus']]],
  ['supportsencryption',['supportsEncryption',['../interfacewgss_s_t_u_1_1_i_protocol_helper.html#a15f6d87494af389a7e81b38c091c1c43',1,'wgssSTU::IProtocolHelper']]],
  ['supportsencryption_5fdhprime',['supportsEncryption_DHprime',['../interfacewgss_s_t_u_1_1_i_protocol_helper.html#aa5bad9a0bbecd8f8eb524fa35da82d26',1,'wgssSTU::IProtocolHelper']]],
  ['supportswrite',['supportsWrite',['../interfacewgss_s_t_u_1_1_i_interface.html#a2711d1a14aaa40cd60a69f2323aee80e',1,'wgssSTU::IInterface::supportsWrite()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#a2711d1a14aaa40cd60a69f2323aee80e',1,'wgssSTU::ITablet::supportsWrite()']]],
  ['sw',['sw',['../interfacewgss_s_t_u_1_1_i_pen_data.html#a89efeff1ccbc6a81cf22642f0ae2b102',1,'wgssSTU::IPenData']]],
  ['symmetrickeytype',['symmetricKeyType',['../interfacewgss_s_t_u_1_1_i_encryption_status.html#a68fe73fa777edd948ed9e26b59b8f6fa',1,'wgssSTU::IEncryptionStatus::symmetricKeyType()'],['../namespacewgss_s_t_u.html#ae9f99996d0ed81a5da39794f31ccc554',1,'wgssSTU::SymmetricKeyType()']]],
  ['symmetrickeytype_5faes128',['SymmetricKeyType_AES128',['../namespacewgss_s_t_u.html#ae9f99996d0ed81a5da39794f31ccc554abf529c569b14c1ca8f62f9a0d42585ef',1,'wgssSTU']]],
  ['symmetrickeytype_5faes192',['SymmetricKeyType_AES192',['../namespacewgss_s_t_u.html#ae9f99996d0ed81a5da39794f31ccc554a595bddce0f4523d7499856cb85d40a89',1,'wgssSTU']]],
  ['symmetrickeytype_5faes256',['SymmetricKeyType_AES256',['../namespacewgss_s_t_u.html#ae9f99996d0ed81a5da39794f31ccc554a885191742fe4c4cb3a7e12455cf04cb4',1,'wgssSTU']]]
];
