var _serial_protocol_8hpp =
[
    [ "crc_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_protocol_1_1crc__error.html", null ],
    [ "Container", "_serial_protocol_8hpp.html#a51d8fbf9e1f745e60031effd80ced6df", null ],
    [ "getHeaderLength", "_serial_protocol_8hpp.html#a4be3bb4e91cf53506b4e1321ce83e0c1", null ],
    [ "isStartReport", "_serial_protocol_8hpp.html#a2612b34ac775bf925742f66c05d7aae4", null ],
    [ "decodeHasCrc", "_serial_protocol_8hpp.html#a0cd4327751cf8e93263d696c3b59f9e3", null ],
    [ "decodeEncodedDataLength", "_serial_protocol_8hpp.html#a89c741286b17dc0a00fc884c9058cbca", null ],
    [ "maxEncodedDataLength", "_serial_protocol_8hpp.html#acc895b6c8377b4c4f97aef8770b4572b", null ],
    [ "calcEncodedDataLength", "_serial_protocol_8hpp.html#a0be8f59c9135f2cd35a16f4b1f42bef0", null ],
    [ "encodeHeader", "_serial_protocol_8hpp.html#aaec658b749ec208a7377e007cb317e27", null ],
    [ "encodeReport", "_serial_protocol_8hpp.html#a91e7e9ca5b327764763742493e251908", null ],
    [ "decodeData", "_serial_protocol_8hpp.html#a16f9d3ee8cb8b9f6062f215ae0230d18", null ],
    [ "checkCrcAndRemove", "_serial_protocol_8hpp.html#a60e14ba596f3dacf5cb83c3f149cced1", null ],
    [ "ReportId_GetReport", "_serial_protocol_8hpp.html#a4776c8ec62ced0d9807635e23f5b176b", null ],
    [ "ReportId_SetResult", "_serial_protocol_8hpp.html#a4c41218143b10cff164ad77ac54aa166", null ],
    [ "Error_None", "_serial_protocol_8hpp.html#a375ebdc570b3c3adef774e11b8bba327", null ]
];