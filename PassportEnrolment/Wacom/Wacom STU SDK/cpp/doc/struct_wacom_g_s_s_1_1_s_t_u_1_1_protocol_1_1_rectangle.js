var struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle =
[
    [ "Rectangle", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#a2b08e199a1c49c2fe71b243df944285c", null ],
    [ "Rectangle", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#a5743e64aa91cfad65d78b5b6cc550cc4", null ],
    [ "upperLeftXpixel", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#a4acbd8e8ad4f25487133113879cc9038", null ],
    [ "upperLeftYpixel", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#a18eaef163cf3545d97550f23e25b911c", null ],
    [ "lowerRightXpixel", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#a73a0b691b2d49ce4dd19e23b173a7c2a", null ],
    [ "lowerRightYpixel", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#a88965f108de1cda46508f0d072bc778d", null ]
];