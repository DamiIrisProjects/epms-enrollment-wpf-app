var annotated =
[
    [ "WacomGSS", null, [
      [ "OpenSSL", null, [
        [ "BIGNUM", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_b_i_g_n_u_m.html", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_b_i_g_n_u_m" ],
        [ "DH", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_d_h.html", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_d_h" ],
        [ "RSA", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_r_s_a.html", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_r_s_a" ],
        [ "AES_KEY", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_a_e_s___k_e_y.html", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_a_e_s___k_e_y" ]
      ] ],
      [ "STU", "namespace_wacom_g_s_s_1_1_s_t_u.html", "namespace_wacom_g_s_s_1_1_s_t_u" ],
      [ "Win32", null, [
        [ "Gdiplus_StartupToken_traits", "struct_wacom_g_s_s_1_1_win32_1_1_gdiplus___startup_token__traits.html", "struct_wacom_g_s_s_1_1_win32_1_1_gdiplus___startup_token__traits" ]
      ] ],
      [ "concurrent_queue_with_exception", "class_wacom_g_s_s_1_1concurrent__queue__with__exception.html", "class_wacom_g_s_s_1_1concurrent__queue__with__exception" ],
      [ "Crc32_accumulator", "class_wacom_g_s_s_1_1_crc32__accumulator.html", "class_wacom_g_s_s_1_1_crc32__accumulator" ],
      [ "crc16_ansi_accumulator", "class_wacom_g_s_s_1_1crc16__ansi__accumulator.html", "class_wacom_g_s_s_1_1crc16__ansi__accumulator" ]
    ] ]
];