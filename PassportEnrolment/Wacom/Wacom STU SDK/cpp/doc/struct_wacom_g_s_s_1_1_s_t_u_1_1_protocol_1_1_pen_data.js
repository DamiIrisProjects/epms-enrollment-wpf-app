var struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data =
[
    [ "rdy", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#a3fc09880659b16202dbce80dea731627", null ],
    [ "sw", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#ab3a522911dab1f1805d7553589087f7c", null ],
    [ "pressure", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#ad870cb7ecc0fdd1edade1540a050c62d", null ],
    [ "x", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#a0088b1105c470797e4e3f3b32a266f8a", null ],
    [ "y", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#ae0284495d999005070264b35d8cb736f", null ],
    [ "reportId", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#a2dbc11176e1b8706235a3c6a6a787846", null ],
    [ "reportSize", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#a84dd2d00980959657c9b42e651fc984a", null ]
];