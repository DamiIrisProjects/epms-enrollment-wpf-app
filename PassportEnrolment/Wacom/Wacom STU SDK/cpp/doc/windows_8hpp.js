var windows_8hpp =
[
    [ "Handle", "windows_8hpp.html#a12e2d1fdf78cc272acba652778c3f38e", null ],
    [ "FindHandle", "windows_8hpp.html#a0e4bac2b6623754e9f40418367d374a5", null ],
    [ "HModule", "windows_8hpp.html#a3f3a6c2cf35eae86d560793077eeac3e", null ],
    [ "HGlobal", "windows_8hpp.html#a7def9c464537415b3adbc5989c6c83f7", null ],
    [ "HDevNotify", "windows_8hpp.html#ac2104a7a6058be54b8772be7e7230843", null ],
    [ "SC_Handle", "windows_8hpp.html#a7c3480bb3e28378b56fc6d0cdf635218", null ],
    [ "HKey", "windows_8hpp.html#a80b7207de866f565eb90b9acb207f701", null ],
    [ "HEventSource", "windows_8hpp.html#ae99763d402e347315b97a421420ab932", null ],
    [ "win32api_error_category", "windows_8hpp.html#a3785352824ca87a2d94dcdfe0d2286d0", null ],
    [ "make_win32api_error_code", "windows_8hpp.html#acfe81ace651c2b12b6702f26671ea4a5", null ],
    [ "throw_win32api_error", "windows_8hpp.html#a8a61834c3d2f0dd8f8aceeae4a9f37c5", null ],
    [ "throw_win32api_error", "windows_8hpp.html#a95e3972a261da7e1ea176aee99e9f43a", null ],
    [ "win32api_bool", "windows_8hpp.html#a565da41c38368dd3c2305db1cf082d59", null ],
    [ "win32api_bool", "windows_8hpp.html#a5dd83c60679c7db570764b80045425e6", null ],
    [ "win32api_BOOL", "windows_8hpp.html#ae8322685598f7c5fa7c2f977e10c82a1", null ],
    [ "win32api_BOOL", "windows_8hpp.html#ae3ea5e109cde79b4c37dacc5501b09d4", null ],
    [ "win32api_BOOLEAN", "windows_8hpp.html#aa62b7064a7c85da717626663607a23ef", null ],
    [ "win32api_BOOLEAN", "windows_8hpp.html#adbc5675f8f47c103f5a56bf88515bae9", null ],
    [ "win32api_DWORD", "windows_8hpp.html#a99aae18d3468bf29b23d25e7805b0ff2", null ],
    [ "win32api_LONG", "windows_8hpp.html#ac1e3cf427bd4014c05a4adbcc0e6806c", null ],
    [ "win32api_LONG", "windows_8hpp.html#a070a5d51689af1282d1190ed33643576", null ]
];