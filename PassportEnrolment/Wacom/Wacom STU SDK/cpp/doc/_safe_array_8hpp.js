var _safe_array_8hpp =
[
    [ "getVarType", "_safe_array_8hpp.html#a698cb2e3044bd5275ed5548b4de6935d", null ],
    [ "getVarType< std::uint8_t >", "_safe_array_8hpp.html#ae9d2d32285ce3046d19746de64763bb2", null ],
    [ "getVarType< std::uint16_t >", "_safe_array_8hpp.html#a1ea761e47a8dadb08473c88817c9424e", null ],
    [ "toSafeArray", "_safe_array_8hpp.html#a1edc5de8d1f8792460170c3145c9781a", null ],
    [ "toSafeArray", "_safe_array_8hpp.html#a4248397fabe79110837bc80adf785d80", null ],
    [ "toSafeArray_n", "_safe_array_8hpp.html#ac64767b324269c1f4ed0c1d702fe127d", null ],
    [ "is1dArray", "_safe_array_8hpp.html#a3b365de9574f7bd103108395c530bda2", null ],
    [ "isByteArray", "_safe_array_8hpp.html#abf32e78b36162d585d848acaced713a6", null ]
];