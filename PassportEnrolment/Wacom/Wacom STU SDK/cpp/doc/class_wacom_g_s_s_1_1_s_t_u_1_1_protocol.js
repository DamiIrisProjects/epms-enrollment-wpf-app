var class_wacom_g_s_s_1_1_s_t_u_1_1_protocol =
[
    [ "Capability", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability", [
      [ "tabletMaxX", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a976e7297293d858cce366587e74b7cfd", null ],
      [ "tabletMaxY", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c3cdbff2712064e0aa83b3eed2f3477", null ],
      [ "tabletMaxPressure", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a8d6a72cc37f606fa4fe760dd717ce9f6", null ],
      [ "screenWidth", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a761c684ae83e26358200d3ee388425c5", null ],
      [ "screenHeight", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a6e4b9ad91b54c1692737facde710d2bc", null ],
      [ "maxReportRate", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56d2d4feec15ea989441ac96e082bfa7", null ],
      [ "resolution", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab61058530dba6ad6f4bb5a0c96b8f308", null ]
    ] ],
    [ "DevicePublicKey", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_device_public_key.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_device_public_key" ],
    [ "EncryptionCommand", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_command.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_command" ],
    [ "EncryptionStatus", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_status.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_status" ],
    [ "HandwritingDisplayArea", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_display_area.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_display_area" ],
    [ "HandwritingThicknessColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_thickness_color", [
      [ "penColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a87c30b7589047627fb76215e29e0a947", null ],
      [ "penThickness", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa8ac09fbe558a27cb96ca38a86e6584c", null ]
    ] ],
    [ "HandwritingThicknessColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_thickness_color24", [
      [ "penColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a7312a4eb9ec0cd33380916e6acdb2532", null ],
      [ "penThickness", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9a8732a48950bc1641238ea193a713a9", null ]
    ] ],
    [ "ImageDataBlock", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_image_data_block", [
      [ "length", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afdfdd4eb62c6fba7984cf54a3f3139ed", null ],
      [ "data", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab5c3dabda471303211b5a4a01868f512", null ]
    ] ],
    [ "Information", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information", [
      [ "firmwareMajorVersion", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a57e3b3e27a57c4f798ebd42a09008764", null ],
      [ "firmwareMinorVersion", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a29bf1aaec5111afdc331827f78d12cd7", null ],
      [ "secureIc", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9fb7685bc7a3b62f0a0d4d30673637e2", null ],
      [ "secureIcVersion", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af3c546fe13583fc0e0d3e31f7ba5ccfd", null ]
    ] ],
    [ "InkThreshold", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_ink_threshold", [
      [ "onPressureMark", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afc1a7e9dfeff55b6b20067db7f840b91", null ],
      [ "offPressureMark", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a81b24b28d3dad11a440b368ba85c644d", null ]
    ] ],
    [ "PenData", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data" ],
    [ "PenDataEncrypted", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted" ],
    [ "PenDataEncryptedOption", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option" ],
    [ "PenDataOption", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option" ],
    [ "PenDataTimeCountSequence", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence" ],
    [ "PenDataTimeCountSequenceEncrypted", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence_encrypted.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence_encrypted" ],
    [ "Rectangle", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle" ],
    [ "Status", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status", [
      [ "statusCode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aae1fa796857b617fa1cff2c64980f1be", null ],
      [ "lastResultCode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9fb1067330b66fc9913639199093a860", null ],
      [ "statusWord", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aff20448e3bfb6bbcc6a29b965cfce188", null ]
    ] ],
    [ "Uid2", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_uid2", null ],
    [ "PublicKey", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af861455a5783ccf2b1373a283bab4703", null ],
    [ "DHprime", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a075c5212e66e805d4b4e82ac32c73a4f", null ],
    [ "DHbase", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a461acd3acf38b0b9be22a720efb932ce", null ],
    [ "ReportId", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0", [
      [ "ReportId_PenData", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a6249c5bdce2bfaaa59dd9d3f267814c3", null ],
      [ "ReportId_Status", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a4bcf4ef9ef8d7280ceb47dae6eda9f33", null ],
      [ "ReportId_Reset", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a53842274e7da8b8cc5406536341cad17", null ],
      [ "ReportId_Information", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0adf150219f9d24179c507d5b32ab9ceb7", null ],
      [ "ReportId_Capability", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0ad743a2beaa347741af8bfefe6cdd329f", null ],
      [ "ReportId_Uid", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a064437bd64e90ff9f7c4eaa7ed5f4322", null ],
      [ "ReportId_Uid2", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a3f6c4eb0ec7eb2b9d6546c5e632a2ac8", null ],
      [ "ReportId_PenDataEncrypted", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0ab2762caba32af1eeaf5b479c30692598", null ],
      [ "ReportId_HostPublicKey", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0abbb723719a68a69fb2951ec2685ac86b", null ],
      [ "ReportId_DevicePublicKey", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0aa0de07b4339761c79cb5b3e52d6d44bd", null ],
      [ "ReportId_StartCapture", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0ae72bc5244d55226f45822128a45951fe", null ],
      [ "ReportId_EndCapture", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a15c109bbc255b8844ac31e6a0bd5dbc9", null ],
      [ "ReportId_DHprime", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a5e89bce0fa16993da7eadd418f473f5c", null ],
      [ "ReportId_DHbase", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0ae5ea52664265106ee3260a49c3f8c027", null ],
      [ "ReportId_ClearScreen", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a7a6c9ceb6cb47f7ea6ac31080020bed0", null ],
      [ "ReportId_InkingMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a4300b69ea2cdaf6d370c4fa676efdd1c", null ],
      [ "ReportId_InkThreshold", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a2e6a6a55bad0e51937614382a909e852", null ],
      [ "ReportId_ClearScreenArea", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a9337949c7b0c2309e0259fc2266eed23", null ],
      [ "ReportId_StartImageDataArea", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a9be33d15b5bb7d1c5074e25b99fe2ce0", null ],
      [ "ReportId_StartImageData", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a056aa8dd0106a222c1b35a9c0ef16de0", null ],
      [ "ReportId_ImageDataBlock", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0aacbb9c1c35959b4a0a9868930ded3d31", null ],
      [ "ReportId_EndImageData", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a8ca4f157f3dbdf6bccd777ff56f3ee8d", null ],
      [ "ReportId_HandwritingThicknessColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0aa361754383d64b48bb4b0be749e6e06a", null ],
      [ "ReportId_BackgroundColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a408015addade47e48840f52336d15789", null ],
      [ "ReportId_HandwritingDisplayArea", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a8e6e80801888080d5e50d59383cb8fc1", null ],
      [ "ReportId_BacklightBrightness", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a587bade5df2d103d12eb131f3fdbe3ae", null ],
      [ "ReportId_ScreenContrast", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a8ab539da99ab3ea50e82a71ae0824c4d", null ],
      [ "ReportId_HandwritingThicknessColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a13994d0191266e7aa8098c05fa5d40d5", null ],
      [ "ReportId_BackgroundColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0ac5b0cb02f41220e2dd142e0d796eb814", null ],
      [ "ReportId_BootScreen", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0aad268f61152792ecc38b71101a1eb80c", null ],
      [ "ReportId_PenDataOption", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a2ca67674e2ed3659448d83817425dee7", null ],
      [ "ReportId_PenDataEncryptedOption", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0ad7ce0b254b2d86ad1eb6426bab98e725", null ],
      [ "ReportId_PenDataOptionMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a53f30290d3afe072e0fba96597d84dd5", null ],
      [ "ReportId_PenDataTimeCountSequenceEncrypted", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a0b7e844a9798102920487c56d68b65f8", null ],
      [ "ReportId_PenDataTimeCountSequence", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a3cd369e64def4fc0d9dcba6e99f85977", null ],
      [ "ReportId_EncryptionCommand", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0adbec70888b07da89d00bbcf5146aeba5", null ],
      [ "ReportId_EncryptionStatus", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0ac7d232d91981912460936d5fea02f8a6", null ],
      [ "ReportId_GetReport", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0ae9c919a1788fe807f08fd415170cfcce", null ],
      [ "ReportId_SetResult", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a958f52de9966537b40238e26425ba0c0a4decaab7ff40ffdad30a6a3d2d91c849", null ]
    ] ],
    [ "StatusCode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56494ea11ab7c5319ecc756df5c5b7fa", [
      [ "StatusCode_Ready", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56494ea11ab7c5319ecc756df5c5b7faa3f4e2c6d38ebdfc33a508e0b88a97f44", null ],
      [ "StatusCode_Image", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56494ea11ab7c5319ecc756df5c5b7faaf7891c9ca90330f4bb959c34c2fdc55f", null ],
      [ "StatusCode_Capture", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56494ea11ab7c5319ecc756df5c5b7faa5da5565087799ead684c547241f7768a", null ],
      [ "StatusCode_Calculation", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56494ea11ab7c5319ecc756df5c5b7faa5083e4f4f3580ee76487b561eca5bb56", null ],
      [ "StatusCode_Image_Boot", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56494ea11ab7c5319ecc756df5c5b7faaa66392dd6881841495260f5697e76f77", null ],
      [ "StatusCode_SystemReset", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56494ea11ab7c5319ecc756df5c5b7faa192bd8f92025f914bd8f4f13eb0fd35f", null ]
    ] ],
    [ "ErrorCode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13", [
      [ "ErrorCode_None", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13a08267a2fbeaf56eb9393b8841e59effe", null ],
      [ "ErrorCode_WrongReportId", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13af8b796d19ccc70395edda6e3b45ed0da", null ],
      [ "ErrorCode_WrongState", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13afdf2189a88f099ec7e85e0b85a7bb950", null ],
      [ "ErrorCode_CRC", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13a10f01d2a37228e01e4a00d0365547348", null ],
      [ "ErrorCode_GraphicsWrongEncodingType", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13aeaff401761323be584b9446ad65688b9", null ],
      [ "ErrorCode_GraphicsImageTooLong", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13aa6a8342f029ef80c816c93cda2c9965a", null ],
      [ "ErrorCode_GraphicsZlibError", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13a4a511e3847cbf1741ff528136a5f49e1", null ],
      [ "ErrorCode_GraphicsWrongParameters", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c5e083615b749a6a72302592d63ee13ad30b6908ed325afbce141d8cf2d1c450", null ]
    ] ],
    [ "ResetFlag", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a33af8840e9adf50fda554d7b22cb94f7", [
      [ "ResetFlag_Software", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a33af8840e9adf50fda554d7b22cb94f7a8b52e2400f429dc94f1c25a528f00bcd", null ],
      [ "ResetFlag_Hardware", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a33af8840e9adf50fda554d7b22cb94f7af9f6c3187880352e17b75d23aa6f6ea5", null ]
    ] ],
    [ "EncodingFlag", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa6991a25576bd1f899d40238fe781866", [
      [ "EncodingFlag_Zlib", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa6991a25576bd1f899d40238fe781866a7a6883a4c3bb505f22a4e3fb12bac43c", null ],
      [ "EncodingFlag_1bit", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa6991a25576bd1f899d40238fe781866a6558f2a55befa1d40bb7ba2bd7a03351", null ],
      [ "EncodingFlag_16bit", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa6991a25576bd1f899d40238fe781866a0a2e4f810933cf4b1bd3611c01995a37", null ],
      [ "EncodingFlag_24bit", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa6991a25576bd1f899d40238fe781866a770ad86e5523341b14808fcb9f51eb82", null ]
    ] ],
    [ "InkingMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a06fd7a6490d6985bea45cbb39bdb10ae", [
      [ "InkingMode_Off", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a06fd7a6490d6985bea45cbb39bdb10aeafcda846b61e36e2adbac3d0225ebbfa8", null ],
      [ "InkingMode_On", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a06fd7a6490d6985bea45cbb39bdb10aea8625930f7a63c0c169a6c705befaab4b", null ]
    ] ],
    [ "EncodingMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae09537ed3f34319a645ecc6fcd7987e3", [
      [ "EncodingMode_1bit", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae09537ed3f34319a645ecc6fcd7987e3a7c58fedf606275c706285ffc644ada0d", null ],
      [ "EncodingMode_1bit_Zlib", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae09537ed3f34319a645ecc6fcd7987e3a916eaac0e0b01af10ee6dcb8b0083093", null ],
      [ "EncodingMode_16bit", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae09537ed3f34319a645ecc6fcd7987e3a60037be963d70dd29ad9046eb8a9a82f", null ],
      [ "EncodingMode_24bit", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae09537ed3f34319a645ecc6fcd7987e3a90b3aa50430b9d182a12d1c5684f13ff", null ],
      [ "EncodingMode_1bit_Bulk", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae09537ed3f34319a645ecc6fcd7987e3ab178c865e3be242ff73640b09590b171", null ],
      [ "EncodingMode_16bit_Bulk", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae09537ed3f34319a645ecc6fcd7987e3a64a95a0225241ede180a9230f89c9b86", null ],
      [ "EncodingMode_24bit_Bulk", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae09537ed3f34319a645ecc6fcd7987e3af0f7615a0ee25da31b1495b57546419c", null ]
    ] ],
    [ "EndImageDataFlag", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af3c13f35e19b1a1ea98e511956d9938b", [
      [ "EndImageDataFlag_Commit", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af3c13f35e19b1a1ea98e511956d9938baf53730aec4589395a0a4eb63f273a981", null ],
      [ "EndImageDataFlag_Abandon", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af3c13f35e19b1a1ea98e511956d9938ba94b201275eff809eef7e3bd2c7db9bc2", null ]
    ] ],
    [ "PenDataOptionMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9774de27ed9cd39b7187e56eb056688b", [
      [ "PenDataOptionMode_None", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9774de27ed9cd39b7187e56eb056688ba5223d18c2b1e0a05b7b92740255ddbf9", null ],
      [ "PenDataOptionMode_TimeCount", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9774de27ed9cd39b7187e56eb056688bae0bf39cad31fe1755d94bc3cfac0b2d6", null ],
      [ "PenDataOptionMode_SequenceNumber", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9774de27ed9cd39b7187e56eb056688ba43b0b83b1cceb48aba481ef4fb5e7f5a", null ],
      [ "PenDataOptionMode_TimeCountSequence", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9774de27ed9cd39b7187e56eb056688ba74334d806b6fa109ba8d559fc886c1a9", null ]
    ] ],
    [ "StatusCodeRSA", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0535e3cfc6ffb34b5579f7c60ca281a2", [
      [ "StatusCodeRSA_Ready", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0535e3cfc6ffb34b5579f7c60ca281a2a50dffa2e537b74d82a13e3f5bbf9f153", null ],
      [ "StatusCodeRSA_Calculating", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0535e3cfc6ffb34b5579f7c60ca281a2a76cc349d374c36445451960bf777e62d", null ],
      [ "StatusCodeRSA_Even", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0535e3cfc6ffb34b5579f7c60ca281a2ab4d72f20eac3676644581ce8ab7b2842", null ],
      [ "StatusCodeRSA_Long", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0535e3cfc6ffb34b5579f7c60ca281a2a1af581bca10d2bcfdc80d66f2e7811cb", null ],
      [ "StatusCodeRSA_Short", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0535e3cfc6ffb34b5579f7c60ca281a2a64667ce77513ad12ea09de3f4e245368", null ],
      [ "StatusCodeRSA_Invalid", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0535e3cfc6ffb34b5579f7c60ca281a2a8b5f50be1bbe22a8f4430f62c2610ec4", null ],
      [ "StatusCodeRSA_NotReady", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0535e3cfc6ffb34b5579f7c60ca281a2a53a2e68d488cfc11e12b9df236e202ef", null ]
    ] ],
    [ "ErrorCodeRSA", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568", [
      [ "ErrorCodeRSA_None", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568a1322e61fcd287e1659ee0e9b6d999566", null ],
      [ "ErrorCodeRSA_BadParameter", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568a8b27ab9b2c2b0d52608cac9fdad35216", null ],
      [ "ErrorCodeRSA_ParameterTooLong", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568a7863a29cbc7b7c8698de952ad933a530", null ],
      [ "ErrorCodeRSA_PublicKeyNotReady", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568aeb3478fbf7cc4fd22f9a116a5c3907fd", null ],
      [ "ErrorCodeRSA_PublicExponentNotReady", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568af56bed0dc34e5315471cc42915fac76e", null ],
      [ "ErrorCodeRSA_SpecifiedKeyInUse", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568a4744e3013e669a216f5b4fd7927bec8e", null ],
      [ "ErrorCodeRSA_SpecifiedKeyNotInUse", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568a437517485b29f30e5ea1431aa3bb3a61", null ],
      [ "ErrorCodeRSA_BadCommandCode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568ace9e81033b78bea97df45e7c58683fe9", null ],
      [ "ErrorCodeRSA_CommandPending", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568aa69f05e86f3672583e2e3640c16ed529", null ],
      [ "ErrorCodeRSA_SpecifiedKeyExists", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568ad9d0a50cfe631f77786801b0c781a358", null ],
      [ "ErrorCodeRSA_SpecifiedKeyNotExist", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568a0c61e158bd97720165e397da5bcaea3a", null ],
      [ "ErrorCodeRSA_NotInitialized", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a84e4ea1970bff86c1e6a3f7fe6fe1568a97fcebabae88f547f3bd9f27e7c5f77f", null ]
    ] ],
    [ "SymmetricKeyType", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#adb042a4adeb93af62cfe988f913e7299", [
      [ "SymmetricKeyType_AES128", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#adb042a4adeb93af62cfe988f913e7299a64480680d79e53f690ae9babbaf07f15", null ],
      [ "SymmetricKeyType_AES192", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#adb042a4adeb93af62cfe988f913e7299a005411e4961aa1c5382ef6eff673f0d5", null ],
      [ "SymmetricKeyType_AES256", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#adb042a4adeb93af62cfe988f913e7299aaaf32927aeabc0b1d961726865a39471", null ]
    ] ],
    [ "AsymmetricKeyType", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#abc033cd3b0c9dde438a891c161a4ba91", [
      [ "AsymmetricKeyType_RSA1024", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#abc033cd3b0c9dde438a891c161a4ba91a97ce8c0671c70c22e1a1099ae9844912", null ],
      [ "AsymmetricKeyType_RSA1536", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#abc033cd3b0c9dde438a891c161a4ba91afde021a6601e6733b5c26eaad45e4602", null ],
      [ "AsymmetricKeyType_RSA2048", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#abc033cd3b0c9dde438a891c161a4ba91ae1e04a6bf336ff457811b1a8e7cc94b2", null ]
    ] ],
    [ "AsymmetricPaddingType", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab0fdc698242a41441dbe8e55aff33818", [
      [ "AsymmetricPaddingType_None", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab0fdc698242a41441dbe8e55aff33818aad800dbf8c11ab879da90fec33d9e123", null ],
      [ "AsymmetricPaddingType_PKCS1", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab0fdc698242a41441dbe8e55aff33818ab97ce96d4f13bea436726788c6589675", null ],
      [ "AsymmetricPaddingType_OAEP", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab0fdc698242a41441dbe8e55aff33818ac7a6e75a7340696bf50fabef3a179af5", null ]
    ] ],
    [ "EncryptionCommandNumber", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a17acb286814bf9f02fe261e674a1aeaa", [
      [ "EncryptionCommandNumber_SetEncryptionType", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a17acb286814bf9f02fe261e674a1aeaaa7136072fdb49a6594cc0e8a4b87de42b", null ],
      [ "EncryptionCommandNumber_SetParameterBlock", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a17acb286814bf9f02fe261e674a1aeaaa07207794462a3600ff75d9c155f0e6ba", null ],
      [ "EncryptionCommandNumber_GetStatusBlock", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a17acb286814bf9f02fe261e674a1aeaaa79f0635e96f5dc029adc02e6c1e41cbd", null ],
      [ "EncryptionCommandNumber_GetParameterBlock", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a17acb286814bf9f02fe261e674a1aeaaa9cc908a309cc883469a2bd0d2c9c8cff", null ],
      [ "EncryptionCommandNumber_GenerateSymmetricKey", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a17acb286814bf9f02fe261e674a1aeaaa9dff10117dbb03427f21f5cd086da0df", null ]
    ] ],
    [ "EncryptionCommandParameterBlockIndex", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a13300c0f35edf2693308203f6899aff9", [
      [ "EncryptionCommandParameterBlockIndex_RSAe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a13300c0f35edf2693308203f6899aff9a0e7df5c5fb5f700076fdc0c2daff6bd1", null ],
      [ "EncryptionCommandParameterBlockIndex_RSAn", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a13300c0f35edf2693308203f6899aff9a515c886623728178340f8b230bb7118f", null ],
      [ "EncryptionCommandParameterBlockIndex_RSAc", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a13300c0f35edf2693308203f6899aff9ae98c53a1c51b1c216e016281b4161a22", null ],
      [ "EncryptionCommandParameterBlockIndex_RSAm", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a13300c0f35edf2693308203f6899aff9afe565468504be79d5ed73952e69362d8", null ]
    ] ],
    [ "Protocol", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a2f4c5bae6103589da127aa3dff90f25a", null ],
    [ "rebind", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a15dfc8033e73fa08cc9246272920cf91", null ],
    [ "operator->", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a990791f3fa7d9e215a7ab3759f19b88d", null ],
    [ "getStatus", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a896ba2b1a2093ac0891981eedae9c0c8", null ],
    [ "setReset", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afc30c32caa7eb886483886a7a40d3c7a", null ],
    [ "getInformation", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ae89283e6f41f377132e95c8e0d358cc2", null ],
    [ "getCapability", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a1d0558da6b87b500e009512dd088fc5c", null ],
    [ "getUid", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a2d4e11979e66fa0029db07d46c2a6b91", null ],
    [ "setUid", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a3280612801077ef546108ec3d1d166ad", null ],
    [ "getUid2", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a00fd399f93197d4d928c246a457ff885", null ],
    [ "getHostPublicKey", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a285f97eafd7acfd56d106d77a4789689", null ],
    [ "setHostPublicKey", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0c6b1c6629a6dc4b415516a286162c30", null ],
    [ "getDevicePublicKey", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9689f3b17909783c0523c17072754556", null ],
    [ "setStartCapture", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a85445c66a2f032a17065ea85b422a0af", null ],
    [ "setEndCapture", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ac7a725f81731bd6f5c63054fd5b8d6b9", null ],
    [ "getDHprime", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a6e66910fc662aa8e9501589ffb326447", null ],
    [ "setDHprime", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aca004768c4818a9fc888120a1ea420aa", null ],
    [ "getDHbase", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afd5747b717c22b6df046c4c761176f08", null ],
    [ "setDHbase", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a94b4c4309278385ebaf63aa3f26e2eaa", null ],
    [ "setClearScreen", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a8aa71c19e45fae95f8495423b8205a0c", null ],
    [ "setClearScreenArea", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af77ab82915ccb1c4c4725946aad587d9", null ],
    [ "getInkingMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a7997cb91a56d24f082127a3b72d0ee80", null ],
    [ "setInkingMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a01ed1508bec38dd9b18dfe74c3662770", null ],
    [ "getInkThreshold", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ad1b3cd93c339b233e767d7a67d2e3068", null ],
    [ "setInkThreshold", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab6e7c33a80aeb483ed58d1a33c15e966", null ],
    [ "setStartImageData", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a899868f5e0b7bcb1e3f486c0f700efe0", null ],
    [ "setStartImageData", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a07bdd00d981e41b6b197aa4cc943942f", null ],
    [ "setStartImageDataArea", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a57b8bd220845edd1538a9919c8804cb6", null ],
    [ "setImageDataBlock", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a4539480b544da38472aeee2d5f9bb60d", null ],
    [ "setEndImageData", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af55cc891ca44dbfbdf6c9341f433f907", null ],
    [ "setEndImageData", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a4b9b8d2c642f57596994f7e128ffbfc7", null ],
    [ "getHandwritingThicknessColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a810d04a1763d76debafacd6202fe648b", null ],
    [ "setHandwritingThicknessColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a18adc505bec3eaf905c907e6675bd23c", null ],
    [ "getHandwritingThicknessColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a6f587c5a40525fa98e1f301225920a0f", null ],
    [ "setHandwritingThicknessColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a245080cd5a7f43e198fec20308eead43", null ],
    [ "getBackgroundColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a31ecda4b6bd0e2ecce473e9462fda838", null ],
    [ "setBackgroundColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a587440ef3b4bae0f90e5beed24abdb0d", null ],
    [ "getBackgroundColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0a802528a03d683099a6acfb55165796", null ],
    [ "setBackgroundColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af3592670ee90d7e7fd9bcab5f4cad033", null ],
    [ "getHandwritingDisplayArea", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a726ecb06813970262a524de1f55d1e44", null ],
    [ "setHandwritingDisplayArea", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a69dd8d527517455c5a2a7328fb902e97", null ],
    [ "setHandwritingDisplayArea", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a7e92908627587bc5b75d7ddb0d92ab3b", null ],
    [ "getBacklightBrightness", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a68987f8db4f0d214f84c31a2b2842ab6", null ],
    [ "setBacklightBrightness", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa09db6795667a11fca1a460e728d51fd", null ],
    [ "getScreenContrast", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9a2a2fe6ebf6e312ef5ce6bc7cb934ad", null ],
    [ "setScreenContrast", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a563bce8e35fe274565b7c607e14e5157", null ],
    [ "getPenDataOptionMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0e3bb0144b8c62712982b81668813d10", null ],
    [ "setPenDataOptionMode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a0420107f373ebc920d56b2dea09e95ca", null ],
    [ "getEncryptionStatus", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a82ec34dd7b10eef84e5df69298eca4b0", null ],
    [ "getEncryptionCommand", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afcd7a8b03573edfbdb5e63bbb2feeb63", null ],
    [ "setEncryptionCommand", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a07685c811be552525bce741668282544", null ],
    [ "decodeReport_unsafe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a1d8c1145044c82714966be0addfc7243", null ],
    [ "decodeReport_unsafe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab08ee9dfb921c119b3e199c553d314f4", null ],
    [ "decodeReport_unsafe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a89931abe142f2d9859a258c3d28c95a7", null ],
    [ "decodeReport_unsafe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a080dfe3aaf3aaaf8f043884cdf437377", null ],
    [ "decodeReport_unsafe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a46127ffac47f89e7b6a44a572623804f", null ],
    [ "decodeReport_unsafe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a7fcb17cd9543a34713f66f5b62c7e283", null ],
    [ "decodeReport_unsafe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa09c3fb29d53ebc09927b4dc7ad843a6", null ],
    [ "decodeReport_unsafe", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a093b4255f0b792933495c2dcddce9e5a", null ],
    [ "decodeReport", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afe64182b8b0b7220413be3d37a475a62", null ],
    [ "decodeReport", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afaded78b4c90d9db111d7e4e15fd23e6", null ],
    [ "decodeReport", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab50bc54bdd43b716ff94e186ee81c677", null ],
    [ "decodeReport", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#abadf4ebceade2d18baef627537da51ff", null ],
    [ "decodeReport", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a7f7f3ec1ee03cc3e247a1fe47db6ca31", null ],
    [ "EncodingMode_Raw", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afd4f2de3c3692f9eb7753aaff8001cce", null ],
    [ "EncodingMode_Zlib", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a6586a73a027609234903b7c46cab7e0c", null ],
    [ "EncodingMode_Bulk", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ad4eb496a22f8ea01838ca02c4f889a37", null ],
    [ "EncodingMode_16bit_565", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a272330e6f110f7839a39971c4ace7cfb", null ]
];