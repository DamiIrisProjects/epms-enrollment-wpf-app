var searchData=
[
  ['usbdevice',['UsbDevice',['../struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a281778e1619faf61aba0d617d29869ca',1,'WacomGSS::STU::UsbDevice::UsbDevice(std::uint16_t idVendor, std::uint16_t idProduct, std::uint16_t bcdDevice, std::wstring fn, std::wstring bn, std::uint32_t di)'],['../struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#afe91061a0d2236d2711bf1b8b5fa34df',1,'WacomGSS::STU::UsbDevice::UsbDevice(std::uint16_t idVendor, std::uint16_t idProduct, std::uint16_t bcdDevice, std::uint8_t bn, std::uint8_t da)']]],
  ['usbinterface',['UsbInterface',['../class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a98ffe0b83f33ecf3fff9ac54fb8140bd',1,'WacomGSS::STU::UsbInterface::UsbInterface()'],['../class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#acdbb857c70e2e83871f4a92d66cad00d',1,'WacomGSS::STU::UsbInterface::UsbInterface(std::shared_ptr&lt; Win32::WinusbDll &gt; &amp;winusb)']]]
];
