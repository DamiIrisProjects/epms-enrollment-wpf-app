var searchData=
[
  ['pencolor',['penColor',['../class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a87c30b7589047627fb76215e29e0a947',1,'WacomGSS::STU::Protocol::HandwritingThicknessColor::penColor()'],['../class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a7312a4eb9ec0cd33380916e6acdb2532',1,'WacomGSS::STU::Protocol::HandwritingThicknessColor24::penColor()']]],
  ['penthickness',['penThickness',['../class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa8ac09fbe558a27cb96ca38a86e6584c',1,'WacomGSS::STU::Protocol::HandwritingThicknessColor::penThickness()'],['../class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9a8732a48950bc1641238ea193a713a9',1,'WacomGSS::STU::Protocol::HandwritingThicknessColor24::penThickness()']]],
  ['pressure',['pressure',['../struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#ad870cb7ecc0fdd1edade1540a050c62d',1,'WacomGSS::STU::Protocol::PenData']]]
];
