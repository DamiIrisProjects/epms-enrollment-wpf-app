var dir_ca22d487b5a775c47c3a47d93c83b427 =
[
    [ "cfgmgr32.hpp", "cfgmgr32_8hpp.html", "cfgmgr32_8hpp" ],
    [ "com.hpp", "com_8hpp.html", "com_8hpp" ],
    [ "d2d1.hpp", "d2d1_8hpp.html", null ],
    [ "debug_TypeInfo.hpp", "debug___type_info_8hpp.html", "debug___type_info_8hpp" ],
    [ "dispex.hpp", "dispex_8hpp.html", "dispex_8hpp" ],
    [ "dshow.hpp", "dshow_8hpp.html", null ],
    [ "dwrite.hpp", "dwrite_8hpp.html", null ],
    [ "gdiplus.hpp", "gdiplus_8hpp.html", "gdiplus_8hpp" ],
    [ "getDevicePowerState.hpp", "get_device_power_state_8hpp.html", "get_device_power_state_8hpp" ],
    [ "getFileVersion.hpp", "get_file_version_8hpp.html", "get_file_version_8hpp" ],
    [ "hidsdi.hpp", "hidsdi_8hpp.html", "hidsdi_8hpp" ],
    [ "ocidl.hpp", "ocidl_8hpp.html", null ],
    [ "SafeArray.hpp", "_safe_array_8hpp.html", "_safe_array_8hpp" ],
    [ "setupapi.hpp", "setupapi_8hpp.html", "setupapi_8hpp" ],
    [ "wincodec.hpp", "wincodec_8hpp.html", "wincodec_8hpp" ],
    [ "windows.hpp", "windows_8hpp.html", "windows_8hpp" ],
    [ "winhttp.hpp", "winhttp_8hpp.html", "winhttp_8hpp" ],
    [ "winusb.hpp", "winusb_8hpp.html", null ]
];