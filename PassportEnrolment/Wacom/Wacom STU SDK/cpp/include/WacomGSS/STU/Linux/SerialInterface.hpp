/// @file      WacomGSS/STU/Win32/SerialInterface.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2012-02-27
/// @brief     provides the class that can open an STU device over the Serial

#ifndef WacomGSS_STU_Linux_SerialInterface_hpp
#define WacomGSS_STU_Linux_SerialInterface_hpp

#include <WacomGSS/STU/Interface.hpp>
#include <system_error>


namespace WacomGSS
{

  namespace STU
  {

    class SerialInterface : public Interface
    {
    public:
      std::error_code connect(std::string const & fileName, bool useCrc);
      std::error_code connect(char const * fileName, bool useCrc);

      void disconnect() override;

      bool isConnected() const override;

      void get(uint8_t * data, size_t length) override;

      void set(uint8_t const * data, size_t length) override;

      bool supportsWrite() const override;

      void write(uint8_t const * data, size_t length) override;

      bool getReportCountLengths(std::array<std::uint16_t, 256> & reportCountLengths) const override;

      std::uint16_t getProductId() const override;
    };
    
  } // namespace STU

} // namespace WacomGSS

#endif // WacomGSS_STU_Linux_UsbInterface_hpp
