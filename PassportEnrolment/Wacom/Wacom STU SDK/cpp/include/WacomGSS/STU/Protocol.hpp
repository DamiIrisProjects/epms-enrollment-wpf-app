/// @file      WacomGSS/STU/Protocol.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2011-10-18
/// @brief     Definition of the WacomGSS::STU::Protocol class

#ifndef WacomGSS_STU_Protocol_hpp
#define WacomGSS_STU_Protocol_hpp

#include <WacomGSS/STU/Interface.hpp>


namespace WacomGSS
{
  namespace STU
  {

    /// @brief A very limited iterator that performs a bounds check on access.    
    template<typename InputIterator>
    class CheckedInputIteratorReference
    {
      InputIterator &       m_iter;
      InputIterator const & m_end;
    public:
      typedef typename InputIterator::value_type value_type;
      typedef typename InputIterator::reference  reference;

      CheckedInputIteratorReference(InputIterator & begin, InputIterator const & end)
      :
        m_iter(begin),
        m_end(end)
      {
      }

      reference operator * () const
      {
        if (m_iter != m_end)
          return *m_iter;

        throw std::range_error("The InputIterator did not have enough elements");
      }

      CheckedInputIteratorReference & operator ++ ()
      {
        ++m_iter;
        return *this;
      }
    };
          


    /// @brief Abstracts the documented %STU tablet protocol.
    ///
    /// This class provides an abstraction to send and receive
    /// data packets to and from the tablet using the Interface class.
    class Protocol
    {
      Interface * m_interface; // will never be null

      template<size_t N>
      void get(std::array<uint8_t,N> & data)
      {
        m_interface->get(data);
      }
      

      template<size_t N>
      void set(std::array<uint8_t,N> const & data)
      {
        m_interface->set(data);
      }

      template<typename ForwardIterator, class InputReport>
      static ForwardIterator decodeReport(ForwardIterator begin, ForwardIterator end, InputReport & data, std::forward_iterator_tag)
      {
        auto d = std::distance(begin, end);
        if (d >= 0 && static_cast<size_t>(d) < InputReport::reportSize)
          throw std::range_error("decodeReport(): the ForwardIterator did not have enough elements to decode the requested InputReport");

        return decodeReport_unsafe(begin, data);
      }


      template<typename InputIterator, class InputReport>
      static InputIterator decodeReport(InputIterator begin, InputIterator end, InputReport & data, std::input_iterator_tag)
      {
        CheckedInputIteratorReference<InputIterator> iter(begin, end);
        decodeReport_unsafe(iter, data);
        return begin;
      }

    public:      
      /// @brief Constructs the object.
      ///
      /// @param intf A reference to the Interface to use.
      Protocol(Interface & intf) noexcept
      :
        m_interface(&intf)
      {
      }


      /// @brief Rebinds the object to a different Interface.
      ///
      /// @param intf A reference to the Interface to use.
      Protocol & rebind(Interface & intf) noexcept
      {
        m_interface = &intf;
        return *this;
      }


      /// @brief Provides access to the stored Interface reference.
      Interface * operator -> () const noexcept
      {
        return m_interface;
      }
      

      /// @brief Report Identifier values.
      enum ReportId : uint8_t
      {
        ReportId_PenData                           = 0x01, ///< <b>Mode:</b> in         <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_Status                            = 0x03, ///< <b>Mode:</b>    get     <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_Reset                             = 0x04, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
      //ReportId_05                                = 0x05, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300fw2
        ReportId_Information                       = 0x08, ///< <b>Mode:</b>    get     <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_Capability                        = 0x09, ///< <b>Mode:</b>    get     <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_Uid                               = 0x0a, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_Uid2                              = 0x0b, ///< <b>Mode:</b>    get     <b>Compatibility:</b>  -   -   -   430 530
        ReportId_PenDataEncrypted                  = 0x10, ///< <b>Mode:</b> in         <b>Compatibility:</b> 300 500 520A  ?   ?
        ReportId_HostPublicKey                     = 0x13, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A  *   *
        ReportId_DevicePublicKey                   = 0x14, ///< <b>Mode:</b> in/get     <b>Compatibility:</b> 300 500 520A  *   *
        ReportId_StartCapture                      = 0x15, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_EndCapture                        = 0x16, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_DHprime                           = 0x1a, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A  *   *
        ReportId_DHbase                            = 0x1b, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A  *   *
        ReportId_ClearScreen                       = 0x20, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_InkingMode                        = 0x21, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_InkThreshold                      = 0x22, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_ClearScreenArea                   = 0x23, ///< <b>Mode:</b>        set <b>Compatibility:</b>  -   -   -   430 530
        ReportId_StartImageDataArea                = 0x24, ///< <b>Mode:</b>        set <b>Compatibility:</b>  -   -   -   430 530
        ReportId_StartImageData                    = 0x25, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_ImageDataBlock                    = 0x26, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_EndImageData                      = 0x27, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
        ReportId_HandwritingThicknessColor         = 0x28, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A(430)530  *430=thickness only
        ReportId_BackgroundColor                   = 0x29, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A  -  530
        ReportId_HandwritingDisplayArea            = 0x2a, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A 430 530
        ReportId_BacklightBrightness               = 0x2b, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A  -  530
        ReportId_ScreenContrast                    = 0x2c, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A  -  530
        ReportId_HandwritingThicknessColor24       = 0x2d, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -   -  (430)530  *430=thickness only
        ReportId_BackgroundColor24                 = 0x2e, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -   -    -  530
        ReportId_BootScreen                        = 0x2f, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -   -   430 530
        ReportId_PenDataOption                     = 0x30, ///< <b>Mode:</b> in         <b>Compatibility:</b>  - (500)520A  ?   ?   *500=fw2.03
        ReportId_PenDataEncryptedOption            = 0x31, ///< <b>Mode:</b> in         <b>Compatibility:</b>  -   -  520A  ?   ?
        ReportId_PenDataOptionMode                 = 0x32, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  - (500)520A 430 530  *500=fw2.03
        ReportId_PenDataTimeCountSequenceEncrypted = 0x33, ///< <b>Mode:</b> in         <b>Compatibility:</b>  -   -   -   430 530
        ReportId_PenDataTimeCountSequence          = 0x34, ///< <b>Mode:</b> in         <b>Compatibility:</b>  -   -   -   430 530
        ReportId_EncryptionCommand                 = 0x40, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -   -   430 530
        ReportId_EncryptionStatus                  = 0x50, ///< <b>Mode:</b> in/get     <b>Compatibility:</b>  -   -   -   430 530
      //ReportId_60                                = 0x60, //   <b>Mode:</b> -internal- <b>Compatibility:</b>  -   -   -   430 530
        ReportId_GetReport                         = 0x80, ///< <b>Mode:</b>        set <b>Compatibility:</b> SERIAL ONLY
        ReportId_SetResult                         = 0x81  ///< <b>Mode:</b> in         <b>Compatibility:</b> SERIAL ONLY
     //,ReportId_a0                                = 0xa0, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300 500 520A   430 530
      //ReportId_a2                                = 0xa2, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300 500 520A   430 530
      //ReportId_a3                                = 0xa3, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300 500 520A   430 530
      //ReportId_a5                                = 0xa5, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
      //ReportId_a6                                = 0xa6, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
      //ReportId_a7                                = 0xa7, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
      //ReportId_a8                                = 0xa8, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
      //ReportId_a9                                = 0xa9, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
      //ReportId_aa                                = 0xaa, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
      //ReportId_ab                                = 0xab, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300 500 520A   430 530
      //ReportId_ac                                = 0xac, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
      //ReportId_ad                                = 0xad, //   <b>Mode:</b> -internal- <b>Compatibility:</b>         520A       530
      //ReportId_ae                                = 0xae, //   <b>Mode:</b> -internal- <b>Compatibility:</b>         520A       530
      //ReportId_af                                = 0xaf, //   <b>Mode:</b> -internal- <b>Compatibility:</b>         520A       530
      //ReportId_b0                                = 0xb0, //   <b>Mode:</b> -internal- <b>Compatibility:</b>                430 530
      //ReportId_b2                                = 0xb2, //   <b>Mode:</b> -internal- <b>Compatibility:</b>         520A       530
      //ReportId_b3                                = 0xb3, //   <b>Mode:</b> -internal- <b>Compatibility:</b>                430
      //ReportId_b4                                = 0xb4  //   <b>Mode:</b> -internal- <b>Compatibility:</b>                430 530
      };

      /// @brief Standard definition of a rectangle for the device
      struct Rectangle
      {
        uint16_t upperLeftXpixel;  ///< pixel coordinate of left of rectangle 
        uint16_t upperLeftYpixel;  ///< pixel coordinate of top of rectangle 
        uint16_t lowerRightXpixel; ///< pixel coordinate of right of rectangle 
        uint16_t lowerRightYpixel; ///< pixel coordinate of bottom of rectangle 

        Rectangle() noexcept // =default
        {
        }
        
        Rectangle(uint16_t upperLeftX, uint16_t upperLeftY, uint16_t lowerRightX, uint16_t lowerRightY) noexcept
        :
          upperLeftXpixel(upperLeftX),
          upperLeftYpixel(upperLeftY),
          lowerRightXpixel(lowerRightX),
          lowerRightYpixel(lowerRightY)
        {
        }
      };
      

      /// @see Status::statusCode
      enum StatusCode : uint8_t
      {
        StatusCode_Ready          = 0x00, ///< Normal state; tablet is transferring pen coordinates. Ready to receive commands.
        StatusCode_Image          = 0x01, ///< %Tablet switches to this after ReportId_StartImageData.
        StatusCode_Capture        = 0x02, ///< %Tablet switches to this after ReportId_StartCapture; tablet is transferring encrypted pen coordinates. Ready to receive commands.
        StatusCode_Calculation    = 0x03, ///< %Tablet is calculating encryption keys.        
        StatusCode_Image_Boot     = 0x04, ///< %Tablet is displaying the boot image. The tablet will automatically change to Ready when finished.
        StatusCode_SystemReset    = 0xff  ///< %Tablet is resetting. So, any function isn't available.
      };

      /// @brief Values returned in Protocol::Status::lastResultCode or a ReportId::ReportId_SetResult report.
      enum ErrorCode : uint8_t
      {
        ErrorCode_None                        = 0x00,   ///< Operation completed successfully.  
        ErrorCode_WrongReportId               = 0x01,   ///< Wrong ReportId in received command. 
        ErrorCode_WrongState                  = 0x02,   ///< Command was received when the tablet was in the wrong state, it cannot be processed.
                                                        
        ErrorCode_CRC                         = 0x03,   ///< CRC error was detected in packed received by the tablet (serial interface only). 
        ErrorCode_GraphicsWrongEncodingType   = 0x10,   ///< Wrong encoding type in report data. 
        ErrorCode_GraphicsImageTooLong        = 0x11,   ///< Trying to draw outside video memory.
        ErrorCode_GraphicsZlibError           = 0x12,   ///< An error was returned by the ZLIB inflate function.

        ErrorCode_GraphicsWrongParameters     =  0x15,  ///< Image area or handwriting display area is invalid.
        
      //ErrorCode_GraphicsSprleImageOverflow  =  0x1E, // undocumented
      //ErrorCode_GraphicsSprleBlockOverflow  =  0x1F, // undocumented

      //ErrorCode_Crypt_errors                =  0x20, // undocumented
      //ErrorCode_Crypt_dh_zero_input         =  0x21, // undocumented
      //ErrorCode_Crypt_dh_test_failed        =  0x22, // undocumented
      //ErrorCode_Crypt_dh_wrong_module       =  0x23, // undocumented
      //ErrorCode_Crypt_dh_wrong_base         =  0x24, // undocumented
      //ErrorCode_Crypt_dh_wrong_openkey      =  0x25, // undocumented
      //ErrorCode_Crypt_dh_key_invalid        =  0x26, // undocumented
      //ErrorCode_Crypt_engine_invalid        =  0x27, // undocumented

      //ErrorCode_Internal                    =  0x80 ~ 0xff
      };

      /// @see getStatus()
      struct Status
      {
        StatusCode statusCode;      ///< @see Protocol::StatusCode
        ErrorCode  lastResultCode;  ///< @see Protocol::ErrorCode
        uint16_t   statusWord;      ///< reserved
      };
     
      /// @brief Retrieves the status of the tablet.
      /// 
      /// @return Status.
      ////
      /// @see    ReportId_Status
      Status getStatus();


      /// @see setReset()
      enum ResetFlag : uint8_t
      {
        ResetFlag_Software = 0x00, ///< Perform a soft reset.
        ResetFlag_Hardware = 0x01  ///< Perform a hard reset.
      };

      /// @brief Sends a reset command.
      ///
      /// @param flag A value from Protocol::ResetFlag.
      ///
      /// @remarks Resets the tablet:
      ///   - Status is switched to StatusCode_Ready.
      ///   - Background is cleared to white.
      ///   - Inking Mode is set ON.
      ///   - Ink Threshold is set to the default value.
      ///   - Handwriting Thickness and Color are set to the default values.
      ///   - Handwriting display area is set to the default.
      ///   - Time count function is set OFF.
      ///
      /// @warning Following a Hardware reset you must re-open the USB connection.
      ///
      /// @see ReportId_Reset
      void setReset(ResetFlag flag);
      

      /// @see getInformation()
      struct Information
      {
        union
        {
          char modelName[9];                  ///< model name
          char modelNameNullTerminated[9+1];
        };
        uint8_t firmwareMajorVersion;         ///< firmware version.
        uint8_t firmwareMinorVersion;         ///< firmware version.
        uint8_t secureIc;                     ///< non-zero if the security chip is present.
        uint8_t secureIcVersion[4];           ///< firmware version of the security chip.
      };
      
      /// @brief  Retrieves information from the tablet.
      /// @return Information.
      /// @see    ReportId_Information
      Information getInformation();


      /// @see getCapability()
      struct Capability
      {         
        uint16_t tabletMaxX;        ///< The maximum value returned in PenData::x.
        uint16_t tabletMaxY;        ///< The maximum value returned in PenData::y.
        uint16_t tabletMaxPressure; ///< The maximum value returned in PenData::pressure.
        uint16_t screenWidth;       ///< The LCD width, in pixels.
        uint16_t screenHeight;      ///< The LCD height, in pixels.
        uint8_t  maxReportRate;     ///< <b>STU-520A only:</b> Number of reports per second.
        uint16_t resolution;        ///< <b>STU-520A only:</b> The number of tablet units per inch.
        union
        {
          uint8_t encodingFlag;    ///< <b>STU-520A/430/530 only:</b> Use with ProtocolHelper::simulateEncodingFlag() to ensure compatibility with older tablets.
          WacomGSS_deprecated uint8_t  zlibColorSupport; ///< <b>STU-520A only:</b> bit 0 is set if ZLIB is supported for color compression.
        };
      };

      enum EncodingFlag : uint8_t
      {
        EncodingFlag_Zlib  = 0x01, ///< set if ZLIB is supported for color compression (not bulk).
        EncodingFlag_1bit  = 0x02,      
        EncodingFlag_16bit = 0x04,
        EncodingFlag_24bit = 0x08
      };
      

      /// @brief  Retrieves the capabilities of the tablet.
      ///
      /// @return Capability.
      /// @see    ReportId_Capability
      Capability getCapability();

      /// @name Uid
      /// @{

      /// @brief  Retrieves the unique ID of the tablet.
      /// @return The tablet identifier.
      /// @see    ReportId_Uid
      uint32_t getUid();
      
      /// @brief    Sets the unique ID of the tablet.
      /// @param    uid unique identifier.
      /// @warning  The UID data is stored in non-volatile memory of the tablet. Do NOT write frequently.
      /// @see      ReportId_Uid
      void  setUid(uint32_t uid);
      /// @}

      /// @name Uid2
      /// @{

      /// @see getUid2()
      struct Uid2
      {
        union
        {
          char uid2[10];
          char uid2NullTerminated[10+1];
        };
      };
      
      /// @brief Retrieves the hardware unique ID of the tablet.
      /// @return The hardware unique ID.
      /// @see ReportId_Uid2
      /// @note Not supported on 300/500/520.
      Uid2 getUid2();
      /// @}


      /// @brief 128-bit number.
      typedef std::array<uint8_t,16> PublicKey;

      /// @name HostPublicKey
      /// @{
      
      /// @brief  Retrieves the host public key.
      /// @return 128-bit key.
      /// @note   Not supported on 430/530.
      /// @see    ReportId_HostPublicKey
      PublicKey getHostPublicKey();

      /// @brief  Sets the host public key.
      /// @param  hostPublicKey 128-bit key.
      /// @note   Not supported on 430/530.
      /// @see    ReportId_HostPublicKey
      void      setHostPublicKey(PublicKey const & hostPublicKey);
      /// @}


      /// @brief  Retrieves the tablet's public key.
      /// @return 128-bit key.
      /// @note   Not supported on 430/530.
      /// @see    ReportId_DevicePublicKey
      PublicKey getDevicePublicKey();

      /// @brief    Start encrypted data capture session.
      /// @param    sessionId 32-bit session id.
      /// @remarks  Before using this command, encryption must be initialized.
      ///           If key exchange is not finished, the tablet will return error. 
      ///
      /// @see      ReportId_StartCapture
      void setStartCapture(uint32_t sessionId);

      /// @brief  Ends encrypted data capture session.
      /// @see    ReportId_EndCapture
      void setEndCapture();
      
      /// @brief Diffie-Hellmann prime number.
      typedef std::array<uint8_t,16> DHprime;
      
      /// @name DHprime
      /// @{
      
      /// @brief  Retrieves the Diffie-Hellmann prime number.
      /// @return 128-bit number.
      /// @see    ReportId_DHprime
      DHprime getDHprime();

      /// @brief    Sets the Diffie-Hellmann prime number to use.
      /// @param    prime   128-bit number.
      /// @warning  This data is stored in non-volatile memory of the tablet. Do NOT write frequently.
      /// @note   Not supported on 430/530.
      /// @see      ReportId_DHprime
      void    setDHprime(DHprime const & prime);
      /// @}


      /// @brief Holds a Diffie-Hellmann base number.
      typedef std::array<uint8_t,2> DHbase;

      /// @name DHbase
      /// @{
      
      /// @brief  Retrieves the Diffie-Hellmann base number.
      /// @return 16-bit number.
      /// @see    ReportId_DHbase
      DHbase getDHbase();

      /// @brief    Sets the Diffie-Hellmann base number to use.
      /// @param    base 16-bit number.
      /// @warning  This data is stored in non-volatile memory of the tablet. Do NOT write frequently.
      /// @note   Not supported on 430/530.
      /// @see      ReportId_DHbase
      void   setDHbase(DHbase const & base);
      /// @}

      /// @brief Clears the LCD screen.
      ///
      /// @remark The whole screen is cleared using the selected background color.
      ///         Drawing and data transmission with the pen are interrupted during this operation.
      ///         The screen is cleared asychronously.
      /// @remark <b>STU-520 only:</b>The tablet can take 600ms to complete. During this time the tablet status is StatusCode_Image.
      /// @see    ReportId_ClearScreen
      void setClearScreen();

      /// @brief Clears an area the LCD screen.
      ///
      /// @remark The selected area is cleared using the selected background color.
      ///         Drawing and data transmission with the pen are interrupted during this operation.
      ///         The screen is cleared asychronously.
      /// @note   Not supported on 300/500/520.
      /// @see    ReportId_ClearScreenArea
      void setClearScreenArea(Rectangle const & area);


      /// @name InkingMode
      /// @{

      /// @brief inking mode values.
      enum InkingMode : uint8_t
      {
        InkingMode_Off = 0x00,
        InkingMode_On  = 0x01
      };

      /// @brief  Checks if inking is enabled.
      /// @return non-zero if inking is enabled.
      /// @see    ReportId_InkingMode
      uint8_t getInkingMode();

      /// @brief  Enables and disables inking.
      /// @param  inkingMode InkingMode.
      /// @see    ReportId_InkingMode
      void setInkingMode(InkingMode inkingMode);
      /// @}

      /// @brief  Pen pressure thresholds for drawing ink on LCD screen.
      /// @see    getInkThreshold(), setInkThreshold()
      struct InkThreshold
      {
        uint16_t onPressureMark;  ///< Pressure threshold for starting inking
        uint16_t offPressureMark; ///< Pressure threshold for stopping inking
      };

      /// @name InkThreshold
      /// @{

      /// @brief  Gets pressure thresholds used for drawing ink on LCD screen.
      /// @return InkThreshold.
      /// @see    ReportId_InkThreshold
      InkThreshold getInkThreshold();
      
      /// @brief  Sets pressure thresholds used for drawing ink on LCD screen.
      /// @param  inkThreshold Threshold values to set.
      /// @see    ReportId_InkThreshold
      void setInkThreshold(InkThreshold const & inkThreshold);
      /// @}
      

      /// @see setStartImageData()
      enum EncodingMode : uint8_t
      {
        EncodingMode_1bit       = 0x00, ///< uncompressed monochrome
        EncodingMode_1bit_Zlib  = 0x01, ///< Zlib-compressed monochrome
        EncodingMode_16bit      = 0x02, ///< uncompressed color
        EncodingMode_24bit      = 0x04, ///< uncompressed color (530 only)
        EncodingMode_1bit_Bulk  = 0x10, ///< data will be sent using Interface::write() instead of Interface::set().
        EncodingMode_16bit_Bulk = 0x12, ///< data will be sent using Interface::write() instead of Interface::set().
        EncodingMode_24bit_Bulk = 0x14 ///< data will be sent using Interface::write() instead of Interface::set() (530 only).
      };
      static const uint8_t EncodingMode_Raw       = 0x00; ///< uncompressed monochrome
      static const uint8_t EncodingMode_Zlib      = 0x01; ///< Zlib-compressed monochrome        
      static const uint8_t EncodingMode_Bulk      = 0x10; ///< backwards-compatible
      static const uint8_t EncodingMode_16bit_565 = 0x02; ///< backwards-compatible

      /// @brief  Sends a Start Image Data Transmit command to the tablet.
      /// @param  encodingMode sets how the image data will be transferred.
      /// @see    ReportId_StartImageData @ref EncodingMode      
      void setStartImageData(EncodingMode encodingMode);

      /// @deprecated Superseded by setStartImageData(EncodingMode)
      WacomGSS_deprecated void setStartImageData(uint8_t encodingMode) { setStartImageData(static_cast<EncodingMode>(encodingMode)); }
      
      
      /// @brief  Sends a Start Image Data Transmit command to the tablet.
      /// @param  encodingMode sets how the image data will be transferred.
      /// @param  area the area of the LCD screen to set.
      /// @note   Not supported on 300/500/520.
      /// @see    ReportId_StartImageDataArea @ref EncodingMode      
      void setStartImageDataArea(EncodingMode encodingMode, Rectangle const & area);
      
      /// @brief  Holds a block of image data for transmission to the tablet.
      /// @see    setImageDataBlock()
      struct ImageDataBlock
      {
        uint8_t                 length; ///< number of bytes in data.
        std::array<uint8_t,253> data;   ///< image data.
      };
      
      /// @brief  Sends an image data block to the tablet.
      /// @param  imageDataBlock the next block of image data.
      /// @see    ReportId_ImageDataBlock
      void setImageDataBlock(ImageDataBlock const & imageDataBlock);

      /// @see    setEndImageData()
      enum EndImageDataFlag : uint8_t
      {
        EndImageDataFlag_Commit = 0x00,
        EndImageDataFlag_Abandon = 0x01
      };

      /// @brief  Sends an End Image Data Transmit command to the tablet.
      /// @param  flag 0x00 - Commit image to LCD screen; 0x01 Abandon image update, return to READY state.
      /// @see    ReportId_EndImageData
      void setEndImageData(EndImageDataFlag endImageDataFlag);
      
      /// @deprecated Superseded by setEndImageData(EndImageDataFlag)
      WacomGSS_deprecated void setEndImageData() { setEndImageData(EndImageDataFlag_Commit); }
      
      /// @brief Holds thinkness and color of pen used to draw ink on the tablet's LCD screen.
      struct HandwritingThicknessColor
      {
        uint16_t penColor;      ///< Ink color in 16-bit (5-6-5 RGB) format.
        uint8_t  penThickness;  ///< Ink thickness.
      };

      /// @name HandwritingThicknessColor
      /// @{

      /// @brief  Gets thinkness and color of pen used to draw ink on the tablet's LCD screen.
      /// @return HandwritingThicknessColor.
      /// @see    ReportId_HandwritingThicknessColor
      HandwritingThicknessColor getHandwritingThicknessColor();

      /// @brief  Sets thinkness and color of pen used to draw ink on the tablet's LCD screen.
      /// @param  handwritingThicknessColor The new thickness and color of the ink.
      /// @see    ReportId_HandwritingThicknessColor
      void setHandwritingThicknessColor(HandwritingThicknessColor const & handwritingThicknessColor);
      /// @}


      /// @brief Holds thinkness and color of pen used to draw ink on the tablet's LCD screen.
      struct HandwritingThicknessColor24
      {
        uint32_t penColor;      ///< Ink color in 24-bit 0x00rrggbb format.
        uint8_t  penThickness;  ///< Ink thickness.
      };

      /// @name HandwritingThicknessColor
      /// @{

      /// @brief  Gets thinkness and color of pen used to draw ink on the tablet's LCD screen.
      /// @return HandwritingThicknessColor.
      /// @note   Not supported on 300/500/520.
      /// @see    ReportId_HandwritingThicknessColor
      HandwritingThicknessColor24 getHandwritingThicknessColor24();

      /// @brief  Sets thinkness and color of pen used to draw ink on the tablet's LCD screen.
      /// @param  handwritingThicknessColor The new thickness and color of the ink.
      /// @note   Not supported on 300/500/520.
      /// @see    ReportId_HandwritingThicknessColor
      void setHandwritingThicknessColor24(HandwritingThicknessColor24 const & handwritingThicknessColor);
      /// @}


      /// @name BackgroundColor
      /// @{

      /// @brief  Gets the tablet background color.
      /// @return Color in 16-bit (5-6-5 RGB) format.
      /// @see    ReportId_BackgroundColor
      uint16_t getBackgroundColor();

      /// @brief    Sets the tablet background color.
      /// @param    backgroundColor Color in 16-bit (5-6-5 RGB) format.
      /// @remark   This method does not clear the screen, you must call clearScreen() separately.
      /// @warning  The background color is stored in non-volatile memory of the tablet. Do NOT write frequently.
      /// @see      ReportId_BackgroundColor
      void     setBackgroundColor(uint16_t backgroundColor);
      /// @}


      /// @name BackgroundColor24
      /// @{

      /// @brief  Gets the tablet background color.
      /// @return Color in 24-bit 0x00rrggbb format.
      /// @note   Not supported on 300/500/520.
      /// @see    ReportId_BackgroundColor
      uint32_t getBackgroundColor24();

      /// @brief    Sets the tablet background color.
      /// @param    backgroundColor Color in 24-bit 0x00rrggbb format.
      /// @remark   This method does not clear the screen, you must call clearScreen() separately.
      /// @warning  The background color is stored in non-volatile memory of the tablet. Do NOT write frequently.
      /// @note     Not supported on 300/500/520.
      /// @see      ReportId_BackgroundColor
      void     setBackgroundColor24(uint32_t backgroundColor);
      /// @}
      

      /// @brief Defines rectangular area on the tablet's LCD sceen within which ink is captured.
      /// @deprecated Superseded by use of generic Rectangle struct 
      struct HandwritingDisplayArea : Rectangle 
      { 
        HandwritingDisplayArea() noexcept {}
        HandwritingDisplayArea(Rectangle const & r) noexcept : Rectangle(r) {}
        HandwritingDisplayArea & operator = (Rectangle const & r) noexcept { static_cast<Rectangle &>(*this) = r; return *this; }
      };

      /// @name HandwritingDisplayArea
      /// @{

      /// @brief  Gets the area on the tablet's LCD sceen within which ink capture is enabled.
      /// @return HandwritingDisplayArea holding coordinates of ink capture rectangle.
      /// @see    ReportId_HandwritingDisplayArea
      Rectangle getHandwritingDisplayArea();

      /// @brief  Sets the area on the tablet's LCD sceen within which ink capture is enabled.
      /// @param  handwritingDisplayArea HandwritingDisplayArea holding coordinates of ink capture rectangle to set.
      /// @see    ReportId_HandwritingDisplayArea
      void                   setHandwritingDisplayArea(Rectangle const & area);

      /// @deprecated Superseded by setHandwritingDisplayArea(Rectangle const &)
      WacomGSS_deprecated
      void setHandwritingDisplayArea(HandwritingDisplayArea const & handwritingDisplayArea)
      {
        setHandwritingDisplayArea(static_cast<Rectangle const &>(handwritingDisplayArea));
      }

      /// @}

      /// @brief  Gets the tablet back-light brightness setting.
      /// @return Current brightness setting.
      /// @see    ReportId_BacklightBrightness
      uint16_t getBacklightBrightness();

      /// @brief    Sets the tablet back-light brightness.
      /// @param    backlightBrightness Brightness setting from 0 (dimmest) to 3 (brightest).
      /// @warning  The backlight brightness is stored in non-volatile memory of the tablet. Do NOT write frequently.
      /// @see      ReportId_BacklightBrightness
      void     setBacklightBrightness(uint16_t backlightBrightness);
  

      /// @brief  Gets the tablet contrast setting.
      /// @return Current contrast setting.
      /// @see    ReportId_ScreenContrast
      uint16_t getScreenContrast();

      /// @brief    Sets the tablet screen contrast.
      /// @param    screenContrast contrast setting.
      /// @warning  The screen contrast is stored in non-volatile memory of the tablet. Do NOT write frequently.
      /// @see      ReportId_ScreenContrast
      void     setScreenContrast(uint16_t screenContrast);


      /// @name PenDataOptionMode
      /// @{

      /// @see setPenDataOptionMode()
      enum PenDataOptionMode : uint8_t
      {
        PenDataOptionMode_None              = 0x00, ///< Report PenData/PenDataEncrypted.
        PenDataOptionMode_TimeCount         = 0x01, ///< Report PenDataOption/PenDataEncryptedOption with timeCount field set. (520 only)
        PenDataOptionMode_SequenceNumber    = 0x02, ///< Report PenDataOption/PenDataEncryptedOption with sequenceNumber field set.
        PenDataOptionMode_TimeCountSequence = 0x03  ///< Report PenDataTimeCountSequence/PenDataTimeCountSequenceEncrypted with sequenceNumber field set. (430/530 only)
      };

      /// @brief  Retrieves the set option mode.
      /// @return OptionMode.
      /// @see    ReportId_PenDataOptionMode
      uint8_t getPenDataOptionMode();

      /// @brief  Enables or disables an option mode.
      /// @param  optionMode The new setting.
      /// @note   Not all tablets and firmwares support all option modes.
      /// @see    ReportId_PenDataOptionMode
      void    setPenDataOptionMode(PenDataOptionMode penDataOptionMode);
      /// @}


      /// @see struct EncryptionStatus
      enum StatusCodeRSA : uint8_t
      {
        StatusCodeRSA_Ready       = 0x00,
        StatusCodeRSA_Calculating = 0xFA,
        StatusCodeRSA_Even        = 0xFB,
        StatusCodeRSA_Long        = 0xFC,
        StatusCodeRSA_Short       = 0xFD,
        StatusCodeRSA_Invalid     = 0xFE,
        StatusCodeRSA_NotReady    = 0xFF
      };

      /// @see struct EncryptionStatus
      enum ErrorCodeRSA : uint8_t
      {
        ErrorCodeRSA_None                   = 0x00,
        ErrorCodeRSA_BadParameter           = 0x01,
        ErrorCodeRSA_ParameterTooLong       = 0x02,
        ErrorCodeRSA_PublicKeyNotReady      = 0x03,
        ErrorCodeRSA_PublicExponentNotReady = 0x04,
        ErrorCodeRSA_SpecifiedKeyInUse      = 0x05,
        ErrorCodeRSA_SpecifiedKeyNotInUse   = 0x06,
        ErrorCodeRSA_BadCommandCode         = 0x07,
        ErrorCodeRSA_CommandPending         = 0x08,
        ErrorCodeRSA_SpecifiedKeyExists     = 0x09,
        ErrorCodeRSA_SpecifiedKeyNotExist   = 0x0A,
        ErrorCodeRSA_NotInitialized         = 0x0B
      };

      /// @see struct EncryptionStatus
      enum SymmetricKeyType : uint8_t
      {
        SymmetricKeyType_AES128 = 0,
        SymmetricKeyType_AES192 = 1,
        SymmetricKeyType_AES256 = 2
      };

      /// @see struct EncryptionStatus
      enum AsymmetricKeyType : uint8_t
      {
        AsymmetricKeyType_RSA1024 = 0,
        AsymmetricKeyType_RSA1536 = 1,
        AsymmetricKeyType_RSA2048 = 2
      };

      /// @see struct EncryptionStatus
      enum AsymmetricPaddingType : uint8_t
      {
        AsymmetricPaddingType_None  = 0,
        AsymmetricPaddingType_PKCS1 = 1,
        AsymmetricPaddingType_OAEP  = 2
      };

      /// @brief Provides the current state of the v2 encryption engine
      struct EncryptionStatus
      {
        uint8_t symmetricKeyType;
        uint8_t asymmetricPaddingType;
        uint8_t asymmetricKeyType;
        uint8_t statusCodeRSAe; ///< status of public exponent
        uint8_t statusCodeRSAn; ///< status of host public key
        uint8_t statusCodeRSAc; ///< status of symmetric key
        uint8_t lastResultCode; ///< ErrorCodeRSA
        bool    rng;
        bool    sha1;
        bool    aes;

        static const ReportId reportId   = ReportId_EncryptionStatus;
        static const size_t   reportSize = 16;
      };

      /// @brief returns the encryption status for v2 style encryption
      /// @note not supported on 300/500/520
      EncryptionStatus getEncryptionStatus();


      enum EncryptionCommandNumber : uint8_t
      {
        EncryptionCommandNumber_SetEncryptionType    = 0x01,
        EncryptionCommandNumber_SetParameterBlock    = 0x02,
        EncryptionCommandNumber_GetStatusBlock       = 0x03,
        EncryptionCommandNumber_GetParameterBlock    = 0x04,
        EncryptionCommandNumber_GenerateSymmetricKey = 0x05,
      //EncryptionCommandNumber_Reserved_06          = 0x06,
      //EncryptionCommandNumber_Reserved_07          = 0x07,
      //EncryptionCommandNumber_Reserved_08          = 0x08,
      //EncryptionCommandNumber_Reserved_09          = 0x09       
      };

      enum EncryptionCommandParameterBlockIndex : uint8_t
      {
        EncryptionCommandParameterBlockIndex_RSAe = 0,
        EncryptionCommandParameterBlockIndex_RSAn = 1,
        EncryptionCommandParameterBlockIndex_RSAc = 2,
        EncryptionCommandParameterBlockIndex_RSAm = 3
      };

      /// @see getEncryptionCommand()
      struct EncryptionCommand
      {
        EncryptionCommandNumber encryptionCommandNumber;
        uint8_t                 parameter;
        uint8_t                 lengthOrIndex;
        std::array<uint8_t,64>  data;

        /// @brief Helper function to initialize an EncryptionCommand parameter block with the SetEncryptionType command
        /// @param symmetricKeyType       symmetric key type
        /// @param asymmetricPaddingType  asymmetric padding type
        /// @param asymmetricKeyType      asymmetric key type
        /// @return Initialized EncryptionCommand object
        static EncryptionCommand initializeSetEncryptionType(SymmetricKeyType symmetricKeyType, AsymmetricPaddingType asymmetricPaddingType, AsymmetricKeyType asymmetricKeyType);
        
        /// @brief Helper function to initialize an EncryptionCommand parameter block with the SetParameterBlock command
        /// @param index  parameter block index
        /// @param length length of data block
        /// @param data   data block
        /// @return Initialized EncryptionCommand object
        static EncryptionCommand initializeSetParameterBlock(EncryptionCommandParameterBlockIndex index, uint8_t length, uint8_t const * data);

        /// @brief Helper function to initialize an EncryptionCommand parameter block with the GenerateSymmetricKey command
        /// @return Initialized EncryptionCommand object
        static EncryptionCommand initializeGenerateSymmetricKey();        
        
        /// @brief Helper function to initialize an EncryptionCommand parameter block with the GetParameterBlock command
        /// @param index  parameter block index
        /// @param offset 
        /// @return Initialized EncryptionCommand object
        static EncryptionCommand initializeGetParameterBlock(EncryptionCommandParameterBlockIndex index, uint8_t offset);
      };

      /// @brief  Retrieves the specified data from the v2 encryption engine
      /// @param  encryptionCommandNumber Encryption command to retrieve data for
      /// @return EncryptionCommand struct containing the requested data
      /// @note   not supported on 300/500/520
      EncryptionCommand getEncryptionCommand(EncryptionCommandNumber encryptionCommandNumber);


      /// @brief Sends the specified data to the v2 encryption engine
      /// @param  encryptionCommandNumber Encryption command to set
      /// @note not supported on 300/500/520      
      void setEncryptionCommand(EncryptionCommand const & encryptionCommand);
          
      
      
      /// @brief  Data returned from the %STU tablet in response to pen movement when PenDataOptionMode_None is set.
      /// @remark This data will be sent from the %STU tablet automatically if pen is put in effective area.
      ///         (Pen pressure is not required for the %STU tablet to detect pen movement).
      struct PenData
      {
        uint8_t  rdy;      ///< non-zero if the stylus is in proximity.
        uint8_t  sw;       ///< non-zero if the stylus is considered to be touching the surface.
        uint16_t pressure; ///< pressure value in tablet units.
        uint16_t x;        ///< x-coordinate in tablet units.
        uint16_t y;        ///< y-coordinate in tablet units.

        static const ReportId reportId   = ReportId_PenData;
        static const size_t   reportSize = 6;
      };


      /// @brief Data returned from the %STU tablet in response to pen movement when PenDataOptionMode_TimeCount 
      ///        or PenDataOptionMode_SequenceNumber is set.
      /// @remark This data will be sent from the %STU tablet automatically if pen is put in effective area.
      /// (Pen pressure is not required for the %STU tablet to detect pen movement).
      /// @note The value will wrap at 0xffff.
      struct PenDataOption : PenData
      {
        uint16_t option; ///< data dependant upon PenDataOptionMode setting

        static const ReportId reportId   = ReportId_PenDataOption;
        static const size_t   reportSize = 8;
      };

      /// @brief Data returned from the %STU tablet in response to pen movement when PenDataOptionMode_TimeCountSequence is set.
      struct PenDataTimeCountSequence : PenData
      {
        uint16_t timeCount;     ///< time count (incremented every 1ms)
        uint16_t sequence;      ///< pen data report sequence number

        static const ReportId reportId   = ReportId_PenDataTimeCountSequence;
        static const size_t   reportSize = 10;
      };

      /// @brief Data returned from the %STU tablet in encrypted mode in response to pen movement when PenDataOptionMode_None is set.
      struct PenDataEncrypted
      {
        uint32_t sessionId;  ///< as specified in setStartCapture()
        PenData  penData[2];

        static const ReportId reportId   = ReportId_PenDataEncrypted;
        static const size_t   reportSize = 16;
        static const size_t   encryptedSize = 16;
      };


      /// @brief Data returned from the %STU tablet in encrypted mode in response to pen movement when PenDataOptionMode_TimeCount 
      ///        or PenDataOptionMode_SequenceNumber is set.
      struct PenDataEncryptedOption : PenDataEncrypted
      {
        uint16_t option[2];         ///< data dependant upon PenDataOptionMode setting

        static const ReportId reportId   = ReportId_PenDataEncryptedOption;
        static const size_t   reportSize = 20;
      };


      /// @brief Data returned from the %STU tablet in encrypted mode in response to pen movement when PenDataOptionMode_TimeCountSequence is set.
      struct PenDataTimeCountSequenceEncrypted : PenDataTimeCountSequence
      {
        uint32_t sessionId;
        
        static const ReportId reportId   = ReportId_PenDataTimeCountSequenceEncrypted;
        static const size_t   reportSize = 16;
        static const size_t   encryptedSize = 16;
      };
      
      /// @brief Encryption pulic key retrieved from the %STU tablet.
      struct DevicePublicKey : PublicKey
      {
        static const ReportId reportId   = ReportId_DevicePublicKey;
        static const size_t   reportSize = 16 /* = size()*/;
      };


      /// @brief Unsafe (no checking) decoding of PenData.
      template<typename Iterator> 
      static Iterator decodeReport_unsafe(Iterator i, PenData & data)
      {
        static_assert(std::is_same<typename std::decay<typename std::iterator_traits<Iterator>::value_type>::type,std::uint8_t>::value, "Iterator must decay to type std::uint8_t");

        uint8_t cmd0,cmd1,cmd2,cmd3,cmd4,cmd5;

        cmd0 = *i; ++i;
        data.rdy      = static_cast<uint8_t >((cmd0 >> 7) & 0x01);
        data.sw       = static_cast<uint8_t >((cmd0 >> 4) & 0x07);
        cmd1 = *i; ++i;
        data.pressure = static_cast<uint16_t>(((cmd0 & 0x0f) << 8) | cmd1);
        cmd2 = *i; ++i;
        cmd3 = *i; ++i;
        data.x        = static_cast<uint16_t>(((cmd2       ) << 8) | cmd3);
        cmd4 = *i; ++i;
        cmd5 = *i; ++i;
        data.y        = static_cast<uint16_t>(((cmd4       ) << 8) | cmd5);

        return i;
      }
      

      /// @brief  Unsafe (no checking) decoding of PenDataOption.
      template<typename Iterator> 
      static Iterator decodeReport_unsafe(Iterator iter, PenDataOption & data)
      {
        static_assert(std::is_same<typename std::decay<typename std::iterator_traits<Iterator>::value_type>::type,std::uint8_t>::value, "Iterator must decay to type std::uint8_t");

        uint8_t cmd6,cmd7;

        auto i = std::move(decodeReport_unsafe(std::move(iter), static_cast<PenData &>(data)));
        cmd6 = *i; ++i;
        cmd7 = *i; ++i;
        data.option = static_cast<uint16_t>((cmd6 << 8) | cmd7);

        return i;
      }
      

      /// @brief  Unsafe (no checking) decoding of PenDataTimeCountSequence.
      template<typename Iterator> 
      static Iterator decodeReport_unsafe(Iterator iter, PenDataTimeCountSequence & data)
      {
        static_assert(std::is_same<typename std::decay<typename std::iterator_traits<Iterator>::value_type>::type,std::uint8_t>::value, "Iterator must decay to type std::uint8_t");

        uint8_t cmd6,cmd7,cmd8,cmd9;

        auto i = std::move(decodeReport_unsafe(std::move(iter), static_cast<PenData &>(data)));
        cmd6 = *i; ++i;
        cmd7 = *i; ++i;
        data.timeCount = static_cast<uint16_t>((cmd6 << 8) | cmd7);
        cmd8 = *i; ++i;
        cmd9 = *i; ++i;
        data.sequence = static_cast<uint16_t>((cmd8 << 8) | cmd9);

        return i;
      }
      

      /// @brief  Unsafe (no checking) decoding of PenDataEncrypted.
      template<typename Iterator> 
      static Iterator decodeReport_unsafe(Iterator i, PenDataEncrypted & data)
      {
        static_assert(std::is_same<typename std::decay<typename std::iterator_traits<Iterator>::value_type>::type,std::uint8_t>::value, "Iterator must decay to type std::uint8_t");

        uint8_t cmd0,cmd1,cmd2,cmd3;
        cmd0 = *i; ++i;
        cmd1 = *i; ++i;
        cmd2 = *i; ++i;
        cmd3 = *i; ++i;
        data.sessionId = (static_cast<uint32_t>(cmd0) << 24) | (cmd1 << 16) | (cmd2 << 8) | (cmd3);

        auto i1 = std::move(decodeReport_unsafe(std::move(i), data.penData[0]));
        auto i2 = std::move(decodeReport_unsafe(std::move(i1), data.penData[1]));
        
        return i2;
      }
      

      /// @brief  Unsafe (no checking) decoding of PenDataEncryptedOption.
      template<typename Iterator> 
      static Iterator decodeReport_unsafe(Iterator i, PenDataEncryptedOption & data)
      {
        static_assert(std::is_same<typename std::decay<typename std::iterator_traits<Iterator>::value_type>::type,std::uint8_t>::value, "Iterator must decay to type std::uint8_t");

        uint8_t cmd0,cmd1,cmd2,cmd3, cmd16,cmd17,cmd18,cmd19;
        cmd0 = *i; ++i;
        cmd1 = *i; ++i;
        cmd2 = *i; ++i;
        cmd3 = *i; ++i;
        data.sessionId = (static_cast<uint32_t>(cmd0) << 24) | (cmd1 << 16) | (cmd2 << 8) | (cmd3);

        auto i1 = std::move(decodeReport_unsafe(std::move(i ), data.penData[0]));
        auto i2 = std::move(decodeReport_unsafe(std::move(i1), data.penData[1]));
        
        cmd16 = *i2; ++i2;
        cmd17 = *i2; ++i2;
        data.option[0] = static_cast<uint16_t>((cmd16 << 8) | cmd17);  

        cmd18 = *i2; ++i2;
        cmd19 = *i2; ++i2;
        data.option[1] = static_cast<uint16_t>((cmd18 << 8) | cmd19);  

        return i2;
      }
      

      /// @brief Unsafe (no checking) decoding of PenDataTimeCountSequenceEncrypted
      template<typename Iterator> 
      static Iterator decodeReport_unsafe(Iterator i, PenDataTimeCountSequenceEncrypted & data)
      {
        static_assert(std::is_same<typename std::decay<typename std::iterator_traits<Iterator>::value_type>::type,std::uint8_t>::value, "Iterator must decay to type std::uint8_t");

        uint8_t cmd0,cmd1,cmd2,cmd3,cmd4,cmd5,cmd6,cmd7,cmd8,cmd9/*,cmd10,cmd11*/,cmd12,cmd13,cmd14,cmd15;

        cmd0 = *i; ++i;
        data.rdy      = static_cast<uint8_t >((cmd0 >> 7) & 0x01);
        data.sw       = static_cast<uint8_t >((cmd0 >> 4) & 0x07);
        cmd1 = *i; ++i;
        data.pressure = static_cast<uint16_t>(((cmd0 & 0x0f) << 8) | cmd1);
        cmd2 = *i; ++i;
        cmd3 = *i; ++i;
        data.x        = static_cast<uint16_t>(((cmd2       ) << 8) | cmd3);
        cmd4 = *i; ++i;
        cmd5 = *i; ++i;
        data.y        = static_cast<uint16_t>(((cmd4       ) << 8) | cmd5);
        cmd6 = *i; ++i;
        cmd7 = *i; ++i;
        data.timeCount = static_cast<uint16_t>(((cmd6       ) << 8) | cmd7);        
        cmd8 = *i; ++i;
        cmd9 = *i; ++i;
        data.sequence = static_cast<uint16_t>(((cmd8       ) << 8) | cmd9);        
        /*cmd10 = *i;*/ ++i;
        /*cmd11 = *i;*/ ++i;
        // padding
        cmd12 = *i; ++i;
        cmd13 = *i; ++i;
        cmd14 = *i; ++i;
        cmd15 = *i; ++i;
        data.sessionId = (static_cast<uint32_t>(cmd12) << 24) | (static_cast<uint32_t>(cmd13) << 16) | (static_cast<uint32_t>(cmd14) << 8) | (static_cast<uint32_t>(cmd15));

        return i;
      }



      /// @brief  Unsafe (no checking) decoding of DevicePublicKey.
      template<typename Iterator> 
      static Iterator decodeReport_unsafe(Iterator i, DevicePublicKey & data)
      {
        for (auto d = data.begin(); d != data.end(); ++d)
        {
          *d = *i; ++i;
        }
        return i;
      }
      


      /// @brief Unsafe (no checking) decoding of PenData.
      template<typename Iterator> 
      static Iterator decodeReport_unsafe(Iterator i, EncryptionStatus & data)
      {
        static_assert(std::is_same<typename std::decay<typename std::iterator_traits<Iterator>::value_type>::type,std::uint8_t>::value, "Iterator must decay to type std::uint8_t");

        uint8_t cmd0,cmd1,/*cmd2,*/cmd3,cmd4,cmd5,cmd6,cmd7/*,cmd8,cmd9,cmd10,cmd11,cmd12,cmd13,cmd14,cmd15*/;

        cmd0 = *i; ++i;
        data.symmetricKeyType = static_cast<SymmetricKeyType>(cmd0);
        cmd1 = *i; ++i;
        data.asymmetricPaddingType = static_cast<AsymmetricPaddingType>((cmd1 >> 6) & 0x03);
        data.asymmetricKeyType     = static_cast<AsymmetricKeyType>(cmd1 & 0x3F);
        /*cmd2 = *i;*/ ++i; // reserved
        cmd3 = *i; ++i;
        data.statusCodeRSAe = static_cast<StatusCodeRSA>(cmd3);
        cmd4 = *i; ++i;
        data.statusCodeRSAn = static_cast<StatusCodeRSA>(cmd4);
        cmd5 = *i; ++i;
        data.statusCodeRSAc = static_cast<StatusCodeRSA>(cmd5);
        cmd6 = *i; ++i;
        data.lastResultCode = static_cast<ErrorCodeRSA>(cmd6);
        cmd7 = *i; ++i;
        data.rng  = (cmd7 & 0x4) != 0;
        data.sha1 = (cmd7 & 0x2) != 0;
        data.aes  = (cmd7 & 0x1) != 0;
        /*cmd8  = *i;*/ ++i; //reserved
        /*cmd9  = *i;*/ ++i; //reserved
        /*cmd10 = *i;*/ ++i; //reserved
        /*cmd11 = *i;*/ ++i; //reserved
        /*cmd12 = *i;*/ ++i; //reserved
        /*cmd13 = *i;*/ ++i; //reserved
        /*cmd14 = *i;*/ ++i; //reserved
        /*cmd15 = *i;*/ ++i; //reserved
        return i;
      }

      /// @brief  Generic decodeReport for iterators.
      template<typename InputIterator, class InputReport> 
      static InputIterator decodeReport(InputIterator begin, InputIterator end, InputReport & data)
      {
        return decodeReport(begin, end, data, typename std::iterator_traits<InputIterator>::iterator_category());
      }


      /// @brief  Generic decodeReport for const containers.
      template<class Container, class InputReport> 
      static typename Container::const_iterator decodeReport(Container const & cmd, InputReport & data)
      {
        return decodeReport(cmd.cbegin(), cmd.cend(), data, typename std::iterator_traits<typename Container::const_iterator>::iterator_category());
      }


      /// @brief  Generic decodeReport for non-const containers.
      template<class Container, class InputReport> 
      static typename Container::iterator decodeReport(Container & cmd, InputReport & data)
      {
        return decodeReport(cmd.begin(), cmd.end(), data, typename std::iterator_traits<typename Container::const_iterator>::iterator_category());
      }


      /// @brief  Specialization of decodeReport for const std::array.
      template<class InputReport, std::size_t Size> 
      static void decodeReport(std::array<std::uint8_t, Size> const & cmd, InputReport & data)
      {
        static_assert(Size >= InputReport::reportSize, "array size too small, must be at least InputReport::reportSize");
        decodeReport_unsafe(cmd.cbegin(), data);
      }


      /// @brief  Specialization of decodeReport for non-const std::array.
      template<class InputReport, std::size_t Size> 
      static void decodeReport(std::array<std::uint8_t, Size> & cmd, InputReport & data)
      {
        decodeReport(const_cast<std::array<std::uint8_t, Size> const &>(cmd), data);
      }



    };



  } // namespace STU

} // namespace WacomGSS

#endif // WacomGSS_STU_Protocol_hpp
