#include <WacomGSS/STU/Linux/UsbInterface.hpp>

namespace WacomGSS
{
  namespace STU
  {
    

    void UsbInterface::readerThread() noexcept
    {
      try
      {
        try
        {
          std::vector<uint8_t> buffer(256);
        
          while (!m_quitFlag.load())
          {
            const unsigned char endpoint = 2;

            int transferred = libusbHelper::interrupt_in_sync(m_deviceHandle, endpoint, buffer.data(), buffer.size(), 500);

            if (transferred)
            {
              queueReport(std::vector<uint8_t>(buffer.begin(), buffer.begin()+transferred));
            }
          }
        }
        catch (std::system_error const & ex)
        {
          if (ex.code() == std::errc::no_such_device)
            throw device_removed_error();
          throw;
        }
      }
      catch (...)
      {
        queueException(std::current_exception());
      }
    }



    void UsbInterface::stopReaderThread()
    {
      m_quitFlag.store(true);
      if (m_readerThread.joinable())
        m_readerThread.join();
      queueClear();
    }



    void UsbInterface::startReaderThread()
    {
      stopReaderThread();

      m_quitFlag.store(false);
      m_readerThread = std::move(thread(std::bind(std::mem_fn(&UsbInterface::readerThread), this)));
    }




    UsbInterface::UsbInterface()
    :
      m_supportsWrite(false),
      m_idProduct(0),
      m_quitFlag(false)
    {
      
    }





    UsbInterface::~UsbInterface()
    {
      disconnect();
    }



    std::error_code UsbInterface::connect(UsbDevice const & usbDevice, bool /*exclusiveLock*/)
    {
      disconnect();

      lock_guard<mutex> lock(m_apiMutex);

      std::error_code ec;

      try
      {
        libusb::device dev = libusbHelper::get_device(m_context, usbDevice.idVendor, usbDevice.idProduct, usbDevice.bcdDevice, usbDevice.busNumber, usbDevice.deviceAddress);
        

        libusb::config_descriptor config( dev.get_config_descriptor(0) );
        m_supportsWrite = config->bNumInterfaces > 1;

        m_deviceHandle = std::move(libusbHelper::open(dev));

        if (usbDevice.idVendor == VendorId_Wacom)
        {
          m_idProduct = usbDevice.idProduct;
        }

        startReaderThread();
      }
      catch (std::system_error const & ex)
      {
        ec = ex.code();
      }

      return ec;
    }



    void UsbInterface::disconnect()
    {
      lock_guard<mutex> lock(m_apiMutex);
      try
      {
        stopReaderThread();
        libusbHelper::close(std::move(m_deviceHandle));
        m_idProduct = 0;
      }
      catch (std::system_error const & ex)
      {
        if (&ex.code().category() != &libusb::libusb_category()) // ignore libusb_error
          throw;
      }
    }



    bool UsbInterface::isConnected() const
    {
      return m_deviceHandle;
    }



    void UsbInterface::get(uint8_t * /*out*/ data, size_t length)
    {
      lock_guard<mutex> lock(m_apiMutex);
      if (m_deviceHandle) 
      {
        if (length > 1)
        {
          try
          {
            libusbHelper::hid_get_report(m_deviceHandle, data, length);
          }
          catch (std::system_error const & ex)
          {
            if (ex.code() == std::errc::no_such_device)
              throw device_removed_error();
            throw;
          }
        }
      }
      else 
      {
        throw not_connected_error();
      }
    }



    void UsbInterface::set(uint8_t const * data, size_t length)
    {
      lock_guard<mutex> lock(m_apiMutex);
      if (m_deviceHandle) 
      {
        if (length > 1)
        {
          try
          {
            libusbHelper::hid_set_report(m_deviceHandle, data, length);
          }
          catch (std::system_error const & ex)
          {
            if (ex.code() == std::errc::no_such_device)
              throw device_removed_error();
            throw;
          }
        }
      }
      else 
      {
        throw not_connected_error();
      }
    }



    bool UsbInterface::supportsWrite() const 
    {
      return m_supportsWrite;
    }



    void UsbInterface::write(uint8_t const * data, size_t length)
    {
      if (data && length) 
      {
        lock_guard<mutex> lock(m_apiMutex);

        if (m_deviceHandle) 
        { 
          if (m_supportsWrite)
          {
            int transferred = libusbHelper::bulk_out_sync(m_deviceHandle, 1, data, length, 1000);
            if (transferred != static_cast<int>(length))
              throw_libusb_error(libusb::libusb_errc::other);

            //while (length)
            //{
            //  int transferred = libusbHelper::bulk_out_sync(m_handle, 1, data, length, 1000);
            //  if (transferred == 0)
            //    break;
            //  length -= transferred;
            //}
          }
          else
          {
            throw write_not_supported_error();
          }
        }
        else 
        {
          throw not_connected_error();
        }
      }
      //else 
      //{
      //  // no data to write, do nothing
      //}
    }


    std::uint16_t UsbInterface::getProductId() const
    {
      lock_guard<mutex> lock(m_apiMutex);

      if (m_deviceHandle)
      {
        return m_idProduct;
      }
      else
      {
        throw not_connected_error();
      }
    }

  } // namespace STU
  
} // namespace WacomGSS

