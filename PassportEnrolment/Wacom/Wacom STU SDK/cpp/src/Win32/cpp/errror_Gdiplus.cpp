#include <WacomGSS/Win32/Gdiplus.hpp>
#include <sstream>

namespace WacomGSS
{
  namespace Win32
  {

    class Gdiplus_error_category_impl : public std::error_category
    {
    public:
      char const * name() const override
      {
        return "Gdiplus";
      }

      std::string message(int ev) const override
      {
        std::stringstream s;
        s << static_cast<Gdiplus::Status>(ev);
        return s.str();
      }
    };

    std::error_category const & Gdiplus_error_category() noexcept
    {
      static Gdiplus_error_category_impl ec;
      return ec;
    }


  }
}
