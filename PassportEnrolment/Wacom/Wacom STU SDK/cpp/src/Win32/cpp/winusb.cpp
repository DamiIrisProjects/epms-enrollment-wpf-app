#include <WacomGSS/Win32/winusb.hpp>

namespace WacomGSS
{
  namespace Win32
  {

    WinusbDll::WinusbDll()
    :
      m_hModule(),
      m_Initialize(nullptr),
      m_Free(nullptr),
      m_WritePipe(nullptr),
      m_GetOverlappedResult(nullptr),
      m_ResetPipe(nullptr)
    {
    }

    bool WinusbDll::loadLibrary(wchar_t const * fileName, nothrow_t) noexcept
    {
      if (!isLoaded()) 
      {
        Win32::HModule hModule(::LoadLibrary(fileName), nothrow);
        if (hModule) 
        {
#pragma warning(disable:4191)
          m_Initialize          = reinterpret_cast<BOOL (WINAPI *)(HANDLE, PWINUSB_INTERFACE_HANDLE)                                   >(::GetProcAddress(hModule.get(), "WinUsb_Initialize"         ));
          m_Free                = reinterpret_cast<BOOL (WINAPI *)(WINUSB_INTERFACE_HANDLE)                                            >(::GetProcAddress(hModule.get(), "WinUsb_Free"               ));
          m_WritePipe           = reinterpret_cast<BOOL (WINAPI *)(WINUSB_INTERFACE_HANDLE, UCHAR, PUCHAR, ULONG, PULONG, LPOVERLAPPED)>(::GetProcAddress(hModule.get(), "WinUsb_WritePipe"          ));
          m_GetOverlappedResult = reinterpret_cast<BOOL (WINAPI *)(WINUSB_INTERFACE_HANDLE, LPOVERLAPPED, LPDWORD, BOOL)               >(::GetProcAddress(hModule.get(), "WinUsb_GetOverlappedResult"));
          m_ResetPipe           = reinterpret_cast<BOOL (WINAPI *)(WINUSB_INTERFACE_HANDLE, UCHAR)                                     >(::GetProcAddress(hModule.get(), "WinUsb_ResetPipe"          ));
#pragma warning(default:4191)

          if (m_Initialize && m_Free && m_WritePipe && m_GetOverlappedResult && m_ResetPipe) 
          {
            m_hModule = std::move(hModule);
          }
          else 
          {
            m_Initialize          = nullptr;
            m_Free                = nullptr;
            m_WritePipe           = nullptr;
            m_GetOverlappedResult = nullptr;
            m_ResetPipe           = nullptr;
          }
        }
      }
      return m_hModule;
    }

    /*void WinusbDll::freeLibrary() noexcept
    {
      if (m_hModule) 
      {
        m_hModule.reset();
        m_Initialize          = 0;
        m_Free                = 0;
        m_WritePipe           = 0;
        m_GetOverlappedResult = 0;
        m_ResetPipe           = 0;
      }
    }*/

    bool WinusbDll::isLoaded() const noexcept
    {
      return m_hModule;
    }

    BOOL WinusbDll::Initialize(HANDLE DeviceHandle, PWINUSB_INTERFACE_HANDLE InterfaceHandle) const noexcept
    {
      return m_Initialize(DeviceHandle, InterfaceHandle);
    }

    BOOL WinusbDll::Free(WINUSB_INTERFACE_HANDLE InterfaceHandle) const noexcept
    {
      return m_Free(InterfaceHandle);
    }

    BOOL WinusbDll::WritePipe(WINUSB_INTERFACE_HANDLE InterfaceHandle, UCHAR PipeID, PUCHAR Buffer, ULONG BufferLength, PULONG LengthTransferred, LPOVERLAPPED Overlapped) const noexcept
    {
      return m_WritePipe(InterfaceHandle, PipeID, Buffer, BufferLength, LengthTransferred, Overlapped);
    }

    BOOL WinusbDll::GetOverlappedResult(WINUSB_INTERFACE_HANDLE InterfaceHandle, LPOVERLAPPED lpOverlapped, LPDWORD lpNumberOfBytesTransferred, BOOL bWait) const noexcept
    {
      return m_GetOverlappedResult(InterfaceHandle, lpOverlapped, lpNumberOfBytesTransferred, bWait);
    }

    BOOL WinusbDll::ResetPipe(WINUSB_INTERFACE_HANDLE InterfaceHandle, UCHAR PipeID) const noexcept
    {
      return m_ResetPipe(InterfaceHandle, PipeID);
    }


  } // namespace Win32

  
} // namespace WacomGSS
