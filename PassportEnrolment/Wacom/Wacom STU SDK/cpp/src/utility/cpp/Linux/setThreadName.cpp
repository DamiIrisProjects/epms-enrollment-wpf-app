#include <WacomGSS/setThreadName.hpp>

// http://stackoverflow.com/questions/2369738/can-i-set-the-name-of-a-thread-in-pthreads-linux

namespace WacomGSS
{


#if defined(WacomGSS_Linux)
  void setThreadName(pthread_t thread, char const * threadName) noexcept
  {
    // ignore return
    pthread_setname_np(thread, threadName); 
  }

  void setThreadName(char const * threadName) noexcept
  {
    setThreadName(pthread_self(), threadName);
  }
#endif


#if defined(WacomGSS_BSD)
  void setThreadName(pthread_t tid, char const * threadName) noexcept
  {
    pthread_set_name_np(tid, threadName); 
  }

  void setThreadName(char const * threadName) noexcept
  {
    setThreadName(pthread_self(), threadName);
  }
#endif


#if defined(WacomGSS_OSX)
  //void setThreadName(pthread_t tid, char const * threadName) noexcept // not supported

  void setThreadName(char const * threadName) noexcept
  {
    // ignore return
    pthread_setname_np(threadName); 
  }
#endif


} // namespace WacomGSS
