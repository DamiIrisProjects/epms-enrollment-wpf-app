var interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler =
[
    [ "onGetReportException", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#a537d5cef498da2e378769780ded95047", null ],
    [ "onUnhandledReportData", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#ad1a462380e8ed69fef1cd3ebec0b188e", null ],
    [ "onPenData", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#ae8c5b4d4b659894ada9acb2c34a3b3b8", null ],
    [ "onPenDataOption", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#ac51a0cd63f6cfb9e89fae168a532f007", null ],
    [ "onPenDataEncrypted", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#a814994adf141b42db633bb2bebc9410e", null ],
    [ "onPenDataEncryptedOption", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#a51478d5ad0eeb6c3722f9d02df7e2a64", null ],
    [ "onPenDataTimeCountSequence", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#a6b05e558081cda4734b1bb60a7253b27", null ],
    [ "onPenDataTimeCountSequenceEncrypted", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#af31eb4cc3b7760453a490ca32544130e", null ],
    [ "onDevicePublicKey", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#ad3c8cb0a9dfc9cde58c216d5c8809abf", null ],
    [ "onEncryptionStatus", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_i_tablet_handler.html#a72369b2f5a1fef1fae93e8f99719701a", null ]
];