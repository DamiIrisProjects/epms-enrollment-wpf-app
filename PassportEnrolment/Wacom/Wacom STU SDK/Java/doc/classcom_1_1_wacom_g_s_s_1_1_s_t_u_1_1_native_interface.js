var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface =
[
    [ "disconnect", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#ab1e0b2bb94e3553f40f1bfde30324418", null ],
    [ "isConnected", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#a66fcf5ccf54a39ec2b3eb33350432c96", null ],
    [ "get", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#a17844a40b1d7f49b06760e7a315643dd", null ],
    [ "set", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#a1f4f7316fe00d0491cfa516f67ca5a88", null ],
    [ "supportsWrite", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#a02ebef3ee803e85795e235871d1b9607", null ],
    [ "write", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#ae4463689cbdc16d9a542fabd18e1f084", null ],
    [ "interfaceQueue", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#a4b6665107b9c503b918eadb826b95756", null ],
    [ "queueNotifyAll", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#aaa8285a876aba16a4d1c4b7356253026", null ],
    [ "getReportCountLengths", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#af77c68e30df1f53aea3d4879e6e050bd", null ],
    [ "getProductId", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_native_interface.html#ad48a6d69ed9aae1fe7a2cc5d775eae99", null ]
];