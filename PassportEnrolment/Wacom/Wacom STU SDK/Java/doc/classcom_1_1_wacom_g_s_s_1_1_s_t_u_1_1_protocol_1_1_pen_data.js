var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data =
[
    [ "PenData", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#a83a87f52f8dbd5f2054688586577c39c", null ],
    [ "PenData", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#aa710fe36fb462553cd001721896ce438", null ],
    [ "getRdy", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#ac1c143ca8b936edf547c729f9be1f069", null ],
    [ "getSw", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#adba392b927b11c3432abae8d77f6c4df", null ],
    [ "getPressure", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#ab17d2011a8a7be7c35c970bc7e9db504", null ],
    [ "getX", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#a4aeac13f86e89ed7fd43fc1f6ffc13ac", null ],
    [ "getY", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#a8e5247de92085199d9ddb339bdfb9aae", null ],
    [ "reportId", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#aa41644db2d6850133ec852f595b4babd", null ],
    [ "reportSize", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html#a400a0d889af19f0165d65c212f5c1c3c", null ]
];