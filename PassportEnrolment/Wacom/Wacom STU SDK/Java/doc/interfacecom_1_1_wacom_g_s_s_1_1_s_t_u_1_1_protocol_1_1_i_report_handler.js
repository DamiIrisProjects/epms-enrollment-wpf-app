var interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler =
[
    [ "onPenData", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#a9e8cdbd35d24d9dfcd2a019c34c6da6b", null ],
    [ "onPenDataOption", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#af07c3271579e2f7e73ca0ba32ce0fc21", null ],
    [ "onPenDataEncrypted", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#a247574a7354a6c844a221263e5ec7ffa", null ],
    [ "onPenDataEncryptedOption", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#adaf7ae6761a481a3e8e382f0be58245d", null ],
    [ "onPenDataTimeCountSequence", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#acc8aa83702a321d1009b0754f0f3087c", null ],
    [ "onPenDataTimeCountSequenceEncrypted", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#acd35d8c8cc518bea1fffb62139dede15", null ],
    [ "onDevicePublicKey", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#a2b7c13737dc275490c10f00e3999e110", null ],
    [ "onEncryptionStatus", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#a29bd44d60fc12a3ad2f0e09c7158f980", null ],
    [ "onDecrypt", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_i_report_handler.html#a536451a097dbfb0e23b3e1aa5c2c2368", null ]
];