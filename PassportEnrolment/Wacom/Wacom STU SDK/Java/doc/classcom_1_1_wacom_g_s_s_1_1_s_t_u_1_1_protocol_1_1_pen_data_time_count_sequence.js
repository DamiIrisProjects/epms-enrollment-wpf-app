var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence =
[
    [ "PenDataTimeCountSequence", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html#a6c8b3048966a5a7d35f21e5b81fe3555", null ],
    [ "PenDataTimeCountSequence", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html#ad3b3eb8ab10093172b55f40e380c9923", null ],
    [ "getTimeCount", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html#ab5af1ccb143d5e4e00755ec373897497", null ],
    [ "getSequence", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html#aa4c67448a05ac9a5729eac11929972db", null ],
    [ "reportId", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html#a189ae6a95fe9ae4bf034dcca0db5e51b", null ],
    [ "reportSize", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html#a4607a7040620ea7fd174a93a70cd642e", null ]
];