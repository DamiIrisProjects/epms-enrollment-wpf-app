var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status =
[
    [ "Status", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status.html#ae4adc71f94a2f25e2d3f560cd5d14da4", null ],
    [ "getStatusCode", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status.html#a98c8109e1c9a3917bd0d92caec6b539d", null ],
    [ "getLastResultCode", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status.html#a243a17166a8d5951fa3cbc9e351b47da", null ],
    [ "getStatusWord", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status.html#a788a66ff14f8b7e0790c09ee51c86b24", null ]
];