var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device =
[
    [ "UsbDevice", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a35d7b29f2dfdd3c377670b844c81a002", null ],
    [ "getIdVendor", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#aca754e24b65b93f924597c45f79cd0b8", null ],
    [ "getIdProduct", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#ab08a7f62c0d88ba43b396f6f1a8d9efd", null ],
    [ "getBcdDevice", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a3caf0fe279c708347fbb9e8734b9b46d", null ],
    [ "getUsbDevices", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a1df8290e01e1b5ea5f1d718b9b1b50cc", null ],
    [ "VendorId_Wacom", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a6198c4d02339c0e191920b0cbb280a64", null ],
    [ "ProductId_500", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a0c2ae58746cbca4bd6a51ae9add235d1", null ],
    [ "ProductId_300", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a628e8116fbf3ab78db50e121e57e94df", null ],
    [ "ProductId_520A", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#ab576a2f3aaa4829703818dc7a4e6585a", null ],
    [ "ProductId_430", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a1f446ecbb52a33ce781fa224bd140eec", null ],
    [ "ProductId_530", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a47514761e20ea607a42564747a3a67ce", null ],
    [ "ProductId_min", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a368e821efcc97f9c8ef6649ebc864ed2", null ],
    [ "ProductId_max", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#ac1ab924060e8ed0838717f3a07756e5f", null ]
];