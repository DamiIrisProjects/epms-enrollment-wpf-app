var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option =
[
    [ "PenDataEncryptedOption", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html#ae7676d0e1a6d04467906de32744a8f26", null ],
    [ "getOption1", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html#a21bc2a76e3233a7da0f994f71a4e74fe", null ],
    [ "getOption2", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html#aa910cfc0635b45341a794c958aeaeae1", null ],
    [ "getPenDataOption1", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html#aa553f00ad1185d0b8021a8bf857e40a7", null ],
    [ "getPenDataOption2", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html#a5ed28a7498bce4f6950b6bbeaa56dc8d", null ],
    [ "reportId", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html#a16d11adba7d4603323da690d747bc356", null ],
    [ "reportSize", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html#add5dc8f8a3b617f89304c2400c435c59", null ]
];