﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PassportEnrolmentEntities
{
    public class Operator
    {
        #region Variables

        [DataMember]
        public int OperatorId { get; set; }

        [DataMember]
        public List<Role> OperatorRoles { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }


        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Hash { get; set; }

        [DataMember]
        public string Salt { get; set; }

        [DataMember]
        public int LoginAttempts { get; set; }

        [DataMember]
        public string Middlename { get; set; }

        [DataMember]
        public PersonBiometrics Biometrics { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public int IsActive { get; set; }

        [DataMember]
        public DateTime DateOfBirth { get; set; }

        public string FullName
        {
            get { return String.Format("{0} {1}", FirstName, Surname).Trim(); }
        }

        public string FullNamesAndMiddlename
        {
            get { return String.Format("{0} {1} {2}", FirstName, Middlename, Surname).Trim(); }
        }

        [DataMember]
        public string Gender { get; set; }

        #endregion

        #region Operations

        /// <summary>
        /// Validates the properties of this Person object.
        /// </summary>
        public void Validate(out bool isValid, out string message)
        {
            isValid = true;
            message = String.Empty;
            if (isValid)
            {
                try
                {   // Try to maintain an order close to what is normally displayed in the UI
                    Validations.ValidateNameString(Surname, "Last Name");
                    Validations.ValidateNameString(FirstName, "First Name");
                    if (!String.IsNullOrEmpty(Middlename))
                        Validations.ValidateNameString(Middlename, "Middle Name");
                    ValidateDOBandAge();
                    ValidateGender();
                    ValidateEmail();
                    //ValidateBiometrics();
                }
                catch (Exception ex)
                {
                    isValid = false;
                    message = ex.Message;
                }
            }
        }

        /// <summary>
        /// Either DOB or age must be entered.
        /// </summary>
        public void ValidateDOBandAge()
        {
            if (DateOfBirth == DateTime.MinValue)
                throw new Exception("Please specify the date of birth");
        }

        /// <summary>
        /// Make sure that the person has a valid photograph and fingerprints
        /// </summary>
        public void ValidateBiometrics()
        {

        }

        /// <summary>
        /// Has to have a gender
        /// </summary>
        public void ValidateGender()
        {
            if (string.IsNullOrEmpty(Gender))
                throw new Exception("Please specify a gender.");
        }

        /// <summary>
        /// Check format of phone number
        /// </summary>
        private void ValidatePhoneNumber(string theNumber)
        {
            Validations.ValidatePhoneNumber(theNumber, 11);
        }

        /// <summary>
        /// Check format of email
        /// </summary>
        private void ValidateEmail()
        {
            Validations.ValidateEmailAddress(EmailAddress, true);
        }

        #endregion
    }
}
