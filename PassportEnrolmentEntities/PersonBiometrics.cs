﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows.Media.Imaging;

namespace PassportEnrolmentEntities
{
    public class PersonBiometrics
    {
        [DataMember]
        public List<byte[]> FingerList { get; set; }

        [DataMember]
        public byte[] PictureColor { get; set; }

        [DataMember]
        public byte[] PictureBw { get; set; }

        [DataMember]
        public Afisteam.Biometrics.Person Fingers { get; set; }

        [DataMember]
        public byte[] FingerprintArray { get; set; }

        [DataMember]
        public byte[] SignatureData { get; set; }

        [DataMember]
        public BitmapSource SignatureSource { get; set; }
    }
}
