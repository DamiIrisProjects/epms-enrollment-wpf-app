﻿using System.Collections.Generic;

namespace PassportEnrolmentEntities
{
    public class Role
    {
        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public List<string> Privileges { get; set; }
    }
}
