﻿using System;
using System.Runtime.Serialization;

namespace PassportEnrolmentEntities
{
    [DataContract]
    public class State
    {
        #region Variables

        [DataMember]
        public string StateName { get; set; }

        [DataMember]
        public int StateId { get; set; }

        [DataMember]
        public string StateCode { get; set; }

        #endregion

        #region OPERATIONS

        public override string ToString()
        {
            return String.Format(StateName);
        }

        #endregion

    }
}
