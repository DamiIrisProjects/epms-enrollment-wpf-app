﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace PassportEnrolmentEntities
{
    public class Validations
    {
        static public void ValidateEmailAddress(string address, bool allowBlank)
        {
            //string patternLenient = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
               + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
               + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
               + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
               + @"[a-zA-Z]{2,}))$";

            if (String.IsNullOrEmpty(address))
            {
                if (!allowBlank)
                    throw new ArgumentNullException("Cannot be blank.");
                else
                    return;
            }
            if (!Regex.IsMatch(address, patternStrict))
                throw new ValidationException("This e-mail address is not valid.");
        }

        static public void ValidateNationalIdNumber(string id)
        {
            if (String.IsNullOrEmpty(id))
                throw new Exception("Cannot be empty.");
            if (id.Length > 20)
                throw new Exception("Cannot exceed 20 characters length.");
            char[] AllowedChars = "1234567890".ToCharArray();
            foreach (char C in id)
                if (AllowedChars.Contains(C) == false)
                    throw new Exception(String.Format("\"{0}\" is an invalid character.", C));
            //Double spaces
            if (!String.IsNullOrEmpty(id) && id.Contains("  "))
                throw new Exception("No double spaces allowed.");
            if (!String.IsNullOrEmpty(id) && id[0] == ' ')
                throw new Exception("No leading spaces allowed.");
        }

        static public void ValidateLetters(string str)
        {
            if (String.IsNullOrEmpty(str))
                throw new Exception("Cannot be empty.");
            if (str.Length > 40)
                throw new Exception("Cannot exceed 20 characters length.");
            char[] AllowedChars = " abcdefghijklmnopqrstuvwxyz".ToCharArray();
            foreach (char C in str.ToLower())
                if (AllowedChars.Contains(C) == false)
                    throw new Exception(String.Format("\"{0}\" is an invalid character.", C));
            //Double spaces
            if (!String.IsNullOrEmpty(str) && str.Contains("  "))
                throw new Exception("No double spaces allowed.");
            if (!String.IsNullOrEmpty(str) && str[0] == ' ')
                throw new Exception("No leading spaces allowed.");
        }

        static public void ValidateNumbers(string id)
        {
            if (String.IsNullOrEmpty(id))
                throw new Exception("Cannot be empty.");
            if (id.Length > 20)
                throw new Exception("Cannot exceed 20 characters length.");
            char[] AllowedChars = "1234567890".ToCharArray();
            foreach (char C in id)
                if (AllowedChars.Contains(C) == false)
                    throw new Exception(String.Format("\"{0}\" is an invalid character.", C));
            //Double spaces
            if (!String.IsNullOrEmpty(id) && id.Contains("  "))
                throw new Exception("No double spaces allowed.");
            if (!String.IsNullOrEmpty(id) && id[0] == ' ')
                throw new Exception("No leading spaces allowed.");
        }

        static public void ValidateNumbers(string id, int numberOfDigits)
        {
            if (String.IsNullOrEmpty(id))
                throw new Exception("Cannot be empty.");
            if (id.Length > numberOfDigits)
                throw new Exception("Cannot exceed 10 characters length.");
            char[] AllowedChars = "1234567890".ToCharArray();
            foreach (char C in id)
                if (AllowedChars.Contains(C) == false)
                    throw new Exception(String.Format("\"{0}\" is an invalid character.", C));
            //Double spaces
            if (!String.IsNullOrEmpty(id) && id.Contains("  "))
                throw new Exception("No double spaces allowed.");
            if (!String.IsNullOrEmpty(id) && id[0] == ' ')
                throw new Exception("No leading spaces allowed.");
        }

        /// <summary>
        /// No numbers, double spaces, leading spaces, etc. Dictionaries can be used.
        /// Max 25 characters. Cannot be empty.
        /// </summary>
        /// 
        static public void ValidateNameString(string nameValue, string propertyName)
        {
            if (propertyName == "Middle Name" && nameValue == "")
                return;
            // Cannot be empty
            if (propertyName != "Middle Name")
            {
                if (String.IsNullOrEmpty(nameValue))
                {
                    if (propertyName == "Last Name")
                    {
                        throw new Exception("Last Name cannot be empty.");
                    }
                    if (propertyName == "First Name")
                    {
                        throw new Exception("First Name cannot be empty.");
                    }
                }
            }

            if (nameValue.Length > 25)
                throw new Exception("Cannot exceed 25 characters length.");
            if (nameValue.Length > 0)
            {
                char[] AllowedLetters = " abcdefghijklmnopqrstuvwxyz".ToCharArray();
                // Only letters
                foreach (char C in nameValue.ToLower())
                    if (AllowedLetters.Contains(C) == false)
                        throw new Exception(String.Format("\"{0}\" is an invalid character.", C));
            }
            // Double spaces
            if (nameValue.Contains("  "))
                throw new Exception("No double spaces allowed.");
            if (nameValue[0] == ' ')
                throw new Exception("No leading spaces allowed.");
        }

        /// <summary>
        /// Validates a stakeholder name string. Not empty or null. No leading spaces. No double spaces.
        /// </summary>
        static public void ValidateNameString(string nameValue, string section, string propertyName, bool allowEmpty)
        {
            if (!allowEmpty && String.IsNullOrEmpty(nameValue))
                throw new Exception(section + ": " + propertyName + " cannot be empty.");
            if (!String.IsNullOrEmpty(nameValue) && nameValue.Contains("  "))
                throw new Exception(section + ": The " + propertyName + " : No double spaces allowed.");
            if (!String.IsNullOrEmpty(nameValue) && nameValue[0] == ' ')
                throw new Exception(section + ": The " + propertyName + " : No leading spaces allowed.");
        }

        static public void ValidatePhoneNumber(string value, byte maxLength)
        {
            if (String.IsNullOrEmpty(value))
                throw new ValidationException("Cannot be empty.");
            char[] AllowedCharacters = "0123456789".ToCharArray();
            foreach (Char AChar in value)
                if (!AllowedCharacters.Contains(AChar))
                    throw new ValidationException("Contains an invalid character \"" + AChar + "\"");
            if (value.Length > maxLength)
                throw new ValidationException("Must be less than " + (maxLength + 1).ToString() + " digits");
        }
    }
}
