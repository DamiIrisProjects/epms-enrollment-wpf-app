﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PassportEnrolment.Helpers;

namespace PassportEnrolment.Controls
{
    /// <summary>
    /// Interaction logic for ChangePasswordDetails.xaml
    /// </summary>
    public partial class ChangePasswordDetails : UserControl
    {
        public ChangePasswordDetails()
        {
            InitializeComponent();
        }

        public bool passwordOk = false;

        public bool ValidateUsernamePW()
        {
            bool isValid = true;

            if (txtUsername.Text == string.Empty || txtPassword.Password == string.Empty || txtConfirmPassword.Password == string.Empty)
            {
                isValid = false;
                MessageBox.Show("Please enter a username, current password and password you would like to now use", "Login Details not completed", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            if (isValid && txtUsername.Text.Length < 4)
            {
                isValid = false;
                MessageBox.Show("Minimum of 4 letters on a username", "Invalid Username", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            if (isValid && !passwordOk)
            {
                isValid = false;
                MessageBox.Show("Password does not pass required minimum standard.", "Invalid Password", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            return isValid;
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordScore score = PasswordAdvisor.CheckStrength(txtConfirmPassword.Password);
            Int32 i = (Int32)score;

            if (i < 2)
            {
                passwordOk = false;
                txtPwStrength.Foreground = Brushes.Red;
                txtPwStrength.Text = "weak password";
            }

            if (i == 2)
            {
                passwordOk = false;
                txtPwStrength.Foreground = Brushes.Orange;
                txtPwStrength.Text = "less weak password";
            }

            if (i == 3)
            {
                passwordOk = true;
                txtPwStrength.Foreground = Brushes.YellowGreen;
                txtPwStrength.Text = "medium password";
            }

            if (i == 4)
            {
                passwordOk = true;
                txtPwStrength.Foreground = Brushes.DarkGreen;
                txtPwStrength.Text = "strong password";
            }

            if (i == 5)
            {
                passwordOk = true;
                txtPwStrength.Foreground = Brushes.DarkGreen;
                txtPwStrength.Text = "very strong password";
            }
        }
    }
}
