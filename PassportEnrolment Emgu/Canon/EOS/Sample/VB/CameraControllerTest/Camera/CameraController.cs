﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;


namespace CameraControllerTest
{
    public class CameraController
    {
        public const int clse = 8;

        protected CameraModel model;
        //// Command processing
        protected Processor processor = new Processor();

        public CameraController()
        {
            model = null;
        }

        public void setCameraModel(CameraModel model)
        {
            this.model = model;
        }


        //// Start processor thread   
        public void run()
        {
            processor.start();

            //The communication with the camera begins
            StoreAsync(new OpenSessionCommand(model));

        }

        public void actionPerformed(string strEvent, IntPtr inObject)
        {
            if (strEvent == "download")
            {
                StoreAsync(new DownloadCommand(model, inObject));
                // 
            }
        }


        public void actionPerformed(string strEvent)
        {

            if (strEvent == "opensession")
            {
                //// Start communication with remote camera.
                StoreAsync(new OpenSessionCommand(model));

            }
            else if (strEvent == "takepicture")
            {
                StoreAsync(new TakePictureCommand(model));

            }
            else if (strEvent == "close")
            {
                model.notifyObservers(clse);
                processor.setCloseCommand(new CloseSessionCommand(model));
                processor.stopTh();
                processor.@join();
            }

        }

        public void actionPerformed(string strEvent, int id, int data = 0)
        {
            if (strEvent == "get")
            {
                StoreAsync(new GetPropertyCommand(model, id));

            }
            else if (strEvent == "set")
            {
                StoreAsync(new SetPropertyCommand(model, id, data));

            }
            else if (strEvent == "getlist")
            {
                StoreAsync(new GetPropertyDescCommand(model, id));

            }

        }

        //// Receive a command
        protected void StoreAsync(Command command)
        {
            if ((command == null) == false)
            {
                processor.enqueue(command);

            }

        }
    }
}