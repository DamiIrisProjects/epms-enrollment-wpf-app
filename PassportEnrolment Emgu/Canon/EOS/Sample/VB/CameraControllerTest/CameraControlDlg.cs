﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CameraControllerTest
{
    public class VBSample : Form, Observer
    {
        // Command IDs
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;
        #region "User defined attributes"

        // Save as class variable, new delegates of event handlers.
        public EDSDKTypes.EdsPropertyEventHandler inPropertyEventHandler = new EDSDKTypes.EdsPropertyEventHandler(CameraEventListener.handlePropertyEvent);
        public EDSDKTypes.EdsObjectEventHandler inObjectEventHandler = new EDSDKTypes.EdsObjectEventHandler(CameraEventListener.handleObjectEvent);
        public EDSDKTypes.EdsStateEventHandler inStateEventHandler = new EDSDKTypes.EdsStateEventHandler(CameraEventListener.handleStateEvent);
        //
        public static CameraController controller;
        public static CameraModel model;

        public static Hashtable m_cmbTbl = new Hashtable();


        #endregion


        public delegate void UpdateDelegate(Observable @from, int msg, int data);


        public void UpdateWindow(Observable @from, int msg, int data)
        {
            //Get the name of this thread.
            string threadName = System.Threading.Thread.CurrentThread.Name;

            //// Make this form be able to be updated by other thread.
            if (InvokeRequired)
            {
                //Create UpdateDelegate
                UpdateDelegate dlg = new UpdateDelegate(UpdateWindow);
                try
                {
                    BeginInvoke(dlg, new object[] {
					@from,
					msg,
					data
				});
                }
                catch (Exception e)
                {
                    return;
                }
                finally
                {
                }
                return;
            }

            int propertyID = 0;

            switch (msg)
            {
                case prog:
                    ////Progress of image downloading .
                    progressBar.Value = data;

                    break;
                case strt:
                    //// Start downloading.
                    break;
                ////_progress.SetPos(0);

                case cplt:
                    //// Complete downloading.
                    progressBar.Value = 0;

                    break;
                case updt:
                    //// Update properties.
                    propertyID = data;
                    int propData = model.getPropertyUInt32(propertyID);
                    UpdateProperty(propertyID, propData);

                    break;
                case upls:
                    //// Update an available property list.
                    propertyID = data;
                    EDSDKTypes.EdsPropertyDesc desc = model.getPropertyDesc(propertyID);
                    UpdatePropertyDesc(propertyID, desc);

                    break;
                case warn:
                    //// Warning
                    InfoTextBox.Text = "Device Busy";

                    break;
                case errr:
                    //// Error
                    //// Nothing to do because the first getting property from model 30D is sure to fail. 
                    string ss = null;
                    ss = string.Format("%x", data);
                    InfoTextBox.Text = ss;

                    break;
                case clse:
                    //// Close
                    TakeBtn.Enabled = false;
                    progressBar.Enabled = false;
                    InfoTextBox.Enabled = false;
                    AEModeCmb.Enabled = false;
                    TvCmb.Enabled = false;
                    AvCmb.Enabled = false;
                    ISOSpeedCmb.Enabled = false;

                    break;
            }

            if (msg != errr & msg != warn)
            {
                InfoTextBox.Text = "";
            }


        }
        void Observer.update(Observable @from, int msg, int data)
        {
            UpdateWindow(@from, msg, data);
        }



        public void UpdateProperty(int propertyID, int data)
        {
            Hashtable propList = (Hashtable)CameraProperty.g_PropList[propertyID];
            switch (propertyID)
            {
                case EDSDKTypes.kEdsPropID_AEModeSelect:
                    AEModeCmb.Text = propList[data].ToString();
                    break;
                case EDSDKTypes.kEdsPropID_ISOSpeed:
                    ISOSpeedCmb.Text = propList[data].ToString();
                    break;
                case EDSDKTypes.kEdsPropID_MeteringMode:
                    MeteringModeCmb.Text = propList[data].ToString();
                    break;
                case EDSDKTypes.kEdsPropID_Av:
                    AvCmb.Text = propList[data].ToString();
                    break;
                case EDSDKTypes.kEdsPropID_Tv:
                    TvCmb.Text = propList[data].ToString();
                    break;
                case EDSDKTypes.kEdsPropID_ExposureCompensation:
                    ExposureCompCmb.Text = propList[data].ToString();
                    break;
                case EDSDKTypes.kEdsPropID_ImageQuality:
                    ImageQualityCmb.Text = propList[data].ToString();
                    break;
            }

        }



        public void UpdatePropertyDesc(int propertyID, EDSDKTypes.EdsPropertyDesc desc)
        {
            int err = 0;
            int iCnt = 0;
            ComboBox cmb = (ComboBox)m_cmbTbl[propertyID];
            Hashtable propList = (Hashtable)CameraProperty.g_PropList[propertyID];
            string propStr = null;
            ArrayList propValueList = new ArrayList();

            if (cmb == null)
            {
                return;
            }

            cmb.BeginUpdate();
            cmb.Items.Clear();
            for (iCnt = 0; iCnt <= desc.numElements - 1; iCnt++)
            {
                propStr = propList[desc.propDesc[iCnt]].ToString();
                if (propStr != null)
                {
                    err = cmb.Items.Add(propStr);
                    propValueList.Add(desc.propDesc[iCnt]);
                }
            }

            cmb.Tag = propValueList;
            // Set the property value list

            cmb.EndUpdate();
            if (cmb.Items.Count == 0)
            {
                cmb.Enabled = false;
                //// No available item.
            }
            else
            {
                cmb.Enabled = true;
            }

        }



        public CameraModel cameraModelFactory(IntPtr camera, EDSDKTypes.EdsDeviceInfo deviceInfo)
        {

            // if Legacy protocol.
            if (deviceInfo.DeviceSubType == 0)
            {
                return new CameraModelLegacy(camera);
            }

            // PTP protocol.
            return new CameraModel(camera);

        }



        #region "Window Events"
        private void TakeBtn_Click(System.Object sender, System.EventArgs e)
        {
            //
            // Release button
            //
            controller.actionPerformed("takepicture");

        }



        private void ExitBtn_Click(System.Object sender, System.EventArgs e)
        {
            // Quit button
            this.Close();

            System.Environment.Exit(0);
        }

        #endregion



        private void VBSample_Load(System.Object sender, System.EventArgs e)
        {
            int err = EDSDKErrors.EDS_ERR_OK;
            IntPtr cameraList = IntPtr.Zero;
            IntPtr camera = IntPtr.Zero;
            int count = 0;
            bool isSDKLoaded = false;
            CameraProperty propObj = new CameraProperty();

            // connect property id to combobox. 
            m_cmbTbl.Add(EDSDKTypes.kEdsPropID_AEMode, this.AEModeCmb);
            m_cmbTbl.Add(EDSDKTypes.kEdsPropID_ISOSpeed, this.ISOSpeedCmb);
            m_cmbTbl.Add(EDSDKTypes.kEdsPropID_Av, this.AvCmb);
            m_cmbTbl.Add(EDSDKTypes.kEdsPropID_Tv, this.TvCmb);
            m_cmbTbl.Add(EDSDKTypes.kEdsPropID_MeteringMode, this.MeteringModeCmb);
            m_cmbTbl.Add(EDSDKTypes.kEdsPropID_ExposureCompensation, this.ExposureCompCmb);
            m_cmbTbl.Add(EDSDKTypes.kEdsPropID_ImageQuality, this.ImageQualityCmb);


            err = EDSDK.EdsInitializeSDK();



            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                isSDKLoaded = true;

            }


            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsGetCameraList(ref cameraList);

            }



            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsGetChildCount(cameraList, ref count);
                if (count == 0)
                {
                    err = EDSDKErrors.EDS_ERR_DEVICE_NOT_FOUND;
                }

            }

            //// Get the first camera.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsGetChildAtIndex(cameraList, 0, ref camera);

            }


            EDSDKTypes.EdsDeviceInfo deviceInfo = new EDSDKTypes.EdsDeviceInfo();


            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsGetDeviceInfo(camera, ref deviceInfo);

                if (err == EDSDKErrors.EDS_ERR_OK & (camera == null) == true)
                {
                    err = EDSDKErrors.EDS_ERR_DEVICE_NOT_FOUND;
                }

            }



            if ((cameraList == null) == false)
            {
                EDSDK.EdsRelease(cameraList);

            }


            //// Create the camera model 

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                model = cameraModelFactory(camera, deviceInfo);

                if ((model == null) == true)
                {
                    err = EDSDKErrors.EDS_ERR_DEVICE_NOT_FOUND;

                }
            }



            if (err != EDSDKErrors.EDS_ERR_OK)
            {
                MessageBox.Show("Cannot detect camera");

            }




            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                //// Create a controller
                controller = new CameraController();

                //// Set the model to this controller.
                controller.setCameraModel(model);

                //// Make notify the model updating to the view.
                model.addObserver(this);

                // ------------------------------------------------------------------------
                // ------------------------------------------------------------------------
                // You should create class instance of delegates of event handlers 
                // with 'new' expressly if its attribute is Shared, 
                // because System sometimes do garbage-collect these delegates.
                //
                //
                // This error occurs.
                //
                // CallbackOnCollectedDelegate is detected.
                // Message: Callback was called with
                // garbage-collected delegate of  
                // Type() 'VBSample3!VBSample3.EDSDKTypes+EdsPropertyEventHandler::Invoke' 
                // 
                // It will be able to make data loss or application clash.
                // You should stock delegates when you want to send delegate to unmanaged code.
                //
                // ------------------------------------------------------------------------

                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = EDSDK.EdsSetPropertyEventHandler(camera, EDSDKTypes.kEdsPropertyEvent_All, inPropertyEventHandler, IntPtr.Zero);
                }

                //// Set ObjectEventHandler
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = EDSDK.EdsSetObjectEventHandler(camera, EDSDKTypes.kEdsObjectEvent_All, inObjectEventHandler, IntPtr.Zero);

                }

                //// Set StateEventHandler
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = EDSDK.EdsSetCameraStateEventHandler(camera, EDSDKTypes.kEdsStateEvent_All, inStateEventHandler, IntPtr.Zero);
                }

            }




            if (err != EDSDKErrors.EDS_ERR_OK)
            {
                if ((camera == null) == false)
                {
                    EDSDK.EdsRelease(camera);
                    camera = IntPtr.Zero;
                }

                if ((isSDKLoaded))
                {
                    EDSDK.EdsTerminateSDK();
                }

                if ((model == null) == false)
                {
                    model = null;
                }


                if ((controller == null) == false)
                {
                    controller = null;
                }


                System.Environment.Exit(0);

            }



            ////Execute the controller.
            controller.run();

        }


        private void VBSample_FormClosing(System.Object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            controller.actionPerformed("close");

            if ((model == null))
            {
                if ((model.getCameraObject() == null) == false)
                {
                    EDSDK.EdsRelease(model.getCameraObject());
                }
            }

            EDSDK.EdsTerminateSDK();

        }

    }
}