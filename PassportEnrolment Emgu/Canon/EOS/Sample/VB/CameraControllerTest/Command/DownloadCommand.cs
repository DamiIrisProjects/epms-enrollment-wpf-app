﻿using CameraControllerTest;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class DownloadCommand : Command
    {
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;

        private IntPtr directoryItem;

        public CameraControllerTest.EDSDKTypes.EdsProgressCallback inProgressCallback = new CameraControllerTest.EDSDKTypes.EdsProgressCallback(ProgressFunc);

        public DownloadCommand(CameraModel inModel, IntPtr dirItem)
            : base(inModel)
        {
            this.directoryItem = dirItem;
        }

        ~DownloadCommand()
        {
            //// Release item.
            if ((this.directoryItem == null) == false)
            {
                EDSDK.EdsRelease(this.directoryItem);
                this.directoryItem = IntPtr.Zero;

            }
        }


        //// Execute a command .
        public override bool execute()
        {

            int err = (int)EDSDKErrors.EDS_ERR_OK;
            IntPtr stream = IntPtr.Zero;

            //// Get informations of the downloadling directory item.
            CameraControllerTest.EDSDKTypes.EdsDirectoryItemInfo dirItemInfo = new EDSDKTypes.EdsDirectoryItemInfo();
            err = (int)EDSDK.EdsGetDirectoryItemInfo(this.directoryItem, ref dirItemInfo);

            //// Notify starting transfer.
            if (err == (int)EDSDKErrors.EDS_ERR_OK)
            {
                EOSLib.model.notifyObservers(strt);
            }

            //// Create a file stream for receiving image.
            if (err == (int)EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsCreateFileStream(dirItemInfo.szFileName, (int)EDSDKTypes.EdsFileCreateDisposition.kEdsFileCreateDisposition_CreateAlways, (int)EDSDKTypes.EdsAccess.kEdsAccess_ReadWrite, ref stream);
            }

            //// Set Progress Callback.

            if (err == (int)EDSDKErrors.EDS_ERR_OK)
            {
                GCHandle gch = GCHandle.Alloc(this);
                err = EDSDK.EdsSetProgressCallback(stream, inProgressCallback, EDSDKTypes.EdsProgressOption.kEdsProgressOption_Periodically, GCHandle.ToIntPtr(gch));
            }

            //// Download image

            if (err == (int)EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsDownload(this.directoryItem, dirItemInfo.size, stream);

            }

            //// Complete downloading

            if (err == (int)EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsDownloadComplete(directoryItem);

            }

            //// Release item

            if ((this.directoryItem == null) == false)
            {
                err = EDSDK.EdsRelease(this.directoryItem);
                directoryItem = IntPtr.Zero;

            }

            //// Release the stream

            if ((stream == null) == false)
            {
                err = EDSDK.EdsRelease(stream);
                stream = IntPtr.Zero;

            }

            //// Notify to complete downloading 

            if (err == (int)EDSDKErrors.EDS_ERR_OK)
            {
                base.model.notifyObservers(cplt);

            }

            //// Notify Error
            if (err != (int)EDSDKErrors.EDS_ERR_OK)
            {
                base.model.notifyObservers(errr, err);
            }

            return true;

        }


        public static long ProgressFunc(int inPercent, IntPtr inContext, ref bool outCancel)
        {

            long rtn = Convert.ToInt64((int)EDSDKErrors.EDS_ERR_OK);

            EOSLib.model.notifyObservers(prog, inPercent);

            return rtn;

        }



    }
}