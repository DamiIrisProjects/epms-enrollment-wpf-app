﻿using CameraControllerTest;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public static class commandName
    {
        // Command IDs
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;
    }

    // Abstract command class. 
    // All command classes extends this one.
    public class Command
    {
        protected CameraModel model;
        public Command(CameraModel model)
        {
            this.model = model;
        }


        //// Execute a command.
        public virtual bool execute()
        {
            return true;
        }

    }

}