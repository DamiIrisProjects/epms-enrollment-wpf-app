﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class GetPropertyDescCommand : Command
    {
        // Command IDs
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;

        private int propertyID;

        public GetPropertyDescCommand(CameraModel model, int propertyID)
            : base(model)
        {
            this.propertyID = propertyID;
        }


        //// Execute a command.
        public override bool execute()
        {

            int err = EDSDKErrors.EDS_ERR_OK;
            bool locked = false;

            //// You should do UILock when you send a command to camera models elder than EOS30D.

            if (base.model.isLegacy())
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)CameraControllerTest.EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UILock, 0);


                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    locked = true;
                }
            }

            ////Get an available property list.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = getPropertyDesc(this.propertyID);

            }


            if (locked == true)
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)CameraControllerTest.EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UIUnLock, 0);

            }

            //// Notify Error.

            if (err != EDSDKErrors.EDS_ERR_OK)
            {
                //// Retry when the camera replys deviceBusy.

                if ((err + EDSDKErrors.EDS_ERRORID_MASK) == EDSDKErrors.EDS_ERR_DEVICE_BUSY)
                {
                    base.model.notifyObservers(warn, err);

                    return false;

                }

                base.model.notifyObservers(errr, err);

            }
            return true;

        }


        private int getPropertyDesc(int id)
        {

            int err = EDSDKErrors.EDS_ERR_OK;
            CameraControllerTest.EDSDKTypes.EdsPropertyDesc propertyDesc = new CameraControllerTest.EDSDKTypes.EdsPropertyDesc();


            if (id == EDSDKTypes.kEdsPropID_Unknown)
            {
                //// If the propertyID is invalidID,
                //// you should retry to get available property lists.
                //// InvalidID is able to be published for the models elder than EOS30D.

                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getPropertyDesc(EDSDKTypes.kEdsPropID_AEMode);
                }
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getPropertyDesc(EDSDKTypes.kEdsPropID_Tv);
                }
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getPropertyDesc(EDSDKTypes.kEdsPropID_Av);
                }
                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    err = getPropertyDesc(EDSDKTypes.kEdsPropID_ISOSpeed);
                }

                return err;
            }

            //// Get available property lists.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsGetPropertyDesc(base.model.getCameraObject(), id, ref propertyDesc);
            }

            //// Stock the available property list.	

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                base.model.setPropertyDesc(id, propertyDesc);

            }

            //// Notify updating.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                base.model.notifyObservers(upls, id);

            }

            return err;

        }

    }

}