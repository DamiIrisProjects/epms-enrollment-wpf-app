﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public static class EDSDKTypes
    {

        ///******************************************************************************
        // Definition of Data Types
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        // Data Type
        //-----------------------------------------------------------------------------*/
        public enum EdsDataType
        {
            kEdsDataType_Unknown = 0,
            kEdsDataType_Bool = 1,
            kEdsDataType_String = 2,
            kEdsDataType_Int8 = 3,
            kEdsDataType_UInt8 = 6,
            kEdsDataType_Int16 = 4,
            kEdsDataType_UInt16 = 7,
            kEdsDataType_Int32 = 8,
            kEdsDataType_UInt32 = 9,
            kEdsDataType_Int64 = 10,
            kEdsDataType_UInt64 = 11,
            kEdsDataType_Float = 12,
            kEdsDataType_Double = 13,
            kEdsDataType_ByteBlock = 14,
            kEdsDataType_Rational = 20,
            kEdsDataType_Point = 21,
            kEdsDataType_Rect = 22,
            kEdsDataType_Time,

            kEdsDataType_Bool_Array = 30,
            kEdsDataType_Int8_Array = 31,
            kEdsDataType_Int16_Array = 32,
            kEdsDataType_Int32_Array = 33,
            kEdsDataType_UInt8_Array = 34,
            kEdsDataType_UInt16_Array = 35,
            kEdsDataType_UInt32_Array = 36,
            kEdsDataType_Rational_Array = 37,

            kEdsDataType_FocusInfo = 101,
            kEdsDataType_PictureStyleDesc
        }

        #region "Property IDs"
        ///*-----------------------------------------------------------------------------
        //    Property IDs()
        //-----------------------------------------------------------------------------*/

        //////////////////////////////////////////////
        //// Camera Setting Properties
        //////////////////////////////////////////////   
        //// 
        public const int kEdsPropID_Unknown = 0xffff;

        //// Product name
        public const int kEdsPropID_ProductName = 0x2;
        //// Body ID
        public const int kEdsPropID_BodyIDEx = 0x15;
        //// Owner
        public const int kEdsPropID_OwnerName = 0x4;
        //// Manufacturer
        public const int kEdsPropID_MakerName = 0x5;
        //// For cameras, the system time; for images, the shooting time
        public const int kEdsPropID_DateTime = 0x6;
        //// Firmware Version 
        public const int kEdsPropID_FirmwareVersion = 0x7;
        //// Battery state: 0-100% or "AC"
        public const int kEdsPropID_BatteryLevel = 0x8;

        //// CFn=&h00000002 PFn
        //// Custom Function settings
        public const int kEdsPropID_CFn = 0x9;
        //// Personal Function settings
        public const int kEdsPropID_PFn = 0xa;

        //// Destination where image was saved                         
        public const int kEdsPropID_SaveTo = 0xb;

        //// Set user-defined data in Set1, 2, or 3, or read it out 
        public const int kEdsPropID_UserWhiteBalanceData = 0x201;
        //// // Set user-defined data in Set1, 2, or 3, or read it out 
        public const int kEdsPropID_UserToneCurveData = 0x202;
        //// User picture style data
        public const int kEdsPropID_UserPictureStyleData = 0x203;


        public const int kEdsPropID_UserManualWhiteBalanceData = 0x204;
        public const int kEdsPropID_BatteryQuality = 0x10;
        public const int kEdsPropID_BatteryShutterCount = 0x11;
        public const int kEdsPropID_BatteryCalibration = 0x12;

        public const int kEdsPropID_BatteryName = 0x13;
        public const int kEdsPropID_HDDirectoryStructure = 0x20;

        public const int kEdsPropID_WFTStatus = 0x21;
        public const int kEdsPropID_QuickReviewTime = 0xf;
        public const int kEdsPropID_ShutterCounter = 0x22;
        public const int kEdsPropID_PhotoStudioMode = 0x30;

        public const int kEdsPropID_SpecialOption = 0x31;

        public const int kEdsPropID_DataInputTransmission = 0x50;
        public const int kEdsPropID_Wft_ProfileLockConfig = 0x52;
        public const int kEdsPropID_Wft_TransmissionConfig = 0x53;
        public const int kEdsPropID_Wft_TCPIPConfig = 0x54;
        public const int kEdsPropID_Wft_FTPConfig = 0x55;
        public const int kEdsPropID_Wft_WirelessConfig = 0x56;
        public const int kEdsPropID_Wft_WiredConfig = 0x57;
        public const int kEdsPropID_Wft_SettingsName = 0x58;

        public const int kEdsPropID_Wft_Info = 0x59;

        //////////////////////////////////////////////
        //// Image Properties
        //////////////////////////////////////////////

        //// Image quality
        public const int kEdsPropID_ImageQuality = 0x100;
        //// Value representing compression when saved as a JPEG; 1 to 10 (cap)
        public const int kEdsPropID_JpegQuality = 0x101;
        //// The image orientation
        public const int kEdsPropID_Orientation = 0x102;
        //// ICC Profile data
        public const int kEdsPropID_ICCProfile = 0x103;
        //// focus Info
        public const int kEdsPropID_FocusInfo = 0x104;

        //// Digital exposure compensation
        public const int kEdsPropID_DigitalExposure = 0x105;
        //// White balance (light source)
        public const int kEdsPropID_WhiteBalance = 0x106;
        //// Color temperature setting value
        public const int kEdsPropID_ColorTemperature = 0x107;
        //// White balance shift compensation
        public const int kEdsPropID_WhiteBalanceShift = 0x108;

        //// Contrast setting
        public const int kEdsPropID_Contrast = 0x109;
        //// Saturation setting
        public const int kEdsPropID_ColorSaturation = 0x10a;
        //// Color tone setting
        public const int kEdsPropID_ColorTone = 0x10b;
        //// Sharpness setting value
        public const int kEdsPropID_Sharpness = 0x10c;
        //// Color space setting
        public const int kEdsPropID_ColorSpace = 0x10d;
        //// Tone curve (standard or custom)
        public const int kEdsPropID_ToneCurve = 0x10e;

        //// Color effect setting
        public const int kEdsPropID_PhotoEffect = 0x10f;
        //// Filter effect setting
        public const int kEdsPropID_FilterEffect = 0x110;
        //// Gradation effect setting
        public const int kEdsPropID_ToningEffect = 0x111;

        //// Processing parameter setting
        public const int kEdsPropID_ParameterSet = 0x112;
        //// Color matrix setting
        public const int kEdsPropID_ColorMatrix = 0x113;

        //// Picture style
        public const int kEdsPropID_PictureStyle = 0x114;
        //// Picture style setting details
        public const int kEdsPropID_PictureStyleDesc = 0x115;

        //// Computer settings caption for the picture style at the time of shooting
        public const int kEdsPropID_PictureStyleCaption = 0x200;

        public const int kEdsPropID_CustomWBCaption = 0x204;
        //////////////////////////////////////////////
        //// Develop Properties
        //////////////////////////////////////////////
        //// Linear processing ON/OFF
        public const int kEdsPropID_Linear = 0x300;
        //// Click WB coordinates
        public const int kEdsPropID_ClickWBPoint = 0x301;
        //// WB control value
        public const int kEdsPropID_WBCoeffs = 0x302;

        //////////////////////////////////////////////
        //// Property Masks
        //////////////////////////////////////////////
        public const int kEdsPropID_AtCapture_Flag = unchecked((int)0x80000000);
        //// A supporting property for getting the properties at the time of shooting. 
        //// This property ID cannot be used by itself. 

        //////////////////////////////////////////////
        //// Capture Properties
        //////////////////////////////////////////////
        //// Shooting mode
        public const int kEdsPropID_AEMode = 0x400;
        //// Shooting mode
        public const int kEdsPropID_AEModeSelect = 0x436;
        //// Drive mode (cap)
        public const int kEdsPropID_DriveMode = 0x401;
        //// ISO sensitivity setting value
        public const int kEdsPropID_ISOSpeed = 0x402;
        //// Metering mode
        public const int kEdsPropID_MeteringMode = 0x403;
        //// AF mode (cap)
        public const int kEdsPropID_AFMode = 0x404;
        //// Aperture value (cap) at the time of shooting
        public const int kEdsPropID_Av = 0x405;
        //// Shutter speed setting value (cap)
        public const int kEdsPropID_Tv = 0x406;
        //// Exposure compensation (cap)
        public const int kEdsPropID_ExposureCompensation = 0x407;
        //// Flash compensation setting
        public const int kEdsPropID_FlashCompensation = 0x408;
        //// Lens focal length information at the time of shooting
        public const int kEdsPropID_FocalLength = 0x409;

        //// Number of available shots
        public const int kEdsPropID_AvailableShots = 0x40a;

        //// ISO, auto exposure or flash exposure bracket
        public const int kEdsPropID_Bracket = 0x40b;
        //// White balance bracket
        public const int kEdsPropID_WhiteBalanceBracket = 0x40c;

        //// String representing the lens name
        public const int kEdsPropID_LensName = 0x40d;
        //// Auto exposure bracket value
        public const int kEdsPropID_AEBracket = 0x40e;
        //// Flash exposure bracket value
        public const int kEdsPropID_FEBracket = 0x40f;
        //// ISO bracket value
        public const int kEdsPropID_ISOBracket = 0x410;

        //// Noise reduction
        public const int kEdsPropID_NoiseReduction = 0x411;

        //// Use of the flash (activated or not)
        public const int kEdsPropID_FlashOn = 0x412;
        //// Red-eye reduction
        public const int kEdsPropID_RedEye = 0x413;
        //// Flash type
        public const int kEdsPropID_FlashMode = 0x414;

        public const int kEdsPropID_TempStatus = 0x415;

        public const int kEdsPropID_LensStatus = 0x416;

        //////////////////////////////////////////////
        //// EVF Properties
        //////////////////////////////////////////////
        public const int kEdsPropID_Evf_OutputDevice = 0x500;
        public const int kEdsPropID_Evf_Mode = 0x501;
        public const int kEdsPropID_Evf_WhiteBalance = 0x502;
        public const int kEdsPropID_Evf_ColorTemperature = 0x503;
        public const int kEdsPropID_Evf_DepthOfFieldPreview = 0x504;
        public const int kEdsPropID_Evf_Sharpness = 0x505;

        public const int kEdsPropID_Evf_ClickWBCoeffs = 0x506;
        //// EVF IMAGE DATA Properties
        public const int kEdsPropID_Evf_Zoom = 0x507;
        public const int kEdsPropID_Evf_ZoomPosition = 0x508;
        public const int kEdsPropID_Evf_FocusAid = 0x509;
        public const int kEdsPropID_Evf_Histogram = 0x50a;
        public const int kEdsPropID_Evf_ImagePosition = 0x50b;

        public const int kEdsPropID_Evf_HistogramStatus = 0x50c;
        #endregion

        ///*-----------------------------------------------------------------------------
        // Send Commands
        //-----------------------------------------------------------------------------*/
        public enum EdsCameraCommand
        {
            kEdsCameraCommand_TakePicture,
            //The camera is requested to take a picture.
            kEdsCameraCommand_ExtendShutDownTimer,
            // KeepDeviceOn  
            kEdsCameraCommand_BulbStart,
            kEdsCameraCommand_BulbEnd,
            kEdsCameraCommand_DoAfEvf = 0x102,
            kEdsCameraCommand_DriveLensEvf = 0x103,
            kEdsCameraCommand_DoClickWBEvf = 0x104,
            kEdsCameraCommand_ReflectWftProfile = 0x201,
            kEdsCameraCommand_PassThrough = 0x1000
            // Sends specific commands to a camera.
        }

        ///*-----------------------------------------------------------------------------
        // Camera State
        //-----------------------------------------------------------------------------*/
        public enum EdsCameraStatusCommand
        {
            kEdsCameraStatusCommand_UILock = 0,
            //// 
            kEdsCameraStatusCommand_UIUnLock = 1,
            //// 
            kEdsCameraStatusCommand_EnterDirectTransfer = 2,
            //// The camera is changed to direct transmission mode.
            kEdsCameraStatusCommand_ExitDirectTransfer = 3
            //// The camera is exited from direct transmission mode.
        }

        // NOTE: I HAVE ADDED THE VALUES. DID NOT PREVIOUSLY HAVE VALUES //

        #region "CAMERE EVENTS"
        ///*-----------------------------------------------------------------------------
        // Camera Events
        //-----------------------------------------------------------------------------*/

        //////////////////////////////////////////////
        //// Property Event
        //////////////////////////////////////////////

        public const int kEdsPropertyEvent_All = 0x100;
        public const int kEdsPropertyEvent_PropertyChanged = 0x101;
        // The value of the property specified 
        //// by the parameter has changed. 
        //// If it is necessary, it should be
        //// re-acquired.
        //// When property is not specified,
        //// it is necessary to acquire all parameters. 

        public const int kEdsPropertyEvent_PropertyDescChanged = 0x102;
        //// The value of the property specified 
        //// by the parameter has changed. 
        //// If it is necessary, it should be
        //// re-acquired.
        //// When property is not specified,
        //// it is necessary to acquire all parameters.  


        //////////////////////////////////////////////
        //// Object Event
        //////////////////////////////////////////////
        //// Indicates that the VolumeInfo dataset
        public const int kEdsObjectEvent_All = 0x200;

        public const int kEdsObjectEvent_VolumeInfoChanged = 0x201;
        //// Indicates that the VolumeInfo dataset
        //// for a particular object has changed, 
        //// and that it should be re-requested.

        public const int kEdsObjectEvent_VolumeUpdateItems = 0x202;
        ////

        public const int kEdsObjectEvent_FolderUpdateItems = 0x203;

        public const int kEdsObjectEvent_DirItemCreated = 0x204;
        //// The file or the folder was created. 


        public const int kEdsObjectEvent_DirItemRemoved = 0x205;
        //// The file or the folder was removed. 


        public const int kEdsObjectEvent_DirItemInfoChanged = 0x206;
        //// Indicates that the DirectoryItemInfo dataset
        //// for a particular object has changed, 
        //// and that it should be re-requested.

        public const int kEdsObjectEvent_DirItemContentChanged = 0x207;
        //// DS_Event_ObjectContentChanged


        public const int kEdsObjectEvent_DirItemRequestTransfer = 0x208;
        //// Indicates that there is an object that
        //// should be transferred.
        //// Please download the object specified 
        //// by the parameter when you receive 
        //// this event. 


        public const int kEdsObjectEvent_DirItemRequestTransferDT = 0x209;
        //// Indicates that the direct forwarding 
        //// button of the camera was pushed.
        //// Please download the object specified 
        //// by the parameter when you receive 
        //// this event. 

        public const int kEdsObjectEvent_DirItemCancelTransferDT = 0x20a;
        //// Indicates that the cancellation button
        //// of the camera was pushed while transfering
        //// Please discontinue transfering the object
        //// specified by the parameter. 
        ////         
        public const int kEdsObjectEvent_VolumeAdded = 0x20c;

        public const int kEdsObjectEvent_VolumeRemoved = 0x20d;
        //////////////////////////////////////////////
        //// State Event
        //////////////////////////////////////////////

        public const int kEdsStateEvent_All = 0x300;
        //// The camera shut down.  
        public const int kEdsStateEvent_Shutdown = 0x301;


        public const int kEdsStateEvent_JobStatusChanged = 0x302;
        //// the state of the presence of the object
        //// that should be transferred has changed.


        public const int kEdsStateEvent_WillSoonShutDown = 0x303;
        //// After the time passage of the time-out, 
        //// the connection with the camera is cut. 
        //// It depends on the setting of the camera 
        //// about the time-out time. 



        public const int kEdsStateEvent_ShutDownTimerUpdate = 0x304;
        //// After the WillSoonShutDown event, 
        //// the shutdown timer has updated.                                  


        public const int kEdsStateEvent_CaptureError = 0x305;
        //// It failed in capture because 
        //// hardware error occurred by the camera.


        //// The error occurred in SDK. 
        public const int kEdsStateEvent_InternalError = 0x306;



        public const int kEdsStateEvent_AfResult = 0x309;


        public const int kEdsStateEvent_BulbExposureTime = 0x310;
        #endregion



        ///*-----------------------------------------------------------------------------
        //  Drive Lens
        //-----------------------------------------------------------------------------*/
        public enum EdsEvfDriveLens
        {
            kEdsEvfDriveLens_Near1 = 1,
            kEdsEvfDriveLens_Near2 = 2,
            kEdsEvfDriveLens_Near3 = 3,
            kEdsEvfDriveLens_Far1 = 0x8001,
            kEdsEvfDriveLens_Far2 = 0x8002,
            kEdsEvfDriveLens_Far3 = 0x8003
        }

        ///*-----------------------------------------------------------------------------
        //  Drive Lens
        //-----------------------------------------------------------------------------*/
        public enum EdsEvfDepthOfFieldPreview
        {
            kEdsEvfDepthOfFieldPreview_OFF = 0,
            kEdsEvfDepthOfFieldPreview_ON = 1
        }


        ///*-----------------------------------------------------------------------------
        // Stream Seek Origins
        //-----------------------------------------------------------------------------*/
        public enum EdsSeekOrigin
        {
            kEdsSeek_Cur = 0,
            //// Seek from Current Point
            kEdsSeek_Begin,
            //// Seek from Start Point
            kEdsSeek_End
            //// Seek from End Point
        }


        ///*-----------------------------------------------------------------------------
        // File and Propaties Access
        //-----------------------------------------------------------------------------*/
        public enum EdsAccess
        {
            kEdsAccess_Read = 0,
            //   // Enables subsequent open operations on the object to
            //// request read access. 
            kEdsAccess_Write,
            //,     // Enables subsequent open operations on the object to
            // // request write access.
            kEdsAccess_ReadWrite,
            //,  // Enables subsequent open operations on the object to 
            //// request read and write access.
            kEdsAccess_Error = unchecked((int)unchecked((int)0xffffffff))
        }


        ///*-----------------------------------------------------------------------------
        // File Create Disposition
        //-----------------------------------------------------------------------------*/
        public enum EdsFileCreateDisposition
        {
            kEdsFileCreateDisposition_CreateNew = 0,
            //// Creates a new file. The function fails  
            //// if the specified file already exists.
            kEdsFileCreateDisposition_CreateAlways,
            //// Creates a new file. If the file exists, 
            //// the function overwrites the file and clears
            //// the existing attributes.
            kEdsFileCreateDisposition_OpenExisting,
            //// Opens the file. The function fails 
            //// if the file does not exist. 
            kEdsFileCreateDisposition_OpenAlways,
            //// Opens the file, if it exists. 
            //// If the file does not exist, 
            //// the function creates the file.
            kEdsFileCreateDisposition_TruncateExsisting
            //// Opens the file. Once opened, the file is 
            //// truncated so that its size is zero bytes.
            //// The function fails if the file does not exist.
        }



        ///*-----------------------------------------------------------------------------
        // Image Type 
        //-----------------------------------------------------------------------------*/
        public enum EdsImageType
        {
            kEdsImageType_Unknown = 0x0,
            kEdsImageType_Jpeg = 0x1,
            kEdsImageType_CRW = 0x2,
            kEdsImageType_RAW = 0x4,
            kEdsImageType_CR2 = 0x6
        }


        public enum EdsTargetImageType
        {
            kEdsTargetImageType_Unknown = 0x0,
            kEdsTargetImageType_Jpeg = 0x1,
            kEdsTargetImageType_TIFF = 0x7,
            ////  8bitTIFF
            kEdsTargetImageType_TIFF16 = 0x8,
            ////  16bitTIFF  
            kEdsTargetImageType_RGB = 0x9,
            ////  8bitRGB
            kEdsTargetImageType_RGB16 = 0xa
            ////  16bitRGB
        }


        public enum EdsImageSize
        {
            kEdsImageSize_Large = 0,
            kEdsImageSize_Middle = 1,
            kEdsImageSize_Small = 2,
            kEdsImageSize_Middle1 = 5,
            kEdsImageSize_Middle2 = 6,
            kEdsImageSize_Unknown = unchecked((int)0xffffffff)
        }


        public enum EdsImageQuality
        {
            kEdsImageQuality_Normal = 2,
            kEdsImageQuality_Fine = 3,
            kEdsImageQuality_Lossless = 4,
            kEdsImageQuality_SuperFine = 5,
            kEdsImageQuality_Unknown = unchecked((int)0xffffffff)
        }


        public enum EdsImageSource
        {
            kEdsImageSrc_FullView,
            kEdsImageSrc_Thumbnail,
            kEdsImageSrc_Preview,
            kEdsImageSrc_RAWThumbnail,
            kEdsImageSrc_RAWFullView
        }

        ///*-----------------------------------------------------------------------------
        // Progress Option
        //-----------------------------------------------------------------------------*/
        public enum EdsProgressOption
        {
            kEdsProgressOption_NoReport = 0,
            //// no callback.
            kEdsProgressOption_Done,
            //// performs callback only at once 
            //// at the time of an end.
            kEdsProgressOption_Periodically
            //// performs callback periodically.
        }


        ///*-----------------------------------------------------------------------------
        // file attribute 
        //-----------------------------------------------------------------------------*/
        public enum EdsFileAttributes
        {
            kEdsFileAttribute_Normal = 0x0,
            kEdsFileAttribute_ReadOnly = 0x1,
            kEdsFileAttribute_Hidden = 0x2,
            kEdsFileAttribute_System = 0x4,
            kEdsFileAttribute_Archive = 0x20
        }



        ///*-----------------------------------------------------------------------------
        // Battery level
        //-----------------------------------------------------------------------------*/
        public enum EdsBatteryLevel
        {
            kEdsBatteryLevel_Empty = 1,
            kEdsBatteryLevel_Low = 30,
            kEdsBatteryLevel_Half = 50,
            kEdsBatteryLevel_Normal = 80,
            kEdsBatteryLevel_AC = unchecked((int)0xffffffff)
        }


        ///*-----------------------------------------------------------------------------
        // Save To
        //-----------------------------------------------------------------------------*/
        public enum EdsSaveTo
        {
            kEdsSaveTo_Camera = 1,
            kEdsSaveTo_Host = 2,
            kEdsSaveTo_Both = kEdsSaveTo_Camera + kEdsSaveTo_Host
        }


        ///*-----------------------------------------------------------------------------
        // StorageType
        //-----------------------------------------------------------------------------*/
        public enum EdsStorageType
        {
            kEdsStorageType_Non = 0,
            kEdsStorageType_CF = 1,
            kEdsStorageType_SD = 2
        }




        ///*-----------------------------------------------------------------------------
        // White Balance
        //-----------------------------------------------------------------------------*/
        public enum EdsWhiteBalance
        {
            kEdsWhiteBalance_Auto = 0,
            kEdsWhiteBalance_Daylight = 1,
            kEdsWhiteBalance_Cloudy = 2,
            kEdsWhiteBalance_Tangsten = 3,
            kEdsWhiteBalance_Fluorescent = 4,
            kEdsWhiteBalance_Strobe = 5,
            kEdsWhiteBalance_WhitePaper = 6,
            kEdsWhiteBalance_Shade = 8,
            kEdsWhiteBalance_ColorTemp = 9,
            kEdsWhiteBalance_PCSet1 = 10,
            kEdsWhiteBalance_PCSet2 = 11,
            kEdsWhiteBalance_PCSet3 = 12,

            kEdsWhiteBalance_Click = -1,
            kEdsWhiteBalance_Pasted = -2
        }

        ///*-----------------------------------------------------------------------------
        // Photo Effect
        //-----------------------------------------------------------------------------*/
        public enum EdsPhotoEffect
        {
            kEdsPhotoEffect_Off = 0,
            kEdsPhotoEffect_Monochrome = 5
        }


        ///*-----------------------------------------------------------------------------
        // Color Matrix  
        //-----------------------------------------------------------------------------*/
        public enum EdsColorMatrix
        {
            kEdsColorMatrix_Custom = 0,
            kEdsColorMatrix_1 = 1,
            kEdsColorMatrix_2 = 2,
            kEdsColorMatrix_3 = 3,
            kEdsColorMatrix_4 = 4,
            kEdsColorMatrix_5 = 5,
            kEdsColorMatrix_6 = 6,
            kEdsColorMatrix_7 = 7
        }


        ///*-----------------------------------------------------------------------------
        // Filter Effect
        //-----------------------------------------------------------------------------*/
        public enum EdsFilterEffect
        {
            kEdsFilterEffect_None = 0,
            kEdsFilterEffect_Yellow = 1,
            kEdsFilterEffect_Orange = 2,
            kEdsFilterEffect_Red = 3,
            kEdsFilterEffect_Green = 4
        }

        ///*-----------------------------------------------------------------------------
        // Toning Effect 
        //-----------------------------------------------------------------------------*/
        public enum EdsTonigEffect
        {
            kEdsTonigEffect_None = 0,
            kEdsTonigEffect_Sepia = 1,
            kEdsTonigEffect_Blue = 2,
            kEdsTonigEffect_Purple = 3,
            kEdsTonigEffect_Green = 4
        }


        ///*-----------------------------------------------------------------------------
        // Color Space
        //-----------------------------------------------------------------------------*/
        public enum EdsColorSpace
        {
            kEdsColorSpace_sRGB = 1,
            kEdsColorSpace_AdobeRGB = 2,
            kEdsColorSpace_Unknown = unchecked((int)0xffffffff)
        }



        ///*-----------------------------------------------------------------------------
        // PictureStyle
        //-----------------------------------------------------------------------------*/
        public enum EdsPictureStyle
        {
            kEdsPictureStyle_Standard = 0x81,
            kEdsPictureStyle_Portrait = 0x82,
            kEdsPictureStyle_Landscape = 0x83,
            kEdsPictureStyle_Neutral = 0x84,
            kEdsPictureStyle_Faithful = 0x85,
            kEdsPictureStyle_Monochrome = 0x86,
            kEdsPictureStyle_User1 = 0x21,
            kEdsPictureStyle_User2 = 0x22,
            kEdsPictureStyle_User3 = 0x23,
            kEdsPictureStyle_PC1 = 0x41,
            kEdsPictureStyle_PC2 = 0x42,
            kEdsPictureStyle_PC3 = 0x43
        }


        ///*-----------------------------------------------------------------------------
        // Transfer Option
        //-----------------------------------------------------------------------------*/
        public enum EdsTransferOption
        {
            kEdsTransferOption_ByDirectTransfer = 1,
            kEdsTransferOption_ByRelease = 2,
            kEdsTransferOption_ToDesktop = 0x100
        }



        public enum EdsAEMode
        {
            kEdsAEMode_Program = 0,
            kEdsAEMode_Tv = 1,
            kEdsAEMode_Av = 2,
            kEdsAEMode_Manual = 3,
            kEdsAEMode_Bulb = 4,
            kEdsAEMode_A_DEP = 5,
            kEdsAEMode_DEP = 6,
            kEdsAEMode_Custom = 7,
            kEdsAEMode_Lock = 8,
            kEdsAEMode_Green = 9,
            kEdsAEMode_NightPortrait = 10,
            kEdsAEMode_Sports = 11,
            kEdsAEMode_Portrait = 12,
            kEdsAEMode_Landscape = 13,
            kEdsAEMode_Closeup = 14,
            kEdsAEMode_FlashOff = 15,
            kEdsAEMode_CreativeAuto = 19,
            kEdsAEMode_Movie = 20,
            kEdsAEMode_PhotoInMovie = 21,
            kEdsAEMode_SceneIntelligentAuto = 22,
            kEdsAEMode_SCN = 25,
            kEdsAEMode_Unknown = unchecked((int)0xffffffff)
        }



        ///*-----------------------------------------------------------------------------
        // Bracket
        //-----------------------------------------------------------------------------*/
        public enum EdsBracket
        {
            kEdsBracket_AEB = 0x1,
            kEdsBracket_ISOB = 0x2,
            kEdsBracket_WBB = 0x4,
            kEdsBracket_FEB = 0x8,
            kEdsBracket_Unknown = unchecked((int)0xffffffff)
        }

        ///*-----------------------------------------------------------------------------
        // Temp status
        //-----------------------------------------------------------------------------*/
        public enum EdsTempStatus
        {
            kEdsTempStatus_Normal = 0,
            kEdsTempStatus_Warning = 1,
            kEdsTempStatus_Disable_LV = 2,
            kEdsTempStatus_Disable_Capture = 3
        }

        ///*-----------------------------------------------------------------------------
        // EVF Output Device [Flag]
        //-----------------------------------------------------------------------------*/
        public enum EdsEvfOutputDevice
        {
            kEdsEvfOutputDevice_TFT = 1,
            kEdsEvfOutputDevice_PC = 2
        }

        ///*-----------------------------------------------------------------------------
        // EVF Zoom
        //-----------------------------------------------------------------------------*/
        public enum EdsEvfZoom
        {
            kEdsEvfZoom_Fit = 1,
            kEdsEvfZoom_x5 = 5,
            kEdsEvfZoom_x10 = 10
        }

        ///******************************************************************************
        // Definition of Structures
        //******************************************************************************/
        ///*-----------------------------------------------------------------------------
        // Point
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsPoint
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct EdsSize
        {
            public int width;
            public int height;
        }


        ///*-----------------------------------------------------------------------------
        // Rectangle
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsRect
        {
            //
            public EdsPoint point;
            public EdsSize size;
        }

        ///*-----------------------------------------------------------------------------
        // Rational
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsRational
        {
            public int numerator;
            public int denominator;
        }


        ///*-----------------------------------------------------------------------------
        // Time
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsTime
        {
            //// year
            public int year;
            //// month 1=January, 2=February, ...
            public int month;
            //// day
            public int day;
            //// hour
            public int hour;
            //// minute
            public int minute;
            //// second
            public int second;
            //// reserved
            public int milliseconds;
        }


        ///******************************************************************************
        // Definition of Constants
        //******************************************************************************/
        public const short EDS_MAX_NAME = 256;
        public const int EDS_TRANSFER_BLOCK_SIZE = 512;
        //// When the DirectoryItem is downloaded
        //// or uploaded separately for the plural
        //// block, The size of the block
        //// other than the terminal block should be
        //// assumed to be a multiple in 512 bytes.


        ///******************************************************************************
        // Camera Detect Evnet Handler
        //******************************************************************************/
        public delegate long EdsCameraAddedHandler(IntPtr inContext);


        ///******************************************************************************
        // Callback Functions
        //******************************************************************************/
        public delegate long EdsProgressCallback(int inPercent, IntPtr inContext, ref bool outCancel);


        ///******************************************************************************
        // Evnet Handler
        //******************************************************************************/
        public delegate long EdsPropertyEventHandler(int inEvent, int inPropertyID, int inParam, IntPtr inContext);


        public delegate long EdsObjectEventHandler(int inEvent, IntPtr inRef, IntPtr inContext);


        public delegate long EdsStateEventHandler(int inEvent, int inEventData, IntPtr inContext);



        ///******************************************************************************
        // Definition of Structures
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        // Device Info
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsDeviceInfo
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = EDSDKTypes.EDS_MAX_NAME)]
            public string szPortName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = EDSDKTypes.EDS_MAX_NAME)]
            public string szDeviceDescription;
            public int DeviceSubType;
        }


        ///*-----------------------------------------------------------------------------
        // Volume Info
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsVolumeInfo
        {
            public int storageType;
            public int access;
            public Int64 maxCapacity;
            public Int64 freeSpaceInBytes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = EDSDKTypes.EDS_MAX_NAME)]
            public string szVolumeLabel;
        }


        ///*-----------------------------------------------------------------------------
        // DirectoryItem Info
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsDirectoryItemInfo
        {
            public int size;
            public bool isFolder;
            public int groupID;
            public int opt;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = EDSDKTypes.EDS_MAX_NAME)]
            public string szFileName;
            public int format;
            public int dateTime;
        }


        ///*-----------------------------------------------------------------------------
        // Image Info
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsImageInfo
        {
            //// image width 
            public int width;
            //// image height
            public int height;

            //// number of color components in image.
            public int numOfComponents;
            //// bits per sample.  8 or 16.
            public int componentDepth;

            public EdsRect effectiveRect;
            //// Effective rectangles except 
            //// a black line of the image. 
            //// A black line might be in the top and bottom
            //// of the thumbnail image. 

            public int reserved1;

            public int reserved2;
        }


        ///*-----------------------------------------------------------------------------
        // SaveImageSetting                      
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsSaveImageSetting
        {
            public int JPEGQuality;
            public IntPtr iccProfileStream;
            public int reserved;
        }


        ///*-----------------------------------------------------------------------------
        //    Property Desc
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsPropertyDesc
        {
            public int form;
            public int access;
            public int numElements;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
            public int[] propDesc;
        }


        //////////////////////////////////////////////////////////////////////////////////
        ///*-----------------------------------------------------------------------------
        // Picture Style Desc
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsPictureStyleDesc
        {
            public int contrast;
            public int sharpness;
            public int saturation;
            public int colorTone;
            public int filterEffect;
            public int toningEffect;
        }


        ///*-----------------------------------------------------------------------------
        // Focus Info
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsFocusPoint
        {
            //// if the frame is valid.
            public int valid;
            //// if the frame is just focus.
            public int justFocus;
            //// rectangle of focus point.
            public EdsRect rect;
            //// reserved
            public int reserved;
        }


        [StructLayout(LayoutKind.Sequential)]
        public struct EdsFocusInfo
        {
            //// rectangle of the image.
            public EdsRect imageRect;
            //// number of point.
            public int pointNumber;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
            //// each frame's description.
            public EdsFocusPoint[] focusPoint;
        }


        ///*-----------------------------------------------------------------------------
        // User WhiteBalance (PC set1,2,3)/ User ToneCurve / User PictureStyle dataset 
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsUsersetData
        {
            public int valid;
            public int dataSize;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szCaption;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public byte[] data;
        }


        ///*-----------------------------------------------------------------------------
        // Capacity
        //-----------------------------------------------------------------------------*/
        [StructLayout(LayoutKind.Sequential)]
        public struct EdsCapacity
        {
            //// free clusters
            public int numberOfFreeClusters;
            //// sectors per cluster
            public int bytesPerSector;
            public bool reset;
        }



    }

}