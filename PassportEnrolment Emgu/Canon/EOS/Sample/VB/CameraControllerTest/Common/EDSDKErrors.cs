﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public static class EDSDKErrors
    {


        public const int EDS_ERRORID_MASK = (int)0xffffL;
        public const int EDS_ERR_OK = 0;
        ///* Miscellaneous errors */
        public const int EDS_ERR_UNIMPLEMENTED = (int)0x1L;
        public const int EDS_ERR_INTERNAL_ERROR = (int)0x2L;
        public const int EDS_ERR_MEM_ALLOC_FAILED = (int)0x3L;
        public const int EDS_ERR_MEM_FREE_FAILED = (int)0x4L;
        public const int EDS_ERR_OPERATION_CANCELLED = (int)0x5L;
        public const int EDS_ERR_INCOMPATIBLE_VERSION = (int)0x6L;
        public const int EDS_ERR_NOT_SUPPORTED = (int)0x7L;
        public const int EDS_ERR_UNEXPECTED_EXCEPTION = (int)0x8L;
        public const int EDS_ERR_PROTECTION_VIOLATION = (int)0x9L;
        public const int EDS_ERR_MISSING_SUBCOMPONENT = (int)0xaL;

        public const int EDS_ERR_SELECTION_UNAVAILABLE = (int)0xbL;
        ///* File errors */
        public const int EDS_ERR_FILE_IO_ERROR = (int)0x20L;
        public const int EDS_ERR_FILE_TOO_MANY_OPEN = (int)0x21L;
        public const int EDS_ERR_FILE_NOT_FOUND = (int)0x22L;
        public const int EDS_ERR_FILE_OPEN_ERROR = (int)0x23L;
        public const int EDS_ERR_FILE_CLOSE_ERROR = (int)0x24L;
        public const int EDS_ERR_FILE_SEEK_ERROR = (int)0x25L;
        public const int EDS_ERR_FILE_TELL_ERROR = (int)0x26L;
        public const int EDS_ERR_FILE_READ_ERROR = (int)0x27L;
        public const int EDS_ERR_FILE_WRITE_ERROR = (int)0x28L;
        public const int EDS_ERR_FILE_PERMISSION_ERROR = (int)0x29L;
        public const int EDS_ERR_FILE_DISK_FULL_ERROR = (int)0x2aL;
        public const int EDS_ERR_FILE_ALREADY_EXISTS = (int)0x2bL;
        public const int EDS_ERR_FILE_FORMAT_UNRECOGNIZED = (int)0x2cL;
        public const int EDS_ERR_FILE_DATA_CORRUPT = (int)0x2dL;

        public const int EDS_ERR_FILE_NAMING_NA = (int)0x2eL;
        ///* Directory errors */          
        public const int EDS_ERR_DIR_NOT_FOUND = (int)0x40L;
        public const int EDS_ERR_DIR_IO_ERROR = (int)0x41L;
        public const int EDS_ERR_DIR_ENTRY_NOT_FOUND = (int)0x42L;
        public const int EDS_ERR_DIR_ENTRY_EXISTS = (int)0x43L;

        public const int EDS_ERR_DIR_NOT_EMPTY = (int)0x44L;
        ///* Property errors */
        public const int EDS_ERR_PROPERTIES_UNAVAILABLE = (int)0x50L;
        public const int EDS_ERR_PROPERTIES_MISMATCH = (int)0x51L;

        public const int EDS_ERR_PROPERTIES_NOT_LOADED = (int)0x53L;
        ///* Function Parameter errors */     
        public const int EDS_ERR_INVALID_PARAMETER = (int)0x60L;
        public const int EDS_ERR_INVALID_HANDLE = (int)0x61L;
        public const int EDS_ERR_INVALID_POINTER = (int)0x62L;
        public const int EDS_ERR_INVALID_INDEX = (int)0x63L;
        public const int EDS_ERR_INVALID_LENGTH = (int)0x64L;
        public const int EDS_ERR_INVALID_FN_POINTER = (int)0x65L;

        public const int EDS_ERR_INVALID_SORT_FN = (int)0x66L;
        ///* Device errors */
        public const int EDS_ERR_DEVICE_NOT_FOUND = (int)0x80L;
        public const int EDS_ERR_DEVICE_BUSY = (int)0x81L;
        public const int EDS_ERR_DEVICE_INVALID = (int)0x82L;
        public const int EDS_ERR_DEVICE_EMERGENCY = (int)0x83L;
        public const int EDS_ERR_DEVICE_MEMORY_FULL = (int)0x84L;
        public const int EDS_ERR_DEVICE_INTERNAL_ERROR = (int)0x85L;
        public const int EDS_ERR_DEVICE_INVALID_PARAMETER = (int)0x86L;
        public const int EDS_ERR_DEVICE_NO_DISK = (int)0x87L;
        public const int EDS_ERR_DEVICE_DISK_ERROR = (int)0x88L;
        public const int EDS_ERR_DEVICE_CF_GATE_CHANGED = (int)0x89L;
        public const int EDS_ERR_DEVICE_DIAL_CHANGED = (int)0x8aL;
        public const int EDS_ERR_DEVICE_NOT_INSTALLED = (int)0x8bL;
        public const int EDS_ERR_DEVICE_STAY_AWAKE = (int)0x8cL;

        public const int EDS_ERR_DEVICE_NOT_RELEASED = (int)0x8dL;
        ///* Stream errors */
        public const int EDS_ERR_STREAM_IO_ERROR = (int)0xa0L;
        public const int EDS_ERR_STREAM_NOT_OPEN = (int)0xa1L;
        public const int EDS_ERR_STREAM_ALREADY_OPEN = (int)0xa2L;
        public const int EDS_ERR_STREAM_OPEN_ERROR = (int)0xa3L;
        public const int EDS_ERR_STREAM_CLOSE_ERROR = (int)0xa4L;
        public const int EDS_ERR_STREAM_SEEK_ERROR = (int)0xa5L;
        public const int EDS_ERR_STREAM_TELL_ERROR = (int)0xa6L;
        public const int EDS_ERR_STREAM_READ_ERROR = (int)0xa7L;
        public const int EDS_ERR_STREAM_WRITE_ERROR = (int)0xa8L;
        public const int EDS_ERR_STREAM_PERMISSION_ERROR = (int)0xa9L;
        public const int EDS_ERR_STREAM_COULDNT_BEGIN_THREAD = (int)0xaaL;
        public const int EDS_ERR_STREAM_BAD_OPTIONS = (int)0xabL;

        public const int EDS_ERR_STREAM_END_OF_STREAM = (int)0xacL;
        ///* Communications errors */
        public const int EDS_ERR_COMM_PORT_IS_IN_USE = (int)0xc0L;
        public const int EDS_ERR_COMM_DISCONNECTED = (int)0xc1L;
        public const int EDS_ERR_COMM_DEVICE_INCOMPATIBLE = (int)0xc2L;
        public const int EDS_ERR_COMM_BUFFER_FULL = (int)0xc3L;

        public const int EDS_ERR_COMM_USB_BUS_ERR = (int)0xc4L;
        ///* Lock/Unlock */
        public const int EDS_ERR_USB_DEVICE_LOCK_ERROR = (int)0xd0L;

        public const int EDS_ERR_USB_DEVICE_UNLOCK_ERROR = (int)0xd1L;
        ///* STI/WIA */
        public const int EDS_ERR_STI_UNKNOWN_ERROR = (int)0xe0L;
        public const int EDS_ERR_STI_INTERNAL_ERROR = (int)0xe1L;
        public const int EDS_ERR_STI_DEVICE_CREATE_ERROR = (int)0xe2L;
        public const int EDS_ERR_STI_DEVICE_RELEASE_ERROR = (int)0xe3L;

        public const int EDS_ERR_DEVICE_NOT_LAUNCHED = (int)0xe4L;
        public const int EDS_ERR_ENUM_NA = (int)0xf0L;
        public const int EDS_ERR_INVALID_FN_CALL = (int)0xf1L;
        public const int EDS_ERR_HANDLE_NOT_FOUND = (int)0xf2L;
        public const int EDS_ERR_INVALID_ID = (int)0xf3L;

        public const int EDS_ERR_WAIT_TIMEOUT_ERROR = (int)0xf4L;
        ///* PTP */
        public const int EDS_ERR_SESSION_NOT_OPEN = (int)0x2003;
        public const int EDS_ERR_INVALID_TRANSACTIONID = (int)0x2004;
        public const int EDS_ERR_INCOMPLETE_TRANSFER = (int)0x2007;
        public const int EDS_ERR_INVALID_STRAGEID = (int)0x2008;
        public const int EDS_ERR_DEVICEPROP_NOT_SUPPORTED = (int)0x200a;
        public const int EDS_ERR_INVALID_OBJECTFORMATCODE = (int)0x200b;
        public const int EDS_ERR_SELF_TEST_FAILED = (int)0x2011;
        public const int EDS_ERR_PARTIAL_DELETION = (int)0x2012;
        public const int EDS_ERR_SPECIFICATION_BY_FORMAT_UNSUPPORTED = (int)0x2014;
        public const int EDS_ERR_NO_VALID_OBJECTINFO = (int)0x2015;
        public const int EDS_ERR_INVALID_CODE_FORMAT = (int)0x2016;
        public const int EDS_ERR_UNKNOWN_VENDER_CODE = (int)0x2017;
        public const int EDS_ERR_CAPTURE_ALREADY_TERMINATED = (int)0x2018;
        public const int EDS_ERR_INVALID_PARENTOBJECT = (int)0x201a;
        public const int EDS_ERR_INVALID_DEVICEPROP_FORMAT = (int)0x201b;
        public const int EDS_ERR_INVALID_DEVICEPROP_VALUE = (int)0x201c;
        public const int EDS_ERR_SESSION_ALREADY_OPEN = (int)0x201e;
        public const int EDS_ERR_TRANSACTION_CANCELLED = (int)0x201f;
        public const int EDS_ERR_SPECIFICATION_OF_DESTINATION_UNSUPPORTED = (int)0x2020;
        public const int EDS_ERR_UNKNOWN_COMMAND = (int)0xa001;
        public const int EDS_ERR_OPERATION_REFUSED = (int)0xa005;
        public const int EDS_ERR_LENS_COVER_CLOSE = (int)0xa006;
        public const int EDS_ERR_LOW_BATTERY = (int)0xa101;

        public const int EDS_ERR_OBJECT_NOTREADY = (int)0xa102;
        public const int EDS_ERR_LAST_GENERIC_ERROR_PLUS_ONE = (int)0xf5L;
    }

}