﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public struct TPropStrVal
    {
        public int val;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string str;
    }

    public class CameraProperty
    {
        // Uniting camera properties and express strings table

        public static Hashtable g_AEMode = new Hashtable();
        public static Hashtable g_ISOSpeed = new Hashtable();
        public static Hashtable g_Av = new Hashtable();
        public static Hashtable g_Tv = new Hashtable();
        public static Hashtable g_MeteringMode = new Hashtable();
        public static Hashtable g_ExposureComp = new Hashtable();

        public static Hashtable g_ImageQuality = new Hashtable();

        public static Hashtable g_PropList = new Hashtable();

        public CameraProperty()
        {
            tableInit();
        }




        private void tableInit()
        {
            g_MeteringMode.Add(1, "Spot Metering");
            g_MeteringMode.Add(3, "Evaluative Metering");
            g_MeteringMode.Add(4, "Partial Metering");
            g_MeteringMode.Add(5, "Center-Weighted Average Metering");
            g_MeteringMode.Add(0xffffffff, "unkown");


            g_ExposureComp.Add(0x18, "+3");
            g_ExposureComp.Add(0x15, "+2 2/3");
            g_ExposureComp.Add(0x14, "+2 1/2");
            g_ExposureComp.Add(0x13, "+2 1/3");
            g_ExposureComp.Add(0x10, "+2");
            g_ExposureComp.Add(0xd, "+1 2/3");
            g_ExposureComp.Add(0xc, "+1 1/2");
            g_ExposureComp.Add(0xb, "+1 1/3");
            g_ExposureComp.Add(0x8, "+1");
            g_ExposureComp.Add(0x5, "+2/3");
            g_ExposureComp.Add(0x4, "+1/2");
            g_ExposureComp.Add(0x3, "+1/3");
            g_ExposureComp.Add(0x0, "0");
            g_ExposureComp.Add(0xfd, "-1/3");
            g_ExposureComp.Add(0xfc, "-1/2");
            g_ExposureComp.Add(0xfb, "-2/3");
            g_ExposureComp.Add(0xf8, "-1");
            g_ExposureComp.Add(0xf5, "-1 1/3");
            g_ExposureComp.Add(0xf4, "-1 1/2");
            g_ExposureComp.Add(0xf3, "-1 2/3");
            g_ExposureComp.Add(0xf0, "-2");
            g_ExposureComp.Add(0xed, "-2 1/3");
            g_ExposureComp.Add(0xec, "-2 1/2");
            g_ExposureComp.Add(0xeb, "-2 2/3");
            g_ExposureComp.Add(0xe8, "-3");
            g_ExposureComp.Add(0xffffffff, "unkown");


            g_AEMode.Add(0, "P");
            g_AEMode.Add(1, "Tv");
            g_AEMode.Add(2, "Av");
            g_AEMode.Add(3, "M");
            g_AEMode.Add(4, "Bulb");
            g_AEMode.Add(5, "A-DEP");
            g_AEMode.Add(6, "Depth-of-Field AE");
            g_AEMode.Add(7, "Manual");
            g_AEMode.Add(8, "Lock");
            g_AEMode.Add(9, "GreenMode");
            g_AEMode.Add(10, "Night Scene Portrait");
            g_AEMode.Add(11, "Sports");
            g_AEMode.Add(12, "Portrait");
            g_AEMode.Add(13, "Landscape");
            g_AEMode.Add(14, "Close Up");
            g_AEMode.Add(15, "Flash Off");
            g_AEMode.Add(19, "CreativeAuto");
            g_AEMode.Add(20, "Movie");
            g_AEMode.Add(21, "PhotoInMovie");
            g_AEMode.Add(22, "SceneIntelligentAuto");
            g_AEMode.Add(25, "SCN");
            g_AEMode.Add(0xffffffff, "unknown");


            g_ISOSpeed.Add(0x0, "Auto");
            g_ISOSpeed.Add(0x28, "6");
            g_ISOSpeed.Add(0x30, "12");
            g_ISOSpeed.Add(0x38, "25");
            g_ISOSpeed.Add(0x40, "50");
            g_ISOSpeed.Add(0x48, "100");
            g_ISOSpeed.Add(0x4b, "125");
            g_ISOSpeed.Add(0x4d, "160");
            g_ISOSpeed.Add(0x50, "200");
            g_ISOSpeed.Add(0x53, "250");
            g_ISOSpeed.Add(0x55, "320");
            g_ISOSpeed.Add(0x58, "400");
            g_ISOSpeed.Add(0x5b, "500");
            g_ISOSpeed.Add(0x5d, "640");
            g_ISOSpeed.Add(0x60, "800");
            g_ISOSpeed.Add(0x63, "1000");
            g_ISOSpeed.Add(0x65, "1250");
            g_ISOSpeed.Add(0x68, "1600");
            g_ISOSpeed.Add(0x6b, "2000");
            g_ISOSpeed.Add(0x6d, "2500");
            g_ISOSpeed.Add(0x70, "3200");
            g_ISOSpeed.Add(0x73, "4000");
            g_ISOSpeed.Add(0x75, "5000");
            g_ISOSpeed.Add(0x78, "6400");
            g_ISOSpeed.Add(0x7b, "8000");
            g_ISOSpeed.Add(0x7d, "10000");
            g_ISOSpeed.Add(0x80, "12800");
            g_ISOSpeed.Add(0x83, "16000");
            g_ISOSpeed.Add(0x85, "20000");
            g_ISOSpeed.Add(0x88, "25600");
            g_ISOSpeed.Add(0x8b, "32000");
            g_ISOSpeed.Add(0x8d, "40000");
            g_ISOSpeed.Add(0x90, "51200");
            g_ISOSpeed.Add(0x98, "102400");
            g_ISOSpeed.Add(0xffffffff, "unknown");


            g_Av.Add(0x8, "1");
            g_Av.Add(0xb, "1.1");
            g_Av.Add(0xc, "1.2");
            g_Av.Add(0xd, "1.2");
            g_Av.Add(0x10, "1.4");
            g_Av.Add(0x13, "1.6");
            g_Av.Add(0x14, "1.8");
            g_Av.Add(0x15, "1.8");
            g_Av.Add(0x18, "2");
            g_Av.Add(0x1b, "2.2");
            g_Av.Add(0x1c, "2.5");
            g_Av.Add(0x1d, "2.5");
            g_Av.Add(0x20, "2.8");
            g_Av.Add(0x23, "3.2");
            g_Av.Add(0x24, "3.5");
            g_Av.Add(0x25, "3.5");
            g_Av.Add(0x28, "4");
            g_Av.Add(0x2b, "4");
            g_Av.Add(0x2c, "4.5");
            g_Av.Add(0x2d, "5.6");
            g_Av.Add(0x30, "5.6");
            g_Av.Add(0x33, "6.3");
            g_Av.Add(0x34, "6.7");
            g_Av.Add(0x35, "7.1");
            g_Av.Add(0x38, "8");
            g_Av.Add(0x3b, "9");
            g_Av.Add(0x3c, "9.5");
            g_Av.Add(0x3d, "10");
            g_Av.Add(0x40, "11");
            g_Av.Add(0x43, "13");
            g_Av.Add(0x44, "13");
            g_Av.Add(0x45, "14");
            g_Av.Add(0x48, "16");
            g_Av.Add(0x4b, "18");
            g_Av.Add(0x4c, "19");
            g_Av.Add(0x4d, "20");
            g_Av.Add(0x50, "22");
            g_Av.Add(0x53, "25");
            g_Av.Add(0x54, "27");
            g_Av.Add(0x55, "29");
            g_Av.Add(0x58, "32");
            g_Av.Add(0x5b, "36");
            g_Av.Add(0x5c, "38");
            g_Av.Add(0x5d, "40");
            g_Av.Add(0x60, "45");
            g_Av.Add(0x63, "51");
            g_Av.Add(0x64, "54");
            g_Av.Add(0x65, "57");
            g_Av.Add(0x68, "64");
            g_Av.Add(0x6b, "72");
            g_Av.Add(0x6c, "76");
            g_Av.Add(0x6d, "80");
            g_Av.Add(0x70, "91");
            g_Av.Add(0xffffffff, "unknown");


            g_Tv.Add(0x10, "30\"");
            g_Tv.Add(0x13, "25\"");
            g_Tv.Add(0x14, "20\"");
            g_Tv.Add(0x15, "20\"");
            g_Tv.Add(0x18, "15\"");
            g_Tv.Add(0x1b, "13\"");
            g_Tv.Add(0x1c, "10\"");
            g_Tv.Add(0x1d, "10\"");
            g_Tv.Add(0x20, "8\"");
            g_Tv.Add(0x23, "6\"");
            g_Tv.Add(0x24, "6\"");
            g_Tv.Add(0x25, "5\"");
            g_Tv.Add(0x28, "4\"");
            g_Tv.Add(0x2b, "3\"" + "2");
            g_Tv.Add(0x2c, "3\"");
            g_Tv.Add(0x2d, "2\"" + "5");
            g_Tv.Add(0x30, "2\"");
            g_Tv.Add(0x33, "1\"" + "6");
            g_Tv.Add(0x34, "1\"" + "5");
            g_Tv.Add(0x35, "1\"" + "3");
            g_Tv.Add(0x38, "1\"");
            g_Tv.Add(0x3b, "0\"" + "8");
            g_Tv.Add(0x3c, "0\"" + "7");
            g_Tv.Add(0x3d, "0\"" + "6");
            g_Tv.Add(0x40, "0\"" + "5");
            g_Tv.Add(0x43, "0\"" + "4");
            g_Tv.Add(0x44, "0\"" + "3");
            g_Tv.Add(0x45, "0\"" + "3");
            g_Tv.Add(0x48, "4");
            g_Tv.Add(0x4b, "5");
            g_Tv.Add(0x4c, "6");
            g_Tv.Add(0x4d, "6");
            g_Tv.Add(0x50, "8");
            g_Tv.Add(0x53, "10");
            g_Tv.Add(0x54, "10");
            g_Tv.Add(0x55, "13");
            g_Tv.Add(0x58, "15");
            g_Tv.Add(0x5b, "20");
            g_Tv.Add(0x5c, "20");
            g_Tv.Add(0x5d, "25");
            g_Tv.Add(0x60, "30");
            g_Tv.Add(0x63, "40");
            g_Tv.Add(0x64, "45");
            g_Tv.Add(0x65, "50");
            g_Tv.Add(0x68, "60");
            g_Tv.Add(0x6b, "80");
            g_Tv.Add(0x6c, "90");
            g_Tv.Add(0x6d, "100");
            g_Tv.Add(0x70, "125");
            g_Tv.Add(0x73, "160");
            g_Tv.Add(0x74, "180");
            g_Tv.Add(0x75, "200");
            g_Tv.Add(0x78, "250");
            g_Tv.Add(0x7b, "320");
            g_Tv.Add(0x7c, "350");
            g_Tv.Add(0x7d, "400");
            g_Tv.Add(0x80, "500");
            g_Tv.Add(0x83, "640");
            g_Tv.Add(0x84, "750");
            g_Tv.Add(0x85, "800");
            g_Tv.Add(0x88, "1000");
            g_Tv.Add(0x8b, "1250");
            g_Tv.Add(0x8c, "1500");
            g_Tv.Add(0x8d, "1600");
            g_Tv.Add(0x90, "2000");
            g_Tv.Add(0x93, "2500");
            g_Tv.Add(0x94, "3000");
            g_Tv.Add(0x95, "3200");
            g_Tv.Add(0x98, "4000");
            g_Tv.Add(0x9b, "5000");
            g_Tv.Add(0x9c, "6000");
            g_Tv.Add(0x9d, "6400");
            g_Tv.Add(0xa0, "8000");
            g_Tv.Add(0xffffffff, "unknown");


            // PTP Camera
            g_ImageQuality.Add(0x64ff0f, "RAW");
            g_ImageQuality.Add(0x640013, "RAW + Large Fine Jpeg");
            g_ImageQuality.Add(0x640113, "RAW + Middle Fine Jpeg");
            g_ImageQuality.Add(0x640213, "RAW + Small Fine Jpeg");
            g_ImageQuality.Add(0x640012, "RAW + Large Normal Jpeg");
            g_ImageQuality.Add(0x640112, "RAW + Middle Normal Jpeg");
            g_ImageQuality.Add(0x640212, "RAW + Small Normal Jpeg");
            g_ImageQuality.Add(0x640e13, "RAW + Small1 Fine Jpeg");
            g_ImageQuality.Add(0x640e12, "RAW + Small1 Normal Jpeg");
            g_ImageQuality.Add(0x640f13, "RAW + Small2 Jpeg");
            g_ImageQuality.Add(0x641013, "RAW + Small3 Jpeg");

            g_ImageQuality.Add(0x640010, "RAW + Large Jpeg");
            g_ImageQuality.Add(0x640510, "RAW + Middle1 Jpeg");
            g_ImageQuality.Add(0x640610, "RAW + Middle2 Jpeg");
            g_ImageQuality.Add(0x640210, "RAW + Small Jpeg");

            g_ImageQuality.Add(0x164ff0f, "MRAW");
            g_ImageQuality.Add(0x1640013, "MRAW + Large Fine Jpeg");
            g_ImageQuality.Add(0x1640012, "MRAW + Large Normal Jpeg");
            g_ImageQuality.Add(0x1640113, "MRAW + Middle Fine Jpeg");
            g_ImageQuality.Add(0x1640112, "MRAW + Middle Normal Jpeg");
            g_ImageQuality.Add(0x1640213, "MRAW + Small Fine Jpeg");
            g_ImageQuality.Add(0x1640212, "MRAW + Small Normal Jpeg");
            g_ImageQuality.Add(0x1640e13, "MRAW + Small1 Fine Jpeg");
            g_ImageQuality.Add(0x1640e12, "MRAW + Small1 Normal Jpeg");
            g_ImageQuality.Add(0x1640f13, "MRAW + Small2 Jpeg");
            g_ImageQuality.Add(0x1641013, "MRAW + Small3 Jpeg");

            g_ImageQuality.Add(0x264ff0f, "SRAW");
            g_ImageQuality.Add(0x2640010, "SRAW + Large Jpegg");
            g_ImageQuality.Add(0x2640510, "SRAW + Middle1 Jpeg");
            g_ImageQuality.Add(0x2640610, "SRAW + Middle2 Jpeg");
            g_ImageQuality.Add(0x2640210, "SRAW + Small Jpeg");

            g_ImageQuality.Add(0x2640013, "SRAW + Large Fine Jpeg");
            g_ImageQuality.Add(0x2640012, "SRAW + Large Normal Jpeg");
            g_ImageQuality.Add(0x2640113, "SRAW + Middle Fine Jpeg");
            g_ImageQuality.Add(0x2640112, "SRAW + Middle Normal Jpeg");
            g_ImageQuality.Add(0x2640213, "SRAW + Small Fine Jpeg");
            g_ImageQuality.Add(0x2640212, "SRAW + Small Normal Jpeg");
            g_ImageQuality.Add(0x2640e13, "SRAW + Small1 Fine Jpeg");
            g_ImageQuality.Add(0x2640e12, "SRAW + Small1 Normal Jpeg");
            g_ImageQuality.Add(0x2640f13, "SRAW + Small2 Jpeg");
            g_ImageQuality.Add(0x2641013, "SRAW + Small3 Jpeg");

            g_ImageQuality.Add(0x13ff0f, "Large Fine Jpeg");
            g_ImageQuality.Add(0x12ff0f, "Large Normal Jpeg");
            g_ImageQuality.Add(0x113ff0f, "Middle Fine Jpeg");
            g_ImageQuality.Add(0x112ff0f, "Middle Normal Jpeg");
            g_ImageQuality.Add(0x213ff0f, "Small Fine Jpeg");
            g_ImageQuality.Add(0x212ff0f, "Small Normal Jpeg");
            g_ImageQuality.Add(0xe13ff0f, "Small1 Fine Jpeg");
            g_ImageQuality.Add(0xe12ff0f, "Small1 Normal Jpeg");
            g_ImageQuality.Add(0xf13ff0f, "Small2 Jpeg");
            g_ImageQuality.Add(0x1013ff0f, "Small3 Jpeg");

            g_ImageQuality.Add(0x10ff0f, "Large Jpeg");
            g_ImageQuality.Add(0x510ff0f, "Middle1 Jpeg");
            g_ImageQuality.Add(0x610ff0f, "Middle2 Jpeg");
            g_ImageQuality.Add(0x210ff0f, "Small Jpeg");

            // Legacy Camera
            g_ImageQuality.Add(0x240000, "RAW");
            g_ImageQuality.Add(0x240013, "RAW + Large Fine Jpeg");
            g_ImageQuality.Add(0x240113, "RAW + Middle Fine Jpeg");
            g_ImageQuality.Add(0x240213, "RAW + Small Fine Jpeg");
            g_ImageQuality.Add(0x240012, "RAW + Large Normal Jpeg");
            g_ImageQuality.Add(0x240112, "RAW + Middle Normal Jpeg");
            g_ImageQuality.Add(0x240212, "RAW + Small Normal Jpeg");
            g_ImageQuality.Add(0x130000, "Large Fine Jpeg");
            g_ImageQuality.Add(0x1130000, "Middle Fine Jpeg");
            g_ImageQuality.Add(0x2130000, "Small Fine Jpeg");
            g_ImageQuality.Add(0x120000, "Large Normal Jpeg");
            g_ImageQuality.Add(0x1120000, "Middle Normal Jpeg");
            g_ImageQuality.Add(0x2120000, "Small Normal Jpeg");

            g_ImageQuality.Add(0x2f000f, "RAW");
            g_ImageQuality.Add(0x2f001f, "RAW + Large Jpeg");
            g_ImageQuality.Add(0x2f051f, "RAW + Middle1 Jpeg");
            g_ImageQuality.Add(0x2f061f, "RAW + Middle2 Jpeg");
            g_ImageQuality.Add(0x2f021f, "RAW + Small Jpeg");
            g_ImageQuality.Add(0x1f000f, "Large Jpeg");
            g_ImageQuality.Add(0x51f000f, "Middle1 Jpeg");
            g_ImageQuality.Add(0x61f000f, "Middle2 Jpeg");
            g_ImageQuality.Add(0x21f000f, "Small Jpeg");

            g_PropList.Add(EDSDKTypes.kEdsPropID_AEModeSelect, g_AEMode);
            g_PropList.Add(EDSDKTypes.kEdsPropID_ISOSpeed, g_ISOSpeed);
            g_PropList.Add(EDSDKTypes.kEdsPropID_Av, g_Av);
            g_PropList.Add(EDSDKTypes.kEdsPropID_Tv, g_Tv);
            g_PropList.Add(EDSDKTypes.kEdsPropID_MeteringMode, g_MeteringMode);
            g_PropList.Add(EDSDKTypes.kEdsPropID_ExposureCompensation, g_ExposureComp);
            g_PropList.Add(EDSDKTypes.kEdsPropID_ImageQuality, g_ImageQuality);

        }


    }
}