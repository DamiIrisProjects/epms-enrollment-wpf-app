﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class VBSample : System.Windows.Forms.Form, Observer
    {

        #region "Created by Windows form designer."

        public VBSample()
            : base()
        {
            FormClosing += VBSample_FormClosing;
            Load += VBSample_Load;

            InitializeComponent();


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if ((components != null))
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }


        private System.ComponentModel.IContainer components;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label6;
        private System.Windows.Forms.Button withEventsField_TakeBtn;
        internal System.Windows.Forms.Button TakeBtn
        {
            get { return withEventsField_TakeBtn; }
            set
            {
                if (withEventsField_TakeBtn != null)
                {
                    withEventsField_TakeBtn.Click -= TakeBtn_Click;
                }
                withEventsField_TakeBtn = value;
                if (withEventsField_TakeBtn != null)
                {
                    withEventsField_TakeBtn.Click += TakeBtn_Click;
                }
            }
        }
        private System.Windows.Forms.ComboBox withEventsField_ISOSpeedCmb;
        internal System.Windows.Forms.ComboBox ISOSpeedCmb
        {
            get { return withEventsField_ISOSpeedCmb; }
            set
            {
                if (withEventsField_ISOSpeedCmb != null)
                {
                    withEventsField_ISOSpeedCmb.SelectionChangeCommitted -= ISOSpeedCmb_SelectionChangeCommitted;
                }
                withEventsField_ISOSpeedCmb = value;
                if (withEventsField_ISOSpeedCmb != null)
                {
                    withEventsField_ISOSpeedCmb.SelectionChangeCommitted += ISOSpeedCmb_SelectionChangeCommitted;
                }
            }
        }
        private System.Windows.Forms.ComboBox withEventsField_AvCmb;
        internal System.Windows.Forms.ComboBox AvCmb
        {
            get { return withEventsField_AvCmb; }
            set
            {
                if (withEventsField_AvCmb != null)
                {
                    withEventsField_AvCmb.SelectionChangeCommitted -= AvCmb_SelectionChangeCommitted;
                }
                withEventsField_AvCmb = value;
                if (withEventsField_AvCmb != null)
                {
                    withEventsField_AvCmb.SelectionChangeCommitted += AvCmb_SelectionChangeCommitted;
                }
            }
        }
        private System.Windows.Forms.ComboBox withEventsField_TvCmb;
        internal System.Windows.Forms.ComboBox TvCmb
        {
            get { return withEventsField_TvCmb; }
            set
            {
                if (withEventsField_TvCmb != null)
                {
                    withEventsField_TvCmb.SelectionChangeCommitted -= TvCmb_SelectionChangeCommitted;
                }
                withEventsField_TvCmb = value;
                if (withEventsField_TvCmb != null)
                {
                    withEventsField_TvCmb.SelectionChangeCommitted += TvCmb_SelectionChangeCommitted;
                }
            }
        }
        private System.Windows.Forms.ComboBox withEventsField_AEModeCmb;
        private System.Windows.Forms.ComboBox AEModeCmb
        {
            get { return withEventsField_AEModeCmb; }
            set
            {
                if (withEventsField_AEModeCmb != null)
                {
                    withEventsField_AEModeCmb.SelectionChangeCommitted -= AEModeCmb_SelectionChangeCommitted;
                }
                withEventsField_AEModeCmb = value;
                if (withEventsField_AEModeCmb != null)
                {
                    withEventsField_AEModeCmb.SelectionChangeCommitted += AEModeCmb_SelectionChangeCommitted;
                }
            }
        }
        private System.Windows.Forms.ComboBox withEventsField_MeteringModeCmb;
        internal System.Windows.Forms.ComboBox MeteringModeCmb
        {
            get { return withEventsField_MeteringModeCmb; }
            set
            {
                if (withEventsField_MeteringModeCmb != null)
                {
                    withEventsField_MeteringModeCmb.SelectionChangeCommitted -= MeteringModeCmb_SelectionChangeCommitted;
                }
                withEventsField_MeteringModeCmb = value;
                if (withEventsField_MeteringModeCmb != null)
                {
                    withEventsField_MeteringModeCmb.SelectionChangeCommitted += MeteringModeCmb_SelectionChangeCommitted;
                }
            }
        }
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.ComboBox withEventsField_ExposureCompCmb;
        internal System.Windows.Forms.ComboBox ExposureCompCmb
        {
            get { return withEventsField_ExposureCompCmb; }
            set
            {
                if (withEventsField_ExposureCompCmb != null)
                {
                    withEventsField_ExposureCompCmb.SelectionChangeCommitted -= ExposureCompCmb_SelectionChangeCommitted;
                }
                withEventsField_ExposureCompCmb = value;
                if (withEventsField_ExposureCompCmb != null)
                {
                    withEventsField_ExposureCompCmb.SelectionChangeCommitted += ExposureCompCmb_SelectionChangeCommitted;
                }
            }
        }
        internal System.Windows.Forms.Label Label4;
        private System.Windows.Forms.Button withEventsField_ExitBtn;
        internal System.Windows.Forms.Button ExitBtn
        {
            get { return withEventsField_ExitBtn; }
            set
            {
                if (withEventsField_ExitBtn != null)
                {
                    withEventsField_ExitBtn.Click -= ExitBtn_Click;
                }
                withEventsField_ExitBtn = value;
                if (withEventsField_ExitBtn != null)
                {
                    withEventsField_ExitBtn.Click += ExitBtn_Click;
                }
            }
        }
        internal System.Windows.Forms.TextBox InfoTextBox;
        private System.Windows.Forms.ComboBox withEventsField_ImageQualityCmb;
        internal System.Windows.Forms.ComboBox ImageQualityCmb
        {
            get { return withEventsField_ImageQualityCmb; }
            set
            {
                if (withEventsField_ImageQualityCmb != null)
                {
                    withEventsField_ImageQualityCmb.SelectionChangeCommitted -= ImageQualityCmb_SelectionChangeCommitted;
                }
                withEventsField_ImageQualityCmb = value;
                if (withEventsField_ImageQualityCmb != null)
                {
                    withEventsField_ImageQualityCmb.SelectionChangeCommitted += ImageQualityCmb_SelectionChangeCommitted;
                }
            }
        }
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.ProgressBar progressBar;
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.TakeBtn = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.AEModeCmb = new System.Windows.Forms.ComboBox();
            this.ISOSpeedCmb = new System.Windows.Forms.ComboBox();
            this.AvCmb = new System.Windows.Forms.ComboBox();
            this.TvCmb = new System.Windows.Forms.ComboBox();
            this.MeteringModeCmb = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.ExposureCompCmb = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.InfoTextBox = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.ImageQualityCmb = new System.Windows.Forms.ComboBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            //
            //TakeBtn
            //
            this.TakeBtn.Location = new System.Drawing.Point(297, 148);
            this.TakeBtn.Name = "TakeBtn";
            this.TakeBtn.Size = new System.Drawing.Size(96, 48);
            this.TakeBtn.TabIndex = 0;
            this.TakeBtn.Text = "Take Picture";
            //
            //Label2
            //
            this.Label2.AutoSize = true;
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(12, 19);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(53, 12);
            this.Label2.TabIndex = 6;
            this.Label2.Text = "AE Mode:";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //Label3
            //
            this.Label3.AutoSize = true;
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(12, 115);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(25, 12);
            this.Label3.TabIndex = 7;
            this.Label3.Text = "ISO:";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //Label5
            //
            this.Label5.AutoSize = true;
            this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label5.Location = new System.Drawing.Point(12, 83);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(21, 12);
            this.Label5.TabIndex = 9;
            this.Label5.Text = "Av:";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //Label6
            //
            this.Label6.AutoSize = true;
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label6.Location = new System.Drawing.Point(12, 51);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(20, 12);
            this.Label6.TabIndex = 10;
            this.Label6.Text = "Tv:";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //AEModeCmb
            //
            this.AEModeCmb.Location = new System.Drawing.Point(115, 16);
            this.AEModeCmb.Name = "AEModeCmb";
            this.AEModeCmb.Size = new System.Drawing.Size(166, 20);
            this.AEModeCmb.TabIndex = 11;
            //
            //ISOSpeedCmb
            //
            this.ISOSpeedCmb.Location = new System.Drawing.Point(115, 112);
            this.ISOSpeedCmb.Name = "ISOSpeedCmb";
            this.ISOSpeedCmb.Size = new System.Drawing.Size(166, 20);
            this.ISOSpeedCmb.TabIndex = 12;
            //
            //AvCmb
            //
            this.AvCmb.Location = new System.Drawing.Point(115, 80);
            this.AvCmb.Name = "AvCmb";
            this.AvCmb.Size = new System.Drawing.Size(166, 20);
            this.AvCmb.TabIndex = 14;
            //
            //TvCmb
            //
            this.TvCmb.Location = new System.Drawing.Point(115, 48);
            this.TvCmb.Name = "TvCmb";
            this.TvCmb.Size = new System.Drawing.Size(166, 20);
            this.TvCmb.TabIndex = 15;
            //
            //MeteringModeCmb
            //
            this.MeteringModeCmb.Location = new System.Drawing.Point(115, 143);
            this.MeteringModeCmb.Name = "MeteringModeCmb";
            this.MeteringModeCmb.Size = new System.Drawing.Size(166, 20);
            this.MeteringModeCmb.TabIndex = 17;
            //
            //Label1
            //
            this.Label1.AutoSize = true;
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(12, 146);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(82, 12);
            this.Label1.TabIndex = 16;
            this.Label1.Text = "Metering Mode:";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //ExposureCompCmb
            //
            this.ExposureCompCmb.Location = new System.Drawing.Point(115, 176);
            this.ExposureCompCmb.Name = "ExposureCompCmb";
            this.ExposureCompCmb.Size = new System.Drawing.Size(166, 20);
            this.ExposureCompCmb.TabIndex = 19;
            //
            //Label4
            //
            this.Label4.AutoSize = true;
            this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label4.Location = new System.Drawing.Point(12, 179);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(87, 12);
            this.Label4.TabIndex = 18;
            this.Label4.Text = "Exposure Comp:";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //ExitBtn
            //
            this.ExitBtn.Location = new System.Drawing.Point(313, 16);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(80, 32);
            this.ExitBtn.TabIndex = 4;
            this.ExitBtn.Text = "Quit";
            //
            //InfoTextBox
            //
            this.InfoTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.InfoTextBox.Location = new System.Drawing.Point(12, 246);
            this.InfoTextBox.Name = "InfoTextBox";
            this.InfoTextBox.Size = new System.Drawing.Size(381, 19);
            this.InfoTextBox.TabIndex = 20;
            this.InfoTextBox.Text = "Info";
            //
            //progressBar
            //
            this.progressBar.Location = new System.Drawing.Point(297, 215);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(96, 16);
            this.progressBar.TabIndex = 21;
            //
            //ImageQualityCmb
            //
            this.ImageQualityCmb.Location = new System.Drawing.Point(115, 211);
            this.ImageQualityCmb.Name = "ImageQualityCmb";
            this.ImageQualityCmb.Size = new System.Drawing.Size(166, 20);
            this.ImageQualityCmb.TabIndex = 22;
            //
            //Label7
            //
            this.Label7.AutoSize = true;
            this.Label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label7.Location = new System.Drawing.Point(12, 214);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(73, 12);
            this.Label7.TabIndex = 23;
            this.Label7.Text = "ImageQuality:";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //VBSample
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 12);
            this.ClientSize = new System.Drawing.Size(405, 276);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.ImageQualityCmb);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.InfoTextBox);
            this.Controls.Add(this.ExposureCompCmb);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.MeteringModeCmb);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.TvCmb);
            this.Controls.Add(this.AvCmb);
            this.Controls.Add(this.ISOSpeedCmb);
            this.Controls.Add(this.AEModeCmb);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.TakeBtn);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MaximizeBox = false;
            this.Name = "VBSample";
            this.Text = "VBSample";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        #region "User defined attributes"

        // Save as class variable, new delegates of event handlers.
        public EdsPropertyEventHandler inPropertyEventHandler = new EdsPropertyEventHandler(CameraEventListener.handlePropertyEvent);
        public EdsObjectEventHandler inObjectEventHandler = new EdsObjectEventHandler(CameraEventListener.handleObjectEvent);
        public EdsStateEventHandler inStateEventHandler = new EdsStateEventHandler(CameraEventListener.handleStateEvent);
        //
        public static CameraController controller;
        public static CameraModel model;

        public static Hashtable m_cmbTbl = new Hashtable();


        #endregion


        public delegate void UpdateDelegate(Observable @from, int msg, int data);


        public void UpdateWindow(Observable @from, int msg, int data)
        {
            //Get the name of this thread.
            string threadName = System.Threading.Thread.CurrentThread.Name();

            //// Make this form be able to be updated by other thread.
            if (InvokeRequired)
            {
                //Create UpdateDelegate
                UpdateDelegate dlg = new UpdateDelegate(UpdateWindow);
                try
                {
                    BeginInvoke(dlg, new object[] {
					@from,
					msg,
					data
				});
                }
                catch (Exception e)
                {
                    return;
                }
                finally
                {
                }
                return;
            }



            switch (msg)
            {
                case prog:
                    ////Progress of image downloading .
                    progressBar.Value = data;

                    break;
                case strt:
                    //// Start downloading.
                    break;
                ////_progress.SetPos(0);

                case cplt:
                    //// Complete downloading.
                    progressBar.Value = 0;

                    break;
                case updt:
                    //// Update properties.
                    int propertyID = data;
                    int propData = model.getPropertyUInt32(propertyID);
                    UpdateProperty(propertyID, propData);

                    break;
                case upls:
                    //// Update an available property list.
                    int propertyID = data;
                    EdsPropertyDesc desc = model.getPropertyDesc(propertyID);
                    UpdatePropertyDesc(propertyID, desc);

                    break;
                case warn:
                    //// Warning
                    InfoTextBox.Text = "Device Busy";

                    break;
                case errr:
                    //// Error
                    //// Nothing to do because the first getting property from model 30D is sure to fail. 
                    string ss = null;
                    ss = string.Format("%x", data);
                    InfoTextBox.Text = ss;

                    break;
                case clse:
                    //// Close
                    TakeBtn.Enabled = false;
                    progressBar.Enabled = false;
                    InfoTextBox.Enabled = false;
                    AEModeCmb.Enabled = false;
                    TvCmb.Enabled = false;
                    AvCmb.Enabled = false;
                    ISOSpeedCmb.Enabled = false;

                    break;
            }

            if (msg != errr & msg != warn)
            {
                InfoTextBox.Text = "";
            }


        }
        void Observer.update(Observable @from, int msg, int data)
        {
            UpdateWindow(@from, msg, data);
        }



        public void UpdateProperty(int propertyID, int data)
        {
            Hashtable propList = CameraProperty.g_PropList.Item(propertyID);
            switch (propertyID)
            {
                case kEdsPropID_AEModeSelect:
                    AEModeCmb.Text = propList[data];
                    break;
                case kEdsPropID_ISOSpeed:
                    ISOSpeedCmb.Text = propList[data];
                    break;
                case kEdsPropID_MeteringMode:
                    MeteringModeCmb.Text = propList[data];
                    break;
                case kEdsPropID_Av:
                    AvCmb.Text = propList[data];
                    break;
                case kEdsPropID_Tv:
                    TvCmb.Text = propList[data];
                    break;
                case kEdsPropID_ExposureCompensation:
                    ExposureCompCmb.Text = propList[data];
                    break;
                case kEdsPropID_ImageQuality:
                    ImageQualityCmb.Text = propList[data];
                    break;
            }

        }



        public void UpdatePropertyDesc(int propertyID, EdsPropertyDesc desc)
        {
            int err = 0;
            int iCnt = 0;
            ComboBox cmb = m_cmbTbl[propertyID];
            Hashtable propList = CameraProperty.g_PropList.Item(propertyID);
            string propStr = null;
            ArrayList propValueList = new ArrayList();

            if (cmb == null)
            {
                return;
            }

            cmb.BeginUpdate();
            cmb.Items.Clear();
            for (iCnt = 0; iCnt <= desc.numElements - 1; iCnt++)
            {
                propStr = propList[desc.propDesc(iCnt)];
                if (propStr != null)
                {
                    err = cmb.Items.Add(propStr);
                    propValueList.Add(desc.propDesc(iCnt));
                }
            }

            cmb.Tag = propValueList;
            // Set the property value list

            cmb.EndUpdate();
            if (cmb.Items.Count == 0)
            {
                cmb.Enabled = false;
                //// No available item.
            }
            else
            {
                cmb.Enabled = true;
            }

        }



        public CameraModel cameraModelFactory(IntPtr camera, EdsDeviceInfo deviceInfo)
        {

            // if Legacy protocol.
            if (deviceInfo.DeviceSubType == 0)
            {
                return new CameraModelLegacy(camera);
            }

            // PTP protocol.
            return new CameraModel(camera);

        }



        #region "Window Events"
        private void TakeBtn_Click(System.Object sender, System.EventArgs e)
        {
            //
            // Release button
            //
            controller.actionPerformed("takepicture");

        }



        private void ExitBtn_Click(System.Object sender, System.EventArgs e)
        {
            // Quit button
            this.Close();

            System.Environment.Exit(0);
        }

        #endregion



        private void VBSample_Load(System.Object sender, System.EventArgs e)
        {
            int err = EDS_ERR_OK;
            IntPtr cameraList = null;
            IntPtr camera = null;
            int count = 0;
            bool isSDKLoaded = false;
            CameraProperty propObj = new CameraProperty();

            // connect property id to combobox. 
            m_cmbTbl.Add(kEdsPropID_AEMode, this.AEModeCmb);
            m_cmbTbl.Add(kEdsPropID_ISOSpeed, this.ISOSpeedCmb);
            m_cmbTbl.Add(kEdsPropID_Av, this.AvCmb);
            m_cmbTbl.Add(kEdsPropID_Tv, this.TvCmb);
            m_cmbTbl.Add(kEdsPropID_MeteringMode, this.MeteringModeCmb);
            m_cmbTbl.Add(kEdsPropID_ExposureCompensation, this.ExposureCompCmb);
            m_cmbTbl.Add(kEdsPropID_ImageQuality, this.ImageQualityCmb);


            err = EdsInitializeSDK();



            if (err == EDS_ERR_OK)
            {
                isSDKLoaded = true;

            }


            if (err == EDS_ERR_OK)
            {
                err = EdsGetCameraList(cameraList);

            }



            if (err == EDS_ERR_OK)
            {
                err = EdsGetChildCount(cameraList, count);
                if (count == 0)
                {
                    err = EDS_ERR_DEVICE_NOT_FOUND;
                }

            }

            //// Get the first camera.

            if (err == EDS_ERR_OK)
            {
                err = EdsGetChildAtIndex(cameraList, 0, camera);

            }


            EdsDeviceInfo deviceInfo = null;


            if (err == EDS_ERR_OK)
            {
                err = EdsGetDeviceInfo(camera, deviceInfo);

                if (err == EDS_ERR_OK & (camera == null) == true)
                {
                    err = EDS_ERR_DEVICE_NOT_FOUND;
                }

            }



            if ((cameraList == null) == false)
            {
                EdsRelease(cameraList);

            }


            //// Create the camera model 

            if (err == EDS_ERR_OK)
            {
                model = cameraModelFactory(camera, deviceInfo);

                if ((model == null) == true)
                {
                    err = EDS_ERR_DEVICE_NOT_FOUND;

                }
            }



            if (err != EDS_ERR_OK)
            {
                MessageBox.Show("Cannot detect camera");

            }




            if (err == EDS_ERR_OK)
            {
                //// Create a controller
                controller = new CameraController();

                //// Set the model to this controller.
                controller.setCameraModel(model);

                //// Make notify the model updating to the view.
                model.addObserver(this);

                // ------------------------------------------------------------------------
                // ------------------------------------------------------------------------
                // You should create class instance of delegates of event handlers 
                // with 'new' expressly if its attribute is Shared, 
                // because System sometimes do garbage-collect these delegates.
                //
                //
                // This error occurs.
                //
                // CallbackOnCollectedDelegate is detected.
                // Message: Callback was called with
                // garbage-collected delegate of  
                // Type() 'VBSample3!VBSample3.EDSDKTypes+EdsPropertyEventHandler::Invoke' 
                // 
                // It will be able to make data loss or application clash.
                // You should stock delegates when you want to send delegate to unmanaged code.
                //
                // ------------------------------------------------------------------------

                if (err == EDS_ERR_OK)
                {
                    err = EdsSetPropertyEventHandler(camera, kEdsPropertyEvent_All, inPropertyEventHandler, IntPtr.Zero);
                }

                //// Set ObjectEventHandler
                if (err == EDS_ERR_OK)
                {
                    err = EdsSetObjectEventHandler(camera, kEdsObjectEvent_All, inObjectEventHandler, IntPtr.Zero);

                }

                //// Set StateEventHandler
                if (err == EDS_ERR_OK)
                {
                    err = EdsSetCameraStateEventHandler(camera, kEdsStateEvent_All, inStateEventHandler, IntPtr.Zero);
                }

            }




            if (err != EDS_ERR_OK)
            {
                if ((camera == null) == false)
                {
                    EdsRelease(camera);
                    camera = null;
                }

                if ((isSDKLoaded))
                {
                    EdsTerminateSDK();
                }

                if ((model == null) == false)
                {
                    model = null;
                }


                if ((controller == null) == false)
                {
                    controller = null;
                }


                System.Environment.Exit(0);

            }



            ////Execute the controller.
            controller.run();

        }


        private void VBSample_FormClosing(System.Object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            controller.actionPerformed("close");

            if ((model == null))
            {
                if ((model.getCameraObject() == null) == false)
                {
                    EdsRelease(model.getCameraObject());
                }
            }

            EdsTerminateSDK();

        }

        private void AEModeCmb_SelectionChangeCommitted(System.Object sender, System.EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            // "sender" is the combobox
            ArrayList propValueList = (ArrayList)cmb.Tag;
            int data = propValueList[cmb.SelectedIndex];

            controller.actionPerformed("set", kEdsPropID_AEMode, data);

        }

        private void TvCmb_SelectionChangeCommitted(System.Object sender, System.EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            // "sender" is the combobox
            ArrayList propValueList = (ArrayList)cmb.Tag;
            int data = propValueList[cmb.SelectedIndex];
            int id = 0;

            //If cmb.Equals(Me.AEModeCmb) Then
            //    id = kEdsPropID_AEMode
            //ElseIf cmb.Equals(Me.TvCmb) Then
            //    id = kEdsPropID_Tv
            //ElseIf cmb.Equals(Me.AvCmb) Then
            //    id = kEdsPropID_Av
            //ElseIf cmb.Equals(Me.ISOSpeedCmb) Then
            //    id = kEdsPropID_ISOSpeed
            //ElseIf cmb.Equals(Me.MeteringModeCmb) Then
            //    id = kEdsPropID_MeteringMode
            //ElseIf cmb.Equals(Me.ExposureCompCmb) Then
            //    id = kEdsPropID_ExposureCompensation
            //Else
            //    Console.WriteLine("What's this?")
            //End If

            id = kEdsPropID_Tv;
            controller.actionPerformed("set", id, data);

        }

        private void AvCmb_SelectionChangeCommitted(System.Object sender, System.EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            // "sender" is the combobox
            ArrayList propValueList = (ArrayList)cmb.Tag;
            int data = propValueList[cmb.SelectedIndex];

            controller.actionPerformed("set", kEdsPropID_Av, data);

        }

        private void ISOSpeedCmb_SelectionChangeCommitted(System.Object sender, System.EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            // "sender" is the combobox
            ArrayList propValueList = (ArrayList)cmb.Tag;
            int data = propValueList[cmb.SelectedIndex];

            controller.actionPerformed("set", kEdsPropID_ISOSpeed, data);

        }


        private void MeteringModeCmb_SelectionChangeCommitted(System.Object sender, System.EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            // "sender" is the combobox
            ArrayList propValueList = (ArrayList)cmb.Tag;
            int data = propValueList[cmb.SelectedIndex];

            controller.actionPerformed("set", kEdsPropID_MeteringMode, data);

        }

        private void ExposureCompCmb_SelectionChangeCommitted(System.Object sender, System.EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            // "sender" is the combobox
            ArrayList propValueList = (ArrayList)cmb.Tag;
            int data = propValueList[cmb.SelectedIndex];

            controller.actionPerformed("set", kEdsPropID_ExposureCompensation, data);

        }



        private void ImageQualityCmb_SelectionChangeCommitted(System.Object sender, System.EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            // "sender" is the combobox
            ArrayList propValueList = (ArrayList)cmb.Tag;
            int data = propValueList[cmb.SelectedIndex];

            controller.actionPerformed("set", kEdsPropID_ImageQuality, data);

        }

    }

}