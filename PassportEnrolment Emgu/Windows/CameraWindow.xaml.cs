﻿using EDSDKLib;
using Luxand;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PassportEnrolment.Windows
{
    /// <summary>
    /// Interaction logic for CameraWindow.xaml
    /// </summary>
    public partial class CameraWindow : Window
    {
        #region Variables

        int whichCamera = 0;

        // EOS
        SDKHandler CameraHandler;
        List<int> AvList;
        List<int> TvList;
        List<int> ISOList;
        List<Camera> CamList;
        bool IsInit = false;
        int BulbTime = 30;
        ImageSource EvfImage;
        JpegBitmapDecoder dec;
        ImageBrush bgbrush = new ImageBrush();
        ThreadStart SetImageAction;
        bool EOSended = false;
        bool UseFacialRecog = true;

        // Face Recognition
        String cameraName;
        Thread CameraThread;
        int tracker = 0;
        int cameraHandle = 0;
        bool needClose = false;
        bool garbageCollected = false;
        public delegate void UpdateImageDel(BitmapSource src);

        public Bitmap Photo { get; set; }
        public bool PictureTaken; 

	    #endregion

        #region Constructor

        public CameraWindow(bool usefacialrecog)
        {
            InitializeComponent();

            // USB cam
            string[] cameraList;
            int USBcamcount;
            FSDKCam.GetCameraList(out cameraList, out USBcamcount);
            cameraName = cameraList[0]; // choose the first camera
            
            // EOS Camera
            CameraHandler = new SDKHandler();
            UseFacialRecog = usefacialrecog;
            CamList = CameraHandler.GetCameraList();

            if (USBcamcount == 0 && CamList.Count == 0)
            {
                MessageBox.Show("Please attach a camera");
            }
            else
            {
                FSDKCam.VideoFormatInfo[] formatList = null;

                if (UseFacialRecog)
                {
                    // Check that face recog SDK is valid
                    if (FSDK.FSDKE_OK != FSDK.ActivateLibrary("NxLq2hTLcLbL7xD11GUIOcRezUQKSIx4Y0cPOJaztQdnCYV52csmELrQJ41w1L33z25oSvb5CMEjSqRlWm2Fod9BmhJzq4PV2nM9lUqhMNgTBez1oVWRlcV2H4Ezg7fWFZCqF7m+ekBxE7Wc5sQpS2L69PxwvzZ+SpEfrRl4gzM="))
                    {
                        MessageBox.Show("Please run the License Key Wizard (Start - Luxand - FaceSDK - License Key Wizard)");
                    }

                    FSDK.InitializeLibrary();
                    FSDKCam.InitializeCapturing();

                    FSDKCam.GetVideoFormatList(ref cameraName, out formatList, out USBcamcount);
                }

                if (CamList.Count == 0)
                {
                    whichCamera = 0;

                    if (formatList != null)
                    {
                        int VideoFormat = 0; // choose a video format
                        camerabox.Width = formatList[VideoFormat].Width;
                        camerabox.Height = formatList[VideoFormat].Height;
                        this.Width = formatList[VideoFormat].Width + 48;
                        this.Height = formatList[VideoFormat].Height + 96;
                    }
                }
                else
                {
                    whichCamera = 1;
                    CameraHandler.CameraAdded += new SDKHandler.CameraAddedHandler(SDK_CameraAdded);
                    CameraHandler.LiveViewUpdated += new SDKHandler.StreamUpdate(SDK_LiveViewUpdated);
                    CameraHandler.CameraHasShutdown += SDK_CameraHasShutdown;
                    SetImageAction = new ThreadStart(delegate { bgbrush.ImageSource = EvfImage; });
                    IsInit = true;
                }
            }

        } 

        #endregion

        #region Operations

        #region Face Recognition

        private void StartVideo()
        {
            try
            {
                string[] cameraList;
                FSDK.CImage crop = null;
                int count;
                FSDKCam.GetCameraList(out cameraList, out count);

                if (0 == count)
                {
                    MessageBox.Show("Please attach a camera");
                }

                cameraName = cameraList[0]; // choose the first camera

                if (FSDK.FSDKE_OK != FSDK.ActivateLibrary("NxLq2hTLcLbL7xD11GUIOcRezUQKSIx4Y0cPOJaztQdnCYV52csmELrQJ41w1L33z25oSvb5CMEjSqRlWm2Fod9BmhJzq4PV2nM9lUqhMNgTBez1oVWRlcV2H4Ezg7fWFZCqF7m+ekBxE7Wc5sQpS2L69PxwvzZ+SpEfrRl4gzM="))
                {
                    MessageBox.Show("Please run the License Key Wizard (Start - Luxand - FaceSDK - License Key Wizard)");
                }

                FSDK.InitializeLibrary();
                FSDKCam.InitializeCapturing();

                FSDKCam.VideoFormatInfo[] formatList;
                FSDKCam.GetVideoFormatList(ref cameraName, out formatList, out count);

                cameraHandle = 0;

                int r = FSDKCam.OpenVideoCamera(ref cameraName, ref cameraHandle);

                tracker = 0;
                FSDK.CreateTracker(ref tracker);

                int err = 0; // set realtime face detection parameters
                FSDK.SetTrackerMultipleParameters(tracker, "RecognizeFaces=false; HandleArbitraryRotations=false; DetermineFaceRotationAngle=false; InternalResizeWidth=100; FaceDetectionThreshold=1;", ref err);

                while (!needClose)
                {
                    Int32 imageHandle = 0;
                    Int32 cropHandle = 0;

                    if (FSDK.FSDKE_OK != FSDKCam.GrabFrame(cameraHandle, ref imageHandle)) // grab the current frame from the camera
                    {
                        DoEvents();
                        continue;
                    }

                    FSDK.CImage image = new FSDK.CImage(imageHandle);
                    crop = new FSDK.CImage();
                    cropHandle = crop.ImageHandle;
                    FSDK.SetJpegCompressionQuality(90);

                    long[] IDs;
                    long faceCount = 0;
                    int width = 0;
                    int height = 0;
                    int left = 0;
                    int top = 0;
                    FSDK.FeedFrame(tracker, 0, image.ImageHandle, ref faceCount, out IDs, sizeof(long) * 256); // maximum 256 faces detected
                    Array.Resize(ref IDs, (int)faceCount);

                    System.Drawing.Image frameImage = image.ToCLRImage();
                    Graphics gr = Graphics.FromImage(frameImage);

                    if (IDs.Length == 0)
                    {
                        Photo = null;
                    }
                    else
                    {
                        for (int i = 0; i < IDs.Length; ++i)
                        {
                            FSDK.TFacePosition facePosition = new FSDK.TFacePosition();
                            FSDK.GetTrackerFacePosition(tracker, 0, IDs[i], ref facePosition);

                            left = facePosition.xc - (int)(facePosition.w * 0.68);
                            top = facePosition.yc - (int)(facePosition.w * 0.75);
                            width = (int)(facePosition.w * 1.32);
                            height = (int)(facePosition.w * 1.76);

                            // get rectangle
                            FSDK.CreateEmptyImage(ref cropHandle);
                            FSDK.CopyRect(image.ImageHandle, left, top, left + width, top + height, crop.ImageHandle);
                            Photo = (Bitmap)crop.ToCLRImage();

                            // draw rectangle
                            gr.DrawRectangle(Pens.LightGreen, left, top, width, height);
                        }
                    }

                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        using (MemoryStream memory = new MemoryStream())
                        {
                            frameImage.Save(memory, ImageFormat.Png);
                            memory.Position = 0;
                            BitmapImage bitmapImage = new BitmapImage();
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = memory;
                            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                            bitmapImage.EndInit();

                            camerabox.Source = bitmapImage;
                        }
                    }));

                    GC.Collect(); // collect the garbage

                    // make UI controls accessible
                    DoEvents();
                }

                // Ensure directory exits
                Directory.CreateDirectory(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Photos"));

                // Check if file exists then save image
                int counter = 1;
                string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Photos", "TempPhoto" + counter + ".jpg");

                while (File.Exists(path))
                {
                    counter++;
                    path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Photos", "TempPhoto" + counter + ".jpg");
                }

                FSDK.SaveImageToFile(crop.ImageHandle, System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Photos", path));

                PictureTaken = true;
                FSDK.FreeTracker(tracker);

                FSDKCam.CloseVideoCamera(cameraHandle);
                FSDKCam.FinalizeCapturing();
                garbageCollected = true;

                this.Dispatcher.Invoke((Action)(() =>
                {
                    Close();
                }));
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        private void UpdateImage(BitmapSource src)
        {
            camerabox.Source = src;
        }
       
        private static void DoEvents()
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                                                  new Action(delegate { }));
        } 

        #endregion

        #region EOS Camera

        private void CloseSession()
        {
            CameraHandler.CloseSession();
            EDSDK.EdsTerminateSDK();
        }

        private void RefreshCamera()
        {
            CloseSession();
            CamList = CameraHandler.GetCameraList();
            //foreach (Camera cam in CamList) CameraListBox.Items.Add(cam.Info.szDeviceDescription);
        }

        private void OpenSession()
        {
            if (CamList.Count != 0)
            {
                // Set which camera we are currently using
                whichCamera = 1;

                CameraHandler.OpenSession(CamList[0]);
                string cameraname = CameraHandler.MainCamera.Info.szDeviceDescription;
                //SessionLabel.Content = cameraname;
                if (CameraHandler.GetSetting(EDSDK.PropID_AEMode) != EDSDK.AEMode_Manual) MessageBox.Show("Camera is not in manual mode. Some features might not work!");
                AvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Av);
                TvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Tv);
                ISOList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_ISOSpeed);
                foreach (int Av in AvList) AvCoBox.Items.Add(CameraValues.AV((uint)Av));
                foreach (int Tv in TvList) TvCoBox.Items.Add(CameraValues.TV((uint)Tv));
                foreach (int ISO in ISOList) ISOCoBox.Items.Add(CameraValues.ISO((uint)ISO));
                AvCoBox.SelectedIndex = AvCoBox.Items.IndexOf(CameraValues.AV((uint)CameraHandler.GetSetting((uint)EDSDK.PropID_Av)));
                TvCoBox.SelectedIndex = TvCoBox.Items.IndexOf(CameraValues.TV((uint)CameraHandler.GetSetting((uint)EDSDK.PropID_Tv)));
                ISOCoBox.SelectedIndex = ISOCoBox.Items.IndexOf(CameraValues.ISO((uint)CameraHandler.GetSetting((uint)EDSDK.PropID_ISOSpeed)));
                int wbidx = (int)CameraHandler.GetSetting((uint)EDSDK.PropID_WhiteBalance);

                switch (wbidx)
                {
                    case EDSDK.WhiteBalance_Auto: WBCoBox.SelectedIndex = 0; break;
                    case EDSDK.WhiteBalance_Daylight: WBCoBox.SelectedIndex = 1; break;
                    case EDSDK.WhiteBalance_Cloudy: WBCoBox.SelectedIndex = 2; break;
                    case EDSDK.WhiteBalance_Tangsten: WBCoBox.SelectedIndex = 3; break;
                    case EDSDK.WhiteBalance_Fluorescent: WBCoBox.SelectedIndex = 4; break;
                    case EDSDK.WhiteBalance_Strobe: WBCoBox.SelectedIndex = 5; break;
                    case EDSDK.WhiteBalance_WhitePaper: WBCoBox.SelectedIndex = 6; break;
                    case EDSDK.WhiteBalance_Shade: WBCoBox.SelectedIndex = 7; break;
                    default: WBCoBox.SelectedIndex = -1; break;
                }

                SettingsGroupBox.IsEnabled = true;

                // Show something on the view
                if (!CameraHandler.IsLiveViewOn)
                {
                    LVCanvas.Background = bgbrush;
                    CameraHandler.StartLiveView();

                    // Begin Face Recognition
                    StartFaceRecog();
                }
                else
                {
                    CameraHandler.StopLiveView();
                    LVCanvas.Background = System.Windows.Media.Brushes.LightGray;
                }
            }
        }

        private void StartFaceRecog()
        {
            FSDK.InitializeLibrary();
            FSDK.SetJpegCompressionQuality(100);
        }

        #endregion

        #endregion

        #region Events

        #region EOS SDK Events

        private void SDK_LiveViewUpdated(Stream img)
        {
            if (CameraHandler.IsLiveViewOn)
            {
                try
                {
                    if (UseFacialRecog)
                    {
                        tracker = 0;
                        Int32 cropHandle = 0;
                        FSDK.CreateTracker(ref tracker);
                        System.Drawing.Image frameImage = Bitmap.FromStream(img);

                        int err = 0; // set realtime face detection parameters
                        FSDK.SetTrackerMultipleParameters(tracker, "RecognizeFaces=false; HandleArbitraryRotations=false; DetermineFaceRotationAngle=false; InternalResizeWidth=100; FaceDetectionThreshold=2;", ref err);

                        Graphics gr = Graphics.FromImage(frameImage);
                        FSDK.CImage crop = new FSDK.CImage();
                        FSDK.CImage image = new FSDK.CImage(frameImage);
                        cropHandle = crop.ImageHandle;
                        FSDK.SetJpegCompressionQuality(100);

                        long[] IDs;
                        long faceCount = 0;
                        int width = 0;
                        int height = 0;
                        int left = 0;
                        int top = 0;
                        FSDK.FeedFrame(tracker, 0, image.ImageHandle, ref faceCount, out IDs, sizeof(long) * 256); // maximum 256 faces detected
                        Array.Resize(ref IDs, (int)faceCount);

                        for (int i = 0; i < IDs.Length; ++i)
                        {
                            FSDK.TFacePosition facePosition = new FSDK.TFacePosition();
                            FSDK.GetTrackerFacePosition(tracker, 0, IDs[i], ref facePosition);

                            left = facePosition.xc - (int)(facePosition.w * 0.612);
                            top = facePosition.yc - (int)(facePosition.w * 0.75);
                            width = (int)(facePosition.w * 1.188);
                            height = (int)(facePosition.w * 1.584);

                            // get rectangle
                            FSDK.CreateEmptyImage(ref cropHandle);
                            FSDK.CopyRect(image.ImageHandle, left, top, left + width, top + height, crop.ImageHandle);
                            Photo = (Bitmap)crop.ToCLRImage();

                            // draw rectangle
                            System.Drawing.Pen greenPen = new System.Drawing.Pen(System.Drawing.Color.LightGreen, 2);
                            gr.DrawRectangle(greenPen, left, top, width, height);
                        }

                        img = new MemoryStream();
                        frameImage.Save(img, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }

                    img.Position = 0;
                    dec = new JpegBitmapDecoder(img, BitmapCreateOptions.None, BitmapCacheOption.None);
                    EvfImage = dec.Frames[0];

                    Application.Current.Dispatcher.Invoke(SetImageAction);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception");
                }
            }
        }

        private void SDK_CameraAdded()
        {
            RefreshCamera();
        }

        private void SDK_CameraHasShutdown(object sender, EventArgs e)
        {
            CloseSession();
        }

        private void FocusNear3Button_Click(object sender, RoutedEventArgs e)
        {
            CameraHandler.SetFocus(EDSDK.EvfDriveLens_Near3);
        }

        private void FocusFar3Button_Click(object sender, RoutedEventArgs e)
        {
            CameraHandler.SetFocus(EDSDK.EvfDriveLens_Far3);
        }

        private void AvCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_Av, CameraValues.AV((string)AvCoBox.SelectedItem));
        }

        private void TvCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_Tv, CameraValues.TV((string)TvCoBox.SelectedItem));
            if ((string)TvCoBox.SelectedItem == "Bulb")
            {
                BulbBox.Visibility = System.Windows.Visibility.Visible;
                BulbSlider.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                BulbBox.Visibility = System.Windows.Visibility.Collapsed;
                BulbSlider.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void BulbBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInit)
            {
                int b;
                if (int.TryParse(BulbBox.Text, out b) && b != BulbTime)
                {
                    BulbTime = b;
                    BulbSlider.Value = BulbTime;
                }
                else BulbBox.Text = BulbTime.ToString();
            }
        }

        private void BulbSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IsInit) BulbBox.Text = BulbSlider.Value.ToString();
        }

        private void ISOCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_ISOSpeed, CameraValues.ISO((string)ISOCoBox.SelectedItem));
        }

        private void WBCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (WBCoBox.SelectedIndex)
            {
                case 0: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Auto); break;
                case 1: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Daylight); break;
                case 2: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Cloudy); break;
                case 3: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Tangsten); break;
                case 4: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Fluorescent); break;
                case 5: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Strobe); break;
                case 6: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_WhitePaper); break;
                case 7: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Shade); break;
            }
        }

        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (whichCamera != 0)
            {
                OpenSession();
            }
            else
            {
                LVCanvas.Visibility = System.Windows.Visibility.Collapsed;
                AdvancedOptions.Visibility = System.Windows.Visibility.Collapsed;
                camerabox.Visibility = System.Windows.Visibility.Visible;

                CameraThread = new Thread(new ThreadStart(StartVideo));
                CameraThread.Start();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            // Ensure everything is disposed of properly
            if (!garbageCollected && UseFacialRecog)
            {
                FSDK.FreeTracker(tracker);

                FSDKCam.CloseVideoCamera(cameraHandle);
                FSDKCam.FinalizeCapturing();
                garbageCollected = true;
            }

            if (!EOSended)
            {
                CloseSession();
            }
        }

        private void btnTakePicture_Click(object sender, RoutedEventArgs e)
        {
            if (whichCamera == 0)
            {
                if (Photo != null)
                {
                    needClose = true;
                }
                else
                {
                    MessageBox.Show("No face found");
                }
            }
            else
            {
                string dir = "C:\\Users\\Dami\\Pictures\\Cannon";
                Directory.CreateDirectory(dir);
               
                CameraHandler.ImageSaveDirectory = dir;
                CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Camera);
                CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Host);
                CameraHandler.SetCapacity();
                if ((string)TvCoBox.SelectedItem == "Bulb") CameraHandler.TakePhoto((uint)BulbTime);
                else CameraHandler.TakePhoto();
                                
                if (Photo != null)
                {
                    // Save picture
                    Photo.Save(System.IO.Path.Combine(dir, "Testfile.jpg"), ImageFormat.Jpeg);
                    PictureTaken = true;
                    Close();
                }
                else
                {
                    if (UseFacialRecog)
                        MessageBox.Show("No face found");
                    else
                    {
                        //Photo = CameraHandler.CurrentImage;
                        Close();
                    }
                }
            }
        }

        #endregion

        private void btnStartSession_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
