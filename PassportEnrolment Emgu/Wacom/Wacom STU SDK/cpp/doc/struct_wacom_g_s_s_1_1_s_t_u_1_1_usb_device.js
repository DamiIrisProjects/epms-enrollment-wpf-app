var struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device =
[
    [ "UsbDevice", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a334e2deb79a483d02938465c73f88321", null ],
    [ "UsbDevice", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a281778e1619faf61aba0d617d29869ca", null ],
    [ "UsbDevice", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#afe91061a0d2236d2711bf1b8b5fa34df", null ],
    [ "fileName", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#afdb6106f06d14358e909d257f78bfea1", null ],
    [ "bulkFileName", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a97063a5a5907e2de7804d12c78533572", null ],
    [ "devInst", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#af7f553d14e34f1cd45079ac9435f438f", null ],
    [ "busNumber", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#ad057149131c8cf28df14de872c33df83", null ],
    [ "deviceAddress", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#acc5c6e2c9e9ff50cab35f23f09792fd6", null ]
];