var _win32_2_protocol_helper_8hpp =
[
    [ "Scale", "_win32_2_protocol_helper_8hpp.html#a83793bd05ed19f010c376f4e27f39574", [
      [ "Scale_Stretch", "_win32_2_protocol_helper_8hpp.html#a83793bd05ed19f010c376f4e27f39574a09d958d0e085816bcf03640dd07a515b", null ],
      [ "Scale_Fit", "_win32_2_protocol_helper_8hpp.html#a83793bd05ed19f010c376f4e27f39574abafc7c4991c6dc13e87fe5bda1ba690f", null ],
      [ "Scale_Clip", "_win32_2_protocol_helper_8hpp.html#a83793bd05ed19f010c376f4e27f39574a578a1c20f080252984574c7bd4d57be7", null ]
    ] ],
    [ "Border", "_win32_2_protocol_helper_8hpp.html#a3f7d6e802ba714ab80b20e32b1b4b38a", [
      [ "Border_Black", "_win32_2_protocol_helper_8hpp.html#a3f7d6e802ba714ab80b20e32b1b4b38aa501b94ad504f79e0e60b79368ee390c5", null ],
      [ "Border_White", "_win32_2_protocol_helper_8hpp.html#a3f7d6e802ba714ab80b20e32b1b4b38aac8544d860e59cd1b0824338bf11cf3fe", null ]
    ] ],
    [ "Clip", "_win32_2_protocol_helper_8hpp.html#ac0ec3b0473cbe0e974332e9a1d2f7840", [
      [ "Clip_Left", "_win32_2_protocol_helper_8hpp.html#ac0ec3b0473cbe0e974332e9a1d2f7840a32adbcb4b2ff7696464f38cc835200fc", null ],
      [ "Clip_Center", "_win32_2_protocol_helper_8hpp.html#ac0ec3b0473cbe0e974332e9a1d2f7840a1a9a2ecf06fd77c00f4cdfdc21abedea", null ],
      [ "Clip_Right", "_win32_2_protocol_helper_8hpp.html#ac0ec3b0473cbe0e974332e9a1d2f7840aa53ba7a6aefdfddd8fe648b9261b15a7", null ],
      [ "Clip_Top", "_win32_2_protocol_helper_8hpp.html#ac0ec3b0473cbe0e974332e9a1d2f7840ac6250b6ce4d1a539f120611ddb15f21f", null ],
      [ "Clip_Middle", "_win32_2_protocol_helper_8hpp.html#ac0ec3b0473cbe0e974332e9a1d2f7840a1bcb860b6b9725cfef9250f0da04d3e8", null ],
      [ "Clip_Bottom", "_win32_2_protocol_helper_8hpp.html#ac0ec3b0473cbe0e974332e9a1d2f7840a4dfd7b8596f37502b96a055fab325f2d", null ]
    ] ],
    [ "flattenMonochrome", "_win32_2_protocol_helper_8hpp.html#a700d5c2e192fdf326e7c324c2678c14f", null ],
    [ "flattenColor16", "_win32_2_protocol_helper_8hpp.html#ab40a2f47cd480a0b4af1f57a437944d2", null ],
    [ "flattenColor16_565", "_win32_2_protocol_helper_8hpp.html#a1e5019329f4698cdb7e4c03057867902", null ],
    [ "flattenColor24", "_win32_2_protocol_helper_8hpp.html#a0f6ad84bbee7e141b6564b5a2a2adb4e", null ],
    [ "flatten", "_win32_2_protocol_helper_8hpp.html#a729c231f2b4d9c853da3b03a7b7c6469", null ],
    [ "flatten", "_win32_2_protocol_helper_8hpp.html#a2a4587a9c2dc7e8ef831462f92e2d57f", null ],
    [ "flattenMonochrome", "_win32_2_protocol_helper_8hpp.html#aceef842c3e4ef1e37c4c9c04e2448d64", null ],
    [ "flattenColor16", "_win32_2_protocol_helper_8hpp.html#afdf8b1612c41f7f90e04bc003da1f928", null ],
    [ "flattenColor16_565", "_win32_2_protocol_helper_8hpp.html#a4b0fc9560d3756dcbedac54aebd0581d", null ],
    [ "flattenColor24", "_win32_2_protocol_helper_8hpp.html#a3187a7451a0b90e3760c8e2f0525898f", null ],
    [ "flatten", "_win32_2_protocol_helper_8hpp.html#aa4e66f9dca652683697ae77b0edf28fd", null ],
    [ "flatten", "_win32_2_protocol_helper_8hpp.html#a2309e3e8b2a2594c1808dce959e93fef", null ],
    [ "resizeImage", "_win32_2_protocol_helper_8hpp.html#a6038efb36c39c2de63d35c587017e4c9", null ],
    [ "resizeImageAndFlatten", "_win32_2_protocol_helper_8hpp.html#a0fcd36f16090c3e5fdc15a7acdd8e4b7", null ],
    [ "resizeImageAndFlatten", "_win32_2_protocol_helper_8hpp.html#a019992f5718b75ceaec0ebc6b4eb2367", null ]
];