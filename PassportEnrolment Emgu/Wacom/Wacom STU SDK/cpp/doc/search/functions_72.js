var searchData=
[
  ['rebind',['rebind',['../class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a15dfc8033e73fa08cc9246272920cf91',1,'WacomGSS::STU::Protocol']]],
  ['reset',['reset',['../class_wacom_g_s_s_1_1_s_t_u_1_1_tablet.html#af4f874d0ee760c2be119435cb55b0479',1,'WacomGSS::STU::Tablet']]],
  ['resizeimage',['resizeImage',['../namespace_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper.html#a6038efb36c39c2de63d35c587017e4c9',1,'WacomGSS::STU::ProtocolHelper']]],
  ['resizeimageandflatten',['resizeImageAndFlatten',['../namespace_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper.html#a0fcd36f16090c3e5fdc15a7acdd8e4b7',1,'WacomGSS::STU::ProtocolHelper::resizeImageAndFlatten(IWICImagingFactory *pIWICImagingFactory, IWICBitmapSource *pIWICBitmapSource, std::uint16_t screenWidth, std::uint16_t screenHeight, Protocol::EncodingMode encodingMode, Scale scale=Scale_Fit, Border border=Border_White, std::uint8_t clipMode=Clip_Left|Clip_Top)'],['../namespace_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper.html#a019992f5718b75ceaec0ebc6b4eb2367',1,'WacomGSS::STU::ProtocolHelper::resizeImageAndFlatten(IWICImagingFactory *pIWICImagingFactory, IWICBitmapSource *pIWICBitmapSource, std::uint16_t screenWidth, std::uint16_t screenHeight, bool color, Scale scale=Scale_Fit, Border border=Border_White, std::uint8_t clipMode=Clip_Left|Clip_Top)']]]
];
