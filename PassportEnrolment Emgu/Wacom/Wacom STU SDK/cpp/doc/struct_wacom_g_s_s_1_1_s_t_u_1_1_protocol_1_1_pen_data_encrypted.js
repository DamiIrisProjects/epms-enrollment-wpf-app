var struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted =
[
    [ "sessionId", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted.html#a697ff37a0b2c91e67806fac404773f8c", null ],
    [ "penData", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted.html#ae7534203d486f67067c73100b075cac1", null ],
    [ "reportId", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted.html#aa99e19271d3d394134ca92eae9c4e630", null ],
    [ "reportSize", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted.html#a750985c1918bf2e9dc70da6518e4e8cc", null ],
    [ "encryptedSize", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted.html#a81c5dbacb9ccb411c2769691c7522fe5", null ]
];