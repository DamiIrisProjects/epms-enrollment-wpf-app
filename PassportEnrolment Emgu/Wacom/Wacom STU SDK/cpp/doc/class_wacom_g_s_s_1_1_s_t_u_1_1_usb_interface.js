var class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface =
[
    [ "UsbInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a98ffe0b83f33ecf3fff9ac54fb8140bd", null ],
    [ "UsbInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#acdbb857c70e2e83871f4a92d66cad00d", null ],
    [ "~UsbInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a2bbd45c17ad4cde99424d5a99646d303", null ],
    [ "connect", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#addeaa6c11fd4ac8d97dc1fb757cf66c1", null ],
    [ "connect", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a19c71a5cab2e147aabd8a355a84d502b", null ],
    [ "connect", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a7b95008683c7db12288edbf5add2b310", null ],
    [ "disconnect", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#aaf435fa2107c0f801a1f639a0efe08e1", null ],
    [ "isConnected", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a8b95541bb3045e2bef0adf5926b09dcb", null ],
    [ "get", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a4df9f0bf07b85c4a3236dc631e96117f", null ],
    [ "set", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#ae1bc49a927e394cc1b684cb50c4dd209", null ],
    [ "supportsWrite", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a6dc6876f316bcd123968251ea600753f", null ],
    [ "write", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#ab0374fb1ecf253d0ca165cfc08e73cde", null ],
    [ "getReportCountLengths", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#a4971a2d98779cf8d83fc466df3a8d1ec", null ],
    [ "getProductId", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html#ad65d43ce9caa1ad1cadf9aa1497022be", null ]
];