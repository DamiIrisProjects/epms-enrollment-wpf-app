var gcc_8hpp =
[
    [ "WacomGSS_GCC_VERSION", "gcc_8hpp.html#a0d8d4bb8b33d1ff3fe4b95f9c8849f76", null ],
    [ "override", "gcc_8hpp.html#a421c2a84099be332814e021bcf72c6cf", null ],
    [ "WacomGSS_compatibility_condition_variable", "gcc_8hpp.html#a8b3e76c7617a6af0d88fab8f3d67fb37", null ],
    [ "WacomGSS_compatibility_mutex", "gcc_8hpp.html#a8eb2a28b32142331b7f4bb919097cafd", null ],
    [ "WacomGSS_compatibility_thread", "gcc_8hpp.html#a59ae932ee5517d2faef1adc4dabf4889", null ],
    [ "WacomGSS_compatibility_chrono", "gcc_8hpp.html#a940b4dfe5eea68fed8d026dc1f9c019b", null ],
    [ "WacomGSS_compatibility_atomic", "gcc_8hpp.html#a8b6c8676c08ac113a991377c9e9f08d3", null ],
    [ "WacomGSS_compatibility_std_codecvt", "gcc_8hpp.html#aad19dfff3f3c2d90f5e2dbdd001f30d2", null ],
    [ "WacomGSS_noreturn", "gcc_8hpp.html#ab83c86dda9957ddea5e2f833c4b55c28", null ],
    [ "WacomGSS_deprecated", "gcc_8hpp.html#a7b038e2f1fb957034173375aa05127f3", null ],
    [ "WacomGSS_thread_local", "gcc_8hpp.html#a7c8fd7470fee57a44550e813c2a10260", null ],
    [ "memset_s", "gcc_8hpp.html#adb8bd9c5eabc95114399401b37ed8240", null ]
];