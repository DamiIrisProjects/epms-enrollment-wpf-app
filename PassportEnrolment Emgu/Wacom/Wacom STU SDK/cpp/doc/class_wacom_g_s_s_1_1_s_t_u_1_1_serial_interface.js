var class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface =
[
    [ "SerialInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#ae98ff64bb574cc2126a020092149dffb", null ],
    [ "~SerialInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#a3f8b19945cf2f20eb6f0784ba6ee6b21", null ],
    [ "connect", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#ae5066740716293a0c5540aff8922f34b", null ],
    [ "connect", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#a07f051cd9a6d6d5250f2d8e243bb1cee", null ],
    [ "disconnect", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#a97170b07431ee67c6e017f9021ae1a95", null ],
    [ "isConnected", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#adebe306a8ef9593f50ad65e3fe8bcdbe", null ],
    [ "get", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#a1da793c33389e62aac9137a539d8957f", null ],
    [ "set", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#ac6e2c082df56ed41e496ed953d6730e0", null ],
    [ "supportsWrite", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#a01d789c809c21144804cf6d3ba0006ed", null ],
    [ "write", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#a97a04b8bfbe7fa7ba9b9c86f6e4ca918", null ],
    [ "getReportCountLengths", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#a854a3b1761418a9100ee8215e0d05385", null ],
    [ "getProductId", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html#a00394dc1f8f14b7fc578933129117f2d", null ]
];