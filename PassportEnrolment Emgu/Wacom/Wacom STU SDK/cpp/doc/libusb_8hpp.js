var libusb_8hpp =
[
    [ "libusb_errc", "libusb_8hpp.html#a3cc6d3716b4c69052c24fd022562d041", null ],
    [ "libusb_category", "libusb_8hpp.html#a9ad5145cd5c2b140d67c423dab86a578", null ],
    [ "make_error_code", "libusb_8hpp.html#abbdb8737e503949cd65f050c8b00b856", null ],
    [ "make_error_condition", "libusb_8hpp.html#ad6bb4ebf4ee85b53708592af6eedc945", null ],
    [ "throw_libusb_error", "libusb_8hpp.html#ad37ffcd2cf5051f11e44073b1cbecbc7", null ],
    [ "throw_libusb_error", "libusb_8hpp.html#a7fc5869de11d8c36609e61810f813b34", null ],
    [ "libusb_succeeded", "libusb_8hpp.html#aec01f615685d8af05116cd4297ee766c", null ],
    [ "get_device", "libusb_8hpp.html#ab7e4270bad108cc1174895ae12af7702", null ],
    [ "open", "libusb_8hpp.html#af9dd38619308f4b37aefb6be7b12cfc4", null ],
    [ "close", "libusb_8hpp.html#a256e343edb65a2f56cc358af2ae7ac53", null ],
    [ "hid_get_report", "libusb_8hpp.html#a602cf8ebd9f343a0d8d1e6d7ac0571c2", null ],
    [ "hid_set_report", "libusb_8hpp.html#a0ffd864234618e5cd5b63e1307a92b83", null ],
    [ "interrupt_in_sync", "libusb_8hpp.html#a22f7f302bea904cb7877beffe5ef1506", null ],
    [ "bulk_out_sync", "libusb_8hpp.html#a66b816fdebc315f0dfb01590915a78fb", null ]
];