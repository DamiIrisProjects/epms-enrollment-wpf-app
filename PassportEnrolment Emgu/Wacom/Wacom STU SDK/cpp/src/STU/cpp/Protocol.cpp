#include <WacomGSS/STU/Protocol.hpp>
#include <WacomGSS/STU/Interface.hpp>
#include <WacomGSS/compatibility/thread.hpp>

namespace WacomGSS
{
  namespace STU
  {



    Protocol::Status Protocol::getStatus()
    {
      std::array<uint8_t,5> cmd = { ReportId_Status };

      get(cmd);

      Status data;

      data.statusCode     = static_cast<StatusCode>(cmd[1]);
      data.lastResultCode = static_cast<ErrorCode>(cmd[2]);
      data.statusWord     = static_cast<uint16_t>(cmd[3] | cmd[4] << 8);

      return std::move(data);
    }
    


    void Protocol::setReset(ResetFlag resetFlag)
    {
      std::array<uint8_t,2> cmd = { ReportId_Reset, resetFlag };

      set(cmd);
    }
    


    Protocol::Information Protocol::getInformation()
    {
      std::array<uint8_t,17> cmd = { ReportId_Information };
  
      get(cmd);

      Information data;

      data.modelName[0]               = static_cast<char>(cmd[ 1]);
      data.modelName[1]               = static_cast<char>(cmd[ 2]);
      data.modelName[2]               = static_cast<char>(cmd[ 3]);
      data.modelName[3]               = static_cast<char>(cmd[ 4]);
      data.modelName[4]               = static_cast<char>(cmd[ 5]);
      data.modelName[5]               = static_cast<char>(cmd[ 6]);
      data.modelName[6]               = static_cast<char>(cmd[ 7]);
      data.modelName[7]               = static_cast<char>(cmd[ 8]);
      data.modelName[8]               = static_cast<char>(cmd[ 9]);
      data.modelNameNullTerminated[9] = 0;
      data.firmwareMajorVersion       = cmd[10];
      data.firmwareMinorVersion       = cmd[11];
      data.secureIc                   = cmd[12]; // & 0x01U;
      data.secureIcVersion[0]         = cmd[13];
      data.secureIcVersion[1]         = cmd[14];
      data.secureIcVersion[2]         = cmd[15];
      data.secureIcVersion[3]         = cmd[16];

      return std::move(data);
    }



    Protocol::Capability Protocol::getCapability()
    {
      std::array<uint8_t,17> cmd = { ReportId_Capability };

      get(cmd);

      Capability data;

      data.tabletMaxX        = static_cast<uint16_t>(cmd[ 1] << 8 | cmd[ 2]);
      data.tabletMaxY        = static_cast<uint16_t>(cmd[ 3] << 8 | cmd[ 4]);
      data.tabletMaxPressure = static_cast<uint16_t>(cmd[ 5] << 8 | cmd[ 6]);
      data.screenWidth       = static_cast<uint16_t>(cmd[ 7] << 8 | cmd[ 8]);
      data.screenHeight      = static_cast<uint16_t>(cmd[ 9] << 8 | cmd[10]);
      data.maxReportRate     = cmd[11];
      data.resolution        = static_cast<uint16_t>(cmd[12] << 8 | cmd[13]);
      data.encodingFlag      = cmd[14];      
      //cmd[15] reserved
      //cmd[16] reserved

      return std::move(data);
    }



    uint32_t Protocol::getUid()
    {
      std::array<uint8_t,5> cmd = { ReportId_Uid };

      get(cmd);

      uint32_t data = static_cast<uint32_t>( (cmd[1] << 24) | (cmd[2] << 16) | (cmd[3] << 8) | cmd[4] );

      return data;
    }



    void Protocol::setUid(uint32_t value)
    {
      std::array<uint8_t,5> cmd = 
      {
        ReportId_Uid,
        static_cast<uint8_t>((value>>24) & 0xffu),
        static_cast<uint8_t>((value>>16) & 0xffu),
        static_cast<uint8_t>((value>> 8) & 0xffu),
        static_cast<uint8_t>((value    ) & 0xffu)
      };

      set(cmd);
    }



    Protocol::Uid2 Protocol::getUid2()
    {
      std::array<uint8_t,11> cmd = { ReportId_Uid2 };

      get(cmd);

      Uid2 data;

      data.uid2[ 0]               = static_cast<char>(cmd[ 1]);
      data.uid2[ 1]               = static_cast<char>(cmd[ 2]);
      data.uid2[ 2]               = static_cast<char>(cmd[ 3]);
      data.uid2[ 3]               = static_cast<char>(cmd[ 4]);
      data.uid2[ 4]               = static_cast<char>(cmd[ 5]);
      data.uid2[ 5]               = static_cast<char>(cmd[ 6]);
      data.uid2[ 6]               = static_cast<char>(cmd[ 7]);
      data.uid2[ 7]               = static_cast<char>(cmd[ 8]);
      data.uid2[ 8]               = static_cast<char>(cmd[ 9]);
      data.uid2[ 9]               = static_cast<char>(cmd[10]);
      data.uid2NullTerminated[10] = 0;
      
      return data;
    }



    Protocol::PublicKey Protocol::getHostPublicKey()
    {
      std::array<uint8_t,17> cmd = { ReportId_HostPublicKey };

      get(cmd);

      PublicKey data = 
      {
        cmd[ 1],
        cmd[ 2],
        cmd[ 3],
        cmd[ 4],
        cmd[ 5],
        cmd[ 6],
        cmd[ 7],
        cmd[ 8],
        cmd[ 9],
        cmd[10],
        cmd[11],
        cmd[12],
        cmd[13],
        cmd[14],
        cmd[15],
        cmd[16]
      };

      return std::move(data);
    }



    void Protocol::setHostPublicKey(PublicKey const & hostPublicKey)
    {
      std::array<uint8_t,17> cmd = 
      {
        ReportId_HostPublicKey,
        hostPublicKey[ 0],
        hostPublicKey[ 1],
        hostPublicKey[ 2],
        hostPublicKey[ 3],
        hostPublicKey[ 4],
        hostPublicKey[ 5],
        hostPublicKey[ 6],
        hostPublicKey[ 7],
        hostPublicKey[ 8],
        hostPublicKey[ 9],
        hostPublicKey[10],
        hostPublicKey[11],
        hostPublicKey[12],
        hostPublicKey[13],
        hostPublicKey[14],
        hostPublicKey[15]
      };

      set(cmd);
    }



    Protocol::PublicKey Protocol::getDevicePublicKey()
    {
      std::array<uint8_t,17> cmd = { ReportId_DevicePublicKey };

      get(cmd);

      PublicKey data = 
      {
        cmd[ 1],
        cmd[ 2],
        cmd[ 3],
        cmd[ 4],
        cmd[ 5],
        cmd[ 6],
        cmd[ 7],
        cmd[ 8],
        cmd[ 9],
        cmd[10],
        cmd[11],
        cmd[12],
        cmd[13],
        cmd[14],
        cmd[15],
        cmd[16]
      };

      return std::move(data);
    }



    void Protocol::setStartCapture(uint32_t sessionId)
    {
      std::array<uint8_t,5> cmd = 
      { 
        ReportId_StartCapture, 
        static_cast<uint8_t>((sessionId>>24) & 0xffu),
        static_cast<uint8_t>((sessionId>>16) & 0xffu),
        static_cast<uint8_t>((sessionId>> 8) & 0xffu),
        static_cast<uint8_t>((sessionId    ) & 0xffu)
      };

      set(cmd);

      //::boost::this_thread::sleep(::boost::posix_time::millisec(20)); // boost
      this_thread::sleep_for(chrono::milliseconds(20)); // C++11
    }



    void Protocol::setEndCapture() 
    {
      std::array<uint8_t,2> cmd = { ReportId_EndCapture };
     
      set(cmd);

      //::boost::this_thread::sleep(::boost::posix_time::millisec(20)); // boost
      this_thread::sleep_for(chrono::milliseconds(20)); // C++11
    }



    Protocol::DHprime Protocol::getDHprime()
    {
      std::array<uint8_t,17> cmd = { ReportId_DHprime };

      get(cmd);

      Protocol::DHprime data;
      std::copy(cmd.begin()+1, cmd.end(), data.begin());

      return std::move(data);
    }



    void Protocol::setDHprime(DHprime const & prime)
    {
      std::array<uint8_t,17> cmd = 
      {
        ReportId_DHprime,
        prime[ 0],
        prime[ 1],
        prime[ 2],
        prime[ 3],
        prime[ 4],
        prime[ 5],
        prime[ 6],
        prime[ 7],
        prime[ 8],
        prime[ 9],
        prime[10],
        prime[11],
        prime[12],
        prime[13],
        prime[14],
        prime[15]
      };

      set(cmd);
    }



    Protocol::DHbase Protocol::getDHbase()
    {
      std::array<uint8_t,3> cmd = { ReportId_DHbase };

      get(cmd);

      Protocol::DHbase data;
      data[ 0] = cmd[ 1];
      data[ 1] = cmd[ 2];

      return std::move(data);
    }



    void Protocol::setDHbase(DHbase const & base)
    {
      std::array<uint8_t,3> cmd = 
      {
        ReportId_DHbase,
        base[ 0],
        base[ 1]
      };

      set(cmd);
    }



    void Protocol::setClearScreen()
    {
      std::array<uint8_t,2> cmd = { ReportId_ClearScreen };

      set(cmd);
    }



    void Protocol::setClearScreenArea(Protocol::Rectangle const & area)
    {
      std::array<uint8_t,11> cmd = 
      { 
        ReportId_ClearScreenArea,
        0,
        1,
        static_cast<uint8_t>( area.upperLeftXpixel      & 0xffu),
        static_cast<uint8_t>((area.upperLeftXpixel >>8) & 0xffu),
        static_cast<uint8_t>( area.upperLeftYpixel      & 0xffu),
        static_cast<uint8_t>((area.upperLeftYpixel >>8) & 0xffu),
        static_cast<uint8_t>( area.lowerRightXpixel     & 0xffu),
        static_cast<uint8_t>((area.lowerRightXpixel>>8) & 0xffu),
        static_cast<uint8_t>( area.lowerRightYpixel     & 0xffu),
        static_cast<uint8_t>((area.lowerRightYpixel>>8) & 0xffu)
      };

      set(cmd);
    }


    uint8_t Protocol::getInkingMode()
    {
      std::array<uint8_t,2> cmd = { ReportId_InkingMode };

      get(cmd);

      return cmd[1];
    }



    void Protocol::setInkingMode(InkingMode inkingMode)
    {
      std::array<uint8_t,2> cmd = { ReportId_InkingMode, inkingMode };

      set(cmd);
    }



    Protocol::InkThreshold Protocol::getInkThreshold()
    {
      std::array<uint8_t,5> cmd = { ReportId_InkThreshold };
      
      get(cmd);

      InkThreshold data;
      data.onPressureMark  = static_cast<uint16_t>(cmd[ 1] | cmd[ 2] << 8);
      data.offPressureMark = static_cast<uint16_t>(cmd[ 3] | cmd[ 4] << 8);

      return std::move(data);
    }



    void Protocol::setInkThreshold(InkThreshold const & data)
    {
      std::array<uint8_t,5> cmd =  
      { 
        ReportId_InkThreshold,
        static_cast<uint8_t>((data.onPressureMark      ) & 0xffu),
        static_cast<uint8_t>((data.onPressureMark  >> 8) & 0xffu),
        static_cast<uint8_t>((data.offPressureMark     ) & 0xffu),
        static_cast<uint8_t>((data.offPressureMark >> 8) & 0xffu)
      };
    
      set(cmd);
    }



    void Protocol::setStartImageData(EncodingMode encodingMode)
    {
      std::array<uint8_t,2> cmd = { ReportId_StartImageData, encodingMode };

      set(cmd);
    }
       


    void Protocol::setStartImageDataArea(EncodingMode encodingMode, Rectangle const & area)
    {
      std::array<uint8_t,11> cmd = 
      {
        ReportId_StartImageDataArea, 
        encodingMode,
        1,
        static_cast<uint8_t>((area.upperLeftXpixel      ) & 0xffu),
        static_cast<uint8_t>((area.upperLeftXpixel  >> 8) & 0xffu),
        static_cast<uint8_t>((area.upperLeftYpixel      ) & 0xffu),
        static_cast<uint8_t>((area.upperLeftYpixel  >> 8) & 0xffu),
        static_cast<uint8_t>((area.lowerRightXpixel     ) & 0xffu),
        static_cast<uint8_t>((area.lowerRightXpixel >> 8) & 0xffu),
        static_cast<uint8_t>((area.lowerRightYpixel     ) & 0xffu),
        static_cast<uint8_t>((area.lowerRightYpixel >> 8) & 0xffu)        
      };

      set(cmd);
    }



    void Protocol::setImageDataBlock(ImageDataBlock const & data)
    {
      if (data.length > sizeof(data.data))
        throw std::range_error("length is out of range");

      std::array<uint8_t,256> cmd = { ReportId_ImageDataBlock };

      cmd[1] = data.length;
      cmd[2] = 0;
      std::copy_n(data.data.begin(), data.length, cmd.begin()+3);
  
      set(cmd);
    }



    void Protocol::setEndImageData(EndImageDataFlag endImageDataFlag)
    {
      std::array<uint8_t,2> cmd = { ReportId_EndImageData, endImageDataFlag };

      set(cmd);
    }



    Protocol::HandwritingThicknessColor Protocol::getHandwritingThicknessColor()
    {
      std::array<uint8_t,4> cmd = { ReportId_HandwritingThicknessColor };

      get(cmd);

      HandwritingThicknessColor data;

      data.penColor     = static_cast<uint16_t>(cmd[1] | cmd[2] << 8);
      data.penThickness = cmd[3];

      return std::move(data);
    }



    void Protocol::setHandwritingThicknessColor(HandwritingThicknessColor const & handwritingThicknessColor)
    {
      std::array<uint8_t,4> cmd = 
      {
        ReportId_HandwritingThicknessColor, 
        static_cast<uint8_t>( handwritingThicknessColor.penColor     & 0xffu),
        static_cast<uint8_t>((handwritingThicknessColor.penColor>>8) & 0xffu),
        handwritingThicknessColor.penThickness
      };

      set(cmd);
    }




    Protocol::HandwritingThicknessColor24 Protocol::getHandwritingThicknessColor24()
    {
      std::array<uint8_t,5> cmd = { ReportId_HandwritingThicknessColor24 };

      get(cmd);

      HandwritingThicknessColor24 data;

      data.penColor     = static_cast<uint32_t>(cmd[1]) | (static_cast<uint32_t>(cmd[2])<<8) | static_cast<uint32_t>((cmd[3])<<16);
      data.penThickness = cmd[4];

      return std::move(data);
    }



    void Protocol::setHandwritingThicknessColor24(HandwritingThicknessColor24 const & handwritingThicknessColor)
    {
      std::array<uint8_t,5> cmd = 
      {
        ReportId_HandwritingThicknessColor24, 
        static_cast<uint8_t>( handwritingThicknessColor.penColor      & 0xffu),
        static_cast<uint8_t>((handwritingThicknessColor.penColor>>8)  & 0xffu),
        static_cast<uint8_t>((handwritingThicknessColor.penColor>>16) & 0xffu),
        handwritingThicknessColor.penThickness
      };

      set(cmd);
    }




    uint16_t Protocol::getBackgroundColor()
    {
      std::array<uint8_t,3> cmd = { ReportId_BackgroundColor };

      get(cmd);

      uint16_t data = static_cast<uint16_t>(cmd[1] | cmd[2] << 8);

      return data;
    }



    void Protocol::setBackgroundColor(uint16_t backgroundColor)
    {
      std::array<uint8_t,3> cmd = 
      {
        ReportId_BackgroundColor,
        static_cast<uint8_t>( backgroundColor     & 0xffu),
        static_cast<uint8_t>((backgroundColor>>8) & 0xffu)
      };

      set(cmd);
    }




    uint32_t Protocol::getBackgroundColor24()
    {
      std::array<uint8_t,4> cmd = { ReportId_BackgroundColor24 };

      get(cmd);

      uint32_t data = static_cast<uint32_t>(cmd[1]) | (static_cast<uint32_t>(cmd[2]) << 8) | (static_cast<uint32_t>(cmd[3]) << 16);

      return data;
    }



    void Protocol::setBackgroundColor24(uint32_t backgroundColor)
    {
      std::array<uint8_t,4> cmd = 
      {
        ReportId_BackgroundColor24,
        static_cast<uint8_t>( backgroundColor      & 0xffu),
        static_cast<uint8_t>((backgroundColor>>8)  & 0xffu),
        static_cast<uint8_t>((backgroundColor>>16) & 0xffu)
      };

      set(cmd);
    }



    Protocol::Rectangle Protocol::getHandwritingDisplayArea()
    {
      std::array<uint8_t,9> cmd = { ReportId_HandwritingDisplayArea };

      get(cmd);

      Rectangle data;

      data.upperLeftXpixel  = static_cast<uint16_t>(cmd[1] | cmd[2] << 8);
      data.upperLeftYpixel  = static_cast<uint16_t>(cmd[3] | cmd[4] << 8);
      data.lowerRightXpixel = static_cast<uint16_t>(cmd[5] | cmd[6] << 8);
      data.lowerRightYpixel = static_cast<uint16_t>(cmd[7] | cmd[8] << 8);

      return std::move(data);
    }



    void Protocol::setHandwritingDisplayArea(Rectangle const & handwritingDisplayArea)
    {
      std::array<uint8_t,9> cmd = 
      {
        ReportId_HandwritingDisplayArea,
        static_cast<uint8_t>( handwritingDisplayArea.upperLeftXpixel      & 0xffu),
        static_cast<uint8_t>((handwritingDisplayArea.upperLeftXpixel >>8) & 0xffu),
        static_cast<uint8_t>( handwritingDisplayArea.upperLeftYpixel      & 0xffu),
        static_cast<uint8_t>((handwritingDisplayArea.upperLeftYpixel >>8) & 0xffu),
        static_cast<uint8_t>( handwritingDisplayArea.lowerRightXpixel     & 0xffu),
        static_cast<uint8_t>((handwritingDisplayArea.lowerRightXpixel>>8) & 0xffu),
        static_cast<uint8_t>( handwritingDisplayArea.lowerRightYpixel     & 0xffu),
        static_cast<uint8_t>((handwritingDisplayArea.lowerRightYpixel>>8) & 0xffu)
      };

      set(cmd);
    }



    uint16_t Protocol::getBacklightBrightness()
    {
      std::array<uint8_t,3> cmd = { ReportId_BacklightBrightness };

      get(cmd);

      uint16_t data = static_cast<uint16_t>(cmd[1] | cmd[2]<<8);

      return data;
    }



    void Protocol::setBacklightBrightness(uint16_t backlightBrightness)
    {
      std::array<uint8_t,3> cmd = 
      {
        ReportId_BacklightBrightness,
        static_cast<uint8_t>( backlightBrightness     & 0xffu),
        static_cast<uint8_t>((backlightBrightness>>8) & 0xffu)
      };

      set(cmd);
    }
    


    uint16_t Protocol::getScreenContrast()
    {
      std::array<uint8_t,3> cmd = { ReportId_ScreenContrast };

      get(cmd);

      uint16_t data = static_cast<uint16_t>(cmd[1] | cmd[2]<<8);

      return data;
    }
    


    void Protocol::setScreenContrast(uint16_t screenContrast)
    {
      std::array<uint8_t,3> cmd = 
      {
        ReportId_ScreenContrast,
        static_cast<uint8_t>( screenContrast     & 0xffu),
        static_cast<uint8_t>((screenContrast>>8) & 0xffu)
      };

      set(cmd);
    }



    uint8_t Protocol::getPenDataOptionMode()
    {
      std::array<uint8_t,2> cmd = { ReportId_PenDataOptionMode };

      get(cmd);

      return cmd[1];
    }



    void Protocol::setPenDataOptionMode(PenDataOptionMode penDataOptionMode)
    {
      std::array<uint8_t,2> cmd = { ReportId_PenDataOptionMode, penDataOptionMode };

      set(cmd);
    }



    Protocol::EncryptionStatus Protocol::getEncryptionStatus()
    {
      std::array<uint8_t,17> cmd = { ReportId_EncryptionStatus };

      get(cmd);

      EncryptionStatus data;

      data.symmetricKeyType      = cmd[1];
      data.asymmetricPaddingType = static_cast<uint8_t>((cmd[2] >> 6) & 0x03);
      data.asymmetricKeyType     = static_cast<uint8_t>(cmd[2] & 0x3F);
      // cmd[3] reserved
      data.statusCodeRSAe = cmd[4];
      data.statusCodeRSAn = cmd[5];
      data.statusCodeRSAc = cmd[6];
      data.lastResultCode = cmd[7];
      data.rng            = (cmd[8] & 0x4) != 0;
      data.sha1           = (cmd[8] & 0x2) != 0;
      data.aes            = (cmd[8] & 0x1) != 0;

      return std::move(data);
    }



    Protocol::EncryptionCommand Protocol::EncryptionCommand::initializeSetEncryptionType(Protocol::SymmetricKeyType symmetricKeyType, Protocol::AsymmetricPaddingType asymmetricPaddingType, Protocol::AsymmetricKeyType asymmetricKeyType)
    {
      EncryptionCommand encryptionCommand =
      {
        EncryptionCommandNumber_SetEncryptionType,
        symmetricKeyType,
        static_cast<uint8_t>((((asymmetricPaddingType & 0x3) << 6) | (asymmetricKeyType & 0x3f)) & 0xffu)
      };

      return std::move(encryptionCommand);
    }



    Protocol::EncryptionCommand Protocol::EncryptionCommand::initializeSetParameterBlock(EncryptionCommandParameterBlockIndex index, uint8_t length, uint8_t const * data)
    {
      EncryptionCommand encryptionCommand =
      {
        EncryptionCommandNumber_SetParameterBlock,
        index,
        length
      };
      if (data)
      {
        std::copy_n(data, length, encryptionCommand.data.begin());
      }

      return std::move(encryptionCommand);
    }

       

    Protocol::EncryptionCommand Protocol::EncryptionCommand::initializeGenerateSymmetricKey()
    {
      EncryptionCommand encryptionCommand =
      {
        EncryptionCommandNumber_GenerateSymmetricKey
      };

      return std::move(encryptionCommand);
    }


    Protocol::EncryptionCommand Protocol::EncryptionCommand::initializeGetParameterBlock(EncryptionCommandParameterBlockIndex index, uint8_t offset)
    {
      EncryptionCommand encryptionCommand =
      {
        EncryptionCommandNumber_GetParameterBlock,
        index,
        offset
      };

      return std::move(encryptionCommand);
    }


#if 0
    void Protocol::getEncryptionCommand(EncryptionCommand & data)
    {
      std::array<uint8_t,68> cmd = { ReportId_EncryptionCommand };
  
      cmd[1] = data.encryptionCommandNumber;
      cmd[2] = data.parameter;
      cmd[3] = data.lengthOrIndex;      
      std::copy_n(data.data, 64, cmd.begin()+4);
  
      get(cmd);

      data.encryptionCommandNumber = static_cast<EncryptionCommandNumber>(cmd[1]);
      data.parameter               = cmd[2];
      data.lengthOrIndex           = cmd[3];
      std::copy(cmd.begin()+4, cmd.end(), data.data);  
    }
#else
    Protocol::EncryptionCommand Protocol::getEncryptionCommand(Protocol::EncryptionCommandNumber encryptionCommandNumber)
    {
      std::array<uint8_t,68> cmd = { ReportId_EncryptionCommand, encryptionCommandNumber };
  
      get(cmd);

      EncryptionCommand data;

      data.encryptionCommandNumber = static_cast<EncryptionCommandNumber>(cmd[1]);
      data.parameter               = cmd[2];
      data.lengthOrIndex           = cmd[3];

      std::copy(cmd.begin()+4, cmd.end(), data.data.begin());  

      return std::move(data);
    }
#endif



    void Protocol::setEncryptionCommand(EncryptionCommand const & data)
    {
      std::array<uint8_t,68> cmd = { ReportId_EncryptionCommand };
  
      cmd[1] = data.encryptionCommandNumber;
      cmd[2] = data.parameter;
      cmd[3] = data.lengthOrIndex;      
      std::copy(data.data.begin(), data.data.end(), cmd.begin()+4);
  
      set(cmd);
    }

    
      

  } // namespace STU

} // namespace WacomGSS
