#include <WacomGSS/STU/Linux/SerialInterface.hpp>
#include <WacomGSS/STU/getUsbDevices.hpp>
#include <system_error>

namespace WacomGSS
{
  namespace STU
  {

    std::error_code SerialInterface::connect(std::string const & /*fileName*/, bool /*useCrc*/)
    {
      return std::make_error_code(std::errc::not_supported);
    }

    std::error_code SerialInterface::connect(char const * /*fileName*/, bool /*useCrc*/)
    {
      return std::make_error_code(std::errc::not_supported);
    }



    void SerialInterface::disconnect()
    {
    }



    bool SerialInterface::isConnected() const
    {
      return false;
    }



    void SerialInterface::get(uint8_t * /*out*/ data, size_t length)
    {
      throw std::make_error_code(std::errc::not_supported);
    }



    void SerialInterface::set(uint8_t const * data, size_t length)
    {
      throw std::make_error_code(std::errc::not_supported);
    }



    bool SerialInterface::supportsWrite() const 
    {
      throw std::make_error_code(std::errc::not_supported);
    }



    void SerialInterface::write(uint8_t const * data, size_t length)
    {
      throw std::make_error_code(std::errc::not_supported);
    }



    bool SerialInterface::getReportCountLengths(std::array<std::uint16_t, 256> & reportCountLengths) const
    {
      // Taken from STU-500 firmware 1.04
      static const std::array<std::uint16_t, 256> data =
      {{
         0,  0,  0,  5,  2,  0,  0,  0, 17, 17,  5,  0,  0,  0,  0,  0,
         0,  0,  0, 17, 17,  5,  2,  0,  0,  0, 17,  3,  0,  0,  0,  0,
         2,  2,  5,  0,  0,  2,256,  2,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         5,  0,  5, 17,  0, 17, 64,  2,  2, 17,  3, 12,  2,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
      }};
      std::copy(data.begin(), data.end(), reportCountLengths.begin());
      return true;
    }



    std::uint16_t SerialInterface::getProductId() const
    {
      // Only the STU-500 supports serial interface.
      return ProductId_500;
    }

  } // namespace STU
  
} // namespace WacomGSS

