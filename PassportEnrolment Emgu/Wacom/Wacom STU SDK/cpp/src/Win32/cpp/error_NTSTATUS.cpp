#include <WacomGSS/Win32/hidsdi.hpp>
#include <sstream>

namespace WacomGSS
{
  namespace Win32
  {

    class NTSTATUS_error_category_impl : public std::error_category
    {
    public:
      char const * name() const override
      {
        return "NTSTATUS";
      }
      
      std::string message(int ev) const override;
    };

    std::string NTSTATUS_error_category_impl::message(int ev) const
    {    
      std::stringstream s;
      s << static_cast<NTSTATUS>(ev);
      return s.str();
    }



    std::error_category const & NTSTATUS_error_category() noexcept
    {
      static NTSTATUS_error_category_impl ec;
      return ec;
    }

  }
}
