#include <WacomGSS/Win32/cfgmgr32.hpp>
#include <sstream>

namespace WacomGSS
{
  namespace Win32
  {

    class CONFIGRET_error_category_impl : public std::error_category
    {
    public:
      char const * name() const override
      {
        return "CONFIGRET";
      }
      
      std::string message(int ev) const override
      {
        std::stringstream s;
        s << static_cast<CONFIGRET>(ev);
        return s.str();
      }
    };

    std::error_category const & CONFIGRET_error_category() noexcept
    {
      static CONFIGRET_error_category_impl ec;
      return ec;
    }


  }
}
