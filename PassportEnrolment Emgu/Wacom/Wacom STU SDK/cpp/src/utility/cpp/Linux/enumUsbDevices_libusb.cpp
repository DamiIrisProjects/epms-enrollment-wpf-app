#include <WacomGSS/enumUsbDevices.hpp>
#include <WacomGSS/Linux/libusb.hpp>


namespace WacomGSS
{


  void enumUsbDevices(std::function<bool (UsbDevice & usbDevice)> f)
  {
    libusb::context     context;
    libusb::device_list deviceList( context.get_device_list() );
    for (ssize_t i = 0; i < deviceList.size(); ++i)
    {
      libusb::device dev(deviceList[i]);
      
      ::libusb_device_descriptor deviceDescriptor( dev.get_device_descriptor() );

      libusb::config_descriptor config( dev.get_config_descriptor(0) );

      UsbDevice usbDevice;

      usbDevice.idVendor  = deviceDescriptor.idVendor;
      usbDevice.idProduct = deviceDescriptor.idProduct;
      usbDevice.bcdDevice = deviceDescriptor.bcdDevice;

      usbDevice.isMI = config->bNumInterfaces > 1;

      usbDevice.busNumber     = dev.get_bus_number();
      usbDevice.deviceAddress = dev.get_device_address();

      if (!f(usbDevice))
      {
        break;
      }
    }
  }


} // namespace WacomGSS

