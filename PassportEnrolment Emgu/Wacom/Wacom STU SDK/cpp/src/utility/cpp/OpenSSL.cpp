/// @file      OpenSSL.cpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2011-10-18
/// @brief     provides a thin layer of objects from the OpenSSL library to provide basic exception safety.

#include <WacomGSS/OpenSSL.hpp>

#include <openssl/dh.h>
#include <openssl/aes.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/rand.h>

#if defined(WacomGSS_WIN32)
#ifdef _DEBUG
# pragma comment (lib, "libeay32MTd.lib")
#else
# pragma comment (lib, "libeay32MT.lib")
#endif
#endif


namespace WacomGSS
{
  namespace OpenSSL
  {


    class error_category : public std::error_category
    {
    public:
      char const * name() const noexcept override
      {
        return "OpenSSL";
      }

      //error_condition default_error_condition(int ev) const noexcept;

      std::string message(int ev) const override;
    };


    static error_category ec;
    std::error_category const & openssl_error_category() noexcept
    {
      return ec;
    }

    std::string error_category::message(int ev) const
    {
      // http://www.openssl.org/docs/crypto/ERR_error_string.html
      // buf must be at least 120 bytes long.
      const int buffer_size = 256;

      char buffer[buffer_size] = { 0 };
      ::ERR_error_string_n(static_cast<unsigned long>(ev), buffer, buffer_size);

      return std::string(buffer);
    }



    error::error()
    :
      std::system_error(static_cast<int>(ERR_get_error()), openssl_error_category())
    {
    }



    error::error(char const * what_arg)
    :
      std::system_error(static_cast<int>(ERR_get_error()), openssl_error_category(), what_arg)
    {
    }



    error::error(unsigned long err)
    :
      std::system_error(static_cast<int>(err), openssl_error_category())
    {
    }



    error::error(unsigned long err, char const * what_arg)
    :
      std::system_error(static_cast<int>(err), openssl_error_category(), what_arg)
    {
    }



    // fake-pointer to real-pointer casts
    static ::BIGNUM *        f2r(BIGNUM_ *         value) { return reinterpret_cast< ::BIGNUM *       >(value); }
    static ::DH *            f2r(DH_ *             value) { return reinterpret_cast< ::DH *           >(value); }
    static ::RSA *           f2r(RSA_ *            value) { return reinterpret_cast< ::RSA *          >(value); }

    // real-pointer to fake-pointer casts
    static BIGNUM_  *        r2f    (::BIGNUM *    value) { return reinterpret_cast< BIGNUM_  *       >(value); }
    static BIGNUM_  * &      r2f_ref(::BIGNUM * &  value) { return reinterpret_cast< BIGNUM_  * &     >(value); }
    static DH_ *             r2f    (::DH *        value) { return reinterpret_cast< DH_  *           >(value); }
    static AES_KEY_ *        r2f    (::AES_KEY *   value) { return reinterpret_cast< AES_KEY_ *       >(value); }
    static RSA_ *            r2f    (::RSA *       value) { return reinterpret_cast< RSA_ *           >(value); }

    // object to real-pointer casts
    static ::BIGNUM const *  o2r(BIGNUM const &    value) { return reinterpret_cast< ::BIGNUM const * >(value.get() ); }
    static ::BIGNUM const *  o2r(BIGNUMref const & value) { return reinterpret_cast< ::BIGNUM const * >(value.get() ); }
    static ::BIGNUM *        o2r(BIGNUM &          value) { return reinterpret_cast< ::BIGNUM *       >(value.get() ); }
    static ::BIGNUM *        o2r(BIGNUMref &       value) { return reinterpret_cast< ::BIGNUM *       >(value.get() ); }
    
    static ::DH *            o2r(DH &              value) { return reinterpret_cast< ::DH *           >(value.get() ); }
    static ::AES_KEY *       o2r(AES_KEY *         value) { return reinterpret_cast< ::AES_KEY *      >(value->get()); }
    static ::AES_KEY const * o2r(AES_KEY const *   value) { return reinterpret_cast< ::AES_KEY const *>(value->get()); }
    static ::RSA *           o2r(RSA &             value) { return reinterpret_cast< ::RSA *          >(value.get() ); }



    void BIGNUM_BN_free_trait::close(BIGNUM_ * value) noexcept
    {
      if (value)
        ::BN_free(f2r(value));
    }



    void BIGNUM_BN_free_trait::throw_invalid_handle_exception()
    {
      throw error(::ERR_get_error());
    }

    BIGNUM::BIGNUM()
    :
      base(r2f(::BN_new()))
    {
    }

    BIGNUM::BIGNUM(BIGNUM_ * ref)
    :
      base(ref)
    {      
    }


    void DH_DH_free_trait::close(DH_ * value) noexcept
    {
      if (value) 
      {
        /*
        if (value->priv_key)
        {
          BN_free(value->priv_key);
          value->priv_key = nullptr;
        }

        if (value->p)
        {
          BN_free(value->p);
          value->p = nullptr;
        }
    
        if (value->g)
        {
          BN_free(value->g);
          value->g = nullptr;
        }
        */
      
        ::DH_free(f2r(value));
      }
    }



    void DH_DH_free_trait::throw_invalid_handle_exception()
    {
      throw error(::ERR_get_error());
    }



    void RSA_RSA_free_trait::close(RSA_ * value) noexcept
    {
      if (value) 
      {
        ::RSA_free(f2r(value));
      }
    }



    void RSA_RSA_free_trait::throw_invalid_handle_exception()
    {
      throw error(::ERR_get_error());
    }


    
    AES_KEY::AES_KEY()
    :
      unique_ptr<AES_KEY_>(r2f(new ::AES_KEY))
    {
      std::memset(get(), 0x00, sizeof(::AES_KEY));
    }

    AES_KEY::~AES_KEY()
    {      
      if (auto p = get())
      {
        memset_s(p, 0x00, sizeof(::AES_KEY)); // use secure memset
      }      
    }

    DH::DH()
    :
      base    (r2f(::DH_new())),
      p       (r2f_ref(f2r(get())->p)),
      g       (r2f_ref(f2r(get())->g)),
      pub_key (r2f_ref(f2r(get())->pub_key)),
      priv_key(r2f_ref(f2r(get())->priv_key))
    {
    }



    BIGNUMref & BIGNUMref::operator = (BIGNUM && value)
    {
      if (m_ref) 
      {
        ::BN_free(f2r(m_ref));
        m_ref = nullptr;
      }
      m_ref = value.release();
      return *this;
    }

    BIGNUMref & BIGNUMref::operator = (std::nullptr_t)
    {
      if (m_ref) 
      {
        ::BN_free(f2r(m_ref));
        m_ref = nullptr;
      }
      return *this;
    }



    int DH_generate_key(DH & dh)
    {
      return ::DH_generate_key(o2r(dh));
    }



    size_t DH_compute_key(unsigned char * key, BIGNUM const & pub_key, DH & dh)
    {
      int ret = ::DH_compute_key(key, o2r(pub_key), o2r(dh));
      return static_cast<size_t>(ret);
    }



    int DH_check(DH & dh, int * codes)
    {
      return ::DH_check(o2r(dh), codes);
    }



    BIGNUM BN_bin2bn(unsigned char const * s, size_t len, std::nullptr_t)
    {
      return BIGNUM(r2f(::BN_bin2bn(s, static_cast<int>(len), nullptr)));
    }



    size_t BN_bn2bin(BIGNUMref const & a, unsigned char * to)
    {
      int ret = ::BN_bn2bin(o2r(a), to);
      return static_cast<size_t>(ret);
    }

    size_t BN_bn2bin(BIGNUM const & a, unsigned char * to)
    {
      int ret = ::BN_bn2bin(o2r(a), to);
      return static_cast<size_t>(ret);
    }

    size_t (BN_num_bytes)(BIGNUMref const & a)
    {
      int ret = BN_num_bytes(o2r(a));
      return static_cast<size_t>(ret);
    }


    size_t (BN_num_bytes)(BIGNUM const & a)
    {
      int ret = BN_num_bytes(o2r(a));
      return static_cast<size_t>(ret);
    }

    int BN_set_word(BIGNUM & a, unsigned long w)
    {
      return ::BN_set_word(o2r(a), w);
    }

    int BN_set_word(BIGNUMref & a, unsigned long w)
    {
      return ::BN_set_word(o2r(a), w);
    }

    int AES_set_decrypt_key(unsigned char const * userKey, const int bits, AES_KEY * key)
    {
      return ::AES_set_decrypt_key(userKey, bits, o2r(key));
    }



    void AES_decrypt(unsigned char const * in, unsigned char * out, AES_KEY const * key)
    {
      ::AES_decrypt(in, out, o2r(key));
    }


    int RAND_pseudo_bytes(unsigned char * buf, int num)
    {
      return ::RAND_pseudo_bytes(buf, num);
    }


    RSA::RSA()
    :
      base(r2f(::RSA_new())),
      e   (r2f_ref(f2r(get())->e)),
      n   (r2f_ref(f2r(get())->n))
    {
    }

    int RSA_generate_key_ex(RSA & rsa, int bits, BIGNUM & e, std::nullptr_t)
    {
      return ::RSA_generate_key_ex(o2r(rsa), bits, o2r(e), nullptr);
    }

    size_t RSA_size(RSA & rsa)
    {
      int ret = ::RSA_size(o2r(rsa));
      return static_cast<size_t>(ret);
    }


    size_t RSA_private_decrypt(size_t flen, unsigned char const * in, unsigned char * to, RSA & rsa, int padding)
    {
      int flen_ = static_cast<int>(flen);
      int ret = ::RSA_private_decrypt(flen_, in, to, o2r(rsa), padding);
      return static_cast<size_t>(ret);
    }


  } // namespace OpenSSL
  
} // namespace WacomGSS
