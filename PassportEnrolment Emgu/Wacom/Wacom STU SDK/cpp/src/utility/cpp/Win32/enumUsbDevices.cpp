#include <WacomGSS/enumUsbDevices.hpp>
#include <WacomGSS/Win32/setupapi.hpp>
#include <WacomGSS/Win32/hidsdi.hpp>
#include <memory>


namespace WacomGSS
{
  static bool parseHexStrict(wchar_t c, WORD shift, WORD & value) noexcept
  {
    if (c) 
    {
      if (c >= L'0' && c <= L'9') { value |= WORD(c - '0'     )<<shift; return true; }
      if (c >= L'A' && c <= L'F') { value |= WORD(c - 'A' + 10)<<shift; return true; }
      if (c >= L'a' && c <= L'f') { value |= WORD(c - 'a' + 10)<<shift; return true; }

      // On Windows 7, it has been seen that a value of ":101" has been returned, instead of "A101".
      if (c >= L':' && c <= L'?') { value |= WORD(c - ':' + 10)<<shift; return true; }
    }
    return false;
  }
  


  static bool parseHexStrict(wchar_t const * p, WORD & value) noexcept
  {
    value = 0;        
    return p && parseHexStrict(p[0], 12, value) && parseHexStrict(p[1], 8, value) && parseHexStrict(p[2], 4, value) && parseHexStrict(p[3], 0, value);
  }



  static bool parseHardwareId(wchar_t const * pszHardwareId, WORD & vendorId, WORD & productId, WORD & versionId, bool & isMI) noexcept
  {
    // Expected string format of pszHardwareId:
    // 
    //           1         2         3 
    // 012345678901234567890123456789012345
    //
    // HID\VID_056A&PID_00A1&REV_0100
    // HID\VID_056A&PID_00A3&REV_0102&MI_00
    //

    vendorId  = 0x0000;
    productId = 0x0000;
    versionId = 0x0000;
    isMI      = false;

    if (pszHardwareId) 
    {
      int l = ::lstrlenW(pszHardwareId);
      if (l && l >= 29) 
      {
        static const WCHAR szHidVid[] = L"HID\\VID_";
        static const int   cchHidVid  = (sizeof(szHidVid)/sizeof(szHidVid[0]))-1;

        static const WCHAR szPid[] = L"&PID_";
        static const int   cchPid  = (sizeof(szPid)/sizeof(szPid[0]))-1;

        static const WCHAR szRev[] = L"&REV_";
        static const int   cchRev  = (sizeof(szRev)/sizeof(szRev[0]))-1;

        static const WCHAR szMi[] = L"&MI_";
        static const int   cchMi = (sizeof(szMi)/sizeof(szMi[0]))-1;

        if (::CompareStringW(LOCALE_INVARIANT, NORM_IGNORECASE, pszHardwareId, cchHidVid, szHidVid, cchHidVid) == CSTR_EQUAL) 
        {         
          pszHardwareId += cchHidVid;
          if (parseHexStrict(pszHardwareId, vendorId)) 
          {
            pszHardwareId += 4;
            if (::CompareStringW(LOCALE_INVARIANT, NORM_IGNORECASE, pszHardwareId, cchPid, szPid, cchPid) == CSTR_EQUAL) 
            {         
              pszHardwareId += cchPid;
              if (parseHexStrict(pszHardwareId, productId)) 
              {
                pszHardwareId += 4;
                if (::CompareStringW(LOCALE_INVARIANT, NORM_IGNORECASE, pszHardwareId, cchRev, szRev, cchRev) == CSTR_EQUAL) 
                {         
                  pszHardwareId += cchRev;
                  if (parseHexStrict(pszHardwareId, versionId)) 
                  {
                    pszHardwareId += 4;
                  
                    isMI = (::CompareStringW(LOCALE_INVARIANT, NORM_IGNORECASE, pszHardwareId, cchMi, szMi, cchMi) == CSTR_EQUAL);

                    return true;
                  }
                }
              }
            }
          }
        }
      }
    }
    return false;
  }
  


  bool (* enumUsbDevices_unhandledParseHardwareId)(wchar_t const *, wchar_t const *);
  


  void enumUsbDevices(std::function<bool (UsbDevice & usbDevice)> f)
  {
    using namespace Win32;

    GUID guidHid;
    HidD_GetHidGuid(&guidHid);

    HDevInfo hDevInfo(::SetupDiGetClassDevsEx(&guidHid, 0, 0, DIGCF_PRESENT|DIGCF_DEVICEINTERFACE, 0, 0, 0));
  
    SP_DEVICE_INTERFACE_DATA DeviceInterfaceData;
    DeviceInterfaceData.cbSize = sizeof(DeviceInterfaceData);

    for (DWORD dwIndex = 0;; ++dwIndex) 
    {    
      if (SetupDiEnumDeviceInterfaces(hDevInfo.get(), 0, &guidHid, dwIndex, &DeviceInterfaceData)) 
      {
        SP_DEVINFO_DATA	DevInfoData;
        DevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
  
        DWORD dwSize;

        dwSize = 0;
        win32api_bool(SetupDiGetDeviceInterfaceDetail(hDevInfo.get(), &DeviceInterfaceData, 0, 0, &dwSize, 0) || GetLastError() == ERROR_INSUFFICIENT_BUFFER);
          
        std::unique_ptr<BYTE[]>          pBuffer(new BYTE[dwSize]);                
        PSP_DEVICE_INTERFACE_DETAIL_DATA pDeviceInterfaceDetailData = reinterpret_cast<PSP_DEVICE_INTERFACE_DETAIL_DATA>(pBuffer.get());          
        pDeviceInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

        win32api_BOOL(SetupDiGetDeviceInterfaceDetail(hDevInfo.get(), &DeviceInterfaceData, pDeviceInterfaceDetailData, dwSize, 0, &DevInfoData));

        dwSize = 0;
        win32api_bool(SetupDiGetDeviceRegistryProperty(hDevInfo.get(), &DevInfoData, SPDRP_HARDWAREID, 0, 0, 0, &dwSize) || GetLastError() == ERROR_INSUFFICIENT_BUFFER);          

        // Although dwSize is returned in bytes, we are allocating in WCHAR due to an error in Windows 2000,
        // referred to at: http://support.microsoft.com/kb/259695
        std::unique_ptr<WCHAR[]> pHardwareId(new WCHAR[dwSize]);
        dwSize *= sizeof(WCHAR);

        win32api_BOOL(SetupDiGetDeviceRegistryProperty(hDevInfo.get(), &DevInfoData, SPDRP_HARDWAREID, 0, reinterpret_cast<PBYTE>(pHardwareId.get()), dwSize, 0));

        UsbDevice usbDevice;
        if (parseHardwareId(pHardwareId.get(), usbDevice.idVendor, usbDevice.idProduct, usbDevice.bcdDevice, usbDevice.isMI))
        {
          usbDevice.fileName = pDeviceInterfaceDetailData->DevicePath;
          usbDevice.devInst  = DevInfoData.DevInst;

          if (!f(usbDevice)) 
          {
            break;
          }
        }
        else
        {
          if (enumUsbDevices_unhandledParseHardwareId && !enumUsbDevices_unhandledParseHardwareId(pHardwareId.get(), pDeviceInterfaceDetailData->DevicePath))
          {
            break;
          }
        }
      }
      else
      {
        DWORD dwError = GetLastError();
        if (dwError != ERROR_NO_MORE_ITEMS) 
        {
          throw_win32api_error(dwError, "SetupDiEnumDeviceInterfaces");
        }
        break;
      }

    } // for(;;)
  }


} // namespace WacomGSS

