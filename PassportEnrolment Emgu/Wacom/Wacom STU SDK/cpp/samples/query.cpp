/// @file      query.cpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2012-03-22
/// @brief     simple test harness

/// TODO: Add comments!

#include <WacomGSS/STU/getUsbDevices.hpp>
#include <WacomGSS/STU/UsbInterface.hpp>
#include <WacomGSS/STU/ProtocolHelper.hpp>

#include <iostream>
#include <iomanip>


template<typename InputIterator> struct ArrHex
{
  InputIterator begin, end;
  ArrHex(InputIterator const & b, InputIterator const & e) : begin(b), end(e) {}
};
template<typename InputIterator> std::ostream & operator << (std::ostream & o, ArrHex<InputIterator> const & arr)
{
  o << std::hex << std::setfill('0');
  for (auto i = arr.begin; i != arr.end; ++i)
    o << std::setw(2) << (unsigned)*i;
  o << std::dec << std::setfill(' ');
  return o;
}
template<typename InputIterator> auto arrhex(InputIterator const & b, InputIterator const & e) -> ArrHex<InputIterator>               { return ArrHex<InputIterator>(b, e); }
template<typename Container>     auto arrhex(Container const & c)                              -> decltype(arrhex(c.begin(),c.end())) { return arrhex(c.begin(), c.end()); }

using namespace WacomGSS::STU::ProtocolHelper::ostream_operators;

void query(WacomGSS::STU::Protocol protocol)
{
  using namespace std;
  using namespace WacomGSS::STU;

  Protocol::Status status = protocol.getStatus();
  cout
    << "Status.statusCode                       = " << status.statusCode << endl
    << "Status.lastResultCode                   = " << status.lastResultCode << endl
    << "Status.statusWord                       = " << status.statusWord << endl
  ;

  Protocol::Information inf = protocol.getInformation();
  cout 
    << "Information.modelName                   = " << inf.modelNameNullTerminated        << endl
    << "Information.firmwareMajorVersion        = " << hex << setw(2) << setfill('0') << (unsigned)inf.firmwareMajorVersion << dec << setfill(' ') << endl
    << "Information.firmwareMinorVersion        = " << hex << setw(2) << setfill('0') << (unsigned)inf.firmwareMinorVersion << dec << setfill(' ') << endl
    << "Information.secureIc                    = " << (unsigned)inf.secureIc             << endl
    << "Information.secureVersion               = " << hex << (unsigned)inf.secureIcVersion[0] << '.'
                                                           << (unsigned)inf.secureIcVersion[1] << '.'
                                                           << (unsigned)inf.secureIcVersion[2] << '.'
                                                           << (unsigned)inf.secureIcVersion[3] <<
                                                       dec << endl
  ;

  Protocol::Capability caps = protocol.getCapability();
  cout
    << "Capability.tabletMaxX                   = " << caps.tabletMaxX              << endl
    << "Capability.tabletMaxY                   = " << caps.tabletMaxY              << endl
    << "Capability.tabletMaxPressure            = " << caps.tabletMaxPressure       << endl
    << "Capability.screenWidth                  = " << caps.screenWidth             << endl
    << "Capability.screenHeight                 = " << caps.screenHeight            << endl
    << "Capability.maxReportRate                = " << (unsigned)caps.maxReportRate << endl
    << "Capability.resolution                   = " << caps.resolution              << endl
    << "Capability.zlibColorSupport             = " << (caps.zlibColorSupport ? "YES":"NO") << endl
  ;

  uint32_t uid = protocol.getUid();
  cout 
    << "Uid                                     = " << "0x" << hex << setw(8) << setfill('0') << uid << dec << setfill(' ') << endl;

  try
  {
    Protocol::PublicKey hostPublicKey = protocol.getHostPublicKey();
  
    cout << "HostPublicKey                           = " << arrhex(hostPublicKey) << endl;
    Protocol::PublicKey devicePublicKey = protocol.getDevicePublicKey();
    cout << "DevicePublicKey                         = " << arrhex(devicePublicKey) << endl;
  }
  catch (system_error const &)
  {
    // Win32  = ERROR_GEN_FAILURE
    // libusb = ::LIBUSB_ERROR_PIPE
    cout << "HostPublicKey / DevicePublicKey - not supported" << endl;
  }

  Protocol::DHprime dhPrime = protocol.getDHprime();
  cout 
    << "DHprime                                 = " << arrhex(dhPrime) << endl;

  Protocol::DHbase dhBase = protocol.getDHbase();
  cout 
    << "DHbase                                  = " << arrhex(dhBase) << endl;

  uint8_t inkingMode = protocol.getInkingMode();
  cout 
    << "InkingMode                              = " << static_cast<Protocol::InkingMode>(inkingMode) << endl;

  Protocol::InkThreshold inkThreshold = protocol.getInkThreshold();
  cout 
    << "InkThreshold.onPressureMark             = " << inkThreshold.onPressureMark << endl
    << "InkThreshold.offPressureMark            = " << inkThreshold.offPressureMark << endl
  ;
  
  try
  {
    Protocol::HandwritingThicknessColor handwritingThicknessColor = protocol.getHandwritingThicknessColor();
    cout
      << "HandwritingThicknessColor.penColor      = " << hex << setw(4) << setfill('0') << handwritingThicknessColor.penColor << setfill(' ') << dec << endl
      << "HandwritingThicknessColor.penThickness  = " << (unsigned)handwritingThicknessColor.penThickness << endl
    ;

    uint16_t backgroundColor = protocol.getBackgroundColor();
    cout 
      << "BackgroundColor                         = " << hex << setw(4) << setfill('0') << backgroundColor << setfill(' ') << dec << endl;

    Protocol::HandwritingDisplayArea handwritingDisplayArea = protocol.getHandwritingDisplayArea();
    cout 
      << "HandwritingDisplayArea.upperLeftXpixel  = " << handwritingDisplayArea.upperLeftXpixel  << endl
      << "HandwritingDisplayArea.upperLeftYpixel  = " << handwritingDisplayArea.upperLeftYpixel  << endl
      << "HandwritingDisplayArea.lowerRightXpixel = " << handwritingDisplayArea.lowerRightXpixel << endl
      << "HandwritingDisplayArea.lowerRightYpixel = " << handwritingDisplayArea.lowerRightYpixel << endl
    ;

    uint16_t backlightBrightness = protocol.getBacklightBrightness();
    cout 
      << "BacklightBrightness                     = " << backlightBrightness << endl;
    
  }
  catch (system_error const &)
  {
    // Win32 = ERROR_CRC
    // libusb = ::LIBUSB_ERROR_PIPE
    cout << "HandwritingThicknessColor / BackgroundColor / BacklightBrightness - not supported" << endl;
  }

  try
  {
    uint8_t penDataOptionMode = protocol.getPenDataOptionMode();
    cout 
      << "PenDataOptionMode                       = " << static_cast<Protocol::PenDataOptionMode>(penDataOptionMode) << endl;
  }
  catch (system_error const &)
  {
    // Win32 = ERROR_CRC
    // libusb = ::LIBUSB_ERROR_PIPE
    cout << "PenDataOptionMode - not supported" << endl;
  }

}


void queryUsb()
{
  using namespace std;
  using namespace WacomGSS::STU;

  vector<UsbDevice> usbDevices = getUsbDevices();

  if (!usbDevices.empty()) 
  {
    for (auto i = usbDevices.begin(); i != usbDevices.end(); ++i)
    {
      try
      {
        cout << "Device: " << hex << setfill('0') << setw(4) << i->idVendor << ':' << setw(4) << i->idProduct << ':' << setw(4) << i->bcdDevice << dec << endl;

        UsbInterface intf;
        std::error_code ec = intf.connect(*i, true);
        if (!ec)
        {
          query(intf);
          intf.disconnect();
        }
        else
        {
          cout << "Failed to connect:" << ec << endl;
        }
        cout << endl;
      }
      catch (system_error const & ex)
      {
        std::cout << "system error: " << ex.what() << " " << ex.code() << " " << ex.code().message() << std::endl;
      }
    }
  }
  else 
  {
    cout << "No USB devices found" << endl;
  }
}



int main()
{
  try
  {
    std::cout << "STU query sample" << std::endl << std::endl;

    queryUsb();
  }
  catch (std::exception const & ex)
  {
    std::cout << "std::exception: " << ex.what() << std::endl;
  }

  return 0;
}
