#pragma once
#include <windows.h>

#define IDD_DemoButtons 1
#define IDC_DemoButtons_Signature 0x8001


#define IDS_DemoButtons_NoTablet 0x00

#define IDS_SignatureForm_Title  0x10
#define IDS_SignatureForm_OK     0x11
#define IDS_SignatureForm_Clear  0x12
#define IDS_SignatureForm_Cancel 0x13
#define IDS_SignatureForm_Exception      0x1e
#define IDS_SignatureForm_ConnectFailed  0x1f
