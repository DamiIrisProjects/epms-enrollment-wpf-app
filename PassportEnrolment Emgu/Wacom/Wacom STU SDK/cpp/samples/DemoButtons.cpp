#include <WacomGSS/Win32/windows.hpp>
#include <WacomGSS/STU/getUsbDevices.hpp>
#include <WacomGSS/STU/UsbInterface.hpp>
#include <WacomGSS/STU/Tablet.hpp>
#include <WacomGSS/STU/Tablet_OpenSSL.hpp>
#include <WacomGSS/STU/ReportHandler.hpp>

#include <WacomGSS/Win32/com.hpp>
#include <WacomGSS/Win32/d2d1.hpp>
#include <WacomGSS/Win32/dwrite.hpp>
#include <WacomGSS/setThreadName.hpp>

#include <WacomGSS/compatibility/atomic.hpp>
#include <WacomGSS/compatibility/thread.hpp>

#include "DemoButtons.h"

#include <locale>
#include <codecvt>


//   tablet: the raw tablet coordinate
//   screen: the tablet LCD screen
//   client: the Form window client area


namespace std
{
  template<class T>
  std::unique_ptr<T> make_unique()
  {
    return std::unique_ptr<T>(new T());
  }  
}

typedef enum tagMONITOR_DPI_TYPE
{
  MDT_Effective_DPI = 0
} MONITOR_DPI_TYPE;

typedef HRESULT (WINAPI *pfn_GetDPIForMonitor)(HMONITOR, MONITOR_DPI_TYPE, UINT *, UINT *);
#define WM_DPICHANGED       0x02E0


std::wstring loadString(HINSTANCE hInstance, UINT uID)
{
  PWSTR p = nullptr;
  int   l = ::LoadStringW(hInstance, uID, reinterpret_cast<PWSTR>(&p), 0);
  WacomGSS::Win32::win32api_bool(l > 0 && p != nullptr, "LoadString");  
  return std::wstring(p, static_cast<std::wstring::size_type>(l));
}

void handleException(HWND hwnd) noexcept
{
  try
  {
    std::stringstream o;      
    try
    {
      throw;
    }
    catch (WacomGSS::STU::Interface::device_removed_error &)
    {
      o << "Device unplugged!";
    }
    catch (std::system_error & e)
    {
      o << "exception: system_error: ";
      char const * what = e.what();
      if (what)
        o << what;
      else
        o << typeid(e).name();

      o << " - ";
      o << e.code();
      o << " \"";
      o << e.code().message();
      o << "\"";
    }
    catch (std::exception & e)
    {
      o << "exception: ";
      char const * what = e.what();
      if (what)
        o << what;
      else
        o << typeid(e).name();
    }
    catch (...)
    {
      o << "unknown exception!";
    }

    auto s = o.str();

    std::wstring_convert<std::codecvt_utf8<wchar_t, 0x10ffff, std::little_endian> > conv;
    auto ws = conv.from_bytes(s);

    HINSTANCE hInstance = reinterpret_cast<HINSTANCE>(::GetWindowLongPtr(hwnd, GWLP_HINSTANCE));

    ::MessageBox(hwnd, ws.c_str(), loadString(hInstance, IDS_SignatureForm_Exception).c_str(), MB_ICONSTOP|MB_OK);          
  }
  catch (...)
  {
  }
}

struct Factory
{
  WacomGSS::Win32::com_ptr<ID2D1Factory>       m_pID2D1Factory; // Factory for creating all D2D1 objects
  WacomGSS::Win32::com_ptr<IWICImagingFactory> m_pIWICImagingFactory; // Factory for creating WIC bitmaps
  WacomGSS::Win32::com_ptr<IDWriteFactory>     m_pIDWriteFactory;
};

class SignatureForm : Factory, WacomGSS::STU::ProtocolHelper::ReportHandler
{
  struct Button
  {
    D2D1_RECT_F boundsScreen;
    D2D1_RECT_F boundsClient;
    std::wstring text;
    void (SignatureForm::* onClick)();
  };
  
  struct RenderData
  {
    bool     dn; // determined by original pressure and ink threshold
    uint16_t x;
    uint16_t y;
    float    pressure; // normalized pressure value

    D2D1_POINT_2F client; // calculated pixel coordinates
  };


  HWND                                  m_hwnd;
  WacomGSS::STU::Tablet                 m_tablet;
  WacomGSS::STU::Protocol::Capability   m_capability;
  WacomGSS::STU::Protocol::InkThreshold m_inkThreshold;
  std::vector<Button>                   m_btns;

  // bitmap sent to screen
  WacomGSS::STU::Protocol::EncodingMode m_encodingMode;
  std::vector<uint8_t>                  m_bitmap;
  
  // pen data collected
  std::deque<RenderData> m_penData;  // The stored pendata for rendering

  size_t                 m_renderIndex; // Currently rendered up to this point
  int                    m_isDown;

  // data needed to render to client.
  D2D1_SIZE_F                                     m_renderDataScale;  // cached size of client window
  FLOAT                                           m_renderInkScale;       // used for inkwidth  
  WacomGSS::Win32::com_ptr<IDWriteTextFormat>     m_textFormat;  
  // D2D1 device dependent data.
  WacomGSS::Win32::com_ptr<ID2D1HwndRenderTarget> m_renderTarget;
  WacomGSS::Win32::com_ptr<ID2D1SolidColorBrush>  m_penInk;
  WacomGSS::Win32::com_ptr<ID2D1SolidColorBrush>  m_textBrush;

  // data used for receiving the data from the tablet and queuing it for processing on the primary thread.  
  std::uint32_t                     m_sessionId;  
  WacomGSS::thread                  m_reader;
  WacomGSS::STU::InterfaceQueue     m_queue; // buffer between Tablet class and this application
  WacomGSS::mutex                                               m_mutex;
  std::deque<WacomGSS::STU::Protocol::PenDataTimeCountSequence> m_readerQueue; // buffer between our background and primary threads.
  std::exception_ptr                                            m_readerException;

  // Windows 8.1 per-monitor DPI 
  WacomGSS::Win32::HModule      m_shcore;
  pfn_GetDPIForMonitor          m_pGetDPIForMonitor; 
   

  void getDPIForMonitor(FLOAT & dpiX, FLOAT & dpiY)
  {
    if (m_pGetDPIForMonitor)
    {
      // Use Windows 8.1 API

      HMONITOR hMonitor = MonitorFromWindow(m_hwnd, MONITOR_DEFAULTTONEAREST);
  
      using namespace WacomGSS::Win32;
      
      UINT ui_dpiX = 0, ui_dpiY = 0;
      hresult_succeeded(m_pGetDPIForMonitor(hMonitor, MDT_Effective_DPI, &ui_dpiX, &ui_dpiY), "GetDPIForMonitor");  
      
      dpiX = static_cast<FLOAT>(ui_dpiX);
      dpiY = static_cast<FLOAT>(ui_dpiY);
    }
    else
    {
      HDC hdc = GetDC(m_hwnd);
      int i_dpiX = GetDeviceCaps(hdc, LOGPIXELSX);
      int i_dpiY = GetDeviceCaps(hdc, LOGPIXELSY);
      ReleaseDC(m_hwnd, hdc);
  
      dpiX = static_cast<FLOAT>(i_dpiX);
      dpiY = static_cast<FLOAT>(i_dpiY);
    }
  }


  void discardDeviceDependentResources()
  {
    m_renderTarget = nullptr;    
    m_penInk       = nullptr;
    m_textBrush    = nullptr;    
  }

  
  template<typename T>
  void calculateButtonPositions(D2D1_SIZE_F renderTargetSize, D2D1_RECT_F Button::* bounds)
  {
    // While we want the button positions to look similar, ensure that 
    // the screen positions are integer while the client positions can be float.
    using namespace WacomGSS::Win32;
        
    T width  = static_cast<T>(renderTargetSize.width);
    T height = static_cast<T>(renderTargetSize.height);

    if (m_tablet.getProductId() != WacomGSS::STU::ProductId_300)
    {
      // Place the buttons across the bottom of the screen.

      FLOAT w2 = static_cast<FLOAT>(width / static_cast<T>(3));
      FLOAT w3 = static_cast<FLOAT>(width / static_cast<T>(3));
      FLOAT w1 = width - w2 - w3;
      FLOAT y  = static_cast<FLOAT>(height * 6 / static_cast<T>(7));
      FLOAT h  = height - y;
            
      m_btns[0].*bounds = D2D1::RectF(0    , y, w1, h);
      m_btns[1].*bounds = D2D1::RectF(w1   , y, w2, h);
      m_btns[2].*bounds = D2D1::RectF(w1+w2, y, w3, h);      
    }
    else
    {
      // The STU-300 is very shallow, so it is better to utilise
      // the buttons to the side of the display instead.

      FLOAT x = static_cast<FLOAT>(width * 3 / static_cast<T>(4));
      FLOAT w = width - x;

      FLOAT h2 = static_cast<FLOAT>(height / static_cast<T>(3));
      FLOAT h3 = h2;
      FLOAT h1 = height - h2 - h3;

      m_btns[0].*bounds = D2D1::RectF(x, 0 , w, h1);
      m_btns[1].*bounds = D2D1::RectF(x, h1, w, h2);
      m_btns[2].*bounds = D2D1::RectF(x, h1+h2, w, h3);
    }
    
    // Convert width/height to right/bottom
    // Note that -0.5 is required in order to deal with the +0.5 that 
    // is added for pixel alignment for the screen bitmap.
    for (auto i = m_btns.begin(); i != m_btns.end(); ++i)
    {
      (*i.*bounds).right  = static_cast<FLOAT>(static_cast<T>((*i.*bounds).left + (*i.*bounds).right  - 0.5f));
      (*i.*bounds).bottom = static_cast<FLOAT>(static_cast<T>((*i.*bounds).top  + (*i.*bounds).bottom - 0.5f));
    }       
  }


  void renderBackground_Screen(ID2D1RenderTarget * renderTarget)
  {
    bool useColor = m_encodingMode != WacomGSS::STU::Protocol::EncodingMode_1bit;
    
    using namespace WacomGSS::Win32;

    auto textFormat = createFont(&Button::boundsScreen);

    com_ptr<ID2D1SolidColorBrush> fillBrush;
    if (useColor)
    {
      hresult_succeeded
      (
        renderTarget->CreateSolidColorBrush( D2D1::ColorF(D2D1::ColorF::LightGray), &fillBrush)
      );
    }

    com_ptr<ID2D1SolidColorBrush> blackBrush;
    hresult_succeeded
    (
    renderTarget->CreateSolidColorBrush( D2D1::ColorF(D2D1::ColorF::Black), &blackBrush)
    );  

    renderTarget->Clear(D2D1::ColorF(D2D1::ColorF::White));
    if (!useColor)
    {
      renderTarget->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_ALIASED);
    }

    for (auto i = m_btns.begin(); i != m_btns.end(); ++i)
    {   
      auto b = D2D1::RectF(i->boundsScreen.left+0.5f, i->boundsScreen.top+0.5f, i->boundsScreen.right+0.5f, i->boundsScreen.bottom+0.5f);
      
      if (useColor)
      {        
        renderTarget->FillRectangle(b, fillBrush.get());
      }
      renderTarget->DrawRectangle(b, blackBrush.get());        
      renderTarget->DrawText(i->text.data(), i->text.length(), textFormat.get(), b, blackBrush.get(), D2D1_DRAW_TEXT_OPTIONS_CLIP, DWRITE_MEASURING_MODE_NATURAL);      
    }    
  }
  

  void renderBackground_Client(ID2D1RenderTarget * renderTarget)
  {
    using namespace WacomGSS::Win32;

    com_ptr<ID2D1SolidColorBrush> fillBrush;
    hresult_succeeded
    (
      renderTarget->CreateSolidColorBrush( D2D1::ColorF(D2D1::ColorF::LightGray), &fillBrush)
    );
        
    renderTarget->Clear(D2D1::ColorF(D2D1::ColorF::White));
    
    if (m_sessionId)
    {
      hresult_succeeded
      (
      m_textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING)
      );
      hresult_succeeded
      (
      m_textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR)
      );

      auto size = renderTarget->GetSize();
      // Unicode LOCK = U+1F512 / UTF-8 = F0 9F 94 92 / UTF-16 = D83D DD12
      renderTarget->DrawText(L"\xd83d\xdd12", 2, m_textFormat.get(), D2D1::RectF(0,0, size.width, size.height) , m_textBrush.get(), D2D1_DRAW_TEXT_OPTIONS_CLIP, DWRITE_MEASURING_MODE_NATURAL);      

      hresult_succeeded
      (
        m_textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER)
      );
      hresult_succeeded
      (
        m_textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER)
      );
    }

    for (auto i = m_btns.begin(); i != m_btns.end(); ++i)
    {   
      renderTarget->FillRectangle(i->boundsClient, fillBrush.get());
      renderTarget->DrawRectangle(i->boundsClient, m_textBrush.get());        
      renderTarget->DrawText(i->text.data(), i->text.length(), m_textFormat.get(), i->boundsClient, m_textBrush.get(), D2D1_DRAW_TEXT_OPTIONS_CLIP, DWRITE_MEASURING_MODE_NATURAL);
    }
    
  }


  void setRenderDataScale()
  {
    auto s = m_renderTarget->GetSize();
    m_renderDataScale.width  = s.width  / m_capability.tabletMaxX;
    m_renderDataScale.height = s.height / m_capability.tabletMaxY;      
    m_renderInkScale = ((s.width / m_capability.screenWidth) + (s.height / m_capability.screenHeight)) / 2.0f;
  }


  void createBitmap()
  {
    using namespace WacomGSS::Win32;

    com_ptr<IWICBitmap> pWICBitmap;    
    hresult_succeeded(m_pIWICImagingFactory->CreateBitmap(m_capability.screenWidth, m_capability.screenHeight, GUID_WICPixelFormat32bppBGR, WICBitmapCacheOnDemand, &pWICBitmap));
    
    com_ptr<ID2D1RenderTarget> renderTarget;
    {
      auto renderTargetProperties = D2D1::RenderTargetProperties();
      renderTargetProperties.dpiX = renderTargetProperties.dpiY = 96.0f; // force bitmap to be pixel == DIP to ensure 1:1 pixel mapping.
      hresult_succeeded(m_pID2D1Factory->CreateWicBitmapRenderTarget(pWICBitmap.get(), renderTargetProperties, &renderTarget));
    }

    calculateButtonPositions<int>(renderTarget->GetSize(), &Button::boundsScreen);
    
    using namespace WacomGSS::STU;
    {
      auto encodingFlag = ProtocolHelper::simulateEncodingFlag(m_tablet.getProductId(), m_capability.encodingFlag);
      if (encodingFlag & Protocol::EncodingFlag_24bit)
      {
        m_encodingMode = m_tablet.supportsWrite() ? Protocol::EncodingMode_24bit_Bulk : Protocol::EncodingMode_24bit;
      }
      else if (encodingFlag & Protocol::EncodingFlag_16bit)
      {
        m_encodingMode = m_tablet.supportsWrite() ? Protocol::EncodingMode_16bit_Bulk : Protocol::EncodingMode_16bit;
      }
      else
      {
        m_encodingMode = Protocol::EncodingMode_1bit;
      }
    }

    {
      renderTarget->BeginDraw();
      renderBackground_Screen(renderTarget.get());
      
      hresult_succeeded(renderTarget->EndDraw());
    }

    m_bitmap = WacomGSS::STU::ProtocolHelper::flatten(m_pIWICImagingFactory.get(), pWICBitmap.get(), m_capability.screenWidth, m_capability.screenHeight, m_encodingMode);
  }


  template<class Rect>
  WacomGSS::Win32::com_ptr<IDWriteTextFormat> createFont(Rect Button::* bounds)
  {
    auto fontFamily = L"Arial";
    auto fontSize   = ((m_btns[0].*bounds).bottom - (m_btns[0].*bounds).top)*2.0f/3.0f;
    auto locale     = L"";

    using namespace WacomGSS::Win32;

    com_ptr<IDWriteTextFormat> textFormat;

    hresult_succeeded
    (    
      m_pIDWriteFactory->CreateTextFormat(fontFamily, nullptr, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, fontSize, locale, &textFormat)
    );
    hresult_succeeded
    (
      textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER)
    );
    hresult_succeeded
    (
      textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER)
    );
    return textFormat;
  }


  void createDeviceDependentResources()
  {
    using namespace WacomGSS::Win32;

    RECT clientRect;
    win32api_BOOL(GetClientRect(m_hwnd, &clientRect), "GetClientRect");
    
    auto renderTargetProperties     = D2D1::RenderTargetProperties();
    auto hwndRenderTargetProperties = D2D1::HwndRenderTargetProperties(m_hwnd, D2D1::SizeU(UINT32(clientRect.right-clientRect.left), UINT32(clientRect.bottom-clientRect.top)), D2D1_PRESENT_OPTIONS_RETAIN_CONTENTS|D2D1_PRESENT_OPTIONS_IMMEDIATELY);

    hresult_succeeded
    (
      m_pID2D1Factory->ReloadSystemMetrics()
    );

    hresult_succeeded
    (
      m_pID2D1Factory->CreateHwndRenderTarget(&renderTargetProperties, &hwndRenderTargetProperties, &m_renderTarget)
    );      
    
    hresult_succeeded
    (
      m_renderTarget->CreateSolidColorBrush( D2D1::ColorF(0.0,0,3.0f/5.0f, 0.333f), &m_penInk)
    );

    hresult_succeeded
    (
      m_renderTarget->CreateSolidColorBrush( D2D1::ColorF(D2D1::ColorF::Black), &m_textBrush)
    );

    setRenderDataScale();
    calculateButtonPositions<FLOAT>(m_renderTarget->GetSize(), &Button::boundsClient);

    m_textFormat = createFont(&Button::boundsClient);
  }

  
  void decrypt(uint8_t data[WacomGSS::STU::Protocol::PenDataEncrypted::encryptedSize])
  {
    // forward decryption to tablet handlers.
    m_tablet.decrypt(data);
  }

  void onReport(WacomGSS::STU::Protocol::PenDataTimeCountSequenceEncrypted & v)
  {
    if (v.sessionId == m_sessionId)
    {
      onReport(static_cast<WacomGSS::STU::Protocol::PenData &>(v));      
    }
  }

  void onReport(WacomGSS::STU::Protocol::PenDataEncryptedOption & v)
  {
    if (v.sessionId == m_sessionId)
    {
      onReport(v.penData[0]);      
      onReport(v.penData[1]);
    }
  }

  void onReport(WacomGSS::STU::Protocol::PenDataEncrypted & v)
  {
    if (v.sessionId == m_sessionId)
    {
      onReport(v.penData[0]);
      onReport(v.penData[1]);
    }
  }

  void onReport(WacomGSS::STU::Protocol::PenDataTimeCountSequence & v)
  {
    onReport(static_cast<WacomGSS::STU::Protocol::PenData &>(v));          
  }

  void onReport(WacomGSS::STU::Protocol::PenDataOption & v)
  {
    onReport(static_cast<WacomGSS::STU::Protocol::PenData &>(v));          
  }

  void onReport(WacomGSS::STU::Protocol::PenData & v)
  {
    WacomGSS::STU::Protocol::PenDataTimeCountSequence penData;
    static_cast<WacomGSS::STU::Protocol::PenData &>(penData) = v;
    penData.timeCount = 0;
    penData.sequence  = 0;
    {
      WacomGSS::lock_guard<decltype(m_mutex)> lock(m_mutex);
      m_readerQueue.push_back(penData);
    }

    ::PostMessage(m_hwnd, WM_USER, 0, 0);
  }
  

  void backgroundThread() noexcept
  {
    WacomGSS::setThreadName("SignatureForm::backgroundThread");

    try
    {
      WacomGSS::STU::Report report;
      while (m_queue.wait_getReport_predicate(report))
      {
        handleReport(report.begin(), report.end());
      }
    }
    catch (...)
    {
      {
        WacomGSS::lock_guard<decltype(m_mutex)> lock(m_mutex);
        m_readerException = std::current_exception();
      }
      ::PostMessage(m_hwnd, WM_USER, 0, 0);
    }
  }

  
  void recalc(RenderData & rd) const noexcept
  {
    rd.client.x = rd.x * m_renderDataScale.width;
    rd.client.y = rd.y * m_renderDataScale.height;    
  }


  RenderData renderData(WacomGSS::STU::Protocol::PenDataTimeCountSequence penData, bool dn) const noexcept
  {
    RenderData rd;
    rd.dn = dn;
    rd.x  = penData.x;
    rd.y  = penData.y;
    rd.pressure = static_cast<float>(penData.pressure) / m_capability.tabletMaxPressure;    
    recalc(rd);
    return rd;
  }

  void drawScreen()
  {
    renderBackground_Client(m_renderTarget.get());
    m_renderIndex = 0;
    renderInk_Client();
  }

  void drawScreenBeginEnd()
  {
    m_renderTarget->BeginDraw();
    drawScreen();
    HRESULT hr = m_renderTarget->EndDraw();
    if (hr == D2DERR_RECREATE_TARGET)
    {
      discardDeviceDependentResources();
      createDeviceDependentResources();
      m_renderTarget->BeginDraw();
      drawScreen();    
      hr = m_renderTarget->EndDraw();
    }
  }

  void renderInk_Client()
  {
    if (m_penData.empty() || m_renderIndex == m_penData.size())
      return;

    auto p = m_penData.begin() + m_renderIndex;

    bool dn = p->dn;
    
    for (auto i = p+1; i != m_penData.end(); ++m_renderIndex, ++p, ++i)    
    {
      if (dn)
      {
        m_renderTarget->DrawLine(p->client, i->client, m_penInk.get(), 4.0f* m_renderInkScale );
      }
      dn = i->dn;
    }
  }

  void clearScreen()
  {
    if (!m_bitmap.empty())
    {
      m_tablet.writeImage(m_encodingMode, m_bitmap.data(), m_bitmap.size());
    }
    else
    {
      m_tablet.setClearScreen();
    }
    m_penData.clear();
    m_renderIndex = 0;    
    m_isDown = 0;
  }


  void renderBeginEnd()
  {
    m_renderTarget->BeginDraw();
    renderInk_Client();
    HRESULT hr = m_renderTarget->EndDraw();
    if (hr == D2DERR_RECREATE_TARGET)
    {
      discardDeviceDependentResources();
      createDeviceDependentResources();      
      drawScreenBeginEnd();
    }
  }


  D2D1_POINT_2F tabletToScreen(WacomGSS::STU::Protocol::PenData const & penData) const 
  {
    return D2D1::Point2F(static_cast<FLOAT>(penData.x)*m_capability.screenWidth/m_capability.tabletMaxX, 
                         static_cast<FLOAT>(penData.y)*m_capability.screenHeight/m_capability.tabletMaxY);
  }
  

  static bool ptInRect(D2D1_RECT_F const & rc, D2D1_POINT_2F & pt) noexcept
  {
    return pt.x >= rc.left && pt.x <= rc.right && pt.y >= rc.top && pt.y <= rc.bottom;
  }

  void drawWait()
  {
    if (m_renderTarget)
    {
      m_renderTarget->BeginDraw();
      auto size = m_renderTarget->GetSize();
      auto rc = D2D1::RectF(0,0,size.width, size.height);
      // UNICODE U+231B 'Hourglass'
      m_renderTarget->DrawTextW(L"\x231b", 1, m_textFormat.get(), rc, m_textBrush.get());
      HRESULT hr = m_renderTarget->EndDraw();
    }
  }
  
  void onOK()
  {
    // process collected data as required here.
    
    ::PostMessage(m_hwnd, WM_CLOSE, 0, 0);
  }

  void onClear()
  {
    drawWait();

    clearScreen();
    drawScreenBeginEnd();
    {
      WacomGSS::lock_guard<decltype(m_mutex)> lock(m_mutex);
      m_readerQueue.clear();
    }
  }

  void onCancel()
  {
    ::PostMessage(m_hwnd, WM_CLOSE, 0, 0);
  }



  void processPenData(WacomGSS::STU::Protocol::PenDataTimeCountSequence & penData, bool dn)
  {
    if (!penData.rdy)
      return;      

    auto pt = tabletToScreen(penData);
    
    int btn = 0; // will be +ve if the pen is over a button.
    {
      for (unsigned i = 0; i < m_btns.size(); ++i)
      {
        if (ptInRect(m_btns[i].boundsScreen, pt))
        {
          btn = i+1;
          break;
        }
      }
    }
    
    if (dn)
    {
      if (m_isDown == 0)
      {
        // transition to down
        if (btn > 0)
        {
          // We have put the pen down on a button.
          // Track the pen without inking on the client.

          m_isDown = btn;
        }
        else
        {
          // We have put the pen down somewhere else.
          // Treat it as part of the signature.

          m_isDown = -1;
        }
      }
      else
      {
        // already down, keep doing what we're doing!
      }
    }
    else
    {
      if (m_isDown != 0)
      {
        if (btn > 0)
        {
          if (btn == m_isDown)
          {
            (this->*m_btns[btn-1].onClick)();
          }
        }
        m_isDown = 0;
      }
    }

    m_penData.push_back(renderData(penData, dn));
  }

  LRESULT CALLBACK wndProc(UINT uMessage, WPARAM wParam, LPARAM lParam) noexcept
  {
    LRESULT lResult = 0;
    switch (uMessage)
    {      
      case WM_PAINT:
        {
          PAINTSTRUCT ps;
          BeginPaint(m_hwnd, &ps);
          try
          {
            drawScreenBeginEnd();
          }
          catch (...)
          {
            handleException(m_hwnd);
            DestroyWindow(m_hwnd);
          }
          EndPaint(m_hwnd, &ps);
        }
        break;
        
      case WM_USER:
        try
        {
          decltype(m_readerQueue) queue;
          {
            WacomGSS::lock_guard<decltype(m_mutex)> lock(m_mutex);
            if (m_readerException != nullptr)
            {              
              std::rethrow_exception(m_readerException);
            }
            queue.swap(m_readerQueue);
          }

          if (!queue.empty())
          {
            bool dn = m_penData.empty() ? (queue.front().pressure > m_inkThreshold.onPressureMark) : m_penData.back().dn;
            for (auto i = queue.begin(); i != queue.end(); ++i)
            {
              if (!dn)
              {
                if (i->pressure > m_inkThreshold.onPressureMark)
                  dn = true;                
              }
              else
              {
                if (i->pressure <= m_inkThreshold.offPressureMark)
                  dn = false;
              }

              processPenData(*i, dn);
            }

            renderBeginEnd();
          }          
        }
        catch (...)
        {
          handleException(m_hwnd);
          DestroyWindow(m_hwnd);
        }
        break;

      case WM_DESTROY:
        EnableWindow(GetParent(m_hwnd), TRUE);
        try
        {
          m_tablet.disconnect();
        }
        catch (...)
        {
          handleException(m_hwnd);            
        }
        break;
        
      case WM_SIZE:
        try
        {
          if (m_renderTarget)
          {
            using namespace WacomGSS::Win32;

            RECT clientRect;
            win32api_BOOL(GetClientRect(m_hwnd, &clientRect), "GetClientRect");
    
            m_renderTarget->Resize(D2D1::SizeU(UINT32(clientRect.right-clientRect.left), UINT32(clientRect.bottom-clientRect.top)));
                        
            calculateButtonPositions<FLOAT>(m_renderTarget->GetSize(), &Button::boundsClient);
            m_textFormat = createFont(&Button::boundsClient); // m_textFormat is device independent but needs to be resized

            setRenderDataScale();

            for (auto i = m_penData.begin(); i != m_penData.end(); ++i)
            {
              recalc(*i);
            }

            drawScreenBeginEnd();
          }
        }
        catch (...)
        {
          handleException(m_hwnd);
          DestroyWindow(m_hwnd);
        }
        break;

      case WM_CLOSE:
        {
          try
          {
            drawWait();
            m_tablet.setInkingMode(WacomGSS::STU::Protocol::InkingMode_Off);
            m_tablet.setClearScreen();
          }
          catch (...)
          {
            handleException(m_hwnd);
          }

          EnableWindow(GetParent(m_hwnd), TRUE);
          DestroyWindow(m_hwnd);
        }
        break;

      case WM_DPICHANGED:
        {
          auto rc = (LPRECT)lParam;
          SetWindowPos(m_hwnd, nullptr, rc->left, rc->top, rc->right, rc->bottom, SWP_NOZORDER | SWP_NOACTIVATE); 
        }
        break;

      case WM_CREATE:
        {
          try
          {
            using namespace WacomGSS::Win32;

            FLOAT dpiX, dpiY;
            getDPIForMonitor(dpiX, dpiY);

            // size and position window
            {
              RECT rc;
              float m_tabletWidth  = m_capability.tabletMaxX / 2540.0f * dpiX;
              float m_tabletHeight = m_capability.tabletMaxY / 2540.0f * dpiY;

              rc.right  = static_cast<LONG>(std::ceil(m_tabletWidth));
              rc.bottom = static_cast<LONG>(std::ceil(m_tabletHeight));

              RECT rcParent;
              win32api_BOOL(::GetWindowRect(::GetParent(m_hwnd), &rcParent), "GetWindowRect");
              
              rc.left = rcParent.left + ((rcParent.right - rcParent.left) - rc.right)/2;
              rc.top  = rcParent.top  + ((rcParent.bottom - rcParent.top) - rc.bottom)/2;
              rc.right  += rc.left;
              rc.bottom += rc.top;
              win32api_BOOL(::AdjustWindowRectEx(&rc, WS_CAPTION|WS_SYSMENU|WS_BORDER, FALSE, 0), "AdjustWindowRectEx");
              
              win32api_BOOL(::SetWindowPos(m_hwnd, nullptr, rc.left, rc.top, rc.right-rc.left, rc.bottom-rc.top, SWP_NOACTIVATE|SWP_NOOWNERZORDER|SWP_NOZORDER), "SetWindowPos");
            }

            createDeviceDependentResources();

            createBitmap();

            clearScreen();

            bool enableEncryption = m_tablet.isSupported(WacomGSS::STU::Protocol::ReportId_EncryptionStatus) || WacomGSS::STU::ProtocolHelper::supportsEncryption(m_tablet.getDHprime());


            if (enableEncryption)
            {
              m_sessionId = 0xc0ffee; // recommend to use a random number here.
              m_tablet.startCapture(m_sessionId);
            }
            else
            {
              m_sessionId = 0;
            }
            
            m_tablet.setInkingMode(WacomGSS::STU::Protocol::InkingMode_On);
            m_queue  = std::move(m_tablet.interfaceQueue());
            m_reader = std::move(WacomGSS::thread(std::ref(*this)));

            ::EnableWindow(::GetParent(m_hwnd), FALSE);
            // ignore return
          }
          catch (...)
          {
            handleException(m_hwnd);
            ::DestroyWindow(m_hwnd);
          }
        }
        break;

      default:
        lResult = ::DefWindowProc(m_hwnd, uMessage, wParam, lParam);
    }
    return lResult;
  }
  
  static LRESULT CALLBACK wndProc2_s(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) noexcept
  {
    auto p = reinterpret_cast<SignatureForm *>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
    return p->wndProc(uMessage, wParam, lParam);
  }

  static LRESULT CALLBACK wndProc1_s(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) noexcept
  {
    if (uMessage == WM_NCCREATE)
    {
      auto p = reinterpret_cast<SignatureForm *>(reinterpret_cast<LPCREATESTRUCT>(lParam)->lpCreateParams);
      ::SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(p));
      ::SetWindowLongPtr(hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&wndProc2_s));
      p->m_hwnd = hWnd;
      return p->wndProc(uMessage, wParam, lParam);
    }
    return ::DefWindowProc(hWnd, uMessage, wParam, lParam);
  }


  SignatureForm(std::unique_ptr<WacomGSS::STU::Interface> && intf, Factory const & factory, HINSTANCE hInstance)
  :  
    Factory(factory),
    m_hwnd(nullptr),
    m_tablet(std::move(intf), std::make_shared<WacomGSS::STU::OpenSSL_EncryptionHandler>(),std::make_shared<WacomGSS::STU::OpenSSL_EncryptionHandler2>()),    
    m_shcore(::LoadLibrary(L"Shcore.dll"), std::nothrow),
    m_pGetDPIForMonitor(nullptr),
    m_btns(3),
    m_readerException(nullptr),
    m_renderIndex(0)
  {
    m_capability   = m_tablet.getCapability();
    m_inkThreshold = m_tablet.getInkThreshold();

    m_btns[0].text = loadString(hInstance, IDS_SignatureForm_OK);
    m_btns[0].onClick = &SignatureForm::onOK;
    m_btns[1].text = loadString(hInstance, IDS_SignatureForm_Clear);
    m_btns[1].onClick = &SignatureForm::onClear;
    m_btns[2].text = loadString(hInstance, IDS_SignatureForm_Cancel);
    m_btns[2].onClick = &SignatureForm::onCancel;
    if (m_shcore)
    {
      m_pGetDPIForMonitor = reinterpret_cast<pfn_GetDPIForMonitor>(GetProcAddress(m_shcore.get(), "GetDPIForMonitor"));
    }    
  }


  static const WCHAR k_lpszClassName[];

public:
  ~SignatureForm()
  {
    try
    {
      if (m_reader.joinable())
      {
        m_tablet.queueSetPredicateAll(true);
        m_tablet.queueNotifyAll();
        m_reader.join();
      }
    }
    catch (...)
    {
    }
  }

  static ATOM registerWindow(HINSTANCE hInstance)
  {
    WNDCLASSEX wc = { sizeof(WNDCLASSEX) };
    wc.lpfnWndProc   = &wndProc1_s;
    wc.hInstance     = hInstance;
    wc.hCursor       = LoadCursor(nullptr, IDC_ARROW);
    wc.lpszClassName = k_lpszClassName;

    return ::RegisterClassEx(&wc);
  }

  
  static void dialogBox(HINSTANCE hInstance, HWND hWnd, WacomGSS::STU::UsbDevice const & usbDevice, Factory const & factory)
  {
    auto intf = std::make_unique<WacomGSS::STU::UsbInterface>();
    auto ec = intf->connect(usbDevice, true);
    if (!ec)
    {
      auto cls = std::unique_ptr<SignatureForm>(new SignatureForm(std::move(intf), factory, hInstance));

      HWND hwnd = ::CreateWindowEx(0, k_lpszClassName, loadString(hInstance, IDS_SignatureForm_Title).c_str(), WS_THICKFRAME|WS_BORDER|WS_CAPTION|WS_SYSMENU|WS_POPUP|WS_VISIBLE, 0, 0, 100, 100, hWnd, nullptr, hInstance, cls.get());              
      if (hwnd)
      {
        MSG msg;
        while (::IsWindow(hwnd) && ::GetMessage(&msg, nullptr, 0, 0) > 0)
        {
          if (!IsDialogMessage(hWnd, &msg))
          {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
          }
        }
      }
    }
    else
    {
      auto msg = loadString(hInstance, IDS_SignatureForm_ConnectFailed);

      ::MessageBox(hWnd, msg.c_str(), nullptr, MB_ICONSTOP|MB_OK);
    }
  }

  void operator()() noexcept
  {
    backgroundThread();
  }
};

const WCHAR SignatureForm::k_lpszClassName[] = L"WacomGSS.STU.DemoButtons"; // not localized


class DemoButtons
{  
  Factory m_factory;
  
  DemoButtons()
  {
    D2D1_FACTORY_OPTIONS factoryOptions;
    factoryOptions.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
    WacomGSS::Win32::hresult_succeeded(::D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, IID_ID2D1Factory, &factoryOptions, &m_factory.m_pID2D1Factory));
    WacomGSS::Win32::hresult_succeeded(::CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, &m_factory.m_pIWICImagingFactory));
    WacomGSS::Win32::hresult_succeeded(::DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), &m_factory.m_pIDWriteFactory));
  }


  INT_PTR CALLBACK dialogProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) noexcept
  {
    INT_PTR bSuccess = TRUE;
    switch (uMessage)
    {
      
      case WM_COMMAND:
        if (LOWORD(wParam) == IDC_DemoButtons_Signature)
        {
          try
          {
            auto usbDevices = WacomGSS::STU::getUsbDevices();
            if (!usbDevices.empty())
            {
              SignatureForm::dialogBox(reinterpret_cast<HINSTANCE>(GetWindowLongPtr(hWnd, GWLP_HINSTANCE)), hWnd, usbDevices[0], m_factory);
            }
            else
            {
              auto hInstance = reinterpret_cast<HINSTANCE>(GetWindowLongPtr(hWnd, GWLP_HINSTANCE));
              MessageBox(hWnd, loadString(hInstance, IDS_DemoButtons_NoTablet).c_str(), nullptr, MB_ICONEXCLAMATION|MB_OK);
            }
          }
          catch (...)
          {
            handleException(hWnd);    
          }
        }
        else
        {
          bSuccess = FALSE;
        }
        break;

      case WM_CLOSE:
        EndDialog(hWnd, 0);
        break;

      default:
        bSuccess = FALSE;
    }
    return bSuccess;
  }

  static INT_PTR CALLBACK dialogProc2_s(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) noexcept
  {
    DemoButtons * p = reinterpret_cast<DemoButtons *>(GetWindowLongPtr(hWnd, DWLP_USER));
    return p->dialogProc(hWnd, uMessage, wParam, lParam);    
  }

  static INT_PTR CALLBACK dialogProc1_s(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) noexcept
  {
    if (uMessage == WM_INITDIALOG)
    {
      auto p = reinterpret_cast<DemoButtons *>(lParam);
      ::SetWindowLongPtr(hWnd, DWLP_USER, reinterpret_cast<LONG_PTR>(p));
      ::SetWindowLongPtr(hWnd, DWLP_DLGPROC, reinterpret_cast<LONG_PTR>(dialogProc2_s));
      return p->dialogProc(hWnd, uMessage, wParam, lParam);
    }
    return FALSE;
  }

public:
  static INT_PTR dialogBox(HINSTANCE hInstance)
  {
    std::unique_ptr<DemoButtons> cls(new DemoButtons);
    return ::DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_DemoButtons), nullptr, dialogProc1_s, reinterpret_cast<LPARAM>(cls.get()));
  }
};




int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
{
  ::HeapSetInformation(nullptr, HeapEnableTerminationOnCorruption, nullptr, 0);
  //ignore return

  try
  {
    WacomGSS::Win32::ComInitialize coinit(nullptr, COINIT_APARTMENTTHREADED);
    SignatureForm::registerWindow(hInstance);
    
    return DemoButtons::dialogBox(hInstance);
  }
  catch (...)
  {
    handleException(nullptr);
  }
  return 1;
}
