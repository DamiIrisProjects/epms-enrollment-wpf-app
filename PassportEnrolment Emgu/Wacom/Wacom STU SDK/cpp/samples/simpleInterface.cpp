/// @file      simple.cpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2012-03-08
/// @brief     simple test harness

/// TODO: Add comments!

#include <WacomGSS/STU/getUsbDevices.hpp>
#include <WacomGSS/STU/UsbInterface.hpp>
//#include <WacomGSS/STU/SerialInterface.hpp>
#include <WacomGSS/STU/ProtocolHelper.hpp>
#include <WacomGSS/STU/ReportHandler.hpp>

#include <csignal>
#include <iostream>
#include <iomanip>



static WacomGSS::atomic<WacomGSS::STU::Interface *> g_intf;


extern "C"
{
  static void signalHandler(int) noexcept
  {
    auto intf = g_intf.load();
    intf->queueSetPredicateAll(true);
    intf->queueNotifyAll(); // notify the interface that the predicate has changed
  }
}


struct as_hex { uint32_t v; as_hex(uint32_t _):v(_){} };
std::ostream & operator << (std::ostream & o, as_hex const & v) { return o << std::setfill('0')<<std::hex<<std::setw(8)<<v.v<<std::dec<<std::setfill(' '); }

std::ostream & operator << (std::ostream & o, WacomGSS::STU::Protocol::PenData const & data)
{
  return o
    << std::setw(3) << static_cast<uint16_t>(data.rdy) << " "
    << std::setw(3) << static_cast<uint16_t>(data.sw)  << " "
    << std::setw(5) << data.pressure << " "
    << std::setw(5) << data.x << " "
    << std::setw(5) << data.y;
}

struct Option { uint16_t v; Option(uint16_t _):v(_){} };
std::ostream & operator << (std::ostream & o, Option const & v)
{
  return o
    << " [" << std::setw(5) << v.v << "]";
}



class cout_ReportHandler : public WacomGSS::STU::ProtocolHelper::ReportHandler
{
public:
  void onReport(WacomGSS::STU::Protocol::PenData & data) override
  {
    std::cout << data << std::endl; 
  }

  void onReport(WacomGSS::STU::Protocol::PenDataOption & data) override
  {
    std::cout << data << Option(data.option) << std::endl;
  }

  void decrypt(uint8_t [16])
  {
    // not supported
  }

  void onReport(WacomGSS::STU::Protocol::PenDataEncrypted & data) override
  {
    std::cout
      << "<" << as_hex(data.sessionId) << "> " << data.penData[0] << std::endl
      << "           "                         << data.penData[1] << std::endl;
  }

  void onReport(WacomGSS::STU::Protocol::PenDataEncryptedOption & data) override
  {
    std::cout
      << "<" << as_hex(data.sessionId) << "> " << data.penData[0] << Option(data.option[0]) << std::endl
      << "           "                         << data.penData[1] << Option(data.option[1]) << std::endl;
  }

  void onReport(WacomGSS::STU::Protocol::DevicePublicKey &) override
  {
    std::cout << "received: DevicePublicKey (ignoring)" << std::endl;
  }

  template<class Iterator>
  void onUnknown(Iterator begin, Iterator end)
  {
    using namespace std;
    auto f = cout.fill('0');
    cout << hex;
    for (; begin != end; ++begin)
    {
      cout << ' ' << setw(2) << static_cast<unsigned>(*begin);
    }
    cout.fill(f);
    cout << dec << endl;
  }
};


void run(WacomGSS::STU::Protocol protocol)
{
  using namespace std;
  using namespace WacomGSS::STU;
  using namespace ProtocolHelper::ostream_operators;

  try
  {
    cout << "getInformation()... ";
    Protocol::Information inf = protocol.getInformation();
    cout << "modelName=" << inf.modelNameNullTerminated << std::endl;

    bool supportedPenDataOptionMode = false;
    try
    {
      cout << "setPenDataOptionMode(SequenceNumber)... ";
      protocol.setPenDataOptionMode(Protocol::PenDataOptionMode_SequenceNumber);
      cout << "ok!" << endl;;

      cout << "getPenDataOptionMode()... ";
      uint8_t penDataOptionMode = protocol.getPenDataOptionMode();
      cout << "penDataOptionMode=" << static_cast<Protocol::PenDataOptionMode>(penDataOptionMode) << std::endl;
      
      supportedPenDataOptionMode = true;
    }
    catch (std::system_error &)
    {
      cout << "NOT SUPPORTED" << endl;
    }

    cout << "setClearScreen()... ";
    protocol.setClearScreen();
    cout << "ok!" << std::endl;

    cout << "setInkingMode(On)... ";
    protocol.setInkingMode(Protocol::InkingMode_On);
    cout << "ok!" << std::endl;
    
    cout << "(use stylus, press CTRL-C to quit)" << std::endl;

    auto interfaceQueue = protocol->interfaceQueue();

    cout_ReportHandler reportHandler;
   
    Report report;
    while (interfaceQueue.wait_getReport_predicate(report))
    {
      auto r = reportHandler.handleReport(report.begin(), report.end());
      if (r.first != report.end())
      {
        if (r.second)
        {
          cout << "unknown data in report: ";
        }
        else
        {
          cout << "pending data in report: ";
        }
        reportHandler.onUnknown(r.first, report.end());
      }
    }

    cout << "quitting!" << std::endl;

    cout << "setInkingMode(Off)... ";
    protocol.setInkingMode(Protocol::InkingMode_Off);
    cout << "ok!" << std::endl;

    cout << "setClearScreen()... ";
    protocol.setClearScreen();
    cout << "ok!" << std::endl;

    if (supportedPenDataOptionMode)
    {
      cout << "setPenDataOptionMode(None)... ";
      protocol.setPenDataOptionMode(Protocol::PenDataOptionMode_None);
      cout << "ok!" << endl;;
    }

    protocol->disconnect();
  }
  catch (Interface::device_removed_error const &)
  {
    std::cout << "device removed exception" << std::endl;
  }
  catch (system_error const & e)
  {
    std::cout << "system error: " << e.what() << " code:" << e.code() << " " << e.code().message() << std::endl;
  }
  catch (std::runtime_error const & e)
  {
    std::cout << "runtime_error exception: " << e.what() << std::endl;
  }
}



std::unique_ptr<WacomGSS::STU::UsbInterface> connectUsb()
{
  using namespace std;
  using namespace WacomGSS::STU;

  unique_ptr<UsbInterface> intf;
   
  vector<UsbDevice> usbDevices = getUsbDevices();

  if (!usbDevices.empty()) 
  {
    UsbDevice const & usbDevice = usbDevices.front();

    auto f = cout.fill(L'0');
    cout 
      << "Connecting to first device found: "
      << hex
      << setw(4) << usbDevice.idVendor << ':'
      << setw(4) << usbDevice.idProduct << ':'
      << setw(4) << usbDevice.bcdDevice
      << dec
      ;
    /// TODO: display O/S specific here
    cout << endl;
    cout.fill(f);

    cout << "Connecting... "<<endl;

    intf.reset(new UsbInterface);

    auto ec = intf->connect(usbDevice, true);
    if (!ec) 
    {
      // success
    } 
    else  
    {
      cout << "Failed to connect: " << ec << endl;
      intf = nullptr;
    }
  }
  else 
  {
    cout << "No USB devices found" << endl;
  }

  return intf;
}



#ifdef WacomGSS_STU_SerialInterface_hpp
std::unique_ptr<WacomGSS::STU::SerialInterface> connectSerial()
{
  using namespace std;
  using namespace WacomGSS::STU;
  
  unique_ptr<SerialInterface> intf(new SerialInterface);

  wchar_t const * fn = L"COM1";

  cout << "Connecting to '" << fn << "'... ";
  auto ec = intf->connect(fn, true);
  if (!ec) 
  {
    // success
  } 
  else  
  {
    cout << "Failed to connect: " << ec << std::endl;
    intf = nullptr;
  }

  return intf;
}
#endif



int main()
{
  using namespace std;
  
  try
  {
    cout << "STU simple sample" << endl << endl;

    unique_ptr<WacomGSS::STU::Interface> intf;
  
    intf = connectUsb();

#ifdef WacomGSS_STU_SerialInterface_hpp
    if (!intf)  
    {
      intf = connectSerial();      
    }
#endif
    
    if (!!intf) 
    { 
      cout << "Connected!" << endl;

      g_intf.store(intf.get());
      signal(SIGINT, &signalHandler);
      try
      {
        run(*intf);
      }
      catch (...)
      {
        signal(SIGINT, SIG_DFL);
        g_intf.store(nullptr);
        throw;
      }

      signal(SIGINT, SIG_DFL);
      g_intf.store(nullptr);
    }
    
  }
  catch (system_error const & e)
  {
    std::cout << "system error: " << e.what() << " code:" << e.code() << " " << e.code().message() << std::endl;
  }
  catch (std::exception const & ex)
  {
    std::cout << "std::exception: " << ex.what() << std::endl;
  }

  return 0;
}
