/// @file      WacomGSS/config/msvc.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2011-10-18
/// @brief     meta configuration about the compliation platform and compiler

#ifndef WacomGSS_config_msvc_hpp
#define WacomGSS_config_msvc_hpp

#if _MSC_VER < 1600
# error WacomGSS: Compiler does not support enough of C++11 to compile this code
#elif _MSC_VER <= 1600
#include <exception>
#include <cstdint>
#include <cstring>

#pragma warning(disable:4986)
#include <stdexcept>
#pragma warning(default:4986)

#define noexcept   throw()
#define constexpr  const

namespace std
{
  // The specification states that an exception_ptr should satisfy NullablePtr (18.8.5-2) VS2010 does not adhere to this.
  inline bool operator != (exception_ptr const & lhs, nullptr_t rhs)
  {
    return !operator==(lhs, rhs);
  }
}

namespace WacomGSS
{
  /// MSC06-CPP. Be aware of compiler optimization when dealing with sensitive data 
  /// https://www.securecoding.cert.org/confluence/display/cplusplus/MSC06-CPP.+Be+aware+of+compiler+optimization+when+dealing+with+sensitive+data
  #pragma optimize("g",off)
  inline void * memset_s(void * ptr, int value, size_t num)
  {
    return std::memset(ptr, value, num);
  }
  #pragma optimize("",on)
}

#define WacomGSS_compatibility_condition_variable 1 // use boost
#define WacomGSS_compatibility_mutex              1 // use boost
#define WacomGSS_compatibility_thread             1 // use boost
#define WacomGSS_compatibility_chrono             1 // use boost
#define WacomGSS_compatibility_atomic             1 // use boost

#define WacomGSS_compatibility_std_codecvt        0 /* does not require compatibility - use std */

#define WacomGSS_noreturn __declspec(noreturn) // [[noreturn]] attribute
#define WacomGSS_deprecated __declspec(deprecated)

#define WacomGSS_thread_local __declspec(thread)

#include <WacomGSS/compatibility/except.nested.hpp>

#elif _MSC_VER <= 1800
#include <cstdint>
#include <cstring>

#define noexcept   throw()
#define constexpr  const

namespace WacomGSS
{
	/// MSC06-CPP. Be aware of compiler optimization when dealing with sensitive data 
	/// https://www.securecoding.cert.org/confluence/display/cplusplus/MSC06-CPP.+Be+aware+of+compiler+optimization+when+dealing+with+sensitive+data
#pragma optimize("g",off)
	inline void * memset_s(void * ptr, int value, size_t num)
	{
		return std::memset(ptr, value, num);
	}
#pragma optimize("",on)
}

#define WacomGSS_compatibility_condition_variable 0 // use std
#define WacomGSS_compatibility_mutex              0 // use std
#define WacomGSS_compatibility_thread             0 // use std
#define WacomGSS_compatibility_chrono             0 // use std
#define WacomGSS_compatibility_atomic             0 // use std

#define WacomGSS_compatibility_std_codecvt        0 /* does not require compatibility - use std */

#define WacomGSS_noreturn __declspec(noreturn) // [[noreturn]] attribute
#define WacomGSS_deprecated __declspec(deprecated)

#define WacomGSS_thread_local __declspec(thread)


#else
#error WacomGSS: 'Microsoft Visual Studio' unsupported compiler version
#endif


#endif // WacomGSS_config_msvc_hpp
