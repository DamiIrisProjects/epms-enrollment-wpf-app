/// @file      WacomGSS/config/linux.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    gzagon
/// @date      2012-02-15
/// @brief     meta configuration about the compliation platform and compiler

#ifndef WacomGSS_config_Linux_hpp
#define WacomGSS_config_Linux_hpp

#define WacomGSS_Linux 1
#define WacomGSS_POSIX 1

#endif // WacomGSS_config_lin_hpp
