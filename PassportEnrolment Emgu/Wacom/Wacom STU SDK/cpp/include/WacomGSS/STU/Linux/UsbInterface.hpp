/// @file      WacomGSS/STU/Win32/UsbInterface.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    gzagon
/// @date      2012-02-27
/// @brief     provides the class that can open an STU device over the HID/USB bus.

#ifndef WacomGSS_STU_Linux_UsbInterface_hpp
#define WacomGSS_STU_Linux_UsbInterface_hpp

#include <WacomGSS/STU/Interface.hpp>
#include <WacomGSS/STU/getUsbDevices.hpp>
#include <WacomGSS/Linux/libusb.hpp>
#include <WacomGSS/compatibility/thread.hpp>
#include <WacomGSS/compatibility/atomic.hpp>

namespace WacomGSS
{

  namespace STU
  {

    class UsbInterface : public Interface
    {
      libusb::context       m_context;
      libusb::device_handle m_deviceHandle;
      bool                  m_supportsWrite;
      uint16_t              m_idProduct;

      atomic<bool>    m_quitFlag;
      thread          m_readerThread;
      mutex mutable   m_apiMutex;
          
      void startReaderThread();
      void stopReaderThread();
      void readerThread() noexcept;

    public:
      /// @brief Default constructor.
      UsbInterface();

      /// @brief Destructor.
      /// @exception Whilst effort is made to reduce the possibility of exceptions, it is still possible that this can throw. To reduce the chances, call disconnect() prior to this being called..
      ~UsbInterface();

      /// @brief Connects the class to a tablet.
      std::error_code connect(UsbDevice const & usbDevice, bool exclusiveLock);

      void disconnect() override;

      bool isConnected() const override;

      void get(uint8_t * data, size_t length) override;

      void set(uint8_t const * data, size_t length) override;

      bool supportsWrite() const override;

      void write(uint8_t const * data, size_t length) override;

      std::uint16_t getProductId() const override;
    };
    
  } // namespace STU

} // namespace WacomGSS

#endif // WacomGSS_STU_Linux_UsbInterface_hpp
