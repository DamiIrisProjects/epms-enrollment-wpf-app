var searchData=
[
  ['ondecrypt',['onDecrypt',['../interfacewgss_s_t_u_1_1_i_report_handler_events.html#a6c0b475c30676e17b647650e1e1cd2f6',1,'wgssSTU::IReportHandlerEvents']]],
  ['ondevicepublickey',['onDevicePublicKey',['../interfacewgss_s_t_u_1_1_i_report_handler_events.html#adacb53c0681a327a55cf810704048fbe',1,'wgssSTU::IReportHandlerEvents::onDevicePublicKey()'],['../interfacewgss_s_t_u_1_1_i_tablet_events.html#a2d718fed23344f92d8f44ba1ab745702',1,'wgssSTU::ITabletEvents::onDevicePublicKey()']]],
  ['ongetreportexception',['onGetReportException',['../interfacewgss_s_t_u_1_1_i_tablet_events.html#a01cb2324da2fcb00b5488abf0256bb65',1,'wgssSTU::ITabletEvents']]],
  ['onpendata',['onPenData',['../interfacewgss_s_t_u_1_1_i_report_handler_events.html#a5eed44096c23bb584f278b8702450478',1,'wgssSTU::IReportHandlerEvents::onPenData()'],['../interfacewgss_s_t_u_1_1_i_tablet_events.html#a453735b04806aaff4e4c73a6ec25c7c7',1,'wgssSTU::ITabletEvents::onPenData()']]],
  ['onpendataencrypted',['onPenDataEncrypted',['../interfacewgss_s_t_u_1_1_i_report_handler_events.html#a325b4514ba16f64af1a34a0f511c1925',1,'wgssSTU::IReportHandlerEvents::onPenDataEncrypted()'],['../interfacewgss_s_t_u_1_1_i_tablet_events.html#ac7d3b6209c481f3d631924fb68202ecc',1,'wgssSTU::ITabletEvents::onPenDataEncrypted()']]],
  ['onpendataencryptedoption',['onPenDataEncryptedOption',['../interfacewgss_s_t_u_1_1_i_report_handler_events.html#a7753ae56840fcafaa362a2f6c6fdde17',1,'wgssSTU::IReportHandlerEvents::onPenDataEncryptedOption()'],['../interfacewgss_s_t_u_1_1_i_tablet_events.html#a495cf169d1bce8d3a6049fa68f7faf1a',1,'wgssSTU::ITabletEvents::onPenDataEncryptedOption()']]],
  ['onpendataoption',['onPenDataOption',['../interfacewgss_s_t_u_1_1_i_report_handler_events.html#a5fefd65330d605d57593ac1db3fc6ae4',1,'wgssSTU::IReportHandlerEvents::onPenDataOption()'],['../interfacewgss_s_t_u_1_1_i_tablet_events.html#a55f753944873c41513b0a5aba9a5e0b2',1,'wgssSTU::ITabletEvents::onPenDataOption()']]],
  ['onreport',['onReport',['../interfacewgss_s_t_u_1_1_i_interface_events.html#a202be653f8a2622e36f9da276674b3a4',1,'wgssSTU::IInterfaceEvents']]],
  ['onunhandledreportdata',['onUnhandledReportData',['../interfacewgss_s_t_u_1_1_i_tablet_events.html#a79dd08517bee77e410436cf7b801ffa4',1,'wgssSTU::ITabletEvents']]]
];
