var hierarchy =
[
    [ "ICapability", "interfacewgss_s_t_u_1_1_i_capability.html", [
      [ "ICapability2", "interfacewgss_s_t_u_1_1_i_capability2.html", null ]
    ] ],
    [ "IComponent", "interfacewgss_s_t_u_1_1_i_component.html", null ],
    [ "IComponentFile", "interfacewgss_s_t_u_1_1_i_component_file.html", null ],
    [ "IComponentFiles", "interfacewgss_s_t_u_1_1_i_component_files.html", null ],
    [ "IDecrypt", "interfacewgss_s_t_u_1_1_i_decrypt.html", null ],
    [ "IEncryptionCommand", "interfacewgss_s_t_u_1_1_i_encryption_command.html", null ],
    [ "IEncryptionStatus", "interfacewgss_s_t_u_1_1_i_encryption_status.html", null ],
    [ "IErrorCode", "interfacewgss_s_t_u_1_1_i_error_code.html", null ],
    [ "IHandwritingThicknessColor", "interfacewgss_s_t_u_1_1_i_handwriting_thickness_color.html", null ],
    [ "IHandwritingThicknessColor24", "interfacewgss_s_t_u_1_1_i_handwriting_thickness_color24.html", null ],
    [ "IInformation", "interfacewgss_s_t_u_1_1_i_information.html", null ],
    [ "IInkThreshold", "interfacewgss_s_t_u_1_1_i_ink_threshold.html", null ],
    [ "IInterface", "interfacewgss_s_t_u_1_1_i_interface.html", [
      [ "IInterface2", "interfacewgss_s_t_u_1_1_i_interface2.html", [
        [ "ISerialInterface2", "interfacewgss_s_t_u_1_1_i_serial_interface2.html", null ],
        [ "IUsbInterface2", "interfacewgss_s_t_u_1_1_i_usb_interface2.html", null ]
      ] ],
      [ "ISerialInterface", "interfacewgss_s_t_u_1_1_i_serial_interface.html", null ],
      [ "IUsbInterface", "interfacewgss_s_t_u_1_1_i_usb_interface.html", null ]
    ] ],
    [ "IInterfaceEvents", "interfacewgss_s_t_u_1_1_i_interface_events.html", null ],
    [ "IInterfaceQueue", "interfacewgss_s_t_u_1_1_i_interface_queue.html", null ],
    [ "IInterfaceQueue2", "interfacewgss_s_t_u_1_1_i_interface_queue2.html", null ],
    [ "IJScript", "interfacewgss_s_t_u_1_1_i_j_script.html", [
      [ "IJScript2", "interfacewgss_s_t_u_1_1_i_j_script2.html", null ]
    ] ],
    [ "IPenData", "interfacewgss_s_t_u_1_1_i_pen_data.html", [
      [ "IPenDataOption", "interfacewgss_s_t_u_1_1_i_pen_data_option.html", null ],
      [ "IPenDataTimeCountSequence", "interfacewgss_s_t_u_1_1_i_pen_data_time_count_sequence.html", [
        [ "IPenDataTimeCountSequenceEncrypted", "interfacewgss_s_t_u_1_1_i_pen_data_time_count_sequence_encrypted.html", null ]
      ] ]
    ] ],
    [ "IPenDataEncrypted", "interfacewgss_s_t_u_1_1_i_pen_data_encrypted.html", [
      [ "IPenDataEncryptedOption", "interfacewgss_s_t_u_1_1_i_pen_data_encrypted_option.html", null ]
    ] ],
    [ "IPredicate", "interfacewgss_s_t_u_1_1_i_predicate.html", null ],
    [ "IProtocol", "interfacewgss_s_t_u_1_1_i_protocol.html", [
      [ "IProtocol2", "interfacewgss_s_t_u_1_1_i_protocol2.html", null ]
    ] ],
    [ "IProtocolHelper", "interfacewgss_s_t_u_1_1_i_protocol_helper.html", [
      [ "IProtocolHelper2", "interfacewgss_s_t_u_1_1_i_protocol_helper2.html", null ]
    ] ],
    [ "IRectangle", "interfacewgss_s_t_u_1_1_i_rectangle.html", null ],
    [ "IReport", "interfacewgss_s_t_u_1_1_i_report.html", null ],
    [ "IReportHandler", "interfacewgss_s_t_u_1_1_i_report_handler.html", null ],
    [ "IReportHandlerEvents", "interfacewgss_s_t_u_1_1_i_report_handler_events.html", null ],
    [ "IStatus", "interfacewgss_s_t_u_1_1_i_status.html", null ],
    [ "ITablet", "interfacewgss_s_t_u_1_1_i_tablet.html", [
      [ "ITablet2", "interfacewgss_s_t_u_1_1_i_tablet2.html", null ]
    ] ],
    [ "ITabletEncryptionHandler", "interfacewgss_s_t_u_1_1_i_tablet_encryption_handler.html", null ],
    [ "ITabletEncryptionHandler2", "interfacewgss_s_t_u_1_1_i_tablet_encryption_handler2.html", null ],
    [ "ITabletEvents", "interfacewgss_s_t_u_1_1_i_tablet_events.html", null ],
    [ "ITabletEventsException", "interfacewgss_s_t_u_1_1_i_tablet_events_exception.html", null ],
    [ "IUsbDevice", "interfacewgss_s_t_u_1_1_i_usb_device.html", [
      [ "IUsbDevice2", "interfacewgss_s_t_u_1_1_i_usb_device2.html", null ]
    ] ],
    [ "IUsbDevices", "interfacewgss_s_t_u_1_1_i_usb_devices.html", null ],
    [ "JScript", "namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_j_script", null ],
    [ "ProtocolHelper", "namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_protocol_helper", null ],
    [ "ReportHandler", "namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_report_handler", null ],
    [ "SerialInterface", "namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_serial_interface", null ],
    [ "Tablet", "namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_tablet", null ],
    [ "UsbDevices", "namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_usb_devices", null ],
    [ "UsbInterface", "namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_usb_interface", null ]
];