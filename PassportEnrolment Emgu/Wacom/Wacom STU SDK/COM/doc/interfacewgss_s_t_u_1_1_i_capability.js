var interfacewgss_s_t_u_1_1_i_capability =
[
    [ "tabletMaxX", "interfacewgss_s_t_u_1_1_i_capability.html#a07fc811c93fb6567706c682103e76439", null ],
    [ "tabletMaxY", "interfacewgss_s_t_u_1_1_i_capability.html#ae8e81890d3c6e53870eaabafc5751adf", null ],
    [ "tabletMaxPressure", "interfacewgss_s_t_u_1_1_i_capability.html#ad8f0b4bdd6971eda1ff1c193fca604d1", null ],
    [ "screenWidth", "interfacewgss_s_t_u_1_1_i_capability.html#a59332a8c1ca8ef20e96a644a76f1bda9", null ],
    [ "screenHeight", "interfacewgss_s_t_u_1_1_i_capability.html#adbcefc38fd5b508fa5b9e9318c32b2eb", null ],
    [ "maxReportRate", "interfacewgss_s_t_u_1_1_i_capability.html#a3ebb083bf12de1e07b1a6534b812578e", null ],
    [ "resolution", "interfacewgss_s_t_u_1_1_i_capability.html#a57207fc9948d647b513a4be25f5be4f4", null ],
    [ "zlibColorSupport", "interfacewgss_s_t_u_1_1_i_capability.html#a3b3f1b9977aa29b9082ac75081a62295", null ]
];