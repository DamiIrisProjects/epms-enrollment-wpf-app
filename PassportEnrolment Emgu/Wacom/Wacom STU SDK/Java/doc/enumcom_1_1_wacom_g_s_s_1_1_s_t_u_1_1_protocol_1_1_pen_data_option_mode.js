var enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option_mode =
[
    [ "getValue", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option_mode.html#aaa16a8e6dad8f0b51f0ae6cae4e8306d", null ],
    [ "None", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option_mode.html#ae33d9167db64e25ddabdcd8453588953", null ],
    [ "TimeCount", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option_mode.html#ac9430ed2144e410de304629cd52c1aa1", null ],
    [ "SequenceNumber", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option_mode.html#a8ec4499f537ee04d66ff5f5bd7f90283", null ],
    [ "TimeCountSequence", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option_mode.html#a6cc1bdb1fe5c8cdf7196eac5646422e3", null ]
];