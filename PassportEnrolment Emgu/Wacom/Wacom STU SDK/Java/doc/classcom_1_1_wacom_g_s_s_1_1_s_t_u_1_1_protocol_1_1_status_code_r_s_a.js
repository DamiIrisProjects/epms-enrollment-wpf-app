var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code_r_s_a =
[
    [ "Ready", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code_r_s_a.html#abf41d7a8e479882d4a7cc7c952858a23", null ],
    [ "Calculating", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code_r_s_a.html#aa5be3846542ee7ccee0aef72d7310f0e", null ],
    [ "Even", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code_r_s_a.html#a92bc209a6abd8953a0f39c640159b006", null ],
    [ "Long", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code_r_s_a.html#a8ad4e5cf0d3bf08f6f33b2bc92bc46a7", null ],
    [ "Short", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code_r_s_a.html#aaf511ffdd5e1c83115ca15af2c2018c1", null ],
    [ "Invalid", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code_r_s_a.html#a8638b169f488858029943cea4c822cc3", null ],
    [ "NotReady", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code_r_s_a.html#ae77bd0c23d42b32c4c1ea5cdcd710676", null ]
];