var interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface =
[
    [ "disconnect", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#a486be9d61f05bb80a1b319f00f289209", null ],
    [ "isConnected", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#af54b4954816d203bd3ddcf63bf1b09da", null ],
    [ "get", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#a575e48ae5778ddfa9b947144fc4547b7", null ],
    [ "set", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#acbf534b0d2df415a56b5016ffc36c879", null ],
    [ "supportsWrite", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#aa40d12c18be82e7d264214eadaf2ea24", null ],
    [ "write", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#a427b06f02fe0eb81626235588007cb11", null ],
    [ "interfaceQueue", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#a94fc9452fd9c2cb72e74b55b85a47286", null ],
    [ "queueNotifyAll", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#a33419bde2b1781bcab3390b5738c4cb3", null ],
    [ "getReportCountLengths", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#ae2454f67178aa0b442d9c196bd06d67e", null ],
    [ "getProductId", "interfacecom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_interface.html#a6de0fb892eb272a0862e937f799cf74a", null ]
];