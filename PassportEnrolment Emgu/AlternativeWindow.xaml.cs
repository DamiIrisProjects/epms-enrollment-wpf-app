﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using EDSDKLib;
using Emgu.CV;
using System.Threading;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using Emgu.CV.CvEnum;
using PassportEnrolment.Helpers;
using FaceDetection;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.Cuda;

namespace EOScamera
{
    /// <summary>
    /// Interaction logic for AlternativeWindow.xaml
    /// </summary>
    public partial class AlternativeWindow : Window
    {
        #region Variables
        
        // Canon
        SDKHandler CameraHandler;
        List<int> AvList;
        List<int> TvList;
        List<int> ISOList;
        List<Camera> CamList;
        bool IsInit = false;
        int BulbTime = 30;
        int whichCamera = 0;
        int USBcamcount = 0;
        bool takephoto = false;
        bool pausephoto = false;
        ImageSource EvfImage;
        JpegBitmapDecoder dec;
        ImageBrush bgbrush = new ImageBrush();
        ThreadStart SetImageAction;
        string[] cameraList;


        private Capture _capture = null;
        private bool _captureInProgress;

        public bool DontUseFacialRecognition { get; set; }

        public string Filelocation { get; set; }

        public Bitmap CroppedImage { get; set; }

        #endregion

        #region Constructor
        
        public AlternativeWindow()
        {
            InitializeComponent();

            // Confirm there is a camera plugged
            CameraHandler = new SDKHandler();
            CamList = CameraHandler.GetCameraList();

            if (CamList.Count == 0)
            {
                whichCamera = 0;
                
                // Start default camera
                CvInvoke.UseOpenCL = false;
                try
                {
                    _capture = new Capture();
                    _capture.ImageGrabbed += ProcessFrame;
                    _capture.Start();
                }
                catch (NullReferenceException excpt)
                {
                    //MessageBox.Show(excpt.Message);
                }
            }
            else
            {
                whichCamera = 1;
                CameraHandler.CameraAdded += new SDKHandler.CameraAddedHandler(SDK_CameraAdded);
                CameraHandler.LiveViewUpdated += new SDKHandler.StreamUpdate(SDK_LiveViewUpdated);
                CameraHandler.CameraHasShutdown += SDK_CameraHasShutdown;
                SetImageAction = new ThreadStart(delegate { bgbrush.ImageSource = EvfImage; });
                RefreshCamera();
                IsInit = true;
            }
        } 

        #endregion

        #region Subroutines

        private void TakePhotograph()
        {
            Directory.CreateDirectory(Filelocation);
            CameraHandler.ImageSaveDirectory = Filelocation;
            CameraHandler.TakePhoto();

            CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Camera);
            CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Host);
        }

        private void CloseSession()
        {
            CameraHandler.CloseSession();
            SessionButton.Content = "Open Session";
        }

        private void RefreshCamera()
        {
            CloseSession();
            CamList = CameraHandler.GetCameraList();
        }

        private int OpenSession()
        {
            if (CamList.Count > 0)
            {
                RefreshCamera();
                CameraHandler.OpenSession(CamList[0]);
                SessionButton.Content = "Close Session";
                string cameraname = CameraHandler.MainCamera.Info.szDeviceDescription;
                if (CameraHandler.GetSetting(EDSDK.PropID_AEMode) != EDSDK.AEMode_Manual) MessageBox.Show("Camera is not in manual mode. Some features might not work!");
                AvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Av);
                TvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Tv);
                ISOList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_ISOSpeed);
                int wbidx = (int)CameraHandler.GetSetting((uint)EDSDK.PropID_WhiteBalance);

                return 1;
            }
            else
            {
                MessageBox.Show("No camera found");
                return 0;
            }
        }

        #endregion

        #region SDK Events

        private void SDK_LiveViewUpdated(System.IO.Stream img)
        {
            if (CameraHandler.IsLiveViewOn)
            {
                try
                {
                    List<Rectangle> faces = new List<Rectangle>();
                    List<Rectangle> eyes = new List<Rectangle>();
                    bool tryUseCuda = false;
                    bool tryUseOpenCL = true;
                    long detectionTime;                   

                    if (!pausephoto)
                    {
                        this.Dispatcher.Invoke(new Action(() =>
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                //img.Position = 0;
                                dec = new JpegBitmapDecoder(img, BitmapCreateOptions.None, BitmapCacheOption.None);
                                EvfImage = dec.Frames[0];

                                Bitmap bmpImage = new Bitmap(img);
                                Image<Bgr, Byte> img1 = new Image<Bgr, Byte>(bmpImage);

                                Mat frame = img1.Mat;
                                //img.CopyTo(ms);
                                //frame.SetTo((ms.ToArray()));

                                DetectFace.Detect(
                                frame, "haarcascade_frontalface_default.xml", "haarcascade_eye.xml",
                                faces, eyes,
                                tryUseCuda,
                                tryUseOpenCL,
                                false,
                                out detectionTime);

                                foreach (Rectangle face in faces)
                                {
                                    // scale face to passport size
                                    double xscale = 1.5;
                                    double yscale = 1.75;
                                    int newx = Convert.ToInt32(face.X + (face.Width / 2) - (face.Width * xscale / 2));
                                    int newy = Convert.ToInt32(face.Y + (face.Height / 2) - (face.Height * yscale / 2));
                                    int neww = Convert.ToInt32(face.Width * xscale);
                                    int newh = Convert.ToInt32(face.Height * yscale);

                                    Rectangle biggerface = new Rectangle(newx, newy, neww, newh);

                                    if (takephoto)
                                    {
                                        CroppedImage = CropImage(frame.Bitmap, biggerface);
                                        takephoto = false;
                                        pausephoto = true;
                                    }

                                    CvInvoke.Rectangle(frame, biggerface, new Bgr(System.Drawing.Color.Red).MCvScalar, 2);
                                }
                                
                               
                                //EvfImage = BitmapSourceConvert.ToBitmapSource(frame);

                                Application.Current.Dispatcher.Invoke(SetImageAction);
                            }
                        }));
                    }

                    //display the image 
                    //ImageViewer.Show(image, String.Format(
                    //"Completed face and eye detection using {0} in {1} milliseconds", 
                    //(tryUseCuda && CudaInvoke.HasCuda) ? "GPU"
                    //: (tryUseOpenCL && CvInvoke.HaveOpenCLCompatibleGpuDevice) ? "OpenCL" 
                    //: "CPU",
                    //detectionTime));
      

                    //FSDK.CreateTracker(ref tracker);
                    //System.Drawing.Image frameImage = Bitmap.FromStream(img);

                    //int err = 0; // set realtime face detection parameters
                    //FSDK.SetTrackerMultipleParameters(tracker, "RecognizeFaces=false; HandleArbitraryRotations=false; DetermineFaceRotationAngle=false; InternalResizeWidth=100; FaceDetectionThreshold=2;", ref err);

                    //Graphics gr = Graphics.FromImage(frameImage);
                    //FSDK.CImage crop = new FSDK.CImage();
                    //FSDK.CImage image = new FSDK.CImage(frameImage);
                    //cropHandle = crop.ImageHandle;
                    //FSDK.SetJpegCompressionQuality(100);

                    //long[] IDs;
                    //long faceCount = 0;
                    //int width = 0;
                    //int height = 0;
                    //int left = 0;
                    //int top = 0;
                    //FSDK.FeedFrame(tracker, 0, image.ImageHandle, ref faceCount, out IDs, sizeof(long) * 256); // maximum 256 faces detected
                    //Array.Resize(ref IDs, (int)faceCount);

                    //for (int i = 0; i < IDs.Length; ++i)
                    //{
                    //    FSDK.TFacePosition facePosition = new FSDK.TFacePosition();
                    //    FSDK.GetTrackerFacePosition(tracker, 0, IDs[i], ref facePosition);

                    //    left = facePosition.xc - (int)(facePosition.w * 0.612);
                    //    top = facePosition.yc - (int)(facePosition.w * 0.75);
                    //    width = (int)(facePosition.w * 1.188);
                    //    height = (int)(facePosition.w * 1.584);

                    //    // draw rectangle
                    //    System.Drawing.Pen greenPen = new System.Drawing.Pen(System.Drawing.Color.LightGreen, 2);
                    //    gr.DrawRectangle(greenPen, left, top, width, height);

                    //    if (takephoto)
                    //    {
                    //        takephoto = false;
                    //        TakePhotograph();
                    //    }
                    //}

                    //img = new MemoryStream();
                    //frameImage.Save(img, System.Drawing.Imaging.ImageFormat.Jpeg);

                    //img.Position = 0;
                    //dec = new JpegBitmapDecoder(img, BitmapCreateOptions.None, BitmapCacheOption.None);
                    //EvfImage = dec.Frames[0];

                    //Application.Current.Dispatcher.Invoke(SetImageAction);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception");
                }
            }
        }

        private void SDK_CameraAdded()
        {
            RefreshCamera();
        }

        private void SDK_CameraHasShutdown(object sender, EventArgs e)
        {
            CloseSession();
            Close();
        }

        #endregion

        #region Session

        private void OpenSessionButton_Click(object sender, RoutedEventArgs e)
        {
            if (CameraHandler.CameraSessionOpen)
            {
                LVCanvas.Background = System.Windows.Media.Brushes.LightGray;
                CameraHandler.CloseSession();
                SessionButton.Content = "Open Session";
            }
            else
            {
                int result = OpenSession();
                if (result == 1)
                {
                    LVCanvas.Background = bgbrush;
                    CameraHandler.StartLiveView();
                }
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshCamera();
        }

        #endregion

        #region Live view

        private void LVCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CameraHandler.IsLiveViewOn && CameraHandler.IsCoordSystemSet)
            {
                System.Windows.Point p = e.GetPosition(this);
                ushort x = (ushort)((p.X / LVCanvas.Width) * CameraHandler.Evf_CoordinateSystem.width);
                ushort y = (ushort)((p.Y / LVCanvas.Height) * CameraHandler.Evf_CoordinateSystem.height);
                CameraHandler.SetManualWBEvf(x, y);
            }
        }

        private void FocusNear3Button_Click(object sender, RoutedEventArgs e)
        {
            CameraHandler.SetFocus(EDSDK.EvfDriveLens_Near3);
        }

        private void FocusFar3Button_Click(object sender, RoutedEventArgs e)
        {
            CameraHandler.SetFocus(EDSDK.EvfDriveLens_Far3);
        }

        #endregion

        #region Settings

        private void AvCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_Av, CameraValues.AV((string)AvCoBox.SelectedItem));
        }

        private void TvCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_Tv, CameraValues.TV((string)TvCoBox.SelectedItem));
            if ((string)TvCoBox.SelectedItem == "Bulb")
            {
                BulbBox.IsEnabled = true;
                BulbSlider.IsEnabled = true;
            }
            else
            {
                BulbBox.IsEnabled = false;
                BulbSlider.IsEnabled = false;
            }
        }

        private void ISOCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CameraHandler.SetSetting(EDSDK.PropID_ISOSpeed, CameraValues.ISO((string)ISOCoBox.SelectedItem));
        }

        private void WBCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (WBCoBox.SelectedIndex)
            {
                case 0: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Auto); break;
                case 1: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Daylight); break;
                case 2: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Cloudy); break;
                case 3: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Tangsten); break;
                case 4: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Fluorescent); break;
                case 5: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Strobe); break;
                case 6: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_WhitePaper); break;
                case 7: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Shade); break;
            }
        }

        private void BulbSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IsInit) BulbBox.Text = BulbSlider.Value.ToString();
        }

        private void BulbBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInit)
            {
                int b;
                if (int.TryParse(BulbBox.Text, out b) && b != BulbTime)
                {
                    BulbTime = b;
                    BulbSlider.Value = BulbTime;
                }
                else BulbBox.Text = BulbTime.ToString();
            }
        }

        #endregion

        #region Window Events

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (whichCamera == 1)
                {
                    int result = OpenSession();
                    if (result == 1)
                    {
                        LVCanvas.Background = bgbrush;
                        CameraHandler.StartLiveView();
                        btnTakePicture.IsEnabled = true;
                    }
                }
                else
                {
                    LVCanvas.Visibility = System.Windows.Visibility.Collapsed;
                    AdvancedOptions.Visibility = System.Windows.Visibility.Collapsed;
                    camerabox.Visibility = System.Windows.Visibility.Visible;
                    btnTakePicture.IsEnabled = true;

                    // Take picture
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not start video capture");
            }
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void btnRedo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                pausephoto = false;
                btnTakeAnother.IsEnabled = false;
                btnTakeAnother.Opacity = 0.3;
                btnDone.IsEnabled = false;
                btnDone.Opacity = 0.3;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void TakePhotoButton_Click(object sender, RoutedEventArgs e)
        {
            if (whichCamera == 1)
            {
                takephoto = true;
               
            }
            else
            {
                takephoto = true;
                btnDone.IsEnabled = true;
                btnDone.Opacity = 1;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (whichCamera == 1)
            {
                if (CameraHandler.CameraSessionOpen)
                {
                    LVCanvas.Background = System.Windows.Media.Brushes.LightGray;
                    CameraHandler.CloseSession();
                    SessionButton.Content = "Open Session";
                }

                CameraHandler.Dispose();
            }
            else
            {
                
            }
        }


        #endregion

        #region functions

        private void ProcessFrame(object sender, EventArgs arg)
        {
            Mat frame = new Mat();
            _capture.Retrieve(frame, 0);

            long detectionTime;
            List<Rectangle> faces = new List<Rectangle>();
            List<Rectangle> eyes = new List<Rectangle>();

            //The cuda cascade classifier doesn't seem to be able to load "haarcascade_frontalface_default.xml" file in this release
            //disabling CUDA module for now
            bool tryUseCuda = false;
            bool tryUseOpenCL = true;

            DetectFace.Detect(
            frame, "haarcascade_frontalface_default.xml", "haarcascade_eye.xml",
            faces, eyes,
            tryUseCuda,
            tryUseOpenCL,
            true,
            out detectionTime);

            foreach (Rectangle face in faces)
            {
                // scale face to passport size
                double xscale = 1.5;
                double yscale = 1.75;
                int newx = Convert.ToInt32(face.X + (face.Width / 2) - (face.Width * xscale / 2));
                int newy = Convert.ToInt32(face.Y + (face.Height / 2) - (face.Height * yscale /2));
                int neww = Convert.ToInt32(face.Width * xscale);
                int newh = Convert.ToInt32(face.Height * yscale);

                Rectangle biggerface = new Rectangle(newx, newy, neww, newh);

                if (takephoto)
                {
                    CroppedImage = CropImage(frame.Bitmap, biggerface);
                    takephoto = false;
                    pausephoto = true;
                    this.Dispatcher.Invoke(new Action(() =>
                    {
                        btnDone.IsEnabled = true;
                        btnTakeAnother.IsEnabled = true;
                        btnTakeAnother.Opacity = 1;
                    }));
                }

                CvInvoke.Rectangle(frame, biggerface, new Bgr(System.Drawing.Color.Red).MCvScalar, 2);
            }

            foreach (Rectangle eye in eyes)
            {
                //CvInvoke.Rectangle(frame, eye, new Bgr(System.Drawing.Color.Blue).MCvScalar, 2);
            }

            if (!pausephoto)
            {
                this.Dispatcher.Invoke(new Action(() =>
                {
                    camerabox.Source = BitmapSourceConvert.ToBitmapSource(frame);
                }));
            }
        }

        public Bitmap CropImage(System.Drawing.Image source, Rectangle crop)
        {
            var bmp = new Bitmap(crop.Width, crop.Height);
            using (var gr = Graphics.FromImage(bmp))
            {
                gr.DrawImage(source, new Rectangle(0, 0, bmp.Width, bmp.Height), crop, GraphicsUnit.Pixel);
            }
            return bmp;
        }

        public System.Drawing.Image ResizeImageFixedWidth(System.Drawing.Image imgToResize, int width)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = ((float)width / (float)sourceWidth);

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (System.Drawing.Image)b;
        }

        public void Save(Bitmap image, int maxWidth, int maxHeight, int quality, string filePath)
        {
            // Get the image's original width and height
            int originalWidth = image.Width;
            int originalHeight = image.Height;

            // To preserve the aspect ratio
            float ratioX = (float)maxWidth / (float)originalWidth;
            float ratioY = (float)maxHeight / (float)originalHeight;
            float ratio = Math.Min(ratioX, ratioY);

            // New width and height based on aspect ratio
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);

            // Convert other formats (including CMYK) to RGB.
            Bitmap newImage = new Bitmap(newWidth, newHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            // Draws the image in the specified size with quality mode set to HighQuality
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            // Get an ImageCodecInfo object that represents the JPEG codec.
            ImageCodecInfo imageCodecInfo = this.GetEncoderInfo(ImageFormat.Jpeg);

            // Create an Encoder object for the Quality parameter.
            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;

            // Create an EncoderParameters object. 
            EncoderParameters encoderParameters = new EncoderParameters(1);

            // Save the image as a JPEG file with quality level.
            EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
            encoderParameters.Param[0] = encoderParameter;
            newImage.Save(filePath, imageCodecInfo, encoderParameters);
        }

        /// <summary>
        /// Method to get encoder infor for given image format.
        /// </summary>
        /// <param name="format">Image format</param>
        /// <returns>image codec info.</returns>
        private ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }

        #endregion
    }
}
