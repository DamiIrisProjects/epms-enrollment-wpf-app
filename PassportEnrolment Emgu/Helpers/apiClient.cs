﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Add Usings:
using System.Net.Http;

// Add for Identity/Token Deserialization:
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net;

namespace PassportEnrolment.Helpers
{
    public class apiClientProvider
    {
        #region Variables

        string _hostUri;

        public string AccessToken { get; private set; }
       
        #endregion

        #region Constructor

        public apiClientProvider()
        {
            
        }

        public apiClientProvider(string hostUri)
        {
            _hostUri = hostUri;
        }

        #endregion

        #region Login

        public async Task<Dictionary<string, string>> GetTokenDictionary(
           string userName, string password)
        {
            HttpResponseMessage response;
            var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>( "grant_type", "password" ), 
                    new KeyValuePair<string, string>( "username", userName ), 
                    new KeyValuePair<string, string> ( "password", password )
                };
            var content = new FormUrlEncodedContent(pairs);

            using (var client = new HttpClient())
            {
                // Allow untrusted certificates since we are using self-certificates on the server
                ServicePointManager.ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;
                var tokenEndpoint = new Uri(new Uri(_hostUri), "Token");
                response = await client.PostAsync(tokenEndpoint, content);
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return GetTokenDictionary(responseContent);
        }


        private Dictionary<string, string> GetTokenDictionary(
            string responseContent)
        {
            Dictionary<string, string> tokenDictionary =
                JsonConvert.DeserializeObject<Dictionary<string, string>>(
                responseContent);
            return tokenDictionary;
        } 

        #endregion
    }
}
