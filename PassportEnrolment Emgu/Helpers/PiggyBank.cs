﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using PassportApplicationCommon;

namespace PassportEnrolment.Helpers
{
    public static class PiggyBank
    {
        public static List<OperatorRole> CurrentRoles { get; set; }

        public static List<OperatorRole> AllRoles { get; set; }

        public static Operator CurrentOperator { get; set; }

        public static List<string> PrivilegesList { get; set; }

        public static bool hasPrivilege(string privilege)
        {
            foreach (OperatorRole r in CurrentRoles)
            {
                foreach (OperatorPrivilege privi in r.Privileges)
                {
                    if (privi.Name.ToLower() == privilege.ToLower())
                        return true;
                }
            }

            return false;
        }

        
    }
}
