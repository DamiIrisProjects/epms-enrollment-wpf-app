﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PassportEnrolment.Helpers
{
    public enum PasswordScore
    {
        Blank = 0,
        VeryWeak = 1,
        Weak = 2,
        Medium = 3,
        Strong = 4,
        VeryStrong = 5
    }

    public static class PasswordAdvisor
    {
        public static PasswordScore CheckStrength(string password)
        {
            int score = 1;

            if (password.Length < 1)
                return PasswordScore.Blank;
            if (password.Length < 4)
                return PasswordScore.VeryWeak;

            if (password.Length >= 6)
                score++;
            if (password.Length >= 8)
                score++;

            char[] numbers = "0123456789".ToCharArray();
            char[] smallLetters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            char[] bigLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            char[] otherChars = ".?>_<*&".ToCharArray();

            bool add1 = false; bool add2 = false; bool add3 = false; bool add4 = false;

            foreach (char c in password)
            {
                if (numbers.Contains(c))
                    add1 = true;
                if (bigLetters.Contains(c))
                    add2 = true;
                if (smallLetters.Contains(c))
                    add3 = true;
                if (otherChars.Contains(c))
                    add4 = true;           
            }

            if (add1) score++;
            if (add2 && add3) score++;
            if (add4) score++;

            return (PasswordScore)score;
        }
    }
}
