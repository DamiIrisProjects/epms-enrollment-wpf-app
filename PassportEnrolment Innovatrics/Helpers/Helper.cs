﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Drawing.Drawing2D;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Drawing.Printing;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using PassportApplicationCommon.Model;
using PassportApplicationCommon;

namespace PassportEnrolment.Helpers
{
    static public class Helper
    {
        public static Operator Operator { get; set; }

        public static Control MainWindow { get; set; }

        public static bool HasCannon { get; set; }

        public static Bitmap ConvertToFormat(this System.Drawing.Image image, System.Drawing.Imaging.PixelFormat format)
        {
            Bitmap copy = new Bitmap(image.Width, image.Height, format);
            using (Graphics gr = Graphics.FromImage(copy))
            {
                gr.DrawImage(image, new Rectangle(0, 0, copy.Width, copy.Height));
            }
            return copy;
        }

        public static byte[] BitmapToByteArray(Bitmap someBitmap)
        {
            using (Bitmap bitmap = new Bitmap(someBitmap))
            {
                byte[] byteArray = (byte[])TypeDescriptor.GetConverter(someBitmap).ConvertTo(someBitmap, typeof(byte[]));
                return byteArray;
            }
        }

        public static byte[] BitmapSourceToArray(BitmapSource bitmapSource)
        {
            BitmapEncoder encoder = new JpegBitmapEncoder();
            using (MemoryStream stream = new MemoryStream())
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                encoder.Save(stream);
                return stream.ToArray();
            }
        }

        public static Bitmap BytesToBitmap(byte[] byteArray)
        {
            using (MemoryStream ms = new MemoryStream(byteArray))
            {
                Bitmap img = (Bitmap)System.Drawing.Image.FromStream(ms);
                return img;
            }
        }

        public static BitmapSource BitmapToSource(Bitmap someBitmap)
        {
            using (Bitmap bitmap = new Bitmap(someBitmap))
            {
                return Imaging.CreateBitmapSourceFromHBitmap(
                     bitmap.GetHbitmap(),
                     IntPtr.Zero,
                     Int32Rect.Empty,
                     BitmapSizeOptions.FromEmptyOptions());
            }
        }

        public static byte[] BitmapImageToByteArray(BitmapImage imageC)
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.GetBuffer();
        }

        public static Byte[] BufferFromBitmapImage(BitmapImage imageSource)
        {
            Stream stream = imageSource.StreamSource;
            Byte[] buffer = null;
            if (stream != null && stream.Length > 0)
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    buffer = br.ReadBytes((Int32)stream.Length);
                }
            }

            return buffer;
        }

        public static bool NoTextAllowed(string text)
        {
            Regex regex = new Regex("^[0-9]+$"); //regex that matches allowed text
            return regex.IsMatch(text);
        }

        public static bool CheckTextBox(TextBox Textbox)
        {
            if (string.IsNullOrEmpty(Textbox.Text))
            {
                Textbox.Background = System.Windows.Media.Brushes.Pink;
                return false;
            }
            else
                Textbox.Background = System.Windows.Media.Brushes.White;

            return true;
        }

        public static bool CheckDatePicker(DatePicker datePicker)
        {
            if (datePicker.SelectedDate == DateTime.Now || datePicker.SelectedDate == null)
            {
                datePicker.BorderBrush = System.Windows.Media.Brushes.Red;
                return false;
            }
            else
                datePicker.BorderBrush = System.Windows.Media.Brushes.Gray;

            return true;
        }

        public static bool CheckTickBox(CheckBox checkBox)
        {
            if (checkBox.IsChecked != true)
            {
                checkBox.BorderBrush = System.Windows.Media.Brushes.Red;
                checkBox.Foreground = System.Windows.Media.Brushes.Red;
                return false;
            }
            else
            {
                checkBox.BorderBrush = System.Windows.Media.Brushes.Gray;
                checkBox.Foreground = System.Windows.Media.Brushes.Black;
            }

            return true;
        }

        public static bool CheckRadioBox(List<RadioButton> list)
        {
            bool isTicked = false;
            foreach (RadioButton btn in list)
            {
                if (btn.IsChecked == true)
                    isTicked = true;
            }
            
            foreach (RadioButton btn in list)
            {
                if (isTicked == false)
                {
                    btn.BorderBrush = System.Windows.Media.Brushes.Red;
                    btn.Foreground = System.Windows.Media.Brushes.Red;
                }
                else
                {
                    btn.BorderBrush = System.Windows.Media.Brushes.Gray;
                    btn.Foreground = System.Windows.Media.Brushes.Black;
                }
            }

            return isTicked;
        }

        public static bool CheckComboBox(ComboBox combobox)
        {
            if (combobox.SelectedIndex == -1)
            {
                combobox.BorderBrush = System.Windows.Media.Brushes.Red;
                return false;
            }
            else
            {
                combobox.BorderBrush = System.Windows.Media.Brushes.Gray;
                return true;
            }
            return true;
        }

        public static Bitmap ResizeImage(System.Drawing.Image srcImage, int width, int height)
        {
            Bitmap newImage = new Bitmap(width, height);
            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(srcImage, new System.Drawing.Rectangle(0, 0, width, height));
            }

            return newImage;
        }

        public static bool VerifyPassport(string passport)
        {
            if (string.IsNullOrEmpty(passport))
            {
                MessageBox.Show("Please enter a valid Passport", "No Passport Number entered", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            char[] letters = "abcdefgjijklmnopqrstuvwxyz".ToCharArray();
            char[] numbers = "1234567890".ToCharArray();

            if (!letters.Contains(passport.ToLower()[0]))
            {
                MessageBox.Show("A Passport number Should start with a letter then numbers after", "Invalid Passport", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            for (int i = 1; i < passport.Length; i++)
            {
                if (!numbers.Contains(passport[i]))
                {
                    MessageBox.Show("A Passport number Should should have only numbers after the first letter", "Invalid Bin", MessageBoxButton.OK, MessageBoxImage.Information);
                    return false;
                }
            }

            return true;
        }

        public static void SetDefaultFingerprintScannerSettings()
        {
            try
            {
                //ScanningSettings sett = ScanningSettings.Current;
                //sett.MinQuality = Int32.Parse(ConfigurationManager.AppSettings["MinQuality"].ToString());
                //sett.MinMinutiaCount = Int32.Parse(ConfigurationManager.AppSettings["MinMinutiaCount"].ToString());
                //sett.MinArea = 0;
                //sett.MaxRotation = Int32.Parse(ConfigurationManager.AppSettings["MaxRotation"].ToString());
                //sett.CaptureDelay = Int32.Parse(ConfigurationManager.AppSettings["CaptureDelay"].ToString());
                //sett.ShowGauges = Boolean.Parse(ConfigurationManager.AppSettings["ShowGauges"].ToString());
                //sett.SaveImages = Boolean.Parse(ConfigurationManager.AppSettings["SaveImages"].ToString());
                //sett.Futronic.ScanDose = Int32.Parse(ConfigurationManager.AppSettings["Futronic.ScanDose"].ToString());
                //sett.Futronic.EliminateBackground = Boolean.Parse(ConfigurationManager.AppSettings["Futronic.EliminateBackground"].ToString());
                //sett.Futronic.IsTestMode = Boolean.Parse(ConfigurationManager.AppSettings["Futronic.IsTestMode"].ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Default Fingerprint Scanner settings missing. Please contact an Iris Staff", "Fingerprint Settings Not Found", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public static bool Onlyletters(string input)
        {
            char[] letters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

            foreach (char c in input.ToLower())
            {
                if (!letters.Contains(c))
                    return false;
            }

            return true;
        }

        public static bool OnlyNumbers(string input)
        {
            char[] numbers = "1234567890".ToCharArray();

            foreach (char c in input.ToLower())
            {
                if (!numbers.Contains(c))
                    return false;
            }

            return true;
        }

        public static string ToFirstLetterUpper(string input)
        {
            string[] words = input.Split(' ');
            string result = string.Empty;

            foreach (string word in words)
            {
                if (word != string.Empty || word != " ")
                {
                    char[] a = word.ToCharArray();
                    a[0] = char.ToUpper(a[0]);

                    if (result == string.Empty)
                        result = result + new string(a);
                    else
                        result = result + " " + new string(a);
                }
            }

            return result;
        }
    }
}
