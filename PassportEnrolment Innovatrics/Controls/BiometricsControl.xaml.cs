﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Management;
using System.Runtime.InteropServices;

using Afisteam.Biometrics;
using System.IO;
using Afisteam.Biometrics.Scannning;
using Afisteam.Biometrics.Gui;
using System.Configuration;
using System.Drawing;
using Afisteam.Biometrics.Gui.Core;
using Afisteam.Biometrics.Gui.Core.Controls;
using WacomSignaturePad;
using PassportEnrolmentEntities;
using System.DirectoryServices.AccountManagement;
using System.Windows.Threading;
using PassportEnrolment.Windows;
using System.Windows.Interop;
using System.Drawing.Imaging;
using Florentis;
using PassportEnrolment.Helpers;

namespace PassportEnrolment.Controls
{
    /// <summary>
    /// Interaction logic for BiometricsControl.xaml
    /// </summary>
    public partial class BiometricsControl : UserControl
    {
        #region Variables

        PersonController personController;
        Bitmap Signature;
        Bitmap Picture;

        #endregion

        #region Constructor

        public BiometricsControl()
        {
            InitializeComponent();
            //SetPermissions();
        }

        #endregion

        #region Events

        private void btnPicture_Click(object sender, RoutedEventArgs e)
        {
            SetCamera();
        }

        private void btnFingerprint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Helper.SetDefaultFingerprintScannerSettings();  

                Afisteam.Biometrics.Person person = CreateNewPerson();
                NewFingerScanWizard wizard = new NewFingerScanWizard(person);

                if (wizard.ShowDialog().Value)
                {
                    foreach (Finger newFinger in person.Fingers)
                    {
                        personController.UpdateFinger(newFinger);
                    }

                    personController = new PersonController(personController.Person);
                    personController.Validate();

                    this.DataContext = personController;

                    //Biometrics.Fingers = person;                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Initializing Fingerprint", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnSignature_Click(object sender, RoutedEventArgs e)
        {
            wgssSTU.UsbDevices usbDevices = new wgssSTU.UsbDevices();
            if (usbDevices.Count != 0)
            {
                try
                {
                    wgssSTU.IUsbDevice usbDevice = usbDevices[0]; // select a device

                    SignatureForm form = new SignatureForm(this, usbDevice);
                    System.Windows.Forms.DialogResult res = form.ShowDialog();
                    if (res == System.Windows.Forms.DialogResult.OK)
                    {
                        Bitmap bitmap = form.GetSigImage();

                        // Crop image to get it more centered
                        Bitmap croppedsig = bitmap.Clone(new System.Drawing.Rectangle(0,0,300,100), bitmap.PixelFormat);

                        imgSignature.Source = Helper.BitmapToSource(croppedsig);
                    }

                     form.Dispose();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SigCtl sigCtl = new SigCtl();
                DynamicCapture dc = new DynamicCaptureClass();
                DynamicCaptureResult res = dc.Capture(sigCtl, " ", " ", " ");
                if (res == DynamicCaptureResult.DynCaptOK)
                {
                    SigObj sigObj = (SigObj)sigCtl.Signature;
                    String filename = "sig1.png";
                    try
                    {
                        sigObj.RenderBitmap(filename, 200, 150, "image/png", 2.1f, 0x000000, 0xffffff, 10.0f, 10.0f, RBFlags.RenderOutputFilename | RBFlags.RenderColor32BPP | RBFlags.RenderEncodeData);

                        using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                        {
                            imgSignature.Stretch = Stretch.Fill;
                            imgSignature.Source = Helper.BitmapToSource((Bitmap)System.Drawing.Image.FromStream(fs));
                            fs.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
            }
        }

        private void _fingerCtrl_MouseDown(object sender, MouseButtonEventArgs e)
        {            
            Cursor oldCursor = this.Cursor;
            PersonController personController = (PersonController)this.DataContext;

            try
            {
                this.Cursor = System.Windows.Input.Cursors.Wait;
                FingerThumbPresenter thumbPresenter = (FingerThumbPresenter)((Control)sender).DataContext;

                if (thumbPresenter.Finger != null)
                {
                    if (Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl))
                    {
                        FingerDetailsForm form = new FingerDetailsForm(personController.Person.Fingers[thumbPresenter.Finger.Index]);

                        if (form.ShowDialog().Value)
                        {
                            personController = new PersonController(personController.Person);
                            personController.Validate();

                            this.DataContext = personController;
                        }
                    }
                    else
                    {
                        Finger newFinger = new Finger(thumbPresenter.Finger.Index);
                        FingerScanForm fingerScanForm = new FingerScanForm(newFinger);

                        if (fingerScanForm.ShowDialog().Value)
                        {
                            personController.UpdateFinger(newFinger);
                            personController = new PersonController(personController.Person);
                            personController.Validate();

                            this.DataContext = personController;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", ex.Message);
            }
            finally
            {
                this.Cursor = oldCursor;
            }
        }

        #endregion

        #region Operations

        private void SetCamera()
        {
            try
            {
                EOScamera.AlternativeWindow window = new EOScamera.AlternativeWindow();
                window.ShowDialog();
                
                if (window.CroppedImage)
                {
                    Uri uriSource = new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase), "tempcrop.jpg"));
                    BitmapImage imgTemp = new BitmapImage();
                    imgTemp.BeginInit();
                    imgTemp.CacheOption = BitmapCacheOption.OnLoad;
                    imgTemp.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                    imgTemp.UriSource = uriSource;
                    imgTemp.EndInit();
                    imgPhoto.Source = imgTemp;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetPermissions()
        {
            try
            {
                string groupname = "Biometrics";

                // set up domain context
                PrincipalContext ctx = new PrincipalContext(ContextType.Domain);

                // get your group in question
                GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx, groupname);

                if (group != null)
                {
                    // check if current user is member of that group
                    UserPrincipal user = UserPrincipal.Current;

                    if (user.IsMemberOf(group))
                    {
                        btnFingerprint.IsEnabled = false;
                        btnFingerprint.Opacity = 0.2;

                    }
                }
                else
                {
                    MessageBox.Show("Could not find any group associated with " + groupname);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                // Do nothing
            }
        }

        private Afisteam.Biometrics.Person CreateNewPerson()
        {
            Afisteam.Biometrics.Person _person = new Afisteam.Biometrics.Person();

            _person.Fingers.Add(new Finger(FingerIndex.LeftLittle));
            _person.Fingers.Add(new Finger(FingerIndex.LeftRing));
            _person.Fingers.Add(new Finger(FingerIndex.LeftMiddle));
            _person.Fingers.Add(new Finger(FingerIndex.LeftIndex));
            _person.Fingers.Add(new Finger(FingerIndex.LeftThumb));
            _person.Fingers.Add(new Finger(FingerIndex.RightLittle));
            _person.Fingers.Add(new Finger(FingerIndex.RightRing));
            _person.Fingers.Add(new Finger(FingerIndex.RightMiddle));
            _person.Fingers.Add(new Finger(FingerIndex.RightIndex));
            _person.Fingers.Add(new Finger(FingerIndex.RightThumb));

            personController = new PersonController(_person);

            return _person;
        }

        public bool ValidateBiometrics()
        {
            // Check Picture
            if (Picture == null)
            {
                MessageBox.Show("Please ensure you have taken a Photograph of the enrollee.", "Photograph Not Found", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            // Check Signature
            if (Signature == null)
            {
                MessageBox.Show("Please ensure you have taken enrollee's Signature.", "Signature Not Found", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            // Check fingerprints
            int fingerCount = 0;

            foreach (Finger finger in personController.Person.Fingers)
            {
                if (finger.BestFingerprint != null)
                {
                    fingerCount++;                    
                }
            }

            if (fingerCount < 6)
            {
                MessageBox.Show("Please ensure you have taken at least 6 Fingerprints from the enrollee.", "Fingerprints Validation Failed", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            
            return true;
        }

        public void Clear()
        {
            imgPhoto.Source = null;
            imgSignature.Source = null;
            imgPhoto.Source = null;
            DataContext = null;
        }

        public Bitmap Sharpen(Bitmap image)
        {
            Bitmap sharpenImage = (Bitmap)image.Clone();

            int filterWidth = 3;
            int filterHeight = 3;
            int width = image.Width;
            int height = image.Height;

            // Create sharpening filter.
            double[,] filter = new double[filterWidth, filterHeight];
            filter[0, 0] = filter[0, 1] = filter[0, 2] = filter[1, 0] = filter[1, 2] = filter[2, 0] = filter[2, 1] = filter[2, 2] = -1;
            filter[1, 1] = 9;

            double factor = 1.0;
            double bias = 0.0;

            System.Drawing.Color[,] result = new System.Drawing.Color[image.Width, image.Height];

            // Lock image bits for read/write.
            BitmapData pbits = sharpenImage.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            // Declare an array to hold the bytes of the bitmap.
            int bytes = pbits.Stride * height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(pbits.Scan0, rgbValues, 0, bytes);

            int rgb;
            // Fill the color array with the new sharpened color values.
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    double red = 0.0, green = 0.0, blue = 0.0;

                    for (int filterX = 0; filterX < filterWidth; filterX++)
                    {
                        for (int filterY = 0; filterY < filterHeight; filterY++)
                        {
                            int imageX = (x - filterWidth / 2 + filterX + width) % width;
                            int imageY = (y - filterHeight / 2 + filterY + height) % height;

                            rgb = imageY * pbits.Stride + 3 * imageX;

                            red += rgbValues[rgb + 2] * filter[filterX, filterY];
                            green += rgbValues[rgb + 1] * filter[filterX, filterY];
                            blue += rgbValues[rgb + 0] * filter[filterX, filterY];
                        }
                        int r = Math.Min(Math.Max((int)(factor * red + bias), 0), 255);
                        int g = Math.Min(Math.Max((int)(factor * green + bias), 0), 255);
                        int b = Math.Min(Math.Max((int)(factor * blue + bias), 0), 255);

                        result[x, y] = System.Drawing.Color.FromArgb(r, g, b);
                    }
                }
            }

            // Update the image with the sharpened pixels.
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    rgb = y * pbits.Stride + 3 * x;

                    rgbValues[rgb + 2] = result[x, y].R;
                    rgbValues[rgb + 1] = result[x, y].G;
                    rgbValues[rgb + 0] = result[x, y].B;
                }
            }

            // Copy the RGB values back to the bitmap.
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, pbits.Scan0, bytes);
            // Release image bits.
            sharpenImage.UnlockBits(pbits);

            return sharpenImage;
        }

        #endregion       
    }
}
