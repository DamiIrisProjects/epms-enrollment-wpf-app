var interfacewgss_s_t_u_1_1_i_report_handler_events =
[
    [ "onPenData", "interfacewgss_s_t_u_1_1_i_report_handler_events.html#a5eed44096c23bb584f278b8702450478", null ],
    [ "onPenDataOption", "interfacewgss_s_t_u_1_1_i_report_handler_events.html#a5fefd65330d605d57593ac1db3fc6ae4", null ],
    [ "onPenDataEncrypted", "interfacewgss_s_t_u_1_1_i_report_handler_events.html#a325b4514ba16f64af1a34a0f511c1925", null ],
    [ "onPenDataEncryptedOption", "interfacewgss_s_t_u_1_1_i_report_handler_events.html#a7753ae56840fcafaa362a2f6c6fdde17", null ],
    [ "onDevicePublicKey", "interfacewgss_s_t_u_1_1_i_report_handler_events.html#adacb53c0681a327a55cf810704048fbe", null ],
    [ "onDecrypt", "interfacewgss_s_t_u_1_1_i_report_handler_events.html#a6c0b475c30676e17b647650e1e1cd2f6", null ]
];