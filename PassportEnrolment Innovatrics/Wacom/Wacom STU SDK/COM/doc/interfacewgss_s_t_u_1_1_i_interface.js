var interfacewgss_s_t_u_1_1_i_interface =
[
    [ "disconnect", "interfacewgss_s_t_u_1_1_i_interface.html#a23b0b74d81abd3b43b767c32cc78cf25", null ],
    [ "isConnected", "interfacewgss_s_t_u_1_1_i_interface.html#ab55091f17c2afba6839c72b3053c9c6d", null ],
    [ "get", "interfacewgss_s_t_u_1_1_i_interface.html#a2c361ca3562c15b20eae5e48d01ff77b", null ],
    [ "set", "interfacewgss_s_t_u_1_1_i_interface.html#aeb9ac0ba18b4e7e9eb92e528e31ee916", null ],
    [ "supportsWrite", "interfacewgss_s_t_u_1_1_i_interface.html#a2711d1a14aaa40cd60a69f2323aee80e", null ],
    [ "write", "interfacewgss_s_t_u_1_1_i_interface.html#a5c0e00f8d17988cfe2803801b1cde2d0", null ],
    [ "interfaceQueue", "interfacewgss_s_t_u_1_1_i_interface.html#ae3a53536d0140d96479d4dd657f658a7", null ],
    [ "queueNotifyAll", "interfacewgss_s_t_u_1_1_i_interface.html#aaf95c934f82221ec5539cf3ba9fc1861", null ],
    [ "getReportCountLengths", "interfacewgss_s_t_u_1_1_i_interface.html#a63fbe1584d49c6600bc195b6fba5e818", null ],
    [ "Protocol", "interfacewgss_s_t_u_1_1_i_interface.html#aa05b99d0a9f8b45915524c1b8bca905f", null ]
];