var searchData=
[
  ['parameter',['parameter',['../interfacewgss_s_t_u_1_1_i_encryption_command.html#acb2ef56ca0f1c42ae3b50b2ec137dd91',1,'wgssSTU::IEncryptionCommand']]],
  ['pencolor',['penColor',['../interfacewgss_s_t_u_1_1_i_handwriting_thickness_color.html#a7be6153b7c80515f0516ce7a2b6bea5a',1,'wgssSTU::IHandwritingThicknessColor::penColor()'],['../interfacewgss_s_t_u_1_1_i_handwriting_thickness_color24.html#a347d35910554e03b8ebdfdd52eba9362',1,'wgssSTU::IHandwritingThicknessColor24::penColor()']]],
  ['pendata1',['penData1',['../interfacewgss_s_t_u_1_1_i_pen_data_encrypted.html#a3a6c2a05a1d42d4e3fa46a26be5dcf48',1,'wgssSTU::IPenDataEncrypted']]],
  ['pendata2',['penData2',['../interfacewgss_s_t_u_1_1_i_pen_data_encrypted.html#aee05b7bfb7a0470eb3a0a03394762da1',1,'wgssSTU::IPenDataEncrypted']]],
  ['pendataoptionmode',['PenDataOptionMode',['../namespacewgss_s_t_u.html#a32b5f473f2e53ea2c29c3b788af99884',1,'wgssSTU']]],
  ['pendataoptionmode_5fnone',['PenDataOptionMode_None',['../namespacewgss_s_t_u.html#a32b5f473f2e53ea2c29c3b788af99884aca49d1eaf594876cf3fdf654e26fdb36',1,'wgssSTU']]],
  ['pendataoptionmode_5fsequencenumber',['PenDataOptionMode_SequenceNumber',['../namespacewgss_s_t_u.html#a32b5f473f2e53ea2c29c3b788af99884a1e2514f1ef5972c06ff0dd910abeea08',1,'wgssSTU']]],
  ['pendataoptionmode_5ftimecount',['PenDataOptionMode_TimeCount',['../namespacewgss_s_t_u.html#a32b5f473f2e53ea2c29c3b788af99884a0e376c0d4a9699996770463c7a1d384f',1,'wgssSTU']]],
  ['pendataoptionmode_5ftimecountsequence',['PenDataOptionMode_TimeCountSequence',['../namespacewgss_s_t_u.html#a32b5f473f2e53ea2c29c3b788af99884a0a3204d46cb770690fe7844bafba8878',1,'wgssSTU']]],
  ['penthickness',['penThickness',['../interfacewgss_s_t_u_1_1_i_handwriting_thickness_color.html#aad9a7b2644adb453de570559acdc75ed',1,'wgssSTU::IHandwritingThicknessColor::penThickness()'],['../interfacewgss_s_t_u_1_1_i_handwriting_thickness_color24.html#aad9a7b2644adb453de570559acdc75ed',1,'wgssSTU::IHandwritingThicknessColor24::penThickness()']]],
  ['predicate',['predicate',['../interfacewgss_s_t_u_1_1_i_interface_queue2.html#acbeb5dbfea56b53e2733cf1965d17239',1,'wgssSTU::IInterfaceQueue2::predicate()'],['../interfacewgss_s_t_u_1_1_i_predicate.html#a9854e7fcff7cdd7d332ed9e7690559a9',1,'wgssSTU::IPredicate::predicate()']]],
  ['pressure',['pressure',['../interfacewgss_s_t_u_1_1_i_pen_data.html#a6dc62830a51976a09aa61991df5602ed',1,'wgssSTU::IPenData']]],
  ['protocol',['Protocol',['../interfacewgss_s_t_u_1_1_i_interface.html#aa05b99d0a9f8b45915524c1b8bca905f',1,'wgssSTU::IInterface::Protocol()'],['../interfacewgss_s_t_u_1_1_i_interface2.html#a9cfb42a5933ea4f4cacf1f5ddab55515',1,'wgssSTU::IInterface2::Protocol()'],['../interfacewgss_s_t_u_1_1_i_tablet.html#aa05b99d0a9f8b45915524c1b8bca905f',1,'wgssSTU::ITablet::Protocol()'],['../interfacewgss_s_t_u_1_1_i_tablet2.html#a9cfb42a5933ea4f4cacf1f5ddab55515',1,'wgssSTU::ITablet2::Protocol()']]],
  ['protocolhelper',['ProtocolHelper',['../namespacewgss_s_t_u.html#classwgss_s_t_u_1_1_protocol_helper',1,'wgssSTU']]]
];
