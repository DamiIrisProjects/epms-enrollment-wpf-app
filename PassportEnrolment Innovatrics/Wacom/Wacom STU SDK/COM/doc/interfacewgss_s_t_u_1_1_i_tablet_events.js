var interfacewgss_s_t_u_1_1_i_tablet_events =
[
    [ "onGetReportException", "interfacewgss_s_t_u_1_1_i_tablet_events.html#a01cb2324da2fcb00b5488abf0256bb65", null ],
    [ "onUnhandledReportData", "interfacewgss_s_t_u_1_1_i_tablet_events.html#a79dd08517bee77e410436cf7b801ffa4", null ],
    [ "onPenData", "interfacewgss_s_t_u_1_1_i_tablet_events.html#a453735b04806aaff4e4c73a6ec25c7c7", null ],
    [ "onPenDataOption", "interfacewgss_s_t_u_1_1_i_tablet_events.html#a55f753944873c41513b0a5aba9a5e0b2", null ],
    [ "onPenDataEncrypted", "interfacewgss_s_t_u_1_1_i_tablet_events.html#ac7d3b6209c481f3d631924fb68202ecc", null ],
    [ "onPenDataEncryptedOption", "interfacewgss_s_t_u_1_1_i_tablet_events.html#a495cf169d1bce8d3a6049fa68f7faf1a", null ],
    [ "onDevicePublicKey", "interfacewgss_s_t_u_1_1_i_tablet_events.html#a2d718fed23344f92d8f44ba1ab745702", null ]
];