var enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode =
[
    [ "getValue", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a6b4b85fe7ea3fc1c7c00d7e7dad45598", null ],
    [ "EncodingMode_1bit", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a385b32beb3b9076f1a9258b12b97d8ba", null ],
    [ "EncodingMode_1bit_ZLib", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a9aff2a1458e3b39dc0ced1765b54a3e4", null ],
    [ "EncodingMode_16bit", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a0e51fb963efa7aac956457b718abb76e", null ],
    [ "EncodingMode_24bit", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a22c00243005436d98895751f732bb1f3", null ],
    [ "EncodingMode_1bit_Bulk", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a4785e57be5a31cdc6aebd1fec707f463", null ],
    [ "EncodingMode_16bit_Bulk", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a8e9151b18366ed191b24f1e0b89198d5", null ],
    [ "EncodingMode_24bit_Bulk", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a1f6e19432e75b6c682d6aec2f9ce8c9c", null ],
    [ "EncodingMode_Raw", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#ae851772a703eb6a13d83f1ce184c2123", null ],
    [ "EncodingMode_Zlib", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a0d242062efd02d6692a4f67538672c0b", null ],
    [ "EncodingMode_16bit_565", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#a06e2f8466f7d1d8e36bfb47f3545cc87", null ],
    [ "EncodingMode_Bulk", "enumcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encoding_mode.html#ac17be1b29bfeb964c532f85c3c2f4d62", null ]
];