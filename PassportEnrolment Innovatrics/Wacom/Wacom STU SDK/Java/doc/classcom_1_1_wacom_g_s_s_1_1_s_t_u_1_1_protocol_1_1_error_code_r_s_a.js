var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a =
[
    [ "None", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#a20be58a26b84c0169edfccb228acfd3e", null ],
    [ "BadParameter", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#a6e1c2232afafd1042903097b2c691ec8", null ],
    [ "ParameterTooLong", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#a5c3b450d178ca703354d6f7a7b4e60fc", null ],
    [ "PublicKeyNotReady", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#a283d3d9adb6b3ed0b2424fc6d7e216f3", null ],
    [ "PublicExponentNotReady", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#aef1b1af7b41122769150310c9b336886", null ],
    [ "SpecifiedKeyInUse", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#a0aae02c6a536dd188e594257b0b92fa9", null ],
    [ "SpecifiedKeyNotInUse", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#aaefcd5306e1dae3ac86c9fffeb69f3c8", null ],
    [ "BadCommandCode", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#afc1609979dd2fd423bf5fe64a529249d", null ],
    [ "CommandPending", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#a1723c36f1c165983ed22e7cf3cc12dd8", null ],
    [ "SpecifiedKeyExists", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#ae21a1de4d23678af505d392516f64bf4", null ],
    [ "SpecifiedKeyNotExist", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#a0e26b6de0341ffe37fc3ca9f6628f9d3", null ],
    [ "NotInitialized", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_error_code_r_s_a.html#a38ae602cb43773ca5e0f34606c4cede3", null ]
];