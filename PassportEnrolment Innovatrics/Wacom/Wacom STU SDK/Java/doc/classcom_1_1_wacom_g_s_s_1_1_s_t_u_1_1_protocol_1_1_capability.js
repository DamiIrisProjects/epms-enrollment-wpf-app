var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability =
[
    [ "Capability", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a0c889203f6a848adde59353f0950c77b", null ],
    [ "Capability", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a54ddef1c1d00d5b86335a2472a3a48ec", null ],
    [ "getTabletMaxX", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a1bea473ca4849505e0eea8c922ad228d", null ],
    [ "getTabletMaxY", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a5466a71f882c8a881fb319f4f8c83a9b", null ],
    [ "getTabletMaxPressure", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a385624ffd2ab025d272b8b64f6784ff8", null ],
    [ "getScreenWidth", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#aff10702497d6a0332a0baa799d7a2f39", null ],
    [ "getScreenHeight", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a6fddb5748d08b34ee3299cbe776fa636", null ],
    [ "getMaxReportRate", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a5b6c5c09ca1e48e3d3cf218f43b47513", null ],
    [ "getResolution", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a51e79ac9ba62480b74011df3a5ca8061", null ],
    [ "getZlibColorSupport", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#a72218813158660185a8d6d9a0a9147c6", null ],
    [ "getEncodingFlag", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability.html#aea2c0703fcc5a0ca781f828c447dbe0a", null ]
];