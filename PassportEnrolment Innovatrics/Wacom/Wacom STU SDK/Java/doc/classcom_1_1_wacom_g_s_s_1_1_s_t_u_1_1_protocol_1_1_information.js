var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information =
[
    [ "Information", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information.html#aa9043227b784ed5123d719169eb35129", null ],
    [ "getModelName", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information.html#ac6b779a801e371531f209ec50a806ed1", null ],
    [ "getFirmwareMajorVersion", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information.html#a33700f4e8e051e27b986165de21b6d6a", null ],
    [ "getFirmwareMinorVersion", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information.html#a1250323ec39f1c26ccd111bdffcdb0fd", null ],
    [ "getSecureIc", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information.html#abc6609776d2978891bd822143f0e572d", null ],
    [ "getSecureIcVersion", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information.html#ab172282052b90508f14425f964a0cfcc", null ]
];