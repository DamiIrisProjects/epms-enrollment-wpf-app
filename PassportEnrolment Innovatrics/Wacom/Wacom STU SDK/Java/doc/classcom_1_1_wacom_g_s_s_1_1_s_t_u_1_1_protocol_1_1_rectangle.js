var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle =
[
    [ "Rectangle", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#a9486d7ff9eae35dd182c52d23d77b2fc", null ],
    [ "getUpperLeftXpixel", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#ae8f523a17e5f0d4bbc620bd2517be80a", null ],
    [ "getUpperLeftYpixel", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#ad7456f1138684a25d1b301f58073a178", null ],
    [ "getLowerRightXpixel", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#aceed708dd5aa0310f42ddcf8d688617e", null ],
    [ "getLowerRightYpixel", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html#a2bd11eafb34e308c0ad11479204392c8", null ]
];