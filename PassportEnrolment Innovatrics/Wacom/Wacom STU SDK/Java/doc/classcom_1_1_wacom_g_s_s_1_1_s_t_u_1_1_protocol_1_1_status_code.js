var classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code =
[
    [ "Ready", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code.html#af261cbf06c55b1486ecc73741b9d634e", null ],
    [ "Image", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code.html#adad9c651e169ff73fbf26d4e7b202c64", null ],
    [ "Capture", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code.html#a67a05e50d2d76f0431b6d12de729918a", null ],
    [ "Calculation", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code.html#ac0d2a4d4388f018390607224c4c2d74e", null ],
    [ "Image_Boot", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code.html#a5b7f8817417235a0717736848510c134", null ],
    [ "SystemReset", "classcom_1_1_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status_code.html#a8d0fac0b3d34a92c436bd70d7e50068e", null ]
];