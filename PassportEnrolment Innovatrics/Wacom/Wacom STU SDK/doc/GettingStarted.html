<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Wacom STU SDK &#x2015; Getting Started</title>
    <style type="text/css">
      html { font-family:sans-serif;font-size:10pt }
    </style>
  </head>
  <body>
<h1>
Getting Started
</h1>
<p>
Welcome to the Wacom STU SDK. This SDK enables the you to control all aspects of the Wacom STU-series of signature tablets.
</p>
<p>
You can:
<ul>
<li>Query the tablet for its current settings and make changes to them.</li>
<li>Send images to the tablet's LCD screen.</li>
<li>Receive and decode pen stylus data.</li>
</ul>
</p>
<p>
The SDK is written in C++11 and the source code is provided. However there are no limitations if you program in a different language. C and Java language bindings (Java relies upon JNI) are available with virtually all the same functionality as the core C++. All this is supported on Windows and Linux platforms. On Windows, there is also a COM library which provides support for any ActiveX-capable language such as .NET Framework (C#), Delphi, and HTML (JavaScript) from within Internet Explorer.
</p>
<h2>
Which language to use
</h2>
<h3>
C++
</h3>
<p>
The SDK's native language is C++. This is provided as source code and so can be compiled directly into your program. Note that the source code requires a number of prerequisites (<em>Boost</em> library, <em>Windows Driver Kit</em> or <em>libusb</em> library). Dependencies on external components such as zlib compression and encryption are optional. These can be pulled-in by the developer statically or dynamically according to their own preference and own decisions on third-party licensing models. This can be used on all supported platforms. It has been tested with Visual Studio 2010 and GCC 4.6.2.
</p>
<h3>
All other languages
</h3>
<p>
All other language bindings make use a pre-compiled binary library with a base name of <code>wgssSTU</code>. On Windows this is <code>wgssSTU.dll</code>, while on Linux this is <code>libwgssSTU.so</code>. You will need to ensure there is a binary library provided for your target platform.
</p>
<p>
<strong>Java</strong> support is supplied with a package called <code>wgssSTU.jar</code> which uses the binary library to perform the low-level interaction with the hardware. We have not found a way to safely bundle the binary within the package so the developer is required to keep them synchronized and according to platform deployment, as well as running the Java application with the correct settings to locate the binary library. It requires a minimum of Java 1.5, with the <code>Tablet</code> class requiring Java 1.7.
</p>
<p>
<strong>C</strong> language is supported on via a single <code>wgssSTU.h</code> header file to the binary library. The binary library supports linking statically or dynamically directly with functions or through a function table. The interface is a C interpretation of the C++ interface, with C++ exceptions turned back into error codes. Although this is a supported language, we would not typically recommend using this language binding due to the potential for programming errors.
</p>
<p>
<strong>COM</strong> (<strong>ActiveX</strong>) support is available on Windows. The binary library can be registered (using regsvr32.exe) or used without registration with an SxS (side-by-side) manifest (especially if developing a new application). Registration is required for use with Internet Explorer and during the development of .NET applications (when using Visual Studio), but are not necessary for application execution if deployed with a suitable manifest.
</p>
<p>
<strong>HTML</strong> (<strong>JavaScript</strong>) support is currently available via COM support which is limited to Internet Explorer on Windows platforms. Support for other browsers and platforms require a &ldquo;Netscape&rdquo;&#x2010;compatible plugin; this is under consideration for a future release.
</p>
<h2>
Overview
</h2>
<p>
As far as possible for each language and their idoms, classes, interfaces and methods and all named identically. The SDK has been built in discrete layers and you can pick and choose as you prefer. Generally, all functionality has been made public, including the internal workings so that very fine control can be made if necessary. Logically, the SDK can be split into simpler <em>User</em> APIs and the low-level <em>Core</em> APIs.
</p>
<p>
Note that this SDK is a <em>programming</em> interface, not a <em>user</em> interface: there are no user interface components within the API. This is one of the key factors why the API has been readily ported across languages and platforms.
</p>
<p>
The diagram below illustrates the key components within the SDK.
<p>
<object data="Architecture.svg" type="image/svg+xml"></object> 
<p>
Although no encryption code is provided, the API is designed with encryption in mind, so that you can add your own routines very easily.
<h3>
The <em>User</em> API
</h3>
<p>
The User API is the highest-level API, providing the simplest interface to interact with the tablet.
</p>
<p>
Use the <code>getUsbDevices()</code> method to return an array of attached devices, and pass that to the appropriate <code>connect()</code> method of class <code>Tablet</code> (you can also connect via a serial port). 
</p>
<p>
The <code>Tablet</code> class is the primary class that you use, and this provides basic protection of I/O timings and state-changes within the device as well as against calling functionality that the specific tablet may not have. This is especially helpful if encryption is used.
</p>
<p>
To upload an image, use an appropriate method from within <code>ProtocolHelper</code> to prepare the image into the native tablet data format before sending it via your tablet object.
</p>
<p>
The exact mechanism for receiving pen data is language dependant but in all but C++, the class automatically queues and decodes the data for you. In C++ use get an <code>InterfaceQueue</code> from the tablet object and decode incoming data from using class <code>ReportHandler</code>.
</p>
<p>
If necessary, it is entirely possible to use any of the <em>Core</em> API while using the <code>Tablet</code> class.
<h3>
The <em>Core</em> API
</h3>
<p>
Below the <code>Tablet</code> class, the <code>Protocol</code> and <code>Interface</code> classes provide direct access to the workings of the tablet. This is not generally required, but is not hidden from use if any limitation is found within the higher level API. No sanity-checking is performed on the input or output data, which is transferred directly as the developer requests it.
</p>
<p>
As per the User API, use the <code>getUsbDevices()</code> method to return an array of attached devices, and pass that to the appropriate <code>connect()</code> method of <code>UsbInterface</code>. Serial devices can be connected via the <code>SerialInterface</code> class.
</p>
<p>
Then we recommend you use <code>Interface</code> class which abstracts which type of port the tablet is connected to once the connection has been established.
</p>
<p>
You can then use the <code>Protocol</code> class to communicate to the tablet. In theory you can even craft your own API packets and send them to the device directly. Note that the methods in <code>Protocol</code> do not check that the tablet is in a valid state to accept the command, nor does it check whether the operation completed successfully on the device &mdash; you must do all this yourself.
</p>
<p>
For ease of use, there are some helper functions in <code>ProtocolHelper</code> that simplifies common tasks such as waiting for a command to complete and preparing images to send.
</p>
<p>
To receive pen data, you must get an <code>InterfaceQueue</code> object from an <code>Interface</code> object. This reports incoming raw data, which can then be given to <code>ReportHandler</code> to decode.
</p>
<h2>Where next</h2>
<p>
Each language has its own sample code and programming reference.
</p>
<p>
Those upgrading from the original <em>STU Low-Level SDK</em> may want to refer to the &ldquo;Changes since v1&rdquo; document first.
</p>
<h2>Linux installation tips &amp; notes</h2>
<ul>
<li>
  Ensure you have the dependencies installed. This should include: libusb-1.0 and libusb-1.0-devel.
</li>
<li>
  We have currently tested only on fedora16 i686.
</li>
<li>
  Allow user-access to your STU tablet. This usually requires adding a udev rule, for example:<br/>
  <code>
  SUBSYSTEM=="usb",ATTR={idVendor}=="056a",ACTION=="add",MODE="0660"<br/>
  </code>
</li>
<li>
  NOTE: Remember to install the libwgssSTU.so, for example (note paths maybe different):<br/>
  <code>
    sudo cp libwgssSTU.so.<em>0.0.x</em> /usr/local/lib<br/>
    sudo /sbin/ldconfig /usr/local/lib<br/>
  </code>
</li>
<li>
NOTE: You may need to adjust your <code>LD_LIBRARY_PATH</code> variable as well.
</li>
</ul>
<hr/>
<footer>
Copyright &copy; 2013 Wacom Company Limited.<br/>
Wacom is a registered trademark of WACOM Company Limited.<br/>
ActiveX, Internet Explorer, Visual Studio and Windows are registered trademarks of Microsoft Corporation in the United States and other countries.<br/>
Delphi is a registered trademark of Embarcadero Technologies, Inc. in the United States and other countries.<br/>
Linux is the registered trademark of Linus Torvalds in the U.S. and other countries.<br/>
Oracle and Java are registered trademarks of Oracle and/or its affiliates. Other names may be trademarks of their respective owners.
</footer>

  </body>
</html>
