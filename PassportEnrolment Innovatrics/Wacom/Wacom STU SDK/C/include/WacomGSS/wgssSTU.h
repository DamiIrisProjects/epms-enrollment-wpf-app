/*
/// @file      WacomGSS/wgssSTU.h
/// @copyright Copyright (c) 2012 Wacom Company Limited
/// @author    mholden
/// @date      2012-07-13
/// @brief     C language wrapper to the STU interface
*/
#ifndef WacomGSS_wgssSTU_h
#define WacomGSS_wgssSTU_h

#include <stddef.h>
#include <stdint.h>

#if !defined(WacomGSS_WIN32) && (defined(WIN32) || defined(_WIN32) || defined(__WIN32__))
# define WacomGSS_WIN32 1
#endif
#if !defined(WacomGSS_Linux) && (defined(__linux__))
# define WacomGSS_Linux 1
#endif

#if !defined(WacomGSS_EXPORT)
# if defined(WacomGSS_Linux)
#   define WacomGSS_EXPORT __attribute__((visibility("default")))
# elif defined(WacomGSS_WIN32)
#   ifdef __cplusplus
#     define WacomGSS_EXPORT __declspec(dllimport) __declspec(nothrow) 
#   else
#     define WacomGSS_EXPORT __declspec(dllimport)
#   endif
# endif
#endif

#if !defined(WacomGSS_DECL)
# if defined(WacomGSS_WIN32)
#   define WacomGSS_DECL __stdcall
# else
#   define WacomGSS_DECL
# endif
#endif

#if defined(WacomGSS_WIN32)
typedef struct IWICImagingFactory IWICImagingFactory;
typedef struct IWICBitmapSource   IWICBitmapSource;
#endif

#ifdef __cplusplus
extern "C" {
#endif


#if defined(WacomGSS_DOXYGEN)
# undef  WacomGSS_EXPORT
# define WacomGSS_EXPORT
# undef  WacomGSS_DECL
# define WacomGSS_DECL
#endif

#if defined(WacomGSS_bool_enum)
enum tagWacomGSS_bool
{
  WacomGSS_false = 0,
  WacomGSS_true  = 1
};
typedef enum tagWacomGSS_bool WacomGSS_bool;
#else
# if !defined(WacomGSS_bool_int)
#   define WacomGSS_bool_int 1
# endif
typedef int WacomGSS_bool;
const int WacomGSS_false = 0;
const int WacomGSS_true = 1;
#endif

#define WacomGSS_DeclareHandle(Name) struct tagWacomGSS_Handle_##Name { int unused; }; typedef struct tagWacomGSS_Handle_##Name * WacomGSS_##Name;

WacomGSS_DeclareHandle(Interface)
WacomGSS_DeclareHandle(InterfaceQueue)
WacomGSS_DeclareHandle(Tablet)

/*
//WacomGSS_DECLARE_HANDLE(Library)
//int initializeLibrary(WacomGSS_Library * library);
//int freeLibrary(WacomGSS_Library library);
*/


enum tagWacomGSS_Return
{
  WacomGSS_Return_Success,
  WacomGSS_Return_Unspecified,
  WacomGSS_Return_InvalidHandle,
  WacomGSS_Return_InvalidParameter,
  WacomGSS_Return_InvalidParameterNullPointer,
  WacomGSS_Return_Unsupported,
  WacomGSS_Return_Error,
  WacomGSS_Return_ErrorSizeof,
  WacomGSS_Return_Exception_Unknown,
  WacomGSS_Return_Exception_std,
  WacomGSS_Return_Exception_system_error,
  WacomGSS_Return_Exception_not_connected,
  WacomGSS_Return_Exception_device_removed,
  WacomGSS_Return_Exception_write_not_supported,
  WacomGSS_Return_Exception_io,
  WacomGSS_Return_Exception_timeout,
  WacomGSS_Return_Exception_set,
  WacomGSS_Return_Exception_ReportHandler,
  WacomGSS_Return_Exception_EncryptionHandler
};

struct tagWacomGSS_ComponentFile_v1
{
  char * name;
  char * version;
};
typedef struct tagWacomGSS_ComponentFile_v1 WacomGSS_ComponentFile;


enum tagWacomGSS_ProductId
{
  WacomGSS_ProductId_500  = 0x00a1,
  WacomGSS_ProductId_300  = 0x00a2,
  WacomGSS_ProductId_520A = 0x00a3,
  WacomGSS_ProductId_430  = 0x00a4,
  WacomGSS_ProductId_530  = 0x00a5
};

struct tagWacomGSS_UsbDeviceBase_v1
{
  uint16_t  idVendor;
  uint16_t  idProduct;
  uint16_t  bcdDevice;
};

#if defined(WacomGSS_WIN32)
struct tagWacomGSS_UsbDevice_v1
{
  struct tagWacomGSS_UsbDeviceBase_v1 usbDevice;
  // pad[2]
  char * fileName;
  char * bulkFileName;
};
struct tagWacomGSS_UsbDevice_v1_wc
{
  struct tagWacomGSS_UsbDeviceBase_v1 usbDevice;
  // pad[2]
  wchar_t * fileName;
  wchar_t * bulkFileName;
};
typedef struct tagWacomGSS_UsbDevice_v1    WacomGSS_UsbDevice;
typedef struct tagWacomGSS_UsbDevice_v1_wc WacomGSS_UsbDevice_wc;
#endif
#if defined(WacomGSS_Linux)
struct tagWacomGSS_UsbDevice_v1
{
  struct tagWacomGSS_UsbDeviceBase_v1 usbDevice;
  uint8_t busNumber;
  uint8_t deviceAddress;  
};
typedef struct tagWacomGSS_UsbDevice_v1 WacomGSS_UsbDevice;
#endif


enum tagWacomGSS_ReportId
{
  WacomGSS_ReportId_PenData                           = 0x01, ///< <b>Mode:</b> in         <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_Status                            = 0x03, ///< <b>Mode:</b>    get     <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_Reset                             = 0x04, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
//WacomGSS_ReportId_05                                = 0x05, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300fw2
  WacomGSS_ReportId_Information                       = 0x08, ///< <b>Mode:</b>    get     <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_Capability                        = 0x09, ///< <b>Mode:</b>    get     <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_Uid                               = 0x0a, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_Uid2                              = 0x0b, ///< <b>Mode:</b>    get     <b>Compatibility:</b>  -   -   -   430 530
  WacomGSS_ReportId_PenDataEncrypted                  = 0x10, ///< <b>Mode:</b> in         <b>Compatibility:</b> 300 500 520A  ?   ?
  WacomGSS_ReportId_HostPublicKey                     = 0x13, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A  *   *
  WacomGSS_ReportId_DevicePublicKey                   = 0x14, ///< <b>Mode:</b> in/get     <b>Compatibility:</b> 300 500 520A  *   *
  WacomGSS_ReportId_StartCapture                      = 0x15, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_EndCapture                        = 0x16, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_DHprime                           = 0x1a, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A  *   *
  WacomGSS_ReportId_DHbase                            = 0x1b, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A  *   *
  WacomGSS_ReportId_ClearScreen                       = 0x20, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_InkingMode                        = 0x21, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_InkThreshold                      = 0x22, ///< <b>Mode:</b>    get/set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_ClearScreenArea                   = 0x23, ///< <b>Mode:</b>        set <b>Compatibility:</b>  -   -   -   430 530
  WacomGSS_ReportId_StartImageDataArea                = 0x24, ///< <b>Mode:</b>        set <b>Compatibility:</b>  -   -   -   430 530
  WacomGSS_ReportId_StartImageData                    = 0x25, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_ImageDataBlock                    = 0x26, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_EndImageData                      = 0x27, ///< <b>Mode:</b>        set <b>Compatibility:</b> 300 500 520A 430 530
  WacomGSS_ReportId_HandwritingThicknessColor         = 0x28, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A(430)530  *430=thickness only
  WacomGSS_ReportId_BackgroundColor                   = 0x29, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A  -  530
  WacomGSS_ReportId_HandwritingDisplayArea            = 0x2a, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A 430 530
  WacomGSS_ReportId_BacklightBrightness               = 0x2b, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A  -  530
  WacomGSS_ReportId_ScreenContrast                    = 0x2c, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -  520A  -  530
  WacomGSS_ReportId_HandwritingThicknessColor24       = 0x2d, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -   -  (430)530  *430=thickness only
  WacomGSS_ReportId_BackgroundColor24                 = 0x2e, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -   -    -  530
  WacomGSS_ReportId_BootScreen                        = 0x2f, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -   -   430 530
  WacomGSS_ReportId_PenDataOption                     = 0x30, ///< <b>Mode:</b> in         <b>Compatibility:</b>  - (500)520A  ?   ?   *500=fw2.03
  WacomGSS_ReportId_PenDataEncryptedOption            = 0x31, ///< <b>Mode:</b> in         <b>Compatibility:</b>  -   -  520A  ?   ?
  WacomGSS_ReportId_PenDataOptionMode                 = 0x32, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  - (500)520A 430 530  *500=fw2.03
  WacomGSS_ReportId_PenDataTimeCountSequenceEncrypted = 0x33, ///< <b>Mode:</b> in         <b>Compatibility:</b>  -   -   -   430 530
  WacomGSS_ReportId_PenDataTimeCountSequence          = 0x34, ///< <b>Mode:</b> in         <b>Compatibility:</b>  -   -   -   430 530
  WacomGSS_ReportId_EncryptionCommand                 = 0x40, ///< <b>Mode:</b>    get/set <b>Compatibility:</b>  -   -   -   430 530
  WacomGSS_ReportId_EncryptionStatus                  = 0x50, ///< <b>Mode:</b> in/get     <b>Compatibility:</b>  -   -   -   430 530
//WacomGSS_ReportId_60                                = 0x60, //   <b>Mode:</b> -internal- <b>Compatibility:</b>  -   -   -   430 530
  WacomGSS_ReportId_GetReport                         = 0x80, ///< <b>Mode:</b>        set <b>Compatibility:</b> SERIAL ONLY
  WacomGSS_ReportId_SetResult                         = 0x81  ///< <b>Mode:</b> in         <b>Compatibility:</b> SERIAL ONLY
//WacomGSS_,ReportId_a0                                = 0xa0, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300 500 520A   430 530
//WacomGSS_ReportId_a2                                = 0xa2, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300 500 520A   430 530
//WacomGSS_ReportId_a3                                = 0xa3, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300 500 520A   430 530
//WacomGSS_ReportId_a5                                = 0xa5, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
//WacomGSS_ReportId_a6                                = 0xa6, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
//WacomGSS_ReportId_a7                                = 0xa7, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
//WacomGSS_ReportId_a8                                = 0xa8, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
//WacomGSS_ReportId_a9                                = 0xa9, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
//WacomGSS_ReportId_aa                                = 0xaa, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
//WacomGSS_ReportId_ab                                = 0xab, //   <b>Mode:</b> -internal- <b>Compatibility:</b> 300 500 520A   430 530
//WacomGSS_ReportId_ac                                = 0xac, //   <b>Mode:</b> -internal- <b>Compatibility:</b>     500
//WacomGSS_ReportId_ad                                = 0xad, //   <b>Mode:</b> -internal- <b>Compatibility:</b>         520A       530
//WacomGSS_ReportId_ae                                = 0xae, //   <b>Mode:</b> -internal- <b>Compatibility:</b>         520A       530
//WacomGSS_ReportId_af                                = 0xaf, //   <b>Mode:</b> -internal- <b>Compatibility:</b>         520A       530
//WacomGSS_ReportId_b0                                = 0xb0, //   <b>Mode:</b> -internal- <b>Compatibility:</b>                430 530
//WacomGSS_ReportId_b2                                = 0xb2, //   <b>Mode:</b> -internal- <b>Compatibility:</b>         520A       530
//WacomGSS_ReportId_b3                                = 0xb3, //   <b>Mode:</b> -internal- <b>Compatibility:</b>                430
//WacomGSS_ReportId_b4                                = 0xb4  //   <b>Mode:</b> -internal- <b>Compatibility:</b>                430 530
};


enum tagWacomGSS_StatusCode
{
  WacomGSS_StatusCode_Ready          = 0x00,
  WacomGSS_StatusCode_Image          = 0x01,
  WacomGSS_StatusCode_Capture        = 0x02,
  WacomGSS_StatusCode_Calculation    = 0x03,
  WacomGSS_StatusCode_Image_Boot     = 0x04,
  WacomGSS_StatusCode_SystemReset    = 0xff
};

enum tagWacomGSS_ErrorCode
{
  WacomGSS_ErrorCode_None                        = 0x00, 
  WacomGSS_ErrorCode_WrongReportId               = 0x01, 
  WacomGSS_ErrorCode_WrongState                  = 0x02, 

  WacomGSS_ErrorCode_CRC                         = 0x03, 
  WacomGSS_ErrorCode_GraphicsWrongEncodingType   = 0x10, 
  WacomGSS_ErrorCode_GraphicsImageTooLong        = 0x11, 
  WacomGSS_ErrorCode_GraphicsZlibError           = 0x12,

  WacomGSS_ErrorCode_GraphicsWrongParameters     = 0x15
/*
//WacomGSS_ErrorCode_GraphicsWrongParameters     =  0x13, // undocumented
//WacomGSS_ErrorCode_GraphicsSprleImageOverflow  =  0x1E, // undocumented
//WacomGSS_ErrorCode_GraphicsSprleBlockOverflow  =  0x1F, // undocumented

//WacomGSS_ErrorCode_Crypt_errors                =  0x20, // undocumented
//WacomGSS_ErrorCode_Crypt_dh_zero_input         =  0x21, // undocumented
//WacomGSS_ErrorCode_Crypt_dh_test_failed        =  0x22, // undocumented
//WacomGSS_ErrorCode_Crypt_dh_wrong_module       =  0x23, // undocumented
//WacomGSS_ErrorCode_Crypt_dh_wrong_base         =  0x24, // undocumented
//WacomGSS_ErrorCode_Crypt_dh_wrong_openkey      =  0x25, // undocumented
//WacomGSS_ErrorCode_Crypt_dh_key_invalid        =  0x26, // undocumented
//WacomGSS_ErrorCode_Crypt_engine_invalid        =  0x27, // undocumented

//WacomGSS_ErrorCode_Internal                    =  0x80 ~ 0xff
*/
};


struct tagWacomGSS_Status_v1
{
  uint8_t  statusCode;
  uint8_t  lastResultCode;
  uint16_t statusWord;
};
typedef struct tagWacomGSS_Status_v1 WacomGSS_Status;


enum tagWacomGSS_ResetFlag
{
  WacomGSS_ResetFlag_Software = 0x00,
  WacomGSS_ResetFlag_Hardware = 0x01
};


enum tagWacomGSS_InkingMode
{
  WacomGSS_InkingMode_Off = 0x00,
  WacomGSS_InkingMode_On  = 0x01
};

enum tagWacomGSS_PenDataOptionMode
{
  WacomGSS_PenDataOptionMode_None              = 0x00,
  WacomGSS_PenDataOptionMode_TimeCount         = 0x01,
  WacomGSS_PenDataOptionMode_SequenceNumber    = 0x02,
  WacomGSS_PenDataOptionMode_TimeCountSequence = 0x03
};

enum tagWacomGSS_EncodingFlag
{
  WacomGSS_EncodingFlag_Zlib  = 0x01,
  WacomGSS_EncodingFlag_1bit  = 0x02,
  WacomGSS_EncodingFlag_16bit = 0x04,
  WacomGSS_EncodingFlag_24bit = 0x08
};

enum tagWacomGSS_EncodingMode
{
  WacomGSS_EncodingMode_1bit       = 0x00,
  WacomGSS_EncodingMode_1bit_Zlib  = 0x01,
  WacomGSS_EncodingMode_16bit      = 0x02,
  WacomGSS_EncodingMode_24bit      = 0x04,
  WacomGSS_EncodingMode_1bit_Bulk  = 0x10,
  WacomGSS_EncodingMode_16bit_Bulk = 0x12,
  WacomGSS_EncodingMode_24bit_Bulk = 0x14,

  /* deprecated */
  WacomGSS_EncodingMode_Raw       = 0x00,
  WacomGSS_EncodingMode_Zlib      = 0x01,
  WacomGSS_EncodingMode_Bulk      = 0x10,
  WacomGSS_EncodingMode_16bit_565 = 0x02
};

enum tagWacomGSS_EndImageDataFlag
{
  WacomGSS_EndImageDataFlag_Commit  = 0x00,
  WacomGSS_EndImageDataFlag_Abandon = 0x01
};

enum tagWacomGSS_StatusCodeRSA
{
  WacomGSS_StatusCodeRSA_Ready       = 0x00,
  WacomGSS_StatusCodeRSA_Calculating = 0xFA,
  WacomGSS_StatusCodeRSA_Even        = 0xFB,
  WacomGSS_StatusCodeRSA_Long        = 0xFC,
  WacomGSS_StatusCodeRSA_Short       = 0xFD,
  WacomGSS_StatusCodeRSA_Invalid     = 0xFE,
  WacomGSS_StatusCodeRSA_NotReady    = 0xFF
};

enum tagWacomGSS_ErrorCodeRSA
{
  WacomGSS_ErrorCodeRSA_None                   = 0x00,
  WacomGSS_ErrorCodeRSA_BadParameter           = 0x01,
  WacomGSS_ErrorCodeRSA_ParameterTooLong       = 0x02,
  WacomGSS_ErrorCodeRSA_PublicKeyNotReady      = 0x03,
  WacomGSS_ErrorCodeRSA_PublicExponentNotReady = 0x04,
  WacomGSS_ErrorCodeRSA_SpecifiedKeyInUse      = 0x05,
  WacomGSS_ErrorCodeRSA_SpecifiedKeyNotInUse   = 0x06,
  WacomGSS_ErrorCodeRSA_BadCommandCode         = 0x07,
  WacomGSS_ErrorCodeRSA_CommandPending         = 0x08,
  WacomGSS_ErrorCodeRSA_SpecifiedKeyExists     = 0x09,
  WacomGSS_ErrorCodeRSA_SpecifiedKeyNotExist   = 0x0A,
  WacomGSS_ErrorCodeRSA_NotInitialized         = 0x0B
};

enum tagWacomGSS_SymmetricKeyType
{
  WacomGSS_SymmetricKeyType_AES128 = 0,
  WacomGSS_SymmetricKeyType_AES192 = 1,
  WacomGSS_SymmetricKeyType_AES256 = 2
};
typedef enum tagWacomGSS_SymmetricKeyType WacomGSS_SymmetricKeyType;

enum tagWacomGSS_AsymmetricKeyType
{
  WacomGSS_AsymmetricKeyType_RSA1024 = 0,
  WacomGSS_AsymmetricKeyType_RSA1536 = 1,
  WacomGSS_AsymmetricKeyType_RSA2048 = 2
};
typedef enum tagWacomGSS_AsymmetricKeyType WacomGSS_AsymmetricKeyType;

enum tagWacomGSS_AsymmetricPaddingType
{
  WacomGSS_AsymmetricPaddingType_None  = 0,
  WacomGSS_AsymmetricPaddingType_PKCS1 = 1,
  WacomGSS_AsymmetricPaddingType_OAEP  = 2
};
typedef enum tagWacomGSS_AsymmetricPaddingType WacomGSS_AsymmetricPaddingType;

/// @brief  Values for encryption commands
enum tagWacomGSS_EncryptionCommandNumber
{
  WacomGSS_EncryptionCommandNumber_SetEncryptionType    = 0x01,
  WacomGSS_EncryptionCommandNumber_SetParameterBlock    = 0x02,
  WacomGSS_EncryptionCommandNumber_GetStatusBlock       = 0x03,
  WacomGSS_EncryptionCommandNumber_GetParameterBlock    = 0x04,
  WacomGSS_EncryptionCommandNumber_GenerateSymmetricKey = 0x05
/*WacomGSS_EncryptionCommandNumber_Reserved_06          = 0x06,
  WacomGSS_EncryptionCommandNumber_Reserved_07          = 0x07,
  WacomGSS_EncryptionCommandNumber_Reserved_08          = 0x08,
  WacomGSS_EncryptionCommandNumber_Reserved_09          = 0x09*/
};

enum tagWacomGSS_EncryptionCommandParameterBlockIndex
{
  WacomGSS_EncryptionCommandParameterBlockIndex_RSAe = 0,
  WacomGSS_EncryptionCommandParameterBlockIndex_RSAn = 1,
  WacomGSS_EncryptionCommandParameterBlockIndex_RSAc = 2,
  WacomGSS_EncryptionCommandParameterBlockIndex_RSAm = 3
};


struct tagWacomGSS_Information_v1
{
  char    modelNameNullTerminated[9+1];
  uint8_t firmwareMajorVersion;
  uint8_t firmwareMinorVersion;
  uint8_t secureIc;
  uint8_t secureIcVersion[4];
};
typedef struct tagWacomGSS_Information_v1 WacomGSS_Information;

struct tagWacomGSS_Uid2_v1
{
  char uid2NullTerminated[10+1];
};
typedef struct tagWacomGSS_Uid2_v1 WacomGSS_Uid2;

struct tagWacomGSS_Capability_v1
{
  uint16_t tabletMaxX;
  uint16_t tabletMaxY;
  uint16_t tabletMaxPressure;
  uint16_t screenWidth;
  uint16_t screenHeight;
  uint8_t  maxReportRate;
  // pad[1]
  uint16_t resolution;
  uint8_t  zlibColorSupport; /* deprecated */
  // ?pad[1]
};

struct tagWacomGSS_Capability_v2
{
  uint16_t tabletMaxX;
  uint16_t tabletMaxY;
  uint16_t tabletMaxPressure;
  uint16_t screenWidth;
  uint16_t screenHeight;
  uint8_t  maxReportRate;
  // pad[1]
  uint16_t resolution;
  uint8_t  encodingFlag;
  // ?pad[1]
};

typedef struct tagWacomGSS_Capability_v2 WacomGSS_Capability;


struct tagWacomGSS_PublicKey_v1
{
  uint8_t publicKey[16];
};
typedef struct tagWacomGSS_PublicKey_v1 WacomGSS_PublicKey;


struct tagWacomGSS_DHprime_v1
{
  uint8_t dhPrime[16];
};
typedef struct tagWacomGSS_DHprime_v1 WacomGSS_DHprime;


struct tagWacomGSS_DHbase_v1
{
  uint8_t dhBase[2];
};
typedef struct tagWacomGSS_DHbase_v1 WacomGSS_DHbase;


struct tagWacomGSS_InkThreshold_v1
{
  uint16_t onPressureMark;
  uint16_t offPressureMark;
};
typedef struct tagWacomGSS_InkThreshold_v1 WacomGSS_InkThreshold;


struct tagWacomGSS_HandwritingThicknessColor_v1
{
  uint16_t penColor;
  uint8_t  penThickness;
  // ?pad[1]
};
typedef struct tagWacomGSS_HandwritingThicknessColor_v1 WacomGSS_HandwritingThicknessColor;


struct tagWacomGSS_HandwritingThicknessColor24_v1
{
  uint32_t penColor;
  uint8_t  penThickness;
  // ?pad[1]
};
typedef struct tagWacomGSS_HandwritingThicknessColor24_v1 WacomGSS_HandwritingThicknessColor24;


struct tagWacomGSS_Rectangle_v1
{
  uint16_t upperLeftXpixel;
  uint16_t upperLeftYpixel;
  uint16_t lowerRightXpixel;
  uint16_t lowerRightYpixel;
};
typedef struct tagWacomGSS_Rectangle_v1 WacomGSS_Rectangle;

#define tagWacomGSS_HandwritingDisplayArea_v1 tagWacomGSS_Rectangle_v1
#define    WacomGSS_HandwritingDisplayArea       WacomGSS_Rectangle


struct tagWacomGSS_PenData_v1
{
  uint8_t  rdy;
  uint8_t  sw;
  uint16_t pressure;
  uint16_t x;
  uint16_t y;
};
typedef struct tagWacomGSS_PenData_v1 WacomGSS_PenData;


struct tagWacomGSS_PenDataOption_v1
{
  uint8_t  rdy;
  uint8_t  sw;
  uint16_t pressure;
  uint16_t x;
  uint16_t y;
  uint16_t option;
};
typedef struct tagWacomGSS_PenDataOption_v1 WacomGSS_PenDataOption;


struct tagWacomGSS_PenDataEncrypted_v1
{
  uint32_t                      sessionId;
  struct tagWacomGSS_PenData_v1 penData[2];
};
typedef struct tagWacomGSS_PenDataEncrypted_v1 WacomGSS_PenDataEncrypted;


struct tagWacomGSS_PenDataEncryptedOption_v1
{
  uint32_t                      sessionId;
  struct tagWacomGSS_PenData_v1 penData[2];
  uint16_t                      option[2];
};
typedef struct tagWacomGSS_PenDataEncryptedOption_v1 WacomGSS_PenDataEncryptedOption;


struct tagWacomGSS_PenDataTimeCountSequence_v1
{
  uint8_t  rdy;
  uint8_t  sw;
  uint16_t pressure;
  uint16_t x;
  uint16_t y;
  uint16_t timeCount;
  uint16_t sequence;
};
typedef struct tagWacomGSS_PenDataTimeCountSequence_v1 WacomGSS_PenDataTimeCountSequence;

struct tagWacomGSS_PenDataTimeCountSequenceEncrypted_v1
{
  uint8_t  rdy;
  uint8_t  sw;
  uint16_t pressure;
  uint16_t x;
  uint16_t y;
  uint16_t timeCount;
  uint16_t sequence;
  uint32_t sessionId;
};
typedef struct tagWacomGSS_PenDataTimeCountSequenceEncrypted_v1 WacomGSS_PenDataTimeCountSequenceEncrypted;


struct tagWacomGSS_EncryptionStatus_v1
{
  uint8_t       symmetricKeyType;
  uint8_t       asymmetricPaddingType;
  uint8_t       asymmetricKeyType;
  uint8_t       statusCodeRSAe; ///< status of public exponent
  uint8_t       statusCodeRSAn; ///< status of host public key
  uint8_t       statusCodeRSAc; ///< status of symmetric key
  uint8_t       lastResultCode; ///< ErrorCodeRSA
  WacomGSS_bool rng;
  WacomGSS_bool sha1;
  WacomGSS_bool aes;
};
typedef struct tagWacomGSS_EncryptionStatus_v1 WacomGSS_EncryptionStatus;


struct tagWacomGSS_EncryptionCommand_v1
{
  uint8_t encryptionCommandNumber;
  uint8_t parameter;
  uint8_t lengthOrIndex;
  uint8_t data[64];
};
typedef struct tagWacomGSS_EncryptionCommand_v1 WacomGSS_EncryptionCommand;

struct tagWacomGSS_ReportHandlerFunctionTable_v1
{
  int (WacomGSS_DECL * onPenData                ) (void * reportHandler, size_t sizeofPenData, WacomGSS_PenData const * penData);
  int (WacomGSS_DECL * onPenDataOption          ) (void * reportHandler, size_t sizeofPenDataOption, WacomGSS_PenDataOption const * penDataOption);
  int (WacomGSS_DECL * onPenDataEncrypted       ) (void * reportHandler, size_t sizeofPenDataEncrypted, WacomGSS_PenDataEncrypted const * penDataEncrypted);
  int (WacomGSS_DECL * onPenDataEncryptedOption ) (void * reportHandler, size_t sizeofPenDataEncryptedOption, WacomGSS_PenDataEncryptedOption const * penDataEncryptedOption);
  int (WacomGSS_DECL * onDevicePublicKey        ) (void * reportHandler, size_t sizeofDevicePublicKey, WacomGSS_PublicKey const * devicePublicKey);
  int (WacomGSS_DECL * decrypt                  ) (void * reportHandler, uint8_t data[16]);
};

struct tagWacomGSS_ReportHandlerFunctionTable_v2
{
  int (WacomGSS_DECL * onPenData                           ) (void * reportHandler, size_t sizeofPenData, WacomGSS_PenData const * penData);
  int (WacomGSS_DECL * onPenDataOption                     ) (void * reportHandler, size_t sizeofPenDataOption, WacomGSS_PenDataOption const * penDataOption);
  int (WacomGSS_DECL * onPenDataEncrypted                  ) (void * reportHandler, size_t sizeofPenDataEncrypted, WacomGSS_PenDataEncrypted const * penDataEncrypted);
  int (WacomGSS_DECL * onPenDataEncryptedOption            ) (void * reportHandler, size_t sizeofPenDataEncryptedOption, WacomGSS_PenDataEncryptedOption const * penDataEncryptedOption);
  int (WacomGSS_DECL * onDevicePublicKey                   ) (void * reportHandler, size_t sizeofDevicePublicKey, WacomGSS_PublicKey const * devicePublicKey);
  int (WacomGSS_DECL * decrypt                             ) (void * reportHandler, uint8_t data[16]);

  int (WacomGSS_DECL * onPenDataTimeCountSequence          ) (void * reportHandler, size_t sizeofPenDataTimeCountSequence, WacomGSS_PenDataTimeCountSequence const * penDataTimeCountSequence);
  int (WacomGSS_DECL * onPenDataTimeCountSequenceEncrypted ) (void * reportHandler, size_t sizeofPenDataTimeCountSequenceEncrypted, WacomGSS_PenDataTimeCountSequenceEncrypted const * penDataTimeCountSequenceEncrypted);
  int (WacomGSS_DECL * onEncryptionStatus                  ) (void * reportHandler, size_t sizeofEncryptionStatus, WacomGSS_EncryptionStatus const * encryptionStatus);
};
typedef struct tagWacomGSS_ReportHandlerFunctionTable_v2 WacomGSS_ReportHandlerFunctionTable;


enum tagWacomGSS_Scale
{
  WacomGSS_Scale_Stretch,
  WacomGSS_Scale_Fit,
  WacomGSS_Scale_Clip
};
typedef enum tagWacomGSS_Scale WacomGSS_Scale;

enum tagWacomGSS_Border
{
  WacomGSS_Border_Black,
  WacomGSS_Border_White
};
typedef enum tagWacomGSS_Border WacomGSS_Border;

enum tagWacomGSS_Clip
{
  WacomGSS_Clip_Left    = 0x00,
  WacomGSS_Clip_Center  = 0x01,
  WacomGSS_Clip_Right   = 0x02,

  WacomGSS_Clip_Top     = 0x00,
  WacomGSS_Clip_Middle  = 0x10,
  WacomGSS_Clip_Bottom  = 0x20
};

enum tagWacomGSS_OpDirection
{
  WacomGSS_OpDirection_Get = 0x0100,
  WacomGSS_OpDirection_Set = 0x0200
};

struct tagWacomGSS_TabletEncryptionHandlerFunctionTable_v1
{
  void (WacomGSS_DECL * free ) (void * tabletEncryptionHandler);
  
  int  (WacomGSS_DECL * reset                 ) (void * tabletEncryptionHandler);
  int  (WacomGSS_DECL * clearKeys             ) (void * tabletEncryptionHandler);
  int  (WacomGSS_DECL * requireDH             ) (void * tabletEncryptionHandler, WacomGSS_bool * ret);
  int  (WacomGSS_DECL * setDH                 ) (void * tabletEncryptionHandler, size_t sizeofDHprime, WacomGSS_DHprime const * dhPrime, size_t sizeofDHbase, WacomGSS_DHbase const * dhBase);
  int  (WacomGSS_DECL * generateHostPublicKey ) (void * tabletEncryptionHandler, size_t sizeofHostPublicKey, WacomGSS_PublicKey * hostPublicKey);
  int  (WacomGSS_DECL * computeSharedKey      ) (void * tabletEncryptionHandler, size_t sizeofDevicePublicKey, WacomGSS_PublicKey const * devicePublicKey);
  int  (WacomGSS_DECL * decrypt               ) (void * tabletEncryptionHandler, uint8_t data[16]);
};
typedef struct tagWacomGSS_TabletEncryptionHandlerFunctionTable_v1 WacomGSS_TabletEncryptionHandlerFunctionTable;


struct tagWacomGSS_TabletEncryptionHandlerFunctionTable2_v1
{
  void (WacomGSS_DECL * free       ) (void * tabletEncryptionHandler2);
  void (WacomGSS_DECL * freeBuffer ) (void * memory); // required for getPublicExponent and generatePublicKey

  int  (WacomGSS_DECL * reset                 ) (void * tabletEncryptionHandler2);
  int  (WacomGSS_DECL * clearKeys             ) (void * tabletEncryptionHandler2);
  int  (WacomGSS_DECL * getParameters         ) (void * tabletEncryptionHandler2, WacomGSS_SymmetricKeyType * symmetricKeyType, WacomGSS_AsymmetricPaddingType * asymmetricPaddingType, WacomGSS_AsymmetricKeyType * asymmetricKeyType);
  int  (WacomGSS_DECL * getPublicExponent     ) (void * tabletEncryptionHandler2, size_t * length, uint8_t * * publicExponent);
  int  (WacomGSS_DECL * generatePublicKey     ) (void * tabletEncryptionHandler2, size_t * length, uint8_t * * publicKey);
  int  (WacomGSS_DECL * computeSessionKey     ) (void * tabletEncryptionHandler2, size_t length, uint8_t * data);
  int  (WacomGSS_DECL * decrypt               ) (void * tabletEncryptionHandler2, uint8_t data[16]);
};
typedef struct tagWacomGSS_TabletEncryptionHandlerFunctionTable2_v1 WacomGSS_TabletEncryptionHandlerFunctionTable2;



#define WacomGSS_All(X) X
#if defined(WacomGSS_WIN32)
#define WacomGSS_W32(X) X
#else
#define WacomGSS_W32(X)
#endif
#if defined(WacomGSS_Linux)
#define WacomGSS_Lnx(X) X
#else
#define WacomGSS_Lnx(X)
#endif


#define WacomGSS_FunctionList_v1(Function)\
  Function(WacomGSS_All, int, getVersion,   (int * majorVersion, int * minorVersion)                        )\
  Function(WacomGSS_All, int, free,         (void * memory)                                                 )\
  Function(WacomGSS_All, int, getException, (int * exceptionCode, size_t * messageLength, char * * message) )\
\
  Function(WacomGSS_All, int, componentFiles,        (size_t sizeofComponentFile, size_t * count, WacomGSS_ComponentFile * * ret)    )\
  Function(WacomGSS_All, int, diagnosticInformation, (int flag, size_t * length, char * * ret) )\
\
  Function(WacomGSS_All, int, getUsbDevices,    (size_t sizeofUsbDevice, size_t * count, WacomGSS_UsbDevice * * ret)    )\
  Function(WacomGSS_W32, int, getUsbDevices_wc, (size_t sizeofUsbDevice, size_t * count, WacomGSS_UsbDevice_wc * * ret) )\
\
  Function(WacomGSS_All, int, InterfaceQueue_free,                    (WacomGSS_InterfaceQueue handle)                         )\
  Function(WacomGSS_All, int, InterfaceQueue_clear,                   (WacomGSS_InterfaceQueue handle)                         )\
  Function(WacomGSS_All, int, InterfaceQueue_empty,                   (WacomGSS_InterfaceQueue handle, WacomGSS_bool * ret)    )\
  Function(WacomGSS_All, int, InterfaceQueue_notfiy_one,              (WacomGSS_InterfaceQueue handle)                         )\
  Function(WacomGSS_All, int, InterfaceQueue_notfiy_all,              (WacomGSS_InterfaceQueue handle)                         )\
  Function(WacomGSS_All, int, InterfaceQueue_try_getReport,           (WacomGSS_InterfaceQueue handle, uint8_t * * report, size_t * length, WacomGSS_bool * ret) )\
  Function(WacomGSS_All, int, InterfaceQueue_wait_getReport,          (WacomGSS_InterfaceQueue handle, uint8_t * * report, size_t * length)                      )\
  Function(WacomGSS_All, int, InterfaceQueue_wait_getReportPredicate, (WacomGSS_InterfaceQueue handle, void * predicateData, WacomGSS_bool (WacomGSS_DECL * predicate)(void *), uint8_t * * report, size_t * length, WacomGSS_bool * ret) )\
\
  Function(WacomGSS_All, int, Interface_free,                  (WacomGSS_Interface handle)                                                  )\
  Function(WacomGSS_All, int, Interface_disconnect,            (WacomGSS_Interface handle)                                                  )\
  Function(WacomGSS_All, int, Interface_isConnected,           (WacomGSS_Interface handle, WacomGSS_bool * ret)                             )\
  Function(WacomGSS_All, int, Interface_interfaceQueue,        (WacomGSS_Interface handle, WacomGSS_InterfaceQueue * ret)                   )\
  Function(WacomGSS_All, int, Interface_queueNotifyAll,        (WacomGSS_Interface handle)                                                  )\
  Function(WacomGSS_All, int, Interface_get,                   (WacomGSS_Interface handle, uint8_t * data, size_t length)                   )\
  Function(WacomGSS_All, int, Interface_set,                   (WacomGSS_Interface handle, uint8_t const * data, size_t length)             )\
  Function(WacomGSS_All, int, Interface_supportsWrite,         (WacomGSS_Interface handle, WacomGSS_bool * ret)                             )\
  Function(WacomGSS_All, int, Interface_write,                 (WacomGSS_Interface handle, uint8_t const * data, size_t length)             )\
  Function(WacomGSS_All, int, Interface_getReportCountLengths, (WacomGSS_Interface handle, size_t * count, uint16_t * * reportCountLengths) )\
\
  Function(WacomGSS_All, int, UsbInterface_create_1,    (size_t sizeofUsbDevice, WacomGSS_UsbDevice const * usbDevice, WacomGSS_bool exclusiveLock, WacomGSS_Interface * handle)    )\
  Function(WacomGSS_W32, int, UsbInterface_create_1_wc, (size_t sizeofUsbDevice, WacomGSS_UsbDevice_wc const * usbDevice, WacomGSS_bool exclusiveLock, WacomGSS_Interface * handle) )\
  Function(WacomGSS_W32, int, UsbInterface_create_2,    (char const * fileName, char const * bulkFileName, WacomGSS_bool exclusiveLock, WacomGSS_Interface * handle)                )\
  Function(WacomGSS_W32, int, UsbInterface_create_2_wc, (wchar_t const * fileName, wchar_t const * bulkFileName, WacomGSS_bool exclusiveLock, WacomGSS_Interface * handle)          )\
\
  Function(WacomGSS_All, int, SerialInterface_create,    (char const * fileName, WacomGSS_bool useCrc, WacomGSS_Interface * handle)    )\
  Function(WacomGSS_W32, int, SerialInterface_create_wc, (wchar_t const * fileName, WacomGSS_bool useCrc, WacomGSS_Interface * handle) )\
\
  Function(WacomGSS_All, int, Protocol_getStatus,                     (WacomGSS_Interface handle, size_t sizeofStatus, WacomGSS_Status * * ret)                                                                 )\
  Function(WacomGSS_All, int, Protocol_setReset,                      (WacomGSS_Interface handle, uint8_t resetFlag)                                                                                            )\
  Function(WacomGSS_All, int, Protocol_getInformation,                (WacomGSS_Interface handle, size_t sizeofInformation, WacomGSS_Information * * ret)                                                       )\
  Function(WacomGSS_All, int, Protocol_getCapability,                 (WacomGSS_Interface handle, size_t sizeofCapability, WacomGSS_Capability * * ret)                                                         )\
  Function(WacomGSS_All, int, Protocol_getUid,                        (WacomGSS_Interface handle, uint32_t * ret)                                                                                               )\
  Function(WacomGSS_All, int, Protocol_setUid,                        (WacomGSS_Interface handle, uint32_t uid)                                                                                                 )\
  Function(WacomGSS_All, int, Protocol_getHostPublicKey,              (WacomGSS_Interface handle, size_t sizeofKey, WacomGSS_PublicKey * * ret)                                                                 )\
  Function(WacomGSS_All, int, Protocol_setHostPublicKey,              (WacomGSS_Interface handle, size_t sizeofKey, WacomGSS_PublicKey const * key)                                                             )\
  Function(WacomGSS_All, int, Protocol_getDevicePublicKey,            (WacomGSS_Interface handle, size_t sizeofKey, WacomGSS_PublicKey * * ret)                                                                 )\
  Function(WacomGSS_All, int, Protocol_setStartCapture,               (WacomGSS_Interface handle, uint32_t sessionId)                                                                                           )\
  Function(WacomGSS_All, int, Protocol_setEndCapture,                 (WacomGSS_Interface handle)                                                                                                               )\
  Function(WacomGSS_All, int, Protocol_getDHprime,                    (WacomGSS_Interface handle, size_t sizeofValue, WacomGSS_DHprime * * ret)                                                                 )\
  Function(WacomGSS_All, int, Protocol_setDHprime,                    (WacomGSS_Interface handle, size_t sizeofValue, WacomGSS_DHprime const * value)                                                           )\
  Function(WacomGSS_All, int, Protocol_getDHbase,                     (WacomGSS_Interface handle, size_t sizeofValue, WacomGSS_DHbase * * ret)                                                                  )\
  Function(WacomGSS_All, int, Protocol_setDHbase,                     (WacomGSS_Interface handle, size_t sizeofValue, WacomGSS_DHbase const * value)                                                            )\
  Function(WacomGSS_All, int, Protocol_setClearScreen,                (WacomGSS_Interface handle)                                                                                                               )\
  Function(WacomGSS_All, int, Protocol_getInkingMode,                 (WacomGSS_Interface handle, uint8_t * inkingMode)                                                                                         )\
  Function(WacomGSS_All, int, Protocol_setInkingMode,                 (WacomGSS_Interface handle, uint8_t inkingMode)                                                                                           )\
  Function(WacomGSS_All, int, Protocol_getInkThreshold,               (WacomGSS_Interface handle, size_t sizeofInkThreshold, WacomGSS_InkThreshold * * ret)                                                     )\
  Function(WacomGSS_All, int, Protocol_setInkThreshold,               (WacomGSS_Interface handle, size_t sizeofInkThreshold, WacomGSS_InkThreshold const * inkThreshold)                                        )\
  Function(WacomGSS_All, int, Protocol_setStartImageData,             (WacomGSS_Interface handle, uint8_t encodingMode)                                                                                         )\
  Function(WacomGSS_All, int, Protocol_setImageDataBlock,             (WacomGSS_Interface handle, uint8_t const * imageDataBlock, uint8_t length)                                                               )\
  Function(WacomGSS_All, int, Protocol_setEndImageData,               (WacomGSS_Interface handle)                                                                                                               )\
  Function(WacomGSS_All, int, Protocol_getHandwritingThicknessColor,  (WacomGSS_Interface handle, size_t sizeofHandwritingThicknessColor, WacomGSS_HandwritingThicknessColor * * ret)                           )\
  Function(WacomGSS_All, int, Protocol_setHandwritingThicknessColor,  (WacomGSS_Interface handle, size_t sizeofHandwritingThicknessColor, WacomGSS_HandwritingThicknessColor const * handwritingThicknessColor) )\
  Function(WacomGSS_All, int, Protocol_getBackgroundColor,            (WacomGSS_Interface handle, uint16_t * backgroundColor)                                                                                   )\
  Function(WacomGSS_All, int, Protocol_setBackgroundColor,            (WacomGSS_Interface handle, uint16_t backgroundColor)                                                                                     )\
  Function(WacomGSS_All, int, Protocol_getHandwritingDisplayArea,     (WacomGSS_Interface handle, size_t sizeofRectangle, WacomGSS_Rectangle * * ret)                                                           )\
  Function(WacomGSS_All, int, Protocol_setHandwritingDisplayArea,     (WacomGSS_Interface handle, size_t sizeofRectangle, WacomGSS_Rectangle const * area)                                                      )\
  Function(WacomGSS_All, int, Protocol_getBacklightBrightness,        (WacomGSS_Interface handle, uint16_t * backlightBrightness)                                                                               )\
  Function(WacomGSS_All, int, Protocol_setBacklightBrightness,        (WacomGSS_Interface handle, uint16_t backlightBrightness)                                                                                 )\
  Function(WacomGSS_All, int, Protocol_getPenDataOptionMode,          (WacomGSS_Interface handle, uint8_t * penDataOptionMode)                                                                                  )\
  Function(WacomGSS_All, int, Protocol_setPenDataOptionMode,          (WacomGSS_Interface handle, uint8_t penDataOptionMode)                                                                                    )\
\
  Function(WacomGSS_All, int, ProtocolHelper_statusCanSend,                             (uint8_t statusCode, uint8_t reportId, int opDirection, WacomGSS_bool * ret)                                                                                                                                             )\
  Function(WacomGSS_All, int, ProtocolHelper_waitForStatusToSend,                       (WacomGSS_Interface handle, uint8_t reportId, int opDirection, uint32_t retries, uint32_t sleepBetweenRetries)                                                                                                           )\
  Function(WacomGSS_All, int, ProtocolHelper_waitForStatus,                             (WacomGSS_Interface handle, uint8_t statusCode, uint32_t retries, uint32_t sleepBetweenRetries)                                                                                                                          )\
  Function(WacomGSS_All, int, ProtocolHelper_supportsEncryption,                        (WacomGSS_Interface handle, WacomGSS_bool * ret)                                                                                                                                                                         )\
  Function(WacomGSS_All, int, ProtocolHelper_supportsEncryption_DHprime,                (size_t sizeofDHprime, WacomGSS_DHprime const * dhPrime, WacomGSS_bool * ret)                                                                                                                                            )\
  Function(WacomGSS_All, int, ProtocolHelper_setHostPublicKeyAndPollForDevicePublicKey, (WacomGSS_Interface handle, size_t sizeofHostPublicKey, WacomGSS_PublicKey const * hostPublicKey, uint32_t retries, uint32_t sleepBetweenRetries, size_t sizeofDevicePublicKey, WacomGSS_PublicKey * * devicePublicKey)  )\
  Function(WacomGSS_All, int, ProtocolHelper_writeImage,                                (WacomGSS_Interface handle, uint8_t encodingMode, uint8_t const * imageData, size_t imageLength, uint32_t retries, uint32_t sleepBetweenRetries)                                                                         )\
\
  Function(WacomGSS_W32, int, ProtocolHelper_flattenMonochrome_1,                       (IWICImagingFactory * pIWICImagingFactory, IWICBitmapSource * pIWICBitmapSource, uint16_t screenWidth, uint16_t screenHeight, uint8_t * * buffer, size_t * length)                            )\
  Function(WacomGSS_W32, int, ProtocolHelper_flattenColor16_565_1,                      (IWICImagingFactory * pIWICImagingFactory, IWICBitmapSource * pIWICBitmapSource, uint16_t screenWidth, uint16_t screenHeight, uint8_t * * buffer, size_t * length)                            )\
  Function(WacomGSS_W32, int, ProtocolHelper_resizeImageAndFlatten_1,                   (IWICImagingFactory * pIWICImagingFactory, IWICBitmapSource * pIWICBitmapSource, uint16_t screenWidth, uint16_t screenHeight, WacomGSS_bool color, WacomGSS_Scale scale, WacomGSS_Border border, uint8_t clipMode, uint8_t * * buffer, size_t * length) )\
\
  Function(WacomGSS_Lnx, int, ProtocolHelper_flattenMonochrome_1,              (char const * fileName, uint16_t screenWidth, uint16_t screenHeight, uint8_t * * buffer, size_t * length) )\
  Function(WacomGSS_Lnx, int, ProtocolHelper_flattenColor16_565_1,             (char const * fileName, uint16_t screenWidth, uint16_t screenHeight, uint8_t * * buffer, size_t * length) )\
  Function(WacomGSS_Lnx, int, ProtocolHelper_resizeImageAndFlatten_1,          (char const * fileName, uint16_t screenWidth, uint16_t screenHeight, WacomGSS_bool color, WacomGSS_Scale scale, WacomGSS_Border border, uint8_t clipMode, uint8_t * * buffer, size_t * length) )\
\
  Function(WacomGSS_All, int, ReportHandler_handleReport, (size_t sizeofReportHandlerFunctionTable, WacomGSS_ReportHandlerFunctionTable const * reportHandlerFunctionTable, void * reportHandler, uint8_t const * report, size_t length, uint8_t const * * ptr, WacomGSS_bool * ret ) )\
\
  Function(WacomGSS_All, int, Tablet_create_1, (WacomGSS_Tablet * handle)                                                                                                                                                                     )\
  Function(WacomGSS_All, int, Tablet_create_2, (size_t sizeofTabletEncryptionHandlerFunctionTable, WacomGSS_TabletEncryptionHandlerFunctionTable const * encryptionHandlerFunctionTablet, void * encryptionHandler, WacomGSS_Tablet * handle) )\
\
  Function(WacomGSS_All, int, Tablet_free,                          (WacomGSS_Tablet handle)                                                                                                                      )\
  Function(WacomGSS_All, int, Tablet_attach,                        (WacomGSS_Tablet handle, WacomGSS_Interface intf)                                                                                             )\
  Function(WacomGSS_All, int, Tablet_detach,                        (WacomGSS_Tablet handle, WacomGSS_Interface * ret)                                                                                            )\
  Function(WacomGSS_All, int, Tablet_empty,                         (WacomGSS_Tablet handle, WacomGSS_bool * ret)                                                                                                 )\
  Function(WacomGSS_All, int, Tablet_disconnect,                    (WacomGSS_Tablet handle)                                                                                                                      )\
  Function(WacomGSS_All, int, Tablet_isConnected,                   (WacomGSS_Tablet handle, WacomGSS_bool * ret)                                                                                                 )\
  Function(WacomGSS_All, int, Tablet_interfaceQueue,                (WacomGSS_Tablet handle, WacomGSS_InterfaceQueue * ret)                                                                                       )\
  Function(WacomGSS_All, int, Tablet_queueNotifyAll,                (WacomGSS_Tablet handle)                                                                                                                      )\
  Function(WacomGSS_All, int, Tablet_supportsWrite,                 (WacomGSS_Tablet handle, WacomGSS_bool * ret)                                                                                                 )\
  Function(WacomGSS_All, int, Tablet_getReportCountLengths,         (WacomGSS_Tablet handle, size_t * count, uint16_t * * reportCountLengths)                                                                     )\
  Function(WacomGSS_All, int, Tablet_isSupported,                   (WacomGSS_Tablet handle, uint8_t reportId, WacomGSS_bool * ret)                                                                               )\
\
  Function(WacomGSS_All, int, Tablet_getStatus,                     (WacomGSS_Tablet handle, size_t sizeofStatus, WacomGSS_Status * * ret)                                                                        )\
  Function(WacomGSS_All, int, Tablet_reset,                         (WacomGSS_Tablet handle)                                                                                                                      )\
  Function(WacomGSS_All, int, Tablet_getInformation,                (WacomGSS_Tablet handle, size_t sizeofInformation, WacomGSS_Information * * ret)                                                              )\
  Function(WacomGSS_All, int, Tablet_getCapability,                 (WacomGSS_Tablet handle, size_t sizeofCapability, WacomGSS_Capability * * ret)                                                                )\
  Function(WacomGSS_All, int, Tablet_getUid,                        (WacomGSS_Tablet handle, uint32_t * ret)                                                                                                      )\
  Function(WacomGSS_All, int, Tablet_setUid,                        (WacomGSS_Tablet handle, uint32_t uid)                                                                                                        )\
  Function(WacomGSS_All, int, Tablet_getHostPublicKey,              (WacomGSS_Tablet handle, size_t sizeofKey, WacomGSS_PublicKey * * ret)                                                                        )\
  Function(WacomGSS_All, int, Tablet_getDevicePublicKey,            (WacomGSS_Tablet handle, size_t sizeofKey, WacomGSS_PublicKey * * ret)                                                                        )\
  Function(WacomGSS_All, int, Tablet_startCapture,                  (WacomGSS_Tablet handle, uint32_t sessionId)                                                                                                  )\
  Function(WacomGSS_All, int, Tablet_endCapture,                    (WacomGSS_Tablet handle)                                                                                                                      )\
  Function(WacomGSS_All, int, Tablet_getDHprime,                    (WacomGSS_Tablet handle, size_t sizeofValue, WacomGSS_DHprime * * ret)                                                                        )\
  Function(WacomGSS_All, int, Tablet_setDHprime,                    (WacomGSS_Tablet handle, size_t sizeofValue, WacomGSS_DHprime const * value)                                                                  )\
  Function(WacomGSS_All, int, Tablet_getDHbase,                     (WacomGSS_Tablet handle, size_t sizeofValue, WacomGSS_DHbase * * ret)                                                                         )\
  Function(WacomGSS_All, int, Tablet_setDHbase,                     (WacomGSS_Tablet handle, size_t sizeofValue, WacomGSS_DHbase const * value)                                                                   )\
  Function(WacomGSS_All, int, Tablet_setClearScreen,                (WacomGSS_Tablet handle)                                                                                                                      )\
  Function(WacomGSS_All, int, Tablet_getInkingMode,                 (WacomGSS_Tablet handle, uint8_t * inkingMode)                                                                                                )\
  Function(WacomGSS_All, int, Tablet_setInkingMode,                 (WacomGSS_Tablet handle, uint8_t inkingMode)                                                                                                  )\
  Function(WacomGSS_All, int, Tablet_getInkThreshold,               (WacomGSS_Tablet handle, size_t sizeofInkThreshold, WacomGSS_InkThreshold * * ret)                                                            )\
  Function(WacomGSS_All, int, Tablet_setInkThreshold,               (WacomGSS_Tablet handle, size_t sizeofInkThreshold, WacomGSS_InkThreshold const * inkThreshold)                                               )\
  Function(WacomGSS_All, int, Tablet_writeImage,                    (WacomGSS_Tablet handle, uint8_t encodingMode, uint8_t const * imageData, size_t imageLength)                                                 )\
  Function(WacomGSS_All, int, Tablet_endImageData,                  (WacomGSS_Tablet handle)                                                                                                                      )\
  Function(WacomGSS_All, int, Tablet_getHandwritingThicknessColor,  (WacomGSS_Tablet handle, size_t sizeofHandwritingThicknessColor, WacomGSS_HandwritingThicknessColor * * ret)                                  )\
  Function(WacomGSS_All, int, Tablet_setHandwritingThicknessColor,  (WacomGSS_Tablet handle, size_t sizeofHandwritingThicknessColor, WacomGSS_HandwritingThicknessColor const * handwritingThicknessColor)        )\
  Function(WacomGSS_All, int, Tablet_getBackgroundColor,            (WacomGSS_Tablet handle, uint16_t * backgroundColor)                                                                                          )\
  Function(WacomGSS_All, int, Tablet_setBackgroundColor,            (WacomGSS_Tablet handle, uint16_t backgroundColor)                                                                                            )\
  Function(WacomGSS_All, int, Tablet_getHandwritingDisplayArea,     (WacomGSS_Tablet handle, size_t sizeofRectangle, WacomGSS_Rectangle * * ret)                                                                  )\
  Function(WacomGSS_All, int, Tablet_setHandwritingDisplayArea,     (WacomGSS_Tablet handle, size_t sizeofRectangle, WacomGSS_Rectangle const * area)                                                             )\
  Function(WacomGSS_All, int, Tablet_getBacklightBrightness,        (WacomGSS_Tablet handle, uint16_t * backlightBrightness)                                                                                      )\
  Function(WacomGSS_All, int, Tablet_setBacklightBrightness,        (WacomGSS_Tablet handle, uint16_t backlightBrightness)                                                                                        )\
  Function(WacomGSS_All, int, Tablet_getPenDataOptionMode,          (WacomGSS_Tablet handle, uint8_t * penDataOptionMode)                                                                                         )\
  Function(WacomGSS_All, int, Tablet_setPenDataOptionMode,          (WacomGSS_Tablet handle, uint8_t penDataOptionMode)                                                                                           )

#define WacomGSS_FunctionList_v2(Function)\
  Function(WacomGSS_All, int, Interface_getProductId,                          (WacomGSS_Interface handle, uint16_t * productId)                                                                                                        )\
\
  Function(WacomGSS_All, int, Protocol_getUid2,                                (WacomGSS_Interface handle, size_t sizeofUid2, WacomGSS_Uid2 * * ret)                                                                      )\
  Function(WacomGSS_All, int, Protocol_setClearScreenArea,                     (WacomGSS_Interface handle, size_t sizeofRectangle, WacomGSS_Rectangle const * area)                                                                     )\
  Function(WacomGSS_All, int, Protocol_setStartImageDataArea,                  (WacomGSS_Interface handle, uint8_t encodingMode, size_t sizeofRectangle, WacomGSS_Rectangle const * area)                                               )\
  Function(WacomGSS_All, int, Protocol_setEndImageData2,                       (WacomGSS_Interface handle, uint8_t endImageDataFlag)                                                                                                    )\
  Function(WacomGSS_All, int, Protocol_getHandwritingThicknessColor24,         (WacomGSS_Interface handle, size_t sizeofHandwritingThicknessColor24, WacomGSS_HandwritingThicknessColor24 * * ret)                                      )\
  Function(WacomGSS_All, int, Protocol_setHandwritingThicknessColor24,         (WacomGSS_Interface handle, size_t sizeofHandwritingThicknessColor24, WacomGSS_HandwritingThicknessColor24 const * handwritingThicknessColor24)          )\
  Function(WacomGSS_All, int, Protocol_getBackgroundColor24,                   (WacomGSS_Interface handle, uint32_t * backgroundColor24)                                                                                                   )\
  Function(WacomGSS_All, int, Protocol_setBackgroundColor24,                   (WacomGSS_Interface handle, uint32_t backgroundColor24)                                                                                                     )\
  Function(WacomGSS_All, int, Protocol_getScreenContrast,                      (WacomGSS_Interface handle, uint16_t * screenContrast)                                                                                                      )\
  Function(WacomGSS_All, int, Protocol_setScreenContrast,                      (WacomGSS_Interface handle, uint16_t screenContrast)                                                                                                        )\
  Function(WacomGSS_All, int, Protocol_getEncryptionStatus,                    (WacomGSS_Interface handle, size_t sizeofEncryptionStatus, WacomGSS_EncryptionStatus * * encryptionStatus)                                               )\
  Function(WacomGSS_All, int, Protocol_getEncryptionCommand,                   (WacomGSS_Interface handle, uint8_t encryptionCommandNumber, size_t sizeofEncryptionCommand, WacomGSS_EncryptionCommand * * ret)                         )\
  Function(WacomGSS_All, int, Protocol_setEncryptionCommand,                   (WacomGSS_Interface handle, size_t sizeofEncryptionCommand, WacomGSS_EncryptionCommand const * encryptionCommand)                                        )\
\
  Function(WacomGSS_All, int, EncryptionCommand_initializeSetEncryptionType,    (uint8_t symmetricKeyType, uint8_t asymmetricPaddingType, uint8_t asymmetricKeyType, size_t sizeofEncryptionCommand, WacomGSS_EncryptionCommand * * ret) )\
  Function(WacomGSS_All, int, EncryptionCommand_initializeSetParameterBlock,    (uint8_t index, uint8_t length, uint8_t const * data, size_t sizeofEncryptionCommand, WacomGSS_EncryptionCommand * * ret)                                )\
  Function(WacomGSS_All, int, EncryptionCommand_initializeGenerateSymmetricKey, (size_t sizeofEncryptionCommand, WacomGSS_EncryptionCommand * * ret)                                                                                     )\
  Function(WacomGSS_All, int, EncryptionCommand_initializeGetParameterBlock,    (uint8_t index, uint8_t offset, size_t sizeofEncryptionCommand, WacomGSS_EncryptionCommand * * ret)                                                      )\
\
  Function(WacomGSS_All, int, ProtocolHelper_generateSymmetricKeyAndWaitForEncryptionStatus, (WacomGSS_Interface handle, uint32_t retries, uint32_t sleepBetweenRetries, uint32_t timeout, size_t sizeofEncryptionStatus, WacomGSS_EncryptionStatus * * encryptionStatus)      )\
  Function(WacomGSS_All, int, ProtocolHelper_simulateEncodingFlag,                           (uint16_t idProduct, uint8_t encodingFlag, uint8_t * ret)                                                                                                                         )\
  Function(WacomGSS_All, int, ProtocolHelper_encodingFlagSupportsColor,                      (uint8_t encodingFlag, WacomGSS_bool * ret)                                                                                                                                       )\
  Function(WacomGSS_All, int, ProtocolHelper_writeImageArea,                                 (WacomGSS_Interface handle, uint8_t encodingMode, size_t sizeofRectangle, WacomGSS_Rectangle const * area, uint8_t const * imageData, size_t imageLength, uint32_t retries, uint32_t sleepBetweenRetries) )\
\
  Function(WacomGSS_All, int, Tablet_create_3,                               (size_t sizeofTabletEncryptionHandlerFunctionTable, WacomGSS_TabletEncryptionHandlerFunctionTable const * encryptionHandlerFunctionTablet, void * encryptionHandler, size_t sizeofTabletEncryptionHandlerFunctionTable2, WacomGSS_TabletEncryptionHandlerFunctionTable2 const * encryptionHandlerFunctionTablet2, void * encryptionHandler2, WacomGSS_Tablet * handle) )\
  Function(WacomGSS_All, int, Tablet_getProductId,                           (WacomGSS_Tablet handle, uint16_t * productId)                                                                                                        )\
  Function(WacomGSS_All, int, Tablet_getUid2,                                (WacomGSS_Tablet handle, size_t sizeofUid2, WacomGSS_Uid2 * * ret)                                                                                    )\
  Function(WacomGSS_All, int, Tablet_setClearScreenArea,                     (WacomGSS_Tablet handle, size_t sizeofRectangle, WacomGSS_Rectangle const * area)                                                                     )\
  Function(WacomGSS_All, int, Tablet_getHandwritingThicknessColor24,         (WacomGSS_Tablet handle, size_t sizeofHandwritingThicknessColor24, WacomGSS_HandwritingThicknessColor24 * * ret)                                      )\
  Function(WacomGSS_All, int, Tablet_setHandwritingThicknessColor24,         (WacomGSS_Tablet handle, size_t sizeofHandwritingThicknessColor24, WacomGSS_HandwritingThicknessColor24 const * handwritingThicknessColor24)          )\
  Function(WacomGSS_All, int, Tablet_getBackgroundColor24,                   (WacomGSS_Tablet handle, uint32_t * backgroundColor24)                                                                                                )\
  Function(WacomGSS_All, int, Tablet_setBackgroundColor24,                   (WacomGSS_Tablet handle, uint32_t backgroundColor24)                                                                                                  )\
  Function(WacomGSS_All, int, Tablet_getScreenContrast,                      (WacomGSS_Tablet handle, uint16_t * screenContrast)                                                                                                   )\
  Function(WacomGSS_All, int, Tablet_setScreenContrast,                      (WacomGSS_Tablet handle, uint16_t screenContrast)                                                                                                     )\
  Function(WacomGSS_All, int, Tablet_getEncryptionStatus,                    (WacomGSS_Tablet handle, size_t sizeofEncryptionStatus, WacomGSS_EncryptionStatus * * encryptionStatus)                                               )\




#define WacomGSS_FunctionList_struct(Filter, Return, Name, Signature) Filter( Return (WacomGSS_DECL * Name) Signature ; )

struct tagWacomGSS_FunctionTable_v1
{
  WacomGSS_FunctionList_v1(WacomGSS_FunctionList_struct)
};
struct tagWacomGSS_FunctionTable_v2
{
  WacomGSS_FunctionList_v1(WacomGSS_FunctionList_struct)
  WacomGSS_FunctionList_v2(WacomGSS_FunctionList_struct)
};
typedef struct tagWacomGSS_FunctionTable_v2 WacomGSS_FunctionTable;


#if !defined(WacomGSS_NoFunctionDecl)
#define WacomGSS_FunctionList_function_declaration(Filter, Return, Name, Signature) Filter( WacomGSS_EXPORT Return WacomGSS_DECL WacomGSS_##Name Signature ; )
WacomGSS_FunctionList_v1(WacomGSS_FunctionList_function_declaration)
WacomGSS_FunctionList_v2(WacomGSS_FunctionList_function_declaration)
WacomGSS_EXPORT int WacomGSS_DECL WacomGSS_getFunctionTable(size_t sizeofFunctionTable, WacomGSS_FunctionTable * * ret);
#endif

#define WacomGSS_FunctionList_function_pointer_typedef(Filter, Return, Name, Signature) Filter( typedef Return (WacomGSS_DECL * WacomGSS_##Name##_fn) Signature ; )
WacomGSS_FunctionList_v1(WacomGSS_FunctionList_function_pointer_typedef)
WacomGSS_FunctionList_v2(WacomGSS_FunctionList_function_pointer_typedef)
typedef int (WacomGSS_DECL * WacomGSS_getFunctionTable_fn)(size_t sizeofFunctionTable, WacomGSS_FunctionTable * * ret);


#ifdef __cplusplus
}
#endif

#endif
