var dir_1d0c055ca3c373b3eef70b1ace60313d =
[
    [ "Win32", "dir_180a568a7dbedfaa556a79ff8346e369.html", "dir_180a568a7dbedfaa556a79ff8346e369" ],
    [ "Error.hpp", "_error_8hpp.html", [
      [ "runtime_error", "class_wacom_g_s_s_1_1_s_t_u_1_1runtime__error.html", null ]
    ] ],
    [ "getUsbDevices.hpp", "get_usb_devices_8hpp.html", "get_usb_devices_8hpp" ],
    [ "Interface.hpp", "_interface_8hpp.html", [
      [ "Interface", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface" ],
      [ "not_connected_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1not__connected__error.html", null ],
      [ "device_removed_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1device__removed__error.html", null ],
      [ "write_not_supported_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1write__not__supported__error.html", null ],
      [ "io_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1io__error.html", null ],
      [ "timeout_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1timeout__error.html", null ],
      [ "set_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1set__error.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1set__error" ]
    ] ],
    [ "InterfaceImpl.hpp", "_interface_impl_8hpp.html", "_interface_impl_8hpp" ],
    [ "InterfaceQueue.hpp", "_interface_queue_8hpp.html", [
      [ "InterfaceQueue", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_queue.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_queue" ]
    ] ],
    [ "Protocol.hpp", "_protocol_8hpp.html", [
      [ "CheckedInputIteratorReference", "class_wacom_g_s_s_1_1_s_t_u_1_1_checked_input_iterator_reference.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_checked_input_iterator_reference" ],
      [ "Protocol", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol" ],
      [ "Rectangle", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle" ],
      [ "Status", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status", [
        [ "statusCode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aae1fa796857b617fa1cff2c64980f1be", null ],
        [ "lastResultCode", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9fb1067330b66fc9913639199093a860", null ],
        [ "statusWord", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aff20448e3bfb6bbcc6a29b965cfce188", null ]
      ] ],
      [ "Information", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information", [
        [ "firmwareMajorVersion", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a57e3b3e27a57c4f798ebd42a09008764", null ],
        [ "firmwareMinorVersion", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a29bf1aaec5111afdc331827f78d12cd7", null ],
        [ "secureIc", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9fb7685bc7a3b62f0a0d4d30673637e2", null ],
        [ "secureIcVersion", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#af3c546fe13583fc0e0d3e31f7ba5ccfd", null ]
      ] ],
      [ "Capability", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability", [
        [ "tabletMaxX", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a976e7297293d858cce366587e74b7cfd", null ],
        [ "tabletMaxY", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a5c3cdbff2712064e0aa83b3eed2f3477", null ],
        [ "tabletMaxPressure", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a8d6a72cc37f606fa4fe760dd717ce9f6", null ],
        [ "screenWidth", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a761c684ae83e26358200d3ee388425c5", null ],
        [ "screenHeight", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a6e4b9ad91b54c1692737facde710d2bc", null ],
        [ "maxReportRate", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a56d2d4feec15ea989441ac96e082bfa7", null ],
        [ "resolution", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab61058530dba6ad6f4bb5a0c96b8f308", null ]
      ] ],
      [ "Uid2", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_uid2", null ],
      [ "InkThreshold", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_ink_threshold", [
        [ "onPressureMark", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afc1a7e9dfeff55b6b20067db7f840b91", null ],
        [ "offPressureMark", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a81b24b28d3dad11a440b368ba85c644d", null ]
      ] ],
      [ "ImageDataBlock", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_image_data_block", [
        [ "length", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#afdfdd4eb62c6fba7984cf54a3f3139ed", null ],
        [ "data", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#ab5c3dabda471303211b5a4a01868f512", null ]
      ] ],
      [ "HandwritingThicknessColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_thickness_color", [
        [ "penColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a87c30b7589047627fb76215e29e0a947", null ],
        [ "penThickness", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#aa8ac09fbe558a27cb96ca38a86e6584c", null ]
      ] ],
      [ "HandwritingThicknessColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_thickness_color24", [
        [ "penColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a7312a4eb9ec0cd33380916e6acdb2532", null ],
        [ "penThickness", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#a9a8732a48950bc1641238ea193a713a9", null ]
      ] ],
      [ "HandwritingDisplayArea", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_display_area.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_display_area" ],
      [ "EncryptionStatus", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_status.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_status" ],
      [ "EncryptionCommand", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_command.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_command" ],
      [ "PenData", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data" ],
      [ "PenDataOption", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option" ],
      [ "PenDataTimeCountSequence", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence" ],
      [ "PenDataEncrypted", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted" ],
      [ "PenDataEncryptedOption", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option" ],
      [ "PenDataTimeCountSequenceEncrypted", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence_encrypted.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence_encrypted" ],
      [ "DevicePublicKey", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_device_public_key.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_device_public_key" ]
    ] ],
    [ "ProtocolHelper.hpp", "_protocol_helper_8hpp.html", "_protocol_helper_8hpp" ],
    [ "ReportHandler.hpp", "_report_handler_8hpp.html", [
      [ "ReportHandlerBase", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper_1_1_report_handler_base.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper_1_1_report_handler_base" ],
      [ "ReportHandler", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper_1_1_report_handler.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper_1_1_report_handler" ]
    ] ],
    [ "SerialInterface.hpp", "_serial_interface_8hpp.html", [
      [ "SerialInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface" ]
    ] ],
    [ "SerialProtocol.hpp", "_serial_protocol_8hpp.html", "_serial_protocol_8hpp" ],
    [ "Tablet.hpp", "_tablet_8hpp.html", [
      [ "EncryptionHandler", "class_wacom_g_s_s_1_1_s_t_u_1_1_encryption_handler.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_encryption_handler" ],
      [ "EncryptionHandler2", "class_wacom_g_s_s_1_1_s_t_u_1_1_encryption_handler2.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_encryption_handler2" ],
      [ "Tablet", "class_wacom_g_s_s_1_1_s_t_u_1_1_tablet.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_tablet" ],
      [ "not_supported_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_tablet_1_1not__supported__error.html", null ]
    ] ],
    [ "Tablet_OpenSSL.hpp", "_tablet___open_s_s_l_8hpp.html", null ],
    [ "UsbInterface.hpp", "_usb_interface_8hpp.html", [
      [ "UsbInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface" ]
    ] ]
];