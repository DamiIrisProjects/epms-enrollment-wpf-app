var searchData=
[
  ['bcddevice',['bcdDevice',['../struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device_base.html#a4ce7c0ad0bbd47eb527e313c0a341634',1,'WacomGSS::STU::UsbDeviceBase']]],
  ['big_5ftable',['big_table',['../class_wacom_g_s_s_1_1_crc32__accumulator.html#aae826a4f9d6f222d105ebf9724917ec4',1,'WacomGSS::Crc32_accumulator::big_table()'],['../class_wacom_g_s_s_1_1crc16__ansi__accumulator.html#a4fc95665ac0bdca19f9cc7f645053fc2',1,'WacomGSS::crc16_ansi_accumulator::big_table()']]],
  ['bignum',['BIGNUM',['../class_wacom_g_s_s_1_1_open_s_s_l_1_1_b_i_g_n_u_m.html',1,'WacomGSS::OpenSSL']]],
  ['border',['Border',['../namespace_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper.html#a3f7d6e802ba714ab80b20e32b1b4b38a',1,'WacomGSS::STU::ProtocolHelper']]],
  ['bulkfilename',['bulkFileName',['../struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#a97063a5a5907e2de7804d12c78533572',1,'WacomGSS::STU::UsbDevice']]],
  ['busnumber',['busNumber',['../struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html#ad057149131c8cf28df14de872c33df83',1,'WacomGSS::STU::UsbDevice']]]
];
