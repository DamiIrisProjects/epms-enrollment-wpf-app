var gdiplus_8hpp =
[
    [ "Gdiplus_StartupToken_traits", "struct_wacom_g_s_s_1_1_win32_1_1_gdiplus___startup_token__traits.html", "struct_wacom_g_s_s_1_1_win32_1_1_gdiplus___startup_token__traits" ],
    [ "GdiplusStartupToken", "gdiplus_8hpp.html#acd17cec84d52eec4b7b613ee05ade67f", null ],
    [ "Gdiplus_error_category", "gdiplus_8hpp.html#aca2f376fad4d947eed63263075ec4eb6", null ],
    [ "make_Gdiplus_error_code", "gdiplus_8hpp.html#a6d4c983e69754d2f7379994ed4fd1010", null ],
    [ "throw_Gdiplus_error", "gdiplus_8hpp.html#a70a46cd7960bda55afefabd7ecff711e", null ],
    [ "throw_Gdiplus_error", "gdiplus_8hpp.html#af448cb086a11dd0480d8b181f199c065", null ],
    [ "Gdiplus_Ok", "gdiplus_8hpp.html#a519f13cf7d6283305250cbe34fe35004", null ],
    [ "Gdiplus_Ok", "gdiplus_8hpp.html#a7fa13a5ce03504df4389e4e7558b0237", null ]
];