var hierarchy =
[
    [ "std::array< T >", null, [
      [ "WacomGSS::STU::Protocol::DevicePublicKey", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_device_public_key.html", null ]
    ] ],
    [ "WacomGSS::OpenSSL::BIGNUM", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_b_i_g_n_u_m.html", null ],
    [ "WacomGSS::STU::Protocol::Capability", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_capability", null ],
    [ "WacomGSS::STU::CheckedInputIteratorReference< InputIterator >", "class_wacom_g_s_s_1_1_s_t_u_1_1_checked_input_iterator_reference.html", null ],
    [ "WacomGSS::concurrent_queue_with_exception< Container, T >", "class_wacom_g_s_s_1_1concurrent__queue__with__exception.html", null ],
    [ "WacomGSS::crc16_ansi_accumulator", "class_wacom_g_s_s_1_1crc16__ansi__accumulator.html", null ],
    [ "WacomGSS::Crc32_accumulator", "class_wacom_g_s_s_1_1_crc32__accumulator.html", null ],
    [ "WacomGSS::OpenSSL::DH", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_d_h.html", null ],
    [ "WacomGSS::STU::Protocol::EncryptionCommand", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_command.html", null ],
    [ "WacomGSS::STU::EncryptionHandler", "class_wacom_g_s_s_1_1_s_t_u_1_1_encryption_handler.html", null ],
    [ "WacomGSS::STU::EncryptionHandler2", "class_wacom_g_s_s_1_1_s_t_u_1_1_encryption_handler2.html", null ],
    [ "WacomGSS::STU::Protocol::EncryptionStatus", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_encryption_status.html", null ],
    [ "std::exception", null, [
      [ "std::runtime_error", null, [
        [ "WacomGSS::STU::Interface::set_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1set__error.html", null ],
        [ "WacomGSS::STU::runtime_error", "class_wacom_g_s_s_1_1_s_t_u_1_1runtime__error.html", [
          [ "WacomGSS::STU::Interface::device_removed_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1device__removed__error.html", null ],
          [ "WacomGSS::STU::Interface::io_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1io__error.html", null ],
          [ "WacomGSS::STU::Interface::not_connected_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1not__connected__error.html", null ],
          [ "WacomGSS::STU::Interface::timeout_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1timeout__error.html", null ],
          [ "WacomGSS::STU::Interface::write_not_supported_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_1_1write__not__supported__error.html", null ],
          [ "WacomGSS::STU::SerialProtocol::crc_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_protocol_1_1crc__error.html", null ],
          [ "WacomGSS::STU::Tablet::not_supported_error", "class_wacom_g_s_s_1_1_s_t_u_1_1_tablet_1_1not__supported__error.html", null ]
        ] ]
      ] ]
    ] ],
    [ "WacomGSS::Win32::Gdiplus_StartupToken_traits", "struct_wacom_g_s_s_1_1_win32_1_1_gdiplus___startup_token__traits.html", null ],
    [ "WacomGSS::STU::Protocol::HandwritingThicknessColor", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_thickness_color", null ],
    [ "WacomGSS::STU::Protocol::HandwritingThicknessColor24", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_thickness_color24", null ],
    [ "WacomGSS::STU::Protocol::ImageDataBlock", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_image_data_block", null ],
    [ "WacomGSS::STU::Protocol::Information", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_information", null ],
    [ "WacomGSS::STU::Protocol::InkThreshold", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_ink_threshold", null ],
    [ "WacomGSS::STU::Interface", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface.html", [
      [ "WacomGSS::STU::SerialInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_serial_interface.html", null ],
      [ "WacomGSS::STU::UsbInterface", "class_wacom_g_s_s_1_1_s_t_u_1_1_usb_interface.html", null ]
    ] ],
    [ "WacomGSS::STU::InterfaceQueue", "class_wacom_g_s_s_1_1_s_t_u_1_1_interface_queue.html", null ],
    [ "WacomGSS::STU::Protocol::PenData", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data.html", [
      [ "WacomGSS::STU::Protocol::PenDataOption", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_option.html", null ],
      [ "WacomGSS::STU::Protocol::PenDataTimeCountSequence", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence.html", [
        [ "WacomGSS::STU::Protocol::PenDataTimeCountSequenceEncrypted", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_time_count_sequence_encrypted.html", null ]
      ] ]
    ] ],
    [ "WacomGSS::STU::Protocol::PenDataEncrypted", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted.html", [
      [ "WacomGSS::STU::Protocol::PenDataEncryptedOption", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_pen_data_encrypted_option.html", null ]
    ] ],
    [ "WacomGSS::STU::Protocol", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html", null ],
    [ "WacomGSS::STU::Protocol::Rectangle", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_rectangle.html", [
      [ "WacomGSS::STU::Protocol::HandwritingDisplayArea", "struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_handwriting_display_area.html", null ]
    ] ],
    [ "WacomGSS::STU::ProtocolHelper::ReportHandlerBase", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper_1_1_report_handler_base.html", [
      [ "WacomGSS::STU::ProtocolHelper::ReportHandler", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper_1_1_report_handler.html", null ]
    ] ],
    [ "WacomGSS::OpenSSL::RSA", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_r_s_a.html", null ],
    [ "WacomGSS::STU::Protocol::Status", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_status", null ],
    [ "WacomGSS::STU::Tablet", "class_wacom_g_s_s_1_1_s_t_u_1_1_tablet.html", null ],
    [ "WacomGSS::STU::Protocol::Uid2", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol.html#struct_wacom_g_s_s_1_1_s_t_u_1_1_protocol_1_1_uid2", null ],
    [ "std::unique_ptr< T >", null, [
      [ "WacomGSS::OpenSSL::AES_KEY", "class_wacom_g_s_s_1_1_open_s_s_l_1_1_a_e_s___k_e_y.html", null ]
    ] ],
    [ "WacomGSS::STU::UsbDeviceBase", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device_base.html", [
      [ "WacomGSS::STU::UsbDevice", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html", null ]
    ] ],
    [ "WacomGSS::STU::ProtocolHelper::ValidatingInterfaceImpl", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper_1_1_validating_interface_impl.html", [
      [ "WacomGSS::STU::ProtocolHelper::ValidatingInterface< BaseInterface >", "class_wacom_g_s_s_1_1_s_t_u_1_1_protocol_helper_1_1_validating_interface.html", null ]
    ] ]
];