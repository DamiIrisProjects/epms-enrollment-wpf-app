var com_8hpp =
[
    [ "WacomGSS_initializeRetVal", "com_8hpp.html#a55a509eac97932289b6476d32d0bef1f", null ],
    [ "WacomGSS_initializeRetVal2", "com_8hpp.html#a10cf2d029a5325daf5d5f6ac309c1506", null ],
    [ "addref_t", "com_8hpp.html#aeeeed4ca24553e67af3bea1838ebf7ba", [
      [ "addref", "com_8hpp.html#aeeeed4ca24553e67af3bea1838ebf7baad9d856003148be1e56ca2f737ae16822", null ]
    ] ],
    [ "HRESULT_error_category", "com_8hpp.html#a2e4caab6698b9f7d1d19338b8da0e736", null ],
    [ "make_HRESULT_error_code", "com_8hpp.html#a11abbd409ad9973cac4c63ffaa787eb0", null ],
    [ "throw_HRESULT_error", "com_8hpp.html#a731af7e499997cc6a2ed7a52f20454c7", null ],
    [ "throw_HRESULT_error", "com_8hpp.html#a75551e339a774bcff8dd6c28b0ad1178", null ],
    [ "hresult_succeeded", "com_8hpp.html#a11c1d351c1cdc7f08f4c2c5f469560b7", null ],
    [ "hresult_succeeded", "com_8hpp.html#a1737a6658578a944eb1ceb2f04be8921", null ],
    [ "operator<<", "com_8hpp.html#afb5f780ee4dcd6b2e3ecde0aefb483c7", null ],
    [ "initializeRetVal", "com_8hpp.html#aca5488c9911a3e094838a9e470739534", null ],
    [ "initializeRetVal", "com_8hpp.html#a1f0ba12022c25603c22c5e7d0112c6e0", null ],
    [ "initializeRetVal", "com_8hpp.html#a72ae50c8e00b819a96c6c0ecdc0d03f8", null ],
    [ "initializeRetVal", "com_8hpp.html#acbd90a7a409bfc46976325c63954aeeb", null ]
];