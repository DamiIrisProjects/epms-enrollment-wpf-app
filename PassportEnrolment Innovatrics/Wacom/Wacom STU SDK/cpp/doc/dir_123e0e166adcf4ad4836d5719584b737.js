var dir_123e0e166adcf4ad4836d5719584b737 =
[
    [ "compatibility", "dir_eab8117aa204b64630256573de274393.html", "dir_eab8117aa204b64630256573de274393" ],
    [ "config", "dir_9b820a1c2df784bd9d2eadf7a4513b8f.html", "dir_9b820a1c2df784bd9d2eadf7a4513b8f" ],
    [ "Linux", "dir_fa993f2fe686fc711f1795a411808260.html", "dir_fa993f2fe686fc711f1795a411808260" ],
    [ "STU", "dir_1d0c055ca3c373b3eef70b1ace60313d.html", "dir_1d0c055ca3c373b3eef70b1ace60313d" ],
    [ "Win32", "dir_ca22d487b5a775c47c3a47d93c83b427.html", "dir_ca22d487b5a775c47c3a47d93c83b427" ],
    [ "concurrent_queue_with_exception.hpp", "concurrent__queue__with__exception_8hpp.html", [
      [ "concurrent_queue_with_exception", "class_wacom_g_s_s_1_1concurrent__queue__with__exception.html", "class_wacom_g_s_s_1_1concurrent__queue__with__exception" ]
    ] ],
    [ "config.hpp", "config_8hpp.html", null ],
    [ "Crc.hpp", "_crc_8hpp.html", "_crc_8hpp" ],
    [ "enumUsbDevices.hpp", "enum_usb_devices_8hpp.html", "enum_usb_devices_8hpp" ],
    [ "noncopyable.hpp", "noncopyable_8hpp.html", null ],
    [ "OpenSSL.hpp", "_open_s_s_l_8hpp.html", "_open_s_s_l_8hpp" ],
    [ "setThreadName.hpp", "set_thread_name_8hpp.html", "set_thread_name_8hpp" ],
    [ "unique_handle.hpp", "unique__handle_8hpp.html", null ]
];