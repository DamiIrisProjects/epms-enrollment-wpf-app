var get_usb_devices_8hpp =
[
    [ "UsbDeviceBase", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device_base.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device_base" ],
    [ "UsbDevice", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device.html", "struct_wacom_g_s_s_1_1_s_t_u_1_1_usb_device" ],
    [ "VendorId", "get_usb_devices_8hpp.html#abe3ce451b3668d69730cb63c9985aaf6", [
      [ "VendorId_Wacom", "get_usb_devices_8hpp.html#abe3ce451b3668d69730cb63c9985aaf6a711cbf0e46b1abfaee804f5b27d262e2", null ]
    ] ],
    [ "ProductId", "get_usb_devices_8hpp.html#ab4616dcdac77bd92040805c05ccaa549", [
      [ "ProductId_500", "get_usb_devices_8hpp.html#ab4616dcdac77bd92040805c05ccaa549a8071f4d2bb87dbef669fa4248bd26a01", null ],
      [ "ProductId_300", "get_usb_devices_8hpp.html#ab4616dcdac77bd92040805c05ccaa549ac6b017719a8014f813f5cbafd0eaa10d", null ],
      [ "ProductId_520A", "get_usb_devices_8hpp.html#ab4616dcdac77bd92040805c05ccaa549a09dda46de5c610f82f89e656a90e26e2", null ],
      [ "ProductId_430", "get_usb_devices_8hpp.html#ab4616dcdac77bd92040805c05ccaa549a01efc334fc2c8fbbd87bd68065635089", null ],
      [ "ProductId_530", "get_usb_devices_8hpp.html#ab4616dcdac77bd92040805c05ccaa549a0722d9c4d62998047d2ff2bbfd9efc90", null ]
    ] ],
    [ "isSupportedUsbDevice", "get_usb_devices_8hpp.html#a6bb1becdc2f580b9478b593b17897e45", null ],
    [ "getUsbDevices", "get_usb_devices_8hpp.html#ac9493940b53e87be5a81b4d836fc2d7a", null ],
    [ "operator<<", "get_usb_devices_8hpp.html#a3696834c1b851919d936ff4f85f2cad4", null ],
    [ "ProductId_min", "get_usb_devices_8hpp.html#a2c5f174e61e77aecd29af866dd6f7517", null ],
    [ "ProductId_max", "get_usb_devices_8hpp.html#ab173d696896f12ade1edf53aca79953e", null ]
];