/// @file      WacomGSS/STU/InterfaceImpl.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2011-10-18
/// @brief     Internal implementation of the Interface class.

#ifndef WacomGSS_STU_InterfaceImpl_hpp
#define WacomGSS_STU_InterfaceImpl_hpp

#include <WacomGSS/concurrent_queue_with_exception.hpp>
#include <WacomGSS/noncopyable.hpp>
#include <WacomGSS/compatibility/atomic.hpp>
#include <memory>
#include <vector>
#include <deque>
#include <list>


namespace WacomGSS
{
  namespace STU
  {
    class Interface;
    class InterfaceQueue;

    /// @brief Standardised container of a Report
    typedef std::vector<std::uint8_t> Report;

    /// @internal
    class InterfaceImpl : WacomGSS::noncopyable
    {
      friend class Interface;
      friend class InterfaceQueue;
            
      typedef std::shared_ptr<concurrent_queue_with_exception<std::deque<Report>,Report>> shared_queue;

      atomic<bool>            m_empty; // cached value of m_queues.empty()
      mutex                   m_mutex;
      std::list<shared_queue> m_queues;

      shared_queue createInterfaceQueue();
      void         removeInterfaceQueue(shared_queue && queue);

    public:
      InterfaceImpl();
    };


  } // namespace STU

} // namespace WacomGSS

#endif // WacomGSS_STU_InterfaceImpl_hpp
