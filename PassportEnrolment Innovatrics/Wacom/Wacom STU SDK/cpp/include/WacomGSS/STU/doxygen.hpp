/**

@mainpage

This SDK allows developers to interact with the Wacom STU-series of tablets, specifically designed for signature collection.

@section QuickStart Quick Start

For those wanting to skip any preamble, the key classes and functions are:
 - WacomGSS::STU::getUsbDevices() &ndash; retrieves an array of attached USB tablets.
 - WacomGSS::STU::UsbInterface and WacomGSS::STU::SerialInterface &ndash; connects to a specific tablet.
 - WacomGSS::STU::Tablet &ndash; queries and controls a tablet.
 - WacomGSS::STU::Tablet::interfaceQueue() and WacomGSS::STU::ProtocolHelper::ReportHandler &ndash; retrieves and decodes pen data.
 - WacomGSS::STU::ProtocolHelper &ndash; helper routines, including packaging an image to update to the tablet.

@subsection Overview

To read a language agnostic overview of the SDK, refer to the separate &ldquo;Getting Started&rdquo; document.


@subsection ContactSupport Contact & Support

Please send emails to: signature-support@wacom.eu


@subsection Licence

Copyright &copy; 2013 Wacom Company Limited

@subsection Credits

Wacom Global Signature Development Team
 - Martin Holden - lead developer
 - Kevin Parkes - documentation
 - Juan Garrido - sample code
 - Nick Mettyear - project manager

*/


/**
@namespace WacomGSS::STU
@brief     The API for communicating with %STU tablets

@namespace WacomGSS::STU::ostream_operators
@brief     Stream insertion operators for WacomGSS::STU types

@namespace WacomGSS::STU::ProtocolHelper
@brief     Functions and classes providing higher level assistance to using the %STU tablet protocol.

@namespace WacomGSS::STU::ProtocolHelper::ostream_operators
@brief     Stream insertion operators for WacomGSS::STU::ProtocolHelper enums

@namespace WacomGSS::STU::SerialProtocol
@brief     Provides an abstraction of the serial-specific protocol 

*/

