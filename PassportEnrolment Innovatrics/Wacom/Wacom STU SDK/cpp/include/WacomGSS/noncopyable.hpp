/// @file      WacomGSS/noncopyable.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2011-10-18
/// @brief     implements the noncopyable pattern

#ifndef WacomGSS_noncopyable_hpp
#define WacomGSS_noncopyable_hpp

#include <WacomGSS/config.hpp>

namespace WacomGSS
{

	class noncopyable
	{  
    noncopyable(noncopyable const &);
    noncopyable & operator = (noncopyable const &);

  protected:
    noncopyable() {}
    ~noncopyable() {}
	};

}

#endif // WacomGSS_noncopyable_hpp
