/// @file      WacomGSS/config/gcc.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    gzagon
/// @date      2012-02-15
/// @brief     meta configuration about the compliation platform and compiler

#ifndef WacomGSS_config_gcc_hpp
#define WacomGSS_config_gcc_hpp

#define WacomGSS_GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)

#if WacomGSS_GCC_VERSION < 40600
# error WacomGSS: Compiler does not support enough of C++11 to compile this code
#endif

#pragma GCC diagnostic ignored "-Wmissing-braces"

#include <cstring>


namespace WacomGSS
{

  inline void * memset_s(void * ptr, int value, size_t num)
  {
    return ::std::memset(ptr, value, num);
  }

}

#if WacomGSS_GCC_VERSION <= 40603
#define override
#endif


#define WacomGSS_compatibility_condition_variable 1 // use boost
#define WacomGSS_compatibility_mutex              1
#define WacomGSS_compatibility_thread             1
#define WacomGSS_compatibility_chrono             1
#define WacomGSS_compatibility_atomic             0 // use std

#define WacomGSS_compatibility_std_codecvt        1 /* does not exist: requires compatibility */

#define WacomGSS_noreturn   __attribute__((noreturn))
#define WacomGSS_deprecated __attribute__((deprecated))

#define WacomGSS_thread_local __thread

#endif // WacomGSS_config_gcc_hpp
