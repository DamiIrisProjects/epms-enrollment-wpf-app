/// @file      WacomGSS/OpenSSL.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2011-10-18
/// @brief     provides a thin layer of objects from the OpenSSL library to provide basic exception safety.

#ifndef WacomGSS_OpenSSL_hpp
#define WacomGSS_OpenSSL_hpp

#include <WacomGSS/unique_handle.hpp>
#include <system_error>
#include <memory>



namespace WacomGSS
{

  namespace OpenSSL
  {

    std::error_category const & openssl_error_category() noexcept;

    class error : public std::system_error
    {
    public:
      error();
      explicit error(char const * what_arg);
      explicit error(unsigned long err);
      error(unsigned long err, char const * what_arg);
    };

    /// @internal
    /// Aliases
    /// @brief These provide type aliases to point OpenSSL equivilents. 
    ///
    /// These types are provided so that a pointer type can be created
    /// without introducing the real OpenSSL types at global namespace level
    /// which can introduce ambiguities in name resolution.
    /// @{
    struct BIGNUM_  { };
    struct DH_      { };
    struct AES_KEY_ { };
    struct RSA_     { };
    /// @}


    struct BIGNUM_BN_free_trait
    {
      static BIGNUM_ * invalid() noexcept
      {
        return nullptr;
      }
  
      static bool is_valid(BIGNUM_ * value) noexcept
      {
        return value != nullptr;
      }
  
      static void close(BIGNUM_ * value) noexcept;
  
      static void throw_invalid_handle_exception();
    };
    
    /// @brief An exception-safe alias of the OpenSSL %BIGNUM struct.
    class BIGNUM : public unique_handle<BIGNUM_ *,BIGNUM_BN_free_trait>
    {
      typedef unique_handle<BIGNUM_ *,BIGNUM_BN_free_trait> base; 
    
    public:
      BIGNUM();
      BIGNUM(BIGNUM_ * value);
    };


    class BIGNUMref : noncopyable
    {
      BIGNUM_ * & m_ref;

    public:
      BIGNUMref(BIGNUM_ * & ref) noexcept
      :
        m_ref(ref)
      {
      }

      BIGNUMref & operator = (BIGNUM && value);
      BIGNUMref & operator = (std::nullptr_t);

      bool operator !() const noexcept
      {
        return !m_ref;
      }

      BIGNUM_ const * const & get() const noexcept
      {
        return const_cast<BIGNUM_ const * const &>(m_ref);
      }

      BIGNUM_ * & get() noexcept
      {
        return m_ref;
      }
    };



    struct DH_DH_free_trait
    {
      static DH_ * invalid() noexcept
      {
        return nullptr;
      }
  
      static bool is_valid(DH_ * value) noexcept
      {
        return value != nullptr;
      }

      static void close(DH_ * value) noexcept;

      static void throw_invalid_handle_exception();
    };



    struct RSA_RSA_free_trait
    {
      static RSA_ * invalid() noexcept
      {
        return nullptr;
      }

      static bool is_valid(RSA_ * value) noexcept
      {
        return value != nullptr;
      }

      static void close(RSA_ * value) noexcept;

      static void throw_invalid_handle_exception();
    };



    /// @brief Provides an exception safe use of the OpenSSL ::%DH struct
    ///
    /// Use should be identical to using the real ::%DH object, without
    /// having to be concerned with API failures and null pointers.
    class DH : public unique_handle<DH_ *,DH_DH_free_trait>
    {
      typedef unique_handle<DH_ *,DH_DH_free_trait> base;

    public:
      DH();

      BIGNUMref p;
      BIGNUMref g;
      BIGNUMref pub_key;
      BIGNUMref priv_key;
    };

    /// @brief Provides an exception safe use of the OpenSSL ::%RSA struct
    ///
    /// Use should be identical to using the real ::%RSA object, without
    /// having to be concerned with API failures and null pointers.
    class RSA : public unique_handle<RSA_ *,RSA_RSA_free_trait>
    {
      typedef unique_handle<RSA_ *,RSA_RSA_free_trait> base;

    public:
      RSA();

      BIGNUMref e;
      BIGNUMref n;
    };



    /// @brief Provides an exception safe use of the OpenSSL AES_KEY object
    ///
    /// Use should be identical to using the real AES_KEY object, with safe (null)
    /// construction and safe deleting of the key memory at destruction.
    class AES_KEY : public std::unique_ptr<AES_KEY_>
    {
    public:
      /// @brief initializes the key to zero.
      AES_KEY();

      /// @brief securely deletes the key.
      ~AES_KEY();

      AES_KEY & operator = (AES_KEY && other)
      {
        static_cast<std::unique_ptr<AES_KEY_> &>(*this) = std::move(other);
        return *this;
      }
    };

    
    /// @brief Calculdates the public and private keys given the p and g values.
    int DH_generate_key(DH & dh);

    /// @brief Calculates the shared key given the provided public key.
    /// @attention The return type has been changed to size_t to better accomodate C++.
    size_t DH_compute_key(unsigned char * key, BIGNUM const & pub_key, DH & dh); 

    /// @brief Validates Diffie-Hellman parameters.
    /// It checks that p is a safe prime, and that g is a suitable generator.
    /// In the case of an error, the bit flags DH_CHECK_P_NOT_SAFE_PRIME or
    /// DH_NOT_SUITABLE_GENERATOR are set in *codes.
    /// DH_UNABLE_TO_CHECK_GENERATOR is set if the generator cannot be checked,
    /// i.e. it does not equal 2 or 5.
    int DH_check(DH & dh, int * codes);

    /// @attention Parameter @em len has been changed to size_t to better accomodate C++.
    ///            The third parameter is not supported and must be nullptr.
    BIGNUM BN_bin2bn(unsigned char const * s, size_t len, std::nullptr_t);

    /// @attention The return type has been changed to size_t to better accomodate C++.
    size_t BN_bn2bin(BIGNUMref const & a, unsigned char * to);
    
    /// @attention The return type has been changed to size_t to better accomodate C++.
    size_t BN_bn2bin(BIGNUM const & a, unsigned char * to);

    /// @attention The return type has been changed to size_t to better accomodate C++.
    size_t (BN_num_bytes)(BIGNUMref const & a);

    /// @attention The return type has been changed to size_t to better accomodate C++.
    size_t (BN_num_bytes)(BIGNUM const & a);

    /// @brief Initializes the value
    int BN_set_word(BIGNUM & a, unsigned long w);
    
    /// @brief Initializes the value
    int BN_set_word(BIGNUMref & a, unsigned long w);

    /// @brief Initializes the decryption key
    int AES_set_decrypt_key(unsigned char const * userKey, const int bits, AES_KEY * key);
    
    /// @brief Decrypts one data block.
    void AES_decrypt(unsigned char const * in, unsigned char * out, AES_KEY const * key);

    /// @brief Generates random data.
    int RAND_pseudo_bytes(unsigned char * buf, int num);



    /// @brief Generates a new random key pair.
    int RSA_generate_key_ex(RSA & rsa, int bits, BIGNUM & e, std::nullptr_t);

    /// @attention The return type has been changed to size_t to better accomodate C++.
    size_t RSA_size(RSA & rsa);

    /// @attention The return type and flen have been changed to size_t to better accomodate C++.
    size_t RSA_private_decrypt(size_t flen, unsigned char const * in, unsigned char * to, RSA & rsa, int padding);
    
    const unsigned long RSA_F4 = 0x10001;
    const int           RSA_PKCS1_PADDING      = 1;
    const int           RSA_NO_PADDING         = 3;
    const int           RSA_PKCS1_OAEP_PADDING = 4;

  } // namespace OpenSSL

} // namespace WacomGSS
       
#endif // WacomGSS_OpenSSL_hpp
