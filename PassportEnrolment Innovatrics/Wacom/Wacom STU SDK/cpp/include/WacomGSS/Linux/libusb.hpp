/// @file      WacomGSS/Linux/libusb.hpp
/// @copyright Copyright (c) 2012 Wacom Company Limited
/// @author    mholden
/// @date      2012-03-07
/// @brief     provides a thin C++ wrapper for libusb library for exception safety. Converts errors into exceptions.

#ifndef WacomGSS_libusb_hpp
#define WacomGSS_libusb_hpp

#include <WacomGSS/noncopyable.hpp>

#include <libusb.h>
#include <system_error>


namespace WacomGSS
{
  namespace libusb
  {
    enum class libusb_errc
    {
    //success       = ::LIBUSB_SUCCESS             ,
      io            = ::LIBUSB_ERROR_IO            ,
      invalid_param = ::LIBUSB_ERROR_INVALID_PARAM ,
      access        = ::LIBUSB_ERROR_ACCESS        ,
      no_device     = ::LIBUSB_ERROR_NO_DEVICE     ,
      not_found     = ::LIBUSB_ERROR_NOT_FOUND     ,
      busy          = ::LIBUSB_ERROR_BUSY          ,
      timeout       = ::LIBUSB_ERROR_TIMEOUT       ,
      overflow      = ::LIBUSB_ERROR_OVERFLOW      ,
      pipe          = ::LIBUSB_ERROR_PIPE          ,
      interrupted   = ::LIBUSB_ERROR_INTERRUPTED   ,
      no_mem        = ::LIBUSB_ERROR_NO_MEM        ,
      not_supported = ::LIBUSB_ERROR_NOT_SUPPORTED ,
      other         = ::LIBUSB_ERROR_OTHER
    };

    std::error_category const & libusb_category() noexcept;

    std::error_code      make_error_code(libusb_errc ec);
    std::error_condition make_error_condition(libusb_errc ec);



    /*class libusb_error : public std::system_error
    {
    public:
      libusb_error(int errc)
      :
        std::system_error(errc, libusb_category())
      {
      }

      template<typename T>
      libusb_error(int errc, T what)
      :
        std::system_error(errc, libusb_category(), what)
      {
      }
    };*/

    WacomGSS_noreturn inline void throw_libusb_error(int e)
    {
      throw std::system_error(std::error_code(e, libusb_category()));
    }

    WacomGSS_noreturn inline void throw_libusb_error(libusb_errc e)
    {
      throw std::system_error(make_error_code(e));
    }



    template<typename T>
    void libusb_succeeded(T ret)
    {
      if (ret < ::LIBUSB_SUCCESS)
        throw_libusb_error(ret);
    }



    class config_descriptor : noncopyable
    {
      ::libusb_config_descriptor * m_config;

    public:
      config_descriptor(::libusb_config_descriptor * config) noexcept;
      config_descriptor(config_descriptor && other) noexcept;
      ~config_descriptor() noexcept;

      ::libusb_config_descriptor const * operator -> () const noexcept;
    };



    class device_handle : noncopyable
    {
      ::libusb_device_handle * m_handle;

      struct boolean_value { int value; }; typedef int boolean_value::* boolean_type;

    public:
      device_handle() noexcept;
      device_handle(::libusb_device_handle * handle) noexcept;
      device_handle(device_handle && other) noexcept;
      ~device_handle() noexcept;

      device_handle & operator = (device_handle && other) noexcept;

      bool kernel_driver_active(int intf);
      void detach_kernel_driver(int intf);
      int  get_configuration();
      void set_configuration(int configuration);
      void claim_interface(int intf);
      void release_interface(int intf);

      int control_transfer(uint8_t request_type, uint8_t request, uint16_t value, uint16_t index, unsigned char * data, uint16_t length, unsigned int timeout);
      int interrupt_transfer(unsigned char endpoint, unsigned char * data, int length, int * actual_length, unsigned int timeout, std::nothrow_t);
      int bulk_transfer(unsigned char endpoint, unsigned char * data, int length, int * transferred, unsigned int timeout, std::nothrow_t);

      operator boolean_type () const noexcept;
    };



    class device : noncopyable
    {
      ::libusb_device * m_device;

    public:
      device(::libusb_device * dev) noexcept;
      device(device && other) noexcept;
      ~device() noexcept;

      uint8_t get_bus_number() const noexcept;
      uint8_t get_device_address() const noexcept;

      int get_max_packet_size(unsigned char endpoint) const;

      ::libusb_device_descriptor get_device_descriptor() const;
      config_descriptor get_config_descriptor(uint8_t config_index) const;

      device_handle open();
    };



    class device_list : noncopyable
    {
      ::libusb_device * * m_list;
      ssize_t             m_size;

    public:
      device_list(struct ::libusb_device * * list, ssize_t size) noexcept;
      device_list(device_list && other) noexcept;
      ~device_list() noexcept;

      ssize_t  size() const noexcept;
      device operator [] (ssize_t index) const noexcept;
    };



    class context : noncopyable
    {
      ::libusb_context * m_context;

    public:
      context();
      ~context() noexcept;

      void set_debug(int level) noexcept;

      device_list get_device_list();
    };


  } // namespace libusb



  //===========================================================================



  namespace libusbHelper
  {
    libusb::device        get_device(libusb::context & context, uint16_t vendorId, uint16_t productId, uint16_t versionId, uint8_t busNumber, uint8_t deviceAddress);
    libusb::device_handle open(libusb::device & dev);
    void                  close(libusb::device_handle handle);
    void                  hid_get_report(libusb::device_handle & handle, uint8_t * data, size_t length);
    void                  hid_set_report(libusb::device_handle & handle, uint8_t const * data, size_t length);
    int                   interrupt_in_sync(libusb::device_handle & handle, unsigned char endpoint, uint8_t * data, size_t length, unsigned int timeout);
    int                   bulk_out_sync(libusb::device_handle & handle, unsigned char endpoint, uint8_t const * data, size_t length, unsigned int timeout);
  }



} // namespace WacomGSS



namespace std
{
  template<> struct is_error_code_enum<WacomGSS::libusb::libusb_errc> : true_type { };
}

#endif
