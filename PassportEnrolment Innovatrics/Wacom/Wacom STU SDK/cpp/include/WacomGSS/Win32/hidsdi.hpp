/// @file      WacomGSS/Win32/hidsdi.hpp
/// @copyright Copyright (c) 2011 Wacom Company Limited
/// @author    mholden
/// @date      2011-10-18
/// @brief     provides wrapper classes to hidsdi.h for exception safety

#ifndef WacomGSS_Win32_hidsdi_hpp
#define WacomGSS_Win32_hidsdi_hpp

#include <WacomGSS/Win32/windows.hpp>
extern "C" 
{
#include <hidsdi.h>
}

namespace WacomGSS
{
  namespace Win32
  {

    std::error_category const & NTSTATUS_error_category() noexcept;

    inline std::error_code make_NTSTATUS_error_code(NTSTATUS cr)
    {
      return std::error_code(static_cast<int>(cr), NTSTATUS_error_category());
    }

    WacomGSS_noreturn inline void throw_NTSTATUS_error(NTSTATUS cr)
    {
      throw std::system_error(make_NTSTATUS_error_code(cr));
    }

    template<typename Ty>
    WacomGSS_noreturn inline void throw_NTSTATUS_error(NTSTATUS cr, Ty _Message)
    {
      throw std::system_error(make_NTSTATUS_error_code(cr), _Message);
    }



    struct PHIDP_PREPARSED_DATA_traits
    {
      static PHIDP_PREPARSED_DATA invalid() noexcept
      {
        return nullptr;
      }

      static bool is_valid(PHIDP_PREPARSED_DATA value) noexcept
      {
        return value != nullptr;
      }

      static void close(PHIDP_PREPARSED_DATA value) noexcept
      {
        //BOOLEAN
        ::HidD_FreePreparsedData(value);
      }

      static void throw_invalid_handle_exception()
      {
        throw_win32api_error(::GetLastError());
      }
    };
    typedef unique_handle<PHIDP_PREPARSED_DATA,PHIDP_PREPARSED_DATA_traits> HidP_Preparsed_Data;


    inline void win32api_NTSTATUS_equal(NTSTATUS test, NTSTATUS value)
    {
      if (value != test) 
      {
        throw_NTSTATUS_error(value);
      }
    }

    template<typename Ty>
    inline void win32api_NTSTATUS_equal(NTSTATUS test, NTSTATUS value, Ty _Message)
    {
      if (value != test) 
      {
        throw_NTSTATUS_error(value, _Message);
      }
    }
  

    


  } // namespace Win32

} // namespace WacomGSS


#endif // WacomGSS_Win32_hidsdi_hpp
