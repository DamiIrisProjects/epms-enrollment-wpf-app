/// @file      libusb.cpp
/// @copyright Copyright (c) 2012 Wacom Company Limited
/// @author    mholden
/// @date      2012-03-07
/// @brief     provides C++ wrapper to libusb library.

#include <WacomGSS/Linux/libusb.hpp>


namespace WacomGSS
{

  namespace libusb
  {

    //=========================================================================


    class libusb_error_category_impl : public std::error_category
    {
    public:
      char const * name() const noexcept override
      {
        return "libusb";
      }

      std::string message(int ev) const override
      {
        return std::string(::libusb_error_name(ev));
      }

      std::error_condition default_error_condition(int ev) const noexcept override
      {
        switch (static_cast<libusb_errc>(ev))
        {
          case libusb_errc::io            : return std::errc::io_error;
          case libusb_errc::invalid_param : return std::errc::invalid_argument;
          case libusb_errc::access        : return std::errc::permission_denied;         // vs operation_not_permitted; see EACCESS vs EPERM
          case libusb_errc::no_device     : return std::errc::no_such_device;            // vs no_such_device_or_address; see ENXIO vs ENODEV
          case libusb_errc::not_found     : return std::errc::no_such_device_or_address; // vs no_such_device;            see ENXIO vs ENODEV
          case libusb_errc::busy          : return std::errc::device_or_resource_busy;
          case libusb_errc::timeout       : return std::errc::timed_out;
          case libusb_errc::overflow      : return std::errc::value_too_large;
          case libusb_errc::pipe          : return std::errc::broken_pipe;
          case libusb_errc::interrupted   : return std::errc::interrupted;
          case libusb_errc::no_mem        : return std::errc::not_enough_memory;
          case libusb_errc::not_supported : return std::errc::function_not_supported;    // vs operation_not_supported; see ENOTSUP vs EOPNOTSUP
          default: 
            return std::error_condition(ev, *this);
        }
      }

      bool equivalent(int ev, std::error_condition const & condition) const noexcept override
      {
        switch (static_cast<libusb_errc>(ev))
        {
          case libusb_errc::access:
            if (condition == std::errc::permission_denied ||
                condition == std::errc::operation_not_permitted)
              return true;
            break;

          case libusb_errc::no_device:
            if (condition == std::errc::no_such_device ||
                condition == std::errc::no_such_device_or_address ||
                condition == std::errc::no_such_file_or_directory)
              return true;
            break;

          case libusb_errc::not_found:
            if (condition == std::errc::no_such_device ||
                condition == std::errc::no_such_device_or_address)
              return true;
            break;

          case libusb_errc::not_supported:
            if (condition == std::errc::function_not_supported ||
                condition == std::errc::operation_not_supported)
              return true;
            break;

          case libusb_errc::pipe:
          case libusb_errc::other:
              if (condition == std::errc::io_error)
                return true;
              break;

          default:;
        }
        return default_error_condition(ev) == condition;
      }
    };




    std::error_category const & libusb_category() noexcept
    {
      static libusb_error_category_impl instance;

      return instance;
    }



    std::error_code make_error_code(libusb_errc ec)
    {
      return std::error_code(static_cast<int>(ec), libusb_category());
    }



    std::error_condition make_error_condition(libusb_errc ec)
    {
      return std::error_condition(static_cast<int>(ec), libusb_category());
    }



    //=========================================================================


    config_descriptor::config_descriptor(::libusb_config_descriptor * config) noexcept
    :
      m_config(config)
    {
    }



    config_descriptor::config_descriptor(config_descriptor && other) noexcept
    {
      m_config       = other.m_config;
      other.m_config = nullptr;
    }



    config_descriptor::~config_descriptor() noexcept
    {
      ::libusb_free_config_descriptor(m_config);
    }



    ::libusb_config_descriptor const * config_descriptor::operator -> () const noexcept
    {
      return m_config;
    }



    //=========================================================================


    device_handle::device_handle() noexcept
    :
      m_handle(nullptr)
    {
    }



    device_handle::device_handle(::libusb_device_handle * handle) noexcept
    :
      m_handle(handle)
    {
    }



    device_handle::device_handle(device_handle && other) noexcept
    {
      m_handle       = other.m_handle;
      other.m_handle = nullptr;
    }



    device_handle::~device_handle() noexcept
    {
      if (m_handle)
      {
        //void
        ::libusb_close(m_handle);
      }
    }



    device_handle & device_handle::operator = (device_handle && other) noexcept
    {
      auto handle    = m_handle;
      m_handle       = other.m_handle;
      other.m_handle = nullptr;
      
      if (handle)
      {
        //void
        ::libusb_close(handle);
      }

      return *this;
    }



    bool device_handle::kernel_driver_active(int intf)
    {
      int ret = ::libusb_kernel_driver_active(m_handle, intf);
      if (ret == 0)
        return true;
      if (ret == 1)
        return false;
      throw libusb_error(ret);
    }



    void device_handle::detach_kernel_driver(int intf)
    {
      libusb_succeeded( ::libusb_detach_kernel_driver(m_handle, intf) );
    }



    int device_handle::get_configuration()
    {
      int configuration = 0;
      libusb_succeeded( ::libusb_get_configuration(m_handle, &configuration) );
      return configuration;
    }



    void device_handle::set_configuration(int configuration)
    {
      libusb_succeeded( ::libusb_set_configuration(m_handle, configuration) );
    }



    void device_handle::claim_interface(int intf)
    {
      libusb_succeeded( ::libusb_claim_interface(m_handle, intf) );
    }



    void device_handle::release_interface(int intf)
    {
      libusb_succeeded( ::libusb_release_interface(m_handle, intf) );
    }



    int device_handle::control_transfer(uint8_t request_type, uint8_t request, uint16_t value, uint16_t index, unsigned char *data, uint16_t length, unsigned int timeout)
    {
      int ret = ::libusb_control_transfer(m_handle, request_type, request, value, index, data, length, timeout);
      libusb_succeeded( ret );
      return ret;
    }



    int device_handle::interrupt_transfer(unsigned char endpoint, unsigned char * data, int length, int * actual_length, unsigned int timeout, std::nothrow_t)
    {
      return ::libusb_interrupt_transfer(m_handle, endpoint, data, length, actual_length, timeout);
    }



    int device_handle::bulk_transfer(unsigned char endpoint, unsigned char * data, int length, int * transferred, unsigned int timeout, std::nothrow_t)
    {
      return ::libusb_bulk_transfer(m_handle, endpoint, data, length, transferred, timeout);
    }



    device_handle::operator boolean_type () const noexcept
    {
      return m_handle ? &boolean_value::value : nullptr;
    }
    
    //=========================================================================



    device::device(::libusb_device * dev) noexcept
    :
      m_device(::libusb_ref_device(dev))
    {
    }


    device::device(device && other) noexcept
    :
      m_device(other.m_device)
    {
      other.m_device = nullptr;
    }


    device::~device() noexcept
    {
      //void
      ::libusb_unref_device(m_device);
    }
  


    uint8_t device::get_bus_number() const noexcept
    {
      return ::libusb_get_bus_number(m_device);
    }



    uint8_t device::get_device_address() const noexcept
    {
    return ::libusb_get_device_address(m_device);
    
    }
  


    int device::get_max_packet_size(unsigned char endpoint) const
    {
      int ret = ::libusb_get_max_packet_size(m_device, endpoint);
      libusb_succeeded(ret);
      return ret;
    }



    ::libusb_device_descriptor device::get_device_descriptor() const
    {
      ::libusb_device_descriptor desc = { 0 };
      libusb_succeeded( ::libusb_get_device_descriptor(m_device, &desc) );
      return desc;
    }
  

    config_descriptor device::get_config_descriptor(uint8_t config_index) const
    {
      ::libusb_config_descriptor * config = nullptr;
      libusb_succeeded( ::libusb_get_config_descriptor(m_device, config_index, &config) );
      return config_descriptor(config);
    }


    device_handle device::open()
    {
      ::libusb_device_handle * handle = nullptr;

      libusb_succeeded( ::libusb_open(m_device, &handle) );

      return device_handle(handle);
    }

    //=========================================================================



    device_list::device_list(::libusb_device * * list, ssize_t size) noexcept
    :
      m_list(list),
      m_size(size)
    {
    }


    device_list::device_list(device_list && other) noexcept
    :
      m_list(other.m_list),
      m_size(other.m_size)
    {
      other.m_list = nullptr;
      other.m_size = 0;
    }


    device_list::~device_list() noexcept
    {
      if (m_list)
      {
        libusb_free_device_list(m_list, 1);
      }
    }


    ssize_t device_list::size() const noexcept
    {
      return m_size;
    }


    device device_list::operator [] (ssize_t index) const noexcept
    {
      return m_list[index];
    }



    //=========================================================================



    context::context()
    :
      m_context(nullptr)
    {
      int ret = ::libusb_init(&m_context);
      if (ret != ::LIBUSB_SUCCESS)
      {
        m_context = nullptr;
        throw libusb_error(ret);
      }
    }



    context::~context() noexcept
    {
      if (m_context)
      {
        ::libusb_exit(m_context);
      }
    }



    void context::set_debug(int level) noexcept
    {
      //void
      ::libusb_set_debug(m_context, level);
    }



    device_list context::get_device_list()
    {
      ::libusb_device * * list = nullptr;
      ssize_t ret = ::libusb_get_device_list(m_context, &list);

      libusb_succeeded(ret);

      return device_list(list, ret);
    }



  } // namespace libusb

} // namespace WacomGSS


