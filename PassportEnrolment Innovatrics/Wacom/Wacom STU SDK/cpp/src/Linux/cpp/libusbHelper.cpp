/// @file      libusbHelper.cpp
/// @copyright Copyright (c) 2012 Wacom Company Limited
/// @author    mholden
/// @date      2012-03-07
/// @brief     provides higher level functions to the libusb library.

#include <WacomGSS/Linux/libusb.hpp>

#include <unistd.h>

namespace WacomGSS
{


  namespace libusbHelper
  {


    libusb::device get_device(libusb::context & context, uint16_t vendorId, uint16_t productId, uint16_t versionId, uint8_t busNumber, uint8_t deviceAddress)
    {
      libusb::device_list list( context.get_device_list() );
      for (ssize_t i = 0; i < list.size(); ++i)
      {
        libusb::device dev = list[i];
        if (dev.get_bus_number() == busNumber && dev.get_device_address() == deviceAddress)
        {
          // this is the location, is it the same device?
          ::libusb_device_descriptor deviceDescriptor = dev.get_device_descriptor();
          if (deviceDescriptor.idVendor == vendorId && deviceDescriptor.idProduct == productId && deviceDescriptor.bcdDevice == versionId)
          {
            return dev;
          }

          libusb::throw_libusb_error(libusb::libusb_errc::no_device);
        }
      }
      
      libusb::throw_libusb_error(libusb::libusb_errc::not_found);
    }

    
    libusb::device_handle open(libusb::device & dev)
    { 
    
      libusb::config_descriptor configd = dev.get_config_descriptor(0);
      bool m_supportsWrite = configd->bNumInterfaces > 1;
      
      libusb::device_handle handle( dev.open() );

      if (!handle.kernel_driver_active(0))
      {
        handle.detach_kernel_driver(0); 
      }

	  int config = handle.get_configuration();
      if (config != 1)
      {
        handle.set_configuration(1);
      }
      
      

      handle.claim_interface(0);
      
      if(m_supportsWrite)
      { 
         handle.claim_interface(1);
      }

      usleep(10000); // hack needed it seems for Fedora19.

      return handle;
    }


    void close(libusb::device_handle handle)
    {
      if (handle)
      {
        handle.release_interface(0);
      }
    }


    const uint8_t HID_GET_REPORT_BM_TYPE   = 0xA1;   // bmRequestType value
    const uint8_t HID_SET_REPORT_BM_TYPE   = 0x21;   // bmRequestType value
    const uint8_t HID_GET_REPORT           = 0x01;   // bRequest value
    const uint8_t HID_SET_REPORT           = 0x09;   // bRequest value
  
  //const uint16_t HID_REPORT_TYPE_INPUT   = 0x0100; // wValue HI byte
  //const uint16_t HID_REPORT_TYPE_OUTPUT  = 0x0200; // wValue HI byte
    const uint16_t HID_REPORT_TYPE_FEATURE = 0x0300; // wValue HI byte

    void hid_get_report(libusb::device_handle & handle, uint8_t * data, size_t length)
    {
      int ret = handle.control_transfer
                (
                  HID_GET_REPORT_BM_TYPE, 
                  HID_GET_REPORT,
                  static_cast<uint16_t>(HID_REPORT_TYPE_FEATURE | data[0]),
                  0, // index
                  data,
                  static_cast<uint16_t>(length),
                  1000 // timeout milliseconds
                );
      
      libusb::libusb_succeeded(ret);

      if (ret != static_cast<int>(length))
      {
        std::memset(data+ret, 0x00, length-ret); 
        //throw std::runtime_error("ret!=length");
      }
    }



    void hid_set_report(libusb::device_handle & handle, uint8_t const * data, size_t length)
    {

      int ret = handle.control_transfer
                (
                  HID_SET_REPORT_BM_TYPE, 
                  HID_SET_REPORT,
                  static_cast<uint16_t>(HID_REPORT_TYPE_FEATURE | data[0]),
                  0, // index
                  const_cast<uint8_t *>(data),
                  static_cast<uint16_t>(length),
                  1000 // timeout milliseconds
                );
      
      libusb::libusb_succeeded(ret);

      if (ret != static_cast<int>(length))
        throw std::runtime_error("ret!=length");
    }



    int interrupt_in_sync(libusb::device_handle & handle, unsigned char endpoint, uint8_t * data, size_t length, unsigned int timeout)
    {
      /// "LIBUSB_ENDPOINT_IN | 2" is address of STU Tablet interrupt IN endpoint
      /// TODO: add "get endpoint address" feature to update_device_list()

      int transferred = 0;
      int ret = handle.interrupt_transfer(static_cast<unsigned char>(::LIBUSB_ENDPOINT_IN | endpoint), data, static_cast<int>(length), &transferred, timeout, std::nothrow);
            
      if (ret != ::LIBUSB_SUCCESS && ret != ::LIBUSB_ERROR_TIMEOUT)
      {
        libusb::throw_libusb_error(ret);
      }

      return transferred;
    }



    int bulk_out_sync(libusb::device_handle & handle, unsigned char endpoint, uint8_t const * data, size_t length, unsigned int timeout)
    {
      int transferred = 0;
      int ret = handle.bulk_transfer(static_cast<unsigned char>(::LIBUSB_ENDPOINT_OUT | endpoint), const_cast<uint8_t *>(data), static_cast<int>(length), &transferred, timeout, std::nothrow);

      if (ret != ::LIBUSB_SUCCESS && ret != ::LIBUSB_ERROR_TIMEOUT)
      {
        libusb::throw_libusb_error(ret);
      }

      return transferred;
    }
    

  } // namespace libusbHelper


} // namespace WacomGSS
