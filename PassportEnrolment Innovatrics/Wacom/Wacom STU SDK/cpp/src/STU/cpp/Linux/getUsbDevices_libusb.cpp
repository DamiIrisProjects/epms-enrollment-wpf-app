#include <WacomGSS/STU/getUsbDevices.hpp>
#include <WacomGSS/enumUsbDevices.hpp>

namespace WacomGSS
{
  namespace STU
  {


    bool isSupportedUsbDevice(std::uint16_t /*in*/ idVendor, std::uint16_t /*in*/ idProduct) noexcept
    {
      return idVendor == VendorId_Wacom &&
             (idProduct >= ProductId_min && idProduct <= ProductId_max);
    }



    std::vector<UsbDevice> getUsbDevices()
    {
      std::vector<UsbDevice> usbDevices;

      enumUsbDevices
      (
        [&usbDevices] (::WacomGSS::UsbDevice & usbDevice) -> bool
        {
          if (isSupportedUsbDevice(usbDevice.idVendor, usbDevice.idProduct))
          {
            usbDevices.push_back(STU::UsbDevice(usbDevice.idVendor, usbDevice.idProduct, usbDevice.bcdDevice, usbDevice.busNumber, usbDevice.deviceAddress));
          }        
          return true;
        }
      );

      return usbDevices;
    }



  } // namespace STU

} // namespace WacomGSS

