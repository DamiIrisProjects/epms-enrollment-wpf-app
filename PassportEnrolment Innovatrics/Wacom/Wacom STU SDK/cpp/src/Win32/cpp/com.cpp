#include <WacomGSS/Win32/com.hpp>
#include <locale>
#include <codecvt>


namespace WacomGSS
{
  namespace Win32
  {
    namespace Bstr_ostream_operator_utf8
    {


      std::ostream & operator << (std::ostream & o, Bstr const & bstr)
      {
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t, 0x10ffff, std::little_endian>,wchar_t> conv;
        o << conv.to_bytes(bstr.get(), bstr.get() + bstr.len());
        return o;
      }


    } // namespace Bstr_ostream_operator_utf8
  } // namespace Win32
} // namespace WacomGSS
