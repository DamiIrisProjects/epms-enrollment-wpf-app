﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using PassportEnrolment.Helpers;
using System.Windows.Media.Animation;
using Innovatrics.IFace;
using TakeSnapsWithWebcamUsingWpfMvvm.ViewModel;
using AForge.Imaging.Filters;
using AForge.Imaging;
using EDSDKLib;

namespace EOScamera
{
    /// <summary>
    /// Interaction logic for AlternativeWindow.xaml
    /// </summary>
    public partial class AlternativeWindow : Window
    {
        #region Variables

        // Canon
        private SDKHandler CameraHandler;
        private List<int> AvList;
        private List<int> TvList;
        private List<int> ISOList;
        private List<EDSDKLib.Camera> CamList;
        private static Bitmap _latestFrame;
        private bool IsInit = false;
        private int whichCamera = 0;
        private bool takephoto = false;
        private bool pausephoto = false;
        private bool updatingsource = false;
        private bool processing = false;
        private ImageSource EvfImage;
        private JpegBitmapDecoder dec;
        private ImageBrush bgbrush = new ImageBrush();
        private ThreadStart SetImageAction;

        string imagePath = new Uri(Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase), "tempfile.jpg")).LocalPath;
        string justpath = new Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;
        string cropPath = new Uri(Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase), "tempcrop.jpg")).LocalPath;
        string maskpath = new Uri(Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase), "innermask.jpg")).LocalPath;
        string pngcroppath = new Uri(Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase), "cropimg.png")).LocalPath;

        public bool DontUseFacialRecognition { get; set; }

        public string Filelocation { get; set; }

        public bool CroppedImage { get; set; }

        #endregion

        #region Constructor

        public AlternativeWindow()
        {
            InitializeComponent();

            // Confirm there is a camera plugged
            CamList = new SDKHandler().GetCameraList();

            if (CamList.Count == 0)
            {
                ShowCanonItems(false);
                MainViewModel model = new MainViewModel();
                DataContext = model;
            }
            else
            {
                ShowCanonItems(true);
                CameraHandler = new SDKHandler();
                CameraHandler.CameraAdded += new SDKHandler.CameraAddedHandler(SDK_CameraAdded);
                CameraHandler.LiveViewUpdated += new SDKHandler.StreamUpdate(SDK_LiveViewUpdated);
                CameraHandler.CameraHasShutdown += SDK_CameraHasShutdown;
                SetImageAction = new ThreadStart(delegate { bgbrush.ImageSource = EvfImage; });
                RefreshCamera();
                IsInit = true;
            }
        }

        #endregion

        #region Subroutines

        private void TakePhotograph()
        {
            CameraHandler.ImageSaveDirectory = justpath;
            CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Host);
            //CameraHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Camera);

            CameraHandler.TakePhoto();
        }

        private void CloseSession()
        {
            CameraHandler.CloseSession();
        }

        private void RefreshCamera()
        {
            CloseSession();
            CamList = CameraHandler.GetCameraList();
        }

        private int OpenSession()
        {
            if (CamList.Count > 0)
            {
                RefreshCamera();
                if (CameraHandler.CameraSessionOpen)
                    CameraHandler.CloseSession();

                CameraHandler.OpenSession(CamList[0]);
                string cameraname = CameraHandler.MainCamera.Info.szDeviceDescription;
                if (CameraHandler.GetSetting(EDSDK.PropID_AEMode) != EDSDK.AEMode_Manual) MessageBox.Show("Camera is not in manual mode. Some features might not work!");
                AvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Av);
                TvList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_Tv);
                ISOList = CameraHandler.GetSettingsList((uint)EDSDK.PropID_ISOSpeed);
                int wbidx = (int)CameraHandler.GetSetting((uint)EDSDK.PropID_WhiteBalance);

                return 1;
            }
            else
            {
                MessageBox.Show("No camera found");
                return 0;
            }
        }

        #endregion

        #region SDK Events

        private async void SDK_LiveViewUpdated(System.IO.Stream img)
        {
            if (CameraHandler.IsLiveViewOn)
            {
                try
                {
                    if (!pausephoto)
                    {
                        this.Dispatcher.Invoke(new Action(() =>
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                dec = new JpegBitmapDecoder(img, BitmapCreateOptions.None, BitmapCacheOption.None);
                                EvfImage = dec.Frames[0];
                                Application.Current.Dispatcher.Invoke(SetImageAction);
                            }
                        }));
                    }
                    else
                    {  
                        if (!processing)
                        {
                            // Take one more frame
                            this.Dispatcher.Invoke(new Action(() =>
                            {
                                dec = new JpegBitmapDecoder(img, BitmapCreateOptions.None, BitmapCacheOption.None);
                                EvfImage = dec.Frames[0];
                                Application.Current.Dispatcher.Invoke(SetImageAction);

                                System.Drawing.Image image = System.Drawing.Image.FromStream(img);
                            }));

                            // Take photo
                            TakePhotograph();

                            processing = true;
                            this.Dispatcher.Invoke(new Action(() =>
                            {
                                searchpanel.Visibility = System.Windows.Visibility.Visible;
                            }));

                            // Give it time to take the photo
                            bool waitagain = false;
                            await TaskDelay();

                            try
                            {
                                bool facefound = ProcessPhotograph();

                                if (facefound)
                                {
                                    this.Dispatcher.Invoke(new Action(() =>
                                    {
                                        using (MemoryStream ms = new MemoryStream())
                                        {
                                            // Show cropped image
                                            Uri uriSource = new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase), "tempcrop.jpg"));
                                            BitmapImage imgTemp = new BitmapImage();
                                            imgTemp.BeginInit();
                                            imgTemp.CacheOption = BitmapCacheOption.OnLoad;
                                            imgTemp.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                                            imgTemp.UriSource = uriSource;
                                            imgTemp.EndInit();
                                            camerabox.Source = imgTemp;

                                            ShowDoneButtons(true);                                            
                                            camerabox.Visibility = System.Windows.Visibility.Visible;
                                            LVCanvas.Visibility = System.Windows.Visibility.Collapsed;
                                        }
                                    }));
                                }
                                else
                                {
                                   
                                    MessageBox.Show("Could not find a face in the photo; Please take another one.");
                                    pausephoto = false;
                                    processing = false;
                                }

                                this.Dispatcher.Invoke(new Action(() =>
                                {
                                    searchpanel.Visibility = System.Windows.Visibility.Collapsed;
                                }));
                            }
                            catch(Exception)
                            {
                                // perhaps time given wasnt enough. Try one more time
                                waitagain = true;
                            }

                            if (waitagain)
                            {
                                await TaskDelay();
                                ProcessPhotograph();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception");
                }
            }
        }

        private void SDK_CameraAdded()
        {
            RefreshCamera();
        }

        private void SDK_CameraHasShutdown(object sender, EventArgs e)
        {
            CloseSession();
            Close();
        }

        #endregion

        #region aForge Events

        private async void camerabox_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            if (camerabox.Source != null && !updatingsource)
            {
                searchpanel.Visibility = System.Windows.Visibility.Visible;
                using (Bitmap bmp = new Bitmap(CameraVideoDeviceControl.SnapshotBitmap))
                {
                    // Save image
                    bmp.Save(imagePath);

                    // Crop face
                    bool hasface = await Task.Run(() => CropFaces());

                    if (hasface)
                    {
                        // Show the cropped image
                        //ImageSource imageSource = new BitmapImage(new Uri(cropPath));
                        //camerabox.Source = imageSource;
                        Uri uriSource = new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase), "tempcrop.jpg"));
                        BitmapImage imgTemp = new BitmapImage();
                        imgTemp.BeginInit();
                        imgTemp.CacheOption = BitmapCacheOption.OnLoad;
                        imgTemp.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                        imgTemp.UriSource = uriSource;
                        imgTemp.EndInit();
                        camerabox.Source = imgTemp;

                        ShowDoneButtons(true);
                        searchpanel.Visibility = System.Windows.Visibility.Collapsed;
                    }
                    else
                    {
                        MessageBox.Show("Could not find a face. Please take another picture");
                        ShowDoneButtons(false);
                        searchpanel.Visibility = System.Windows.Visibility.Collapsed;
                        ShowVideo(true);
                    }
                }
            }

            updatingsource = false;
        }


        #endregion

        #region Session

        private void OpenSessionButton_Click(object sender, RoutedEventArgs e)
        {
            if (CameraHandler.CameraSessionOpen)
            {
                LVCanvas.Background = System.Windows.Media.Brushes.LightGray;
                CameraHandler.CloseSession();
                SessionButton.Content = "Open Session";
            }
            else
            {
                int result = OpenSession();
                if (result == 1)
                {
                    LVCanvas.Background = bgbrush;
                    CameraHandler.StartLiveView();
                }
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshCamera();
        }

        #endregion

        #region Live view

        private void LVCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CameraHandler.IsLiveViewOn && CameraHandler.IsCoordSystemSet)
            {
                System.Windows.Point p = e.GetPosition(this);
                ushort x = (ushort)((p.X / LVCanvas.Width) * CameraHandler.Evf_CoordinateSystem.width);
                ushort y = (ushort)((p.Y / LVCanvas.Height) * CameraHandler.Evf_CoordinateSystem.height);
                CameraHandler.SetManualWBEvf(x, y);
            }
        }

        private void FocusNear3Button_Click(object sender, RoutedEventArgs e)
        {
            CameraHandler.SetFocus(EDSDK.EvfDriveLens_Near3);
        }

        private void FocusFar3Button_Click(object sender, RoutedEventArgs e)
        {
            CameraHandler.SetFocus(EDSDK.EvfDriveLens_Far3);
        }

        #endregion

        #region Settings

        private void WBCoBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CameraHandler != null)
            {
                switch (WBCoBox.SelectedIndex)
                {
                    case 0: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Auto); break;
                    case 1: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Daylight); break;
                    case 2: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Cloudy); break;
                    case 3: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Tangsten); break;
                    case 4: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Fluorescent); break;
                    case 5: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Strobe); break;
                    case 6: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_WhitePaper); break;
                    case 7: CameraHandler.SetSetting(EDSDK.PropID_WhiteBalance, EDSDK.WhiteBalance_Shade); break;
                }
            }
        }

        #endregion

        #region Window Events

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (whichCamera == 1)
                {
                    int result = OpenSession();
                    if (result == 1)
                    {
                        LVCanvas.Background = bgbrush;
                        CameraHandler.StartLiveView();
                        btnTakePicture.IsEnabled = true;
                    }
                }
                else
                {
                    LVCanvas.Visibility = System.Windows.Visibility.Collapsed;
                    AdvancedOptions.Visibility = System.Windows.Visibility.Collapsed;
                    camerabox.Visibility = System.Windows.Visibility.Visible;
                    btnTakePicture.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not start video capture");
            }
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CroppedImage = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void btnRedo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                processing = false;
                pausephoto = false;

                if (whichCamera == 0)
                {
                    // Rebind camerabox
                    updatingsource = true;
                    Binding myBinding = new Binding();
                    myBinding.Source = DataContext as TakeSnapsWithWebcamUsingWpfMvvm.ViewModel.MainViewModel;
                    myBinding.Path = new PropertyPath("SnapshotTaken");
                    myBinding.NotifyOnTargetUpdated = true;
                    BindingOperations.SetBinding(camerabox, System.Windows.Controls.Image.SourceProperty, myBinding);

                    ShowDoneButtons(false);
                    ShowVideo(true);
                    btnTakePicture.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    CameraHandler.StartLiveView();
                    camerabox.Visibility = System.Windows.Visibility.Collapsed;
                    LVCanvas.Visibility = System.Windows.Visibility.Visible;
                    ShowDoneButtons(false);
                    btnTakeCanonPicture.Visibility = System.Windows.Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
        }

        private void TakePhotoButton_Click(object sender, RoutedEventArgs e)
        {
            if (whichCamera == 1)
            {
                //TakePhotograph();
                pausephoto = true;
            }
            else
            {
                ShowVideo(false);
                searchpanel.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void ShowDoneButtons(bool show)
        {
            if (!show)
            {
                btnTakeAnother.IsEnabled = btnDone.IsEnabled = false;
                btnTakeAnother.Opacity = btnDone.Opacity = 0.3;
            }
            else
            {
                btnDone.IsEnabled = btnTakeAnother.IsEnabled = true;
                btnDone.Opacity = btnTakeAnother.Opacity = 1;
                btnTakePicture.Visibility = btnTakeCanonPicture.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void ShowVideo(bool show)
        {
            if (!show)
            {
                CameraVideoDeviceControl.Visibility = System.Windows.Visibility.Collapsed;
                camerabox.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                CameraVideoDeviceControl.Visibility = System.Windows.Visibility.Visible;
                camerabox.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (whichCamera == 1)
            {
                if (CameraHandler.CameraSessionOpen)
                {
                    LVCanvas.Background = System.Windows.Media.Brushes.LightGray;
                    CameraHandler.CloseSession();
                }

                CameraHandler.Dispose();
            }
            else
            {
                
            }
        }


        #endregion

        #region functions

        async Task TaskDelay()
        {
            await Task.Delay(5000);
        }

        private void ShowCanonItems(bool show)
        {
            if (show)
            {
                Helper.HasCannon = true;
                WBCoBox.SelectedIndex = 0;
                whichCamera = 1;
                CamlistDropdown.Visibility = System.Windows.Visibility.Collapsed;
                LVCanvas.Visibility = System.Windows.Visibility.Visible;
                CameraVideoDeviceControl.Visibility = System.Windows.Visibility.Collapsed;
                btnTakeCanonPicture.Visibility = System.Windows.Visibility.Visible;
                btnTakePicture.Visibility = System.Windows.Visibility.Collapsed;
            } 
            else
            {
                whichCamera = 0;
                CamlistDropdown.Visibility = System.Windows.Visibility.Visible; 
                LVCanvas.Visibility = System.Windows.Visibility.Collapsed;
                CameraVideoDeviceControl.Visibility = System.Windows.Visibility.Visible;
                btnTakeCanonPicture.Visibility = System.Windows.Visibility.Collapsed;
                btnTakePicture.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private bool ProcessPhotograph()
        {
            using (Bitmap bmp = new Bitmap(imagePath))
            {
                using (Bitmap resizedbmp = (Bitmap)ResizeImageFixedWidth(bmp, 800))
                {
                    bmp.Dispose();
                    resizedbmp.Save(imagePath);

                    return CropFaces();
                }
            }
        }

        private Bitmap RemoveWhiteSpaces(Bitmap bmp)
        {
            int w = bmp.Width;
            int h = bmp.Height;

            Func<int, bool> allWhiteRow = row =>
            {
                for (int i = 0; i < w; ++i)
                    if (bmp.GetPixel(i, row).R != 255)
                        return false;
                return true;
            };

            Func<int, bool> allWhiteColumn = col =>
            {
                for (int i = 0; i < h; ++i)
                    if (bmp.GetPixel(col, i).R != 255)
                        return false;
                return true;
            };

            int topmost = 0;
            for (int row = 0; row < h; ++row)
            {
                if (allWhiteRow(row))
                    topmost = row;
                else break;
            }

            int bottommost = 0;
            for (int row = h - 1; row >= 0; --row)
            {
                if (allWhiteRow(row))
                    bottommost = row;
                else break;
            }

            int leftmost = 0, rightmost = 0;
            for (int col = 0; col < w; ++col)
            {
                if (allWhiteColumn(col))
                    leftmost = col;
                else
                    break;
            }

            for (int col = w - 1; col >= 0; --col)
            {
                if (allWhiteColumn(col))
                    rightmost = col;
                else
                    break;
            }

            if (rightmost == 0) rightmost = w; // As reached left
            if (bottommost == 0) bottommost = h; // As reached top.

            int croppedWidth = rightmost - leftmost;
            int croppedHeight = bottommost - topmost;

            if (croppedWidth == 0) // No border on left or right
            {
                leftmost = 0;
                croppedWidth = w;
            }

            if (croppedHeight == 0) // No border on top or bottom
            {
                topmost = 0;
                croppedHeight = h;
            }

            try
            {
                var target = new Bitmap(croppedWidth, croppedHeight);
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(bmp,
                      new RectangleF(0, 0, croppedWidth, croppedHeight),
                      new RectangleF(leftmost, topmost, croppedWidth, croppedHeight),
                      GraphicsUnit.Pixel);
                }
                return target;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight),
                  ex);
            }
        }

        public System.Drawing.Image ResizeImageFixedWidth(System.Drawing.Image imgToResize, int width)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = ((float)width / (float)sourceWidth);

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (System.Drawing.Image)b;
        }

        public void Save(Bitmap image, int maxWidth, int maxHeight, int quality, string filePath)
        {
            // Get the image's original width and height
            int originalWidth = image.Width;
            int originalHeight = image.Height;

            // To preserve the aspect ratio
            float ratioX = (float)maxWidth / (float)originalWidth;
            float ratioY = (float)maxHeight / (float)originalHeight;
            float ratio = Math.Min(ratioX, ratioY);

            // New width and height based on aspect ratio
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);

            // Convert other formats (including CMYK) to RGB.
            Bitmap newImage = new Bitmap(newWidth, newHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            // Draws the image in the specified size with quality mode set to HighQuality
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            // Get an ImageCodecInfo object that represents the JPEG codec.
            ImageCodecInfo imageCodecInfo = this.GetEncoderInfo(ImageFormat.Jpeg);

            // Create an Encoder object for the Quality parameter.
            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;

            // Create an EncoderParameters object. 
            EncoderParameters encoderParameters = new EncoderParameters(1);

            // Save the image as a JPEG file with quality level.
            EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
            encoderParameters.Param[0] = encoderParameter;
            newImage.Save(filePath, imageCodecInfo, encoderParameters);
        }

        private ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }

        private bool CropFaces()
        {
            try
            {
                using (Bitmap originalImage = new Bitmap(imagePath))
                {
                    string licenseFile = @"C:\Users\Dami\Documents\iFace\iengine.lic";
                    IFace.InitWithLicense(File.ReadAllBytes(licenseFile));
                    DetectFacesFlags[] flags = new DetectFacesFlags[] { DetectFacesFlags.NoFlag };

                    // Search for face in file
                    InstanceContext context = new InstanceContext(1);
                    context.SetParam(Parameter.SpeedAccuracyMode, "accurate");
                    context.DetectFaces(originalImage, 25, 200, flags);
                    System.Drawing.Point[] faceBox = null;

                    if (context.DetectedFaces != 0)
                    {
                        FaceContext faceContext = context.GetFaceContext(0);
                        
                        // Remove background
                        using (Bitmap croppedimg = (Bitmap)faceContext.CropImage())
                        {
                            context.DetectFaces(croppedimg, 25, 200, flags);
                            faceContext = context.GetFaceContext(0);

                            using (Bitmap innermask = (Bitmap)faceContext.GetSegmentationMask(0, 1, 0, out faceBox))
                            {
                                for (int x = 0; x < innermask.Width; x++)
                                {
                                    for (int y = 0; y < innermask.Height; y++)
                                    {
                                        if (innermask.GetPixel(x, y).R == 0)
                                        {
                                            croppedimg.SetPixel(x, y, System.Drawing.Color.White);
                                        }
                                    }
                                }

                                croppedimg.MakeTransparent(System.Drawing.Color.White);
                                croppedimg.Save(pngcroppath, ImageFormat.Png);
                                croppedimg.Save(cropPath);
                            }
                        }

                        IFace.Terminate();
                        return true;
                    }
                    else
                    {
                        IFace.Terminate();
                        return false;
                    }
                }
            }
            catch(Exception ex)
            {
                IFace.Terminate();
                MessageBox.Show("Error cropping face");
                return false;
            }
        }

        #endregion
    }
}
