﻿using Afisteam.Biometrics.Gui;
using Newtonsoft.Json;
using PassportApplicationCommon;
using PassportApplicationCommon.Model;
using PassportEnrolment.Helpers;
using PassportEnrolmentEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PassportEnrolment.Windows
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        #region Variables

        private Cursor def;

        #endregion

        #region Constructor

        public LoginScreen()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Password;
            string message = string.Empty;

            // Create an http client provider:
            string serviceurl = ConfigurationManager.AppSettings["serviceurl"].ToString();
            var provider = new apiClientProvider(serviceurl);
            Dictionary<string, string> _tokenDictionary;

            // Get Token
            _tokenDictionary = await provider.GetTokenDictionary(username, password);

            if (_tokenDictionary != null && _tokenDictionary["access_token"] != null)
            {
                Helper.Operator.OperatorId = int.Parse(_tokenDictionary["UserId"]);
                Helper.Operator.Firstname = _tokenDictionary["Firstname"];
                Helper.Operator.Surname = _tokenDictionary["Surname"];
                Helper.Operator.Email = _tokenDictionary["Email"];
                Helper.Operator.RoleType = _tokenDictionary["OperatorType"];
                Helper.Operator.OperatorTypeName = _tokenDictionary["OperatorTypeName"];
                Helper.Operator.Branch = _tokenDictionary["Branch"];
                Helper.Operator.Roles = JsonConvert.DeserializeObject<List<OperatorRole>>(_tokenDictionary["Roles"]);

                MainWindow window = new MainWindow();
                window.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show(message, "Login Failed", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }           

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtUsername.Focus();
        }

        private void TextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            def = this.Cursor;
            this.Cursor = Cursors.Hand;
        }

        private void TextBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = def;
        }

        private void btnChangePassoword_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ChangePassword pwdWin = new ChangePassword();
            pwdWin.ShowDialog();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion
    }
}
