﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PassportEnrolment.Windows
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Window
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool isValid = ChangePasswordDetails.ValidateUsernamePW();

                if (isValid)
                {
                    string message = ""; //DataLink.ChangePassword(ChangePasswordDetails.txtUsername.Text, ChangePasswordDetails.txtPassword.Password, ChangePasswordDetails.txtConfirmPassword.Password);

                    if (message == string.Empty)
                    {
                        MessageBox.Show("Password successfully changed", "Status", MessageBoxButton.OK, MessageBoxImage.Information);
                        Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error changing password", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
