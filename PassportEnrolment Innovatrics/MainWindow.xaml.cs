﻿using PassportEnrolment.Helpers;
using PassportEnrolment.Windows;
using PassportEnrolmentEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PassportEnrolment
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Helper.MainWindow = this;
            mainTabControl.Height = System.Windows.SystemParameters.VirtualScreenHeight - 150;
        }

        public void SetRoleItems()
        {
            if (PiggyBank.CurrentRoles != null && PiggyBank.CurrentRoles.Count != 0)
            {
                if (PiggyBank.hasPrivilege("Is_User_administrator"))
                {
                    
                }
            }
            else
            {
                DefaultText.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            LoginScreen login = new LoginScreen();

            login.Show();
            Close();
        }
    }
}
