﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class SetPropertyCommand : Command
    {
        // Command IDs
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;

        private int propertyID;

        private int data;

        public SetPropertyCommand(CameraModel model, int propertyID, int data)
            : base(model)
        {
            this.propertyID = propertyID;
            this.data = data;
        }

        //// Execute a command.	
        public override bool execute()
        {

            int err = EDSDKErrors.EDS_ERR_OK;
            bool locked = false;

            //// You should do UILock when you send a command to camera models elder than EOS30D.

            if (base.model.isLegacy())
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UILock, 0);


                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    locked = true;

                }
            }

            //// Stock the property.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsSetPropertyData(base.model.getCameraObject(), this.propertyID, 0, Marshal.SizeOf(this.data), this.data);

            }


            if (locked)
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UIUnLock, 0);

            }

            //// Notify Error.
            if (err != EDSDKErrors.EDS_ERR_OK)
            {
                //// Retry when the camera replys deviceBusy.

                if (err == EDSDKErrors.EDS_ERR_DEVICE_BUSY)
                {
                    base.model.notifyObservers(warn, err);

                    return false;
                }

                base.model.notifyObservers(errr, err);

            }

            return true;

        }

    }

}