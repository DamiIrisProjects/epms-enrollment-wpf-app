﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace CameraControllerTest
{
    public class TakePictureCommand : Command
    {
        // Command IDs
        public const int errr = 1;
        public const int prog = 2;
        public const int strt = 3;
        public const int cplt = 4;
        public const int warn = 5;
        public const int updt = 6;
        public const int upls = 7;
        public const int clse = 8;

        public TakePictureCommand(CameraModel model)
            : base(model)
        {
        }

        //// Execute a command.	
        public override bool execute()
        {

            int err = EDSDKErrors.EDS_ERR_OK;
            bool locked = false;

            //// You should do UILock when you send a command to camera models elder than EOS30D.

            if (base.model.isLegacy())
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UILock, 0);


                if (err == EDSDKErrors.EDS_ERR_OK)
                {
                    locked = true;

                }
            }

            //// Take a picture.

            if (err == EDSDKErrors.EDS_ERR_OK)
            {
                err = EDSDK.EdsSendCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraCommand.kEdsCameraCommand_TakePicture, 0);

            }



            if (locked)
            {
                err = EDSDK.EdsSendStatusCommand(base.model.getCameraObject(), (int)EDSDKTypes.EdsCameraStatusCommand.kEdsCameraStatusCommand_UIUnLock, 0);

            }

            //// Notify Error.

            if (err != EDSDKErrors.EDS_ERR_OK)
            {
                //// Do not retry when the camera replys deviceBusy.

                if (err == EDSDKErrors.EDS_ERR_DEVICE_BUSY)
                {
                    base.model.notifyObservers(warn, err);

                    return true;

                }

                base.model.notifyObservers(errr, err);

            }

            return true;

        }

    }

}