using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public static class EDSDK
    {
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        //===================================================
        //
        // EDSDK.h
        //
        //===================================================

        ///******************************************************************************
        //*******************************************************************************
        ////
        ////  initialize / terminate
        ////
        //*******************************************************************************
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsInitializeSDK
        ////
        ////  Description:
        ////      Initializes the libraries. 
        ////      When using the EDSDK libraries, you must call this API once  
        ////          before using EDSDK APIs.
        ////
        ////  Parameters:
        ////       In:    None
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsInitializeSDK();
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsTerminateSDK
        ////
        ////  Description:
        ////      Terminates use of the libraries. 
        ////      This function muse be called when ending the SDK.
        ////      Calling this function releases all resources allocated by the libraries.
        ////
        ////  Parameters:
        ////       In:    None
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsTerminateSDK();
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///******************************************************************************
        //*******************************************************************************
        ////
        ////  reference counter
        ////
        //*******************************************************************************
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsRetain
        ////
        ////  Description:
        ////     Increment the reference counter of the item.
        ////
        ////  Parameters:
        ////       In:   inRef - The reference for the item.
        ////      Out:   None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsRetain(IntPtr inRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsRelease
        ////
        ////  Description:
        ////     Decrement the reference counter of the item.
        ////     The item is not deleted until a reference counter is set to 0.
        ////
        ////  Parameters:
        ////       In:   inRef - The reference of the item.
        ////      Out:   None
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsRelease(IntPtr inRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///******************************************************************************
        //*******************************************************************************
        ////
        ////  item tree 
        ////
        //*******************************************************************************
        //******************************************************************************/


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetChildCount
        ////
        ////  Description:
        ////      Gets the number of child objects of the designated object.
        ////      Example: Number of files in a directory
        ////
        ////  Parameters:
        ////       In:    inRef - The reference of the list.
        ////      Out:    outCount - Number of elements in this list.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetChildCount(IntPtr inCameraListRef, ref int outCount);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetChildAtIndex
        ////
        ////  Description:
        ////       Gets an indexed child object of the designated object. 
        ////
        ////  Parameters:
        ////       In:    inRef - The reference of the item.
        ////              inIndex -  The index that is passed in, is zero based.
        ////      Out:    outRef - The pointer which receives reference of the 
        ////                           specified index .
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetChildAtIndex(IntPtr inRef, int inIndex, ref IntPtr outRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetParent
        ////
        ////  Description:
        ////      Gets the parent object of the designated object.
        ////
        ////  Parameters:
        ////       In:    inRef        - The reference of the item.
        ////      Out:    outParentRef - The pointer which receives reference.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetParent(IntPtr inRef, ref IntPtr outParentRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///******************************************************************************
        //*******************************************************************************
        ////
        ////  property 
        ////
        //*******************************************************************************
        //******************************************************************************/


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetPropertySize
        ////
        ////  Description:
        ////      Gets the byte size and data type of a designated property 
        ////          from a camera object or image object.
        ////
        ////  Parameters:
        ////       In:    inRef - The reference of the item.
        ////              inPropertyID - The ProprtyID
        ////              inParam - Additional information of property.
        ////                   We use this parameter in order to specify an index
        ////                   in case there are two or more values over the same ID.
        ////      Out:    outDataType - Pointer to the buffer that is to receive the property
        ////                        type data.
        ////              outSize - Pointer to the buffer that is to receive the property
        ////                        size.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetPropertySize(IntPtr inRef, int inPropertyID, int inParam, ref CameraControllerTest.EDSDKTypes.EdsDataType outDataType, ref int outSize);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetPropertyData
        ////
        ////  Description:
        ////     Get the property data value.
        ////
        ////  Parameters:
        ////       In:   inRef - The reference of the item.
        ////             inPropertyID - The ProprtyID
        ////             inParam - Additional information of property.
        ////                  We use this parameter in order to specify an index
        ////                  in case there are two or more values over the same ID.
        ////             inPropertySize - The number of bytes of the prepared buffer
        ////                 for receive property-value.
        ////      Out:   outPropertyValue - The buffer pointer to receive property-value.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetPropertyData(IntPtr inRef, int inPropertyID, int inParam, int inPropertySize, IntPtr outPropertyData);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSetPropertyData
        ////
        ////  Description:
        ////      Sets property data for the object designated in inRef. 
        ////
        ////  Parameters:
        ////       In:    inRef - The reference of the item.
        ////              inPropertyID - The ProprtyID
        ////              inParam - Additional information of property.
        ////              inPropertySize - The number of bytes of the prepared buffer
        ////                  for set property-value.
        ////              inPropertyData - The buffer pointer to set property-value.
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSetPropertyData(IntPtr inRef, int inPropertyID, int inParam, int inPropertySize, [MarshalAs(UnmanagedType.AsAny)]
object inPropertyData);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////  
        ////  Function:   EdsGetPropertyDesc
        ////
        ////  Description:
        ////      Gets a list of property data that can be set for the object 
        ////          designated in inRef, as well as maximum and minimum values. 
        ////      This API is intended for only some shooting-related properties.
        ////
        ////  Parameters:
        ////       In:    inRef - The reference of the camera.
        ////              inPropertyID - The Property ID.
        ////       Out:   outPropertyDesc - Array of the value which can be set up.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetPropertyDesc(IntPtr inRef, int inPropertyID, ref CameraControllerTest.EDSDKTypes.EdsPropertyDesc outPropertyDesc);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///******************************************************************************
        //*******************************************************************************
        ////
        //// device-list and device operation 
        ////
        //*******************************************************************************
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetCameraList
        ////
        ////  Description:
        ////      Gets camera list objects.
        ////
        ////  Parameters:
        ////       In:    None
        ////      Out:    outCameraListRef - Pointer to the camera-list.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetCameraList(ref IntPtr outCameraListRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///******************************************************************************
        //*******************************************************************************
        ////
        //// camera operation 
        ////
        //*******************************************************************************
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetDeviceInfo
        ////
        ////  Description:
        ////      Gets device information, such as the device name.  
        ////      Because device information of remote cameras is stored 
        ////          on the host computer, you can use this API 
        ////          before the camera object initiates communication
        ////          (that is, before a session is opened). 
        ////
        ////  Parameters:
        ////       In:    inCameraRef - The reference of the camera.
        ////      Out:    outDeviceInfo - Information as device of camera.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetDeviceInfo(IntPtr inCameraRef, ref CameraControllerTest.EDSDKTypes.EdsDeviceInfo outDeviceInfo);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsOpenSession
        ////
        ////  Description:
        ////      Establishes a logical connection with a remote camera. 
        ////      Use this API after getting the camera's EdsCamera object.
        ////
        ////  Parameters:
        ////       In:    inCameraRef - The reference of the camera 
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsOpenSession(IntPtr inCameraRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCloseSession
        ////
        ////  Description:
        ////       Closes a logical connection with a remote camera.
        ////
        ////  Parameters:
        ////       In:    inCameraRef - The reference of the camera 
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCloseSession(IntPtr inCameraRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSendCommand
        ////
        ////  Description:
        ////       Sends a command such as "Shoot" to a remote camera. 
        ////
        ////  Parameters:
        ////       In:    inCameraRef - The reference of the camera which will receive the 
        ////                      command.
        ////              inCommand - Specifies the command to be sent.
        ////              inParam -     Specifies additional command-specific information.
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSendCommand(IntPtr inCameraRef, int inCommandID, int inParam);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSendStatusCommand
        ////
        ////  Description:
        ////       Sets the remote camera state or mode.
        ////
        ////  Parameters:
        ////       In:    inCameraRef - The reference of the camera which will receive the 
        ////                      command.
        ////              inStatusCommand - Specifies the command to be sent.
        ////              inParam -     Specifies additional command-specific information.
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSendStatusCommand(IntPtr inCameraRef, int inStatusCommand, int inParam);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSetCapacity
        ////
        ////  Description:
        ////      Sets the remaining HDD capacity on the host computer
        ////          (excluding the portion from image transfer),
        ////          as calculated by subtracting the portion from the previous time. 
        ////      Set a reset flag initially and designate the cluster length 
        ////          and number of free clusters.
        ////      Some type 2 protocol standard cameras can display the number of shots 
        ////          left on the camera based on the available disk capacity 
        ////          of the host computer. 
        ////      For these cameras, after the storage destination is set to the computer, 
        ////          use this API to notify the camera of the available disk capacity 
        ////          of the host computer.
        ////
        ////  Parameters:
        ////       In:    inCameraRef - The reference of the camera which will receive the 
        ////                      command.
        ////              inCapacity -  The remaining capacity of a transmission place.
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSetCapacity(IntPtr inCameraRef, CameraControllerTest.EDSDKTypes.EdsCapacity inCapacity);
        [DllImport("EDSDK", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]

        ///******************************************************************************
        //*******************************************************************************
        ////
        //// volume operation
        ////
        //*******************************************************************************
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetVolumeInfo
        ////
        ////  Description:
        ////      Gets volume information for a memory card in the camera.
        ////
        ////  Parameters:
        ////       In:    inVolumeRef - The reference of the volume.
        ////      Out:    outVolumeInfo - information of  the volume.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetVolumeInfo(IntPtr inVolumeRef, ref CameraControllerTest.EDSDKTypes.EdsVolumeInfo outVolumeInfo);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsFormatVolume
        ////
        ////  Description:
        ////       .
        ////
        ////  Parameters:
        ////       In:    inVolumeRef - The reference of volume .
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsFormatVolume(IntPtr inVolumeRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///******************************************************************************
        //*******************************************************************************
        ////
        //// directory item operation
        ////
        //*******************************************************************************
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetDirectoryItemInfo
        ////
        ////  Description:
        ////      Gets information about the directory or file objects 
        ////          on the memory card (volume) in a remote camera.
        ////
        ////  Parameters:
        ////       In:    inDirItemRef - The reference of the directory item.
        ////      Out:    outDirItemInfo - information of the directory item.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetDirectoryItemInfo(IntPtr inDirItemRefs, ref CameraControllerTest.EDSDKTypes.EdsDirectoryItemInfo outDirItemInfo);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsDeleteDirectoryItem
        ////
        ////  Description:
        ////      Deletes a camera folder or file.
        ////      If folders with subdirectories are designated, all files are deleted 
        ////          except protected files. 
        ////      EdsDirectoryItem objects deleted by means of this API are implicitly 
        ////          released by the EDSDK. Thus, there is no need to release them 
        ////          by means of EdsRelease.
        ////
        ////  Parameters:
        ////       In:    inDirItemRef - The reference of the directory item.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsDeleteDirectoryItem(IntPtr inDirItemRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsDownload
        ////
        ////  Description:
        ////       Downloads a file on a remote camera 
        ////          (in the camera memory or on a memory card) to the host computer. 
        ////      The downloaded file is sent directly to a file stream created in advance. 
        ////      When dividing the file being retrieved, call this API repeatedly. 
        ////      Also in this case, make the data block size a multiple of 512 (bytes), 
        ////          excluding the final block.
        ////
        ////  Parameters:
        ////       In:    inDirItemRef - The reference of the directory item.
        ////              inReadSize   - 
        ////
        ////      Out:    outStream    - The reference of the stream.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsDownload(IntPtr inDirItemRef, int inReadSize, IntPtr outStream);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsDownloadCancel
        ////
        ////  Description:
        ////       Must be executed when downloading of a directory item is canceled. 
        ////      Calling this API makes the camera cancel file transmission.
        ////      It also releases resources. 
        ////      This operation need not be executed when using EdsDownloadThumbnail. 
        ////
        ////  Parameters:
        ////       In:    inDirItemRef - The reference of the directory item.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsDownloadCancel(IntPtr inDirItemRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsDownloadComplete
        ////
        ////  Description:
        ////       Must be called when downloading of directory items is complete. 
        ////          Executing this API makes the camera 
        ////              recognize that file transmission is complete. 
        ////          This operation need not be executed when using EdsDownloadThumbnail.
        ////
        ////  Parameters:
        ////       In:    inDirItemRef - The reference of the directory item.
        ////
        ////      Out:    outStream    - None.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsDownloadComplete(IntPtr inDirItemRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsDownloadThumbnail
        ////
        ////  Description:
        ////      Extracts and downloads thumbnail information from image files in a camera. 
        ////      Thumbnail information in the camera's image files is downloaded 
        ////          to the host computer. 
        ////      Downloaded thumbnails are sent directly to a file stream created in advance.
        ////
        ////  Parameters:
        ////       In:    inDirItemRef - The reference of the directory item.
        ////
        ////      Out:    outStream - The reference of the stream.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsDownloadThumbnail(IntPtr inDirItemRef, IntPtr outStream);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetAttribute
        ////
        ////  Description:
        ////      Gets attributes of files on a camera.
        ////  
        ////  Parameters:
        ////       In:    inDirItemRef - The reference of the directory item.
        ////      Out:    outFileAttribute  - Indicates the file attributes. 
        ////                  As for the file attributes, OR values of the value defined
        ////                  by enum EdsFileAttributes can be retrieved. Thus, when 
        ////                  determining the file attributes, you must check 
        ////                  if an attribute flag is set for target attributes. 
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetAttribute(IntPtr inDirItemRef, ref CameraControllerTest.EDSDKTypes.EdsFileAttributes outFileAttribute);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSetAttribute
        ////
        ////  Description:
        ////      Changes attributes of files on a camera.
        ////  
        ////  Parameters:
        ////       In:    inDirItemRef - The reference of the directory item.
        ////              inFileAttribute  - Indicates the file attributes. 
        ////                      As for the file attributes, OR values of the value 
        ////                      defined by enum EdsFileAttributes can be retrieved. 
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSetAttribute(IntPtr inDirItemRef, CameraControllerTest.EDSDKTypes.EdsFileAttributes inFileAttribute);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///******************************************************************************
        //*******************************************************************************
        ////
        //// stream operation
        ////
        //*******************************************************************************
        //******************************************************************************/

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCreateFileStream
        ////
        ////  Description:
        ////      Creates a new file on a host computer (or opens an existing file) 
        ////          and creates a file stream for access to the file. 
        ////      If a new file is designated before executing this API, 
        ////          the file is actually created following the timing of writing 
        ////          by means of EdsWrite or the like with respect to an open stream.
        ////
        ////  Parameters:
        ////       In:    inFileName - Pointer to a null-terminated string that specifies
        ////                           the file name.
        ////              inCreateDisposition - Action to take on files that exist, 
        ////                                and which action to take when files do not exist.  
        ////              inDesiredAccess - Access to the stream (reading, writing, or both).
        ////      Out:    outStream - The reference of the stream.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCreateFileStream(string inFileName, int inCreateDisposition, int inDesiredAccess, ref IntPtr outStream);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCreateMemoryStream
        ////
        ////  Description:
        ////      Creates a stream in the memory of a host computer. 
        ////      In the case of writing in excess of the allocated buffer size, 
        ////          the memory is automatically extended.
        ////
        ////  Parameters:
        ////       In:    inBufferSize - The number of bytes of the memory to allocate.
        ////      Out:    outStream - The reference of the stream.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCreateMemoryStream(int inBufferSize, ref IntPtr outStream);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCreateStreamEx
        ////
        ////  Description:
        ////      An extended version of EdsCreateStreamFromFile. 
        ////      Use this function when working with Unicode file names.
        ////
        ////  Parameters:
        ////       In:    inURL (for Macintosh) - Designate CFURLRef. 
        ////              inFileName (for Windows) - Designate the file name. 
        ////              inCreateDisposition - Action to take on files that exist, 
        ////                                and which action to take when files do not exist.  
        ////              inDesiredAccess - Access to the stream (reading, writing, or both).
        ////
        ////      Out:    outStream - The reference of the stream.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCreateFileStreamEx(ref string inFileName, CameraControllerTest.EDSDKTypes.EdsFileCreateDisposition inCreateDisposition, CameraControllerTest.EDSDKTypes.EdsAccess inDesiredAccess, ref IntPtr outStream);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCreateMemoryStreamFromPointer        
        ////
        ////  Description:
        ////      Creates a stream from the memory buffer you prepare. 
        ////      Unlike the buffer size of streams created by means of EdsCreateMemoryStream, 
        ////      the buffer size you prepare for streams created this way does not expand.
        ////
        ////  Parameters:
        ////       In:    inBufferSize - The number of bytes of the memory to allocate.
        ////      Out:    outStream - The reference of the stream.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCreateMemoryStreamFromPointer(IntPtr inUserBuffer, int inBufferSize, ref IntPtr outStream);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetPointer                   
        ////
        ////  Description:
        ////      Gets the pointer to the start address of memory managed by the memory stream. 
        ////      As the EDSDK automatically resizes the buffer, the memory stream provides 
        ////          you with the same access methods as for the file stream. 
        ////      If access is attempted that is excessive with regard to the buffer size
        ////          for the stream, data before the required buffer size is allocated 
        ////          is copied internally, and new writing occurs. 
        ////      Thus, the buffer pointer might be switched on an unknown timing. 
        ////      Caution in use is therefore advised. 
        ////
        ////  Parameters:
        ////       In:    inStream - Designate the memory stream for the pointer to retrieve. 
        ////      Out:    outPointer - If successful, returns the pointer to the buffer 
        ////                  written in the memory stream.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetPointer(IntPtr inStream, ref IntPtr outPointer);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsRead
        ////
        ////  Description:
        ////      Reads data the size of inReadSize into the outBuffer buffer, 
        ////          starting at the current read or write position of the stream. 
        ////      The size of data actually read can be designated in outReadSize.
        ////
        ////  Parameters:
        ////       In:    inStreamRef - The reference of the stream or image.
        ////              inReadSize -  The number of bytes to read.
        ////      Out:    outBuffer - Pointer to the user-supplied buffer that is to receive
        ////                          the data read from the stream. 
        ////              outReadSize - The actually read number of bytes.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsRead(IntPtr inStreamRef, int inReadSize, IntPtr outBuffer, ref int outReadSize);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsWrite
        ////
        ////  Description:
        ////      Writes data of a designated buffer 
        ////          to the current read or write position of the stream. 
        ////
        ////  Parameters:
        ////       In:    inStreamRef  - The reference of the stream or image.
        ////              inWriteSize - The number of bytes to write.
        ////              inBuffer - A pointer to the user-supplied buffer that contains 
        ////                         the data to be written to the stream.
        ////      Out:    outWrittenSize - The actually written-in number of bytes.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsWrite(IntPtr inStreamRef, int inWriteSize, IntPtr inBuffer, ref int outWrittenSize);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSeek
        ////
        ////  Description:
        ////      Moves the read or write position of the stream
        //            (that is, the file position indicator).
        ////
        ////  Parameters:
        ////       In:    inStreamRef  - The reference of the stream or image. 
        ////              inSeekOffset - Number of bytes to move the pointer. 
        ////              inSeekOrigin - Pointer movement mode. Must be one of the following 
        ////                             values.
        ////                  kEdsSeek_Cur     Move the stream pointer inSeekOffset bytes 
        ////                                   from the current position in the stream. 
        ////                  kEdsSeek_Begin   Move the stream pointer inSeekOffset bytes
        ////                                   forward from the beginning of the stream. 
        ////                  kEdsSeek_End     Move the stream pointer inSeekOffset bytes
        ////                                   from the end of the stream. 
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSeek(IntPtr inStreamRef, int inSeekOffset, CameraControllerTest.EDSDKTypes.EdsSeekOrigin inSeekOrigin);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetPosition
        ////
        ////  Description:
        ////       Gets the current read or write position of the stream
        ////          (that is, the file position indicator).
        ////
        ////  Parameters:
        ////       In:    inStreamRef - The reference of the stream or image.
        ////      Out:    outPosition - The current stream pointer.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetPosition(IntPtr inStreamRef, ref int outPosition);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetLength
        ////
        ////  Description:
        ////      Gets the stream size.
        ////
        ////  Parameters:
        ////       In:    inStreamRef - The reference of the stream or image.
        ////      Out:    outLength - The length of the stream.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetLength(IntPtr inStreamRef, ref int outLength);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCopyData
        ////
        ////  Description:
        ////      Copies data from the copy source stream to the copy destination stream. 
        ////      The read or write position of the data to copy is determined from 
        ////          the current file read or write position of the respective stream. 
        ////      After this API is executed, the read or write positions of the copy source 
        ////          and copy destination streams are moved an amount corresponding to 
        ////          inWriteSize in the positive direction. 
        ////
        ////  Parameters:
        ////       In:    inStreamRef - The reference of the stream or image.
        ////              inWriteSize - The number of bytes to copy.
        ////      Out:    outStreamRef - The reference of the stream or image.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCopyData(IntPtr inStreamRef, int inWriteSize, IntPtr OutStreamRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]



        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSetProgressCallback
        ////
        ////  Description:
        ////      Register a progress callback function. 
        ////      An event is received as notification of progress during processing that 
        ////          takes a relatively long time, such as downloading files from a
        ////          remote camera. 
        ////      If you register the callback function, the EDSDK calls the callback
        ////          function during execution or on completion of the following APIs. 
        ////      This timing can be used in updating on-screen progress bars, for example.
        ////
        ////  Parameters:
        ////       In:    inRef - The reference of the stream or image.
        ////              inProgressCallback - Pointer to a progress callback function.
        ////              inProgressOption - The option about progress is specified.
        ////                              Must be one of the following values.
        ////                         kEdsProgressOption_Done 
        ////                             When processing is completed,a callback function
        ////                             is called only at once.
        ////                         kEdsProgressOption_Periodically
        ////                             A callback function is performed periodically.
        ////              inContext - Application information, passed in the argument 
        ////                      when the callback function is called. Any information 
        ////                      required for your program may be added. 
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSetProgressCallback(IntPtr inRef, CameraControllerTest.EDSDKTypes.EdsProgressCallback inProgressCallback, CameraControllerTest.EDSDKTypes.EdsProgressOption inProgressOption, IntPtr inContext);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]



        ///******************************************************************************
        //*******************************************************************************
        ////
        //// image operation
        ////
        //*******************************************************************************
        //******************************************************************************/


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCreateImageRef
        ////
        ////  Description:
        ////      Creates an image object from an image file. 
        ////      Without modification, stream objects cannot be worked with as images. 
        ////      Thus, when extracting images from image files, 
        ////          you must use this API to create image objects. 
        ////      The image object created this way can be used to get image information 
        ////          (such as the height and width, number of color components, and
        ////           resolution), thumbnail image data, and the image data itself.
        ////
        ////  Parameters:
        ////       In:    inStreamRef - The reference of the stream.
        ////
        ////       Out:    outImageRef - The reference of the image.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCreateImageRef(IntPtr inStreamRef, ref IntPtr outImageRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]




        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetImageInfo
        ////
        ////  Description:
        ////      Gets image information from a designated image object. 
        ////      Here, image information means the image width and height, 
        ////          number of color components, resolution, and effective image area.
        ////
        ////  Parameters:
        ////       In:    inStreamRef - Designate the object for which to get image information. 
        ////              inImageSource - Of the various image data items in the image file,
        ////                  designate the type of image data representing the 
        ////                  information you want to get. Designate the image as
        ////                  defined in Enum EdsImageSource. 
        ////
        ////                      kEdsImageSrc_FullView
        ////                                  The image itself (a full-sized image) 
        ////                      kEdsImageSrc_Thumbnail
        ////                                  A thumbnail image 
        ////                      kEdsImageSrc_Preview
        ////                                  A preview image
        ////                      kEdsImageSrc_RAWThumbnail
        ////                                  A RAW thumbnail image 
        ////                      kEdsImageSrc_RAWFullView
        ////                                  A RAW full-sized image 
        ////       Out:    outImageInfo - Stores the image data information designated 
        ////                      in inImageSource. 
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetImageInfo(IntPtr inImageRef, CameraControllerTest.EDSDKTypes.EdsImageSource inImageSource, ref CameraControllerTest.EDSDKTypes.EdsImageInfo outImageInfo);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsGetImage                         
        ////
        ////  Description:
        ////      Gets designated image data from an image file, in the form of a
        ////          designated rectangle. 
        ////      Returns uncompressed results for JPEGs and processed results 
        ////          in the designated pixel order (RGB, Top-down BGR, and so on) for
        ////           RAW images. 
        ////      Additionally, by designating the input/output rectangle, 
        ////          it is possible to get reduced, enlarged, or partial images. 
        ////      However, because images corresponding to the designated output rectangle 
        ////          are always returned by the SDK, the SDK does not take the aspect 
        ////          ratio into account. 
        ////      To maintain the aspect ratio, you must keep the aspect ratio in mind 
        ////          when designating the rectangle. 
        ////
        ////  Parameters:
        ////      In:     
        ////              inImageRef - Designate the image object for which to get 
        ////                      the image data.
        ////              inImageSource - Designate the type of image data to get from
        ////                      the image file (thumbnail, preview, and so on). 
        ////                      Designate values as defined in Enum EdsImageSource. 
        ////              inImageType - Designate the output image type. Because
        ////                      the output format of EdGetImage may only be RGB, only
        ////                      kEdsTargetImageType_RGB or kEdsTargetImageType_RGB16
        ////                      can be designated. 
        ////                      However, image types exceeding the resolution of 
        ////                      inImageSource cannot be designated. 
        ////              inSrcRect - Designate the coordinates and size of the rectangle
        ////                      to be retrieved (processed) from the source image. 
        ////              inDstSize - Designate the rectangle size for output. 
        ////
        ////      Out:    
        ////              outStreamRef - Designate the memory or file stream for output of
        ////                      the image.
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsGetImage(IntPtr inImageRef, CameraControllerTest.EDSDKTypes.EdsImageSource inImageSource, CameraControllerTest.EDSDKTypes.EdsTargetImageType inImageType, CameraControllerTest.EDSDKTypes.EdsRect inSrcRect, CameraControllerTest.EDSDKTypes.EdsSize inDstSize, IntPtr outStreamRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSaveImage            
        ////
        ////  Description:
        ////      Saves as a designated image type after RAW processing. 
        ////      When saving with JPEG compression, 
        ////          the JPEG quality setting applies with respect to EdsOptionRef.
        ////
        ////  Parameters:
        ////      In:    
        ////          inImageRef - Designate the image object for which to produce the file. 
        ////          inImageType - Designate the image type to produce. Designate the 
        ////                  following image types.
        ////
        ////                  kEdsTargetImageType - Jpeg  JPEG
        ////                  kEdsTargetImageType - TIFF  8-bit TIFF
        ////                  kEdsTargetImageType - TIFF16    16-bit TIFF
        ////          inSaveSetting - Designate saving options, such as JPEG image quality.
        ////      Out:    
        ////          outStreamRef - Specifies the output file stream. The memory stream 
        ////                  cannot be specified here.
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSaveImage(IntPtr inImageRef, CameraControllerTest.EDSDKTypes.EdsTargetImageType inImageType, CameraControllerTest.EDSDKTypes.EdsSaveImageSetting inSaveSetting, IntPtr outStreamRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCacheImage                       
        ////
        ////  Description:
        ////      Switches a setting on and off for creation of an image cache in the SDK 
        ////          for a designated image object during extraction (processing) of
        ////          the image data. 
        ////          Creating the cache increases the processing speed, starting from
        ////          the second time.
        ////
        ////  Parameters:
        ////      In:     inImageRef - The reference of the image.
        ////              inUseCache - If cache image data or not
        ////                          If set to FALSE, the cached image data will released.
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCacheImage(IntPtr inImageRef, bool inUseCache);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsReflectImageProperty         
        ////  Description:
        ////      Incorporates image object property changes 
        ////          (effected by means of EdsSetPropertyData) in the stream. 
        ////
        ////  Parameters:
        ////      In:     inImageRef - The reference of the image.
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsReflectImageProperty(IntPtr inImageRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsCreateEvfImageRef         
        ////  Description:
        ////       Creates an object used to get the live view image data set. 
        ////
        ///  Parameters:
        ////      In:     inStreamRef - The stream reference which opened to get EVF JPEG image.
        ////      Out:    outEvfImageRef - The EVFData reference.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsCreateEvfImageRef(IntPtr inStreamRef, ref IntPtr outEvfImageRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsDownloadEvfImage         
        ////  Description:
        ////		Downloads the live view image data set for a camera currently in live view mode.
        ////		Live view can be started by using the property ID:kEdsPropertyID_Evf_OutputDevice and
        ////		data:EdsOutputDevice_PC to call EdsSetPropertyData.
        ////		In addition to image data, information such as zoom, focus position, and histogram data
        ////		is included in the image data set. Image data is saved in a stream maintained by EdsEvfImageRef.
        ////		EdsGetPropertyData can be used to get information such as the zoom, focus position, etc.
        ////		Although the information of the zoom and focus position can be obtained from EdsEvfImageRef,
        ////		settings are applied to EdsCameraRef.
        ////
        ////  Parameters:
        ////      In:     inCameraRef - The Camera reference.
        ////      In:     inEvfImageRef - The EVFData reference.
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsDownloadEvfImage(IntPtr inCameraRef, IntPtr inEvfImageRef);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]

        ///******************************************************************************
        //*******************************************************************************
        ////
        //// event handler
        ////
        //*******************************************************************************
        //******************************************************************************/


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSetCameraAddedHandler
        ////
        ////  Description:
        ////      Registers a callback function for when a camera is detected.
        ////
        ////  Parameters:
        ////       In:    inCameraAddedHandler - Pointer to a callback function
        ////                          called when a camera is connected physically
        ////              inContext - Specifies an application-defined value to be sent to
        ////                          the callback function pointed to by CallBack parameter.
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSetCameraAddedHandler(CameraControllerTest.EDSDKTypes.EdsCameraAddedHandler inCameraAddedHandler, IntPtr inContext);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSetPropertyEventHandler
        ////              
        ////  Description:
        ////       Registers a callback function for receiving status 
        ////          change notification events for property states on a camera.
        ////
        ////  Parameters:
        ////       In:    inCameraRef - Designate the camera object. 
        ////              inEvent - Designate one or all events to be supplemented.
        ////              inPropertyEventHandler - Designate the pointer to the callback
        ////                      function for receiving property-related camera events.
        ////              inContext - Designate application information to be passed by 
        ////                      means of the callback function. Any data needed for
        ////                      your application can be passed. 
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSetPropertyEventHandler(IntPtr inCameraRef, int inEvnet, CameraControllerTest.EDSDKTypes.EdsPropertyEventHandler inPropertyEventHandler, IntPtr inContext);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:   EdsSetObjectEventHandler
        ////              
        ////  Description:
        ////       Registers a callback function for receiving status 
        ////          change notification events for objects on a remote camera. 
        ////      Here, object means volumes representing memory cards, files and directories, 
        ////          and shot images stored in memory, in particular. 
        ////
        ////  Parameters:
        ////       In:    inCameraRef - Designate the camera object. 
        ////              inEvent - Designate one or all events to be supplemented.
        ////                  To designate all events, use kEdsObjectEvent_All. 
        ////              inObjectEventHandler - Designate the pointer to the callback function
        ////                  for receiving object-related camera events.
        ////              inContext - Passes inContext without modification,
        ////                  as designated as an EdsSetObjectEventHandler argument. 
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSetObjectEventHandler(IntPtr inCameraRef, int inEvnet, CameraControllerTest.EDSDKTypes.EdsObjectEventHandler inObjectEventHandler, IntPtr inContext);
        [DllImport("EDSDK", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]


        ///*-----------------------------------------------------------------------------
        ////
        ////  Function:  EdsSetCameraStateEventHandler
        ////              
        ////  Description:
        ////      Registers a callback function for receiving status 
        ////          change notification events for property states on a camera.
        ////
        ////  Parameters:
        ////       In:    inCameraRef - Designate the camera object. 
        ////              inEvent - Designate one or all events to be supplemented.
        ////                  To designate all events, use kEdsStateEvent_All. 
        ////              inStateEventHandler - Designate the pointer to the callback function
        ////                  for receiving events related to camera object states.
        ////              inContext - Designate application information to be passed
        ////                  by means of the callback function. Any data needed for
        ////                  your application can be passed. 
        ////      Out:    None
        ////
        ////  Returns:    Any of the sdk errors.
        //-----------------------------------------------------------------------------*/
        public static extern int EdsSetCameraStateEventHandler(IntPtr inCameraRef, int inEvnet, CameraControllerTest.EDSDKTypes.EdsStateEventHandler inStateEventHandler, IntPtr inContext);

    }
}