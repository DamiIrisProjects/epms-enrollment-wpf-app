﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace CameraControllerTest
{
    public class CameraModel : Observable
    {
        protected IntPtr camera;
        //// UIlock counter

        protected int lockCount;
        //// Model name

        protected string _modelName;
        //// Parameters
        protected int AEMode;
        protected int Av;
        protected int Tv;
        protected int Iso;
        protected int MeteringMode;
        protected int ExposureCompensation;
        protected int ImageQuality;

        protected int availableShot;
        //// Available parameter lists
        protected CameraControllerTest.EDSDKTypes.EdsPropertyDesc AEModeDesc;
        protected CameraControllerTest.EDSDKTypes.EdsPropertyDesc AvDesc;
        protected CameraControllerTest.EDSDKTypes.EdsPropertyDesc TvDesc;
        protected CameraControllerTest.EDSDKTypes.EdsPropertyDesc IsoDesc;
        protected CameraControllerTest.EDSDKTypes.EdsPropertyDesc MeteringModeDesc;
        protected CameraControllerTest.EDSDKTypes.EdsPropertyDesc ExposureCompensationDesc;

        protected CameraControllerTest.EDSDKTypes.EdsPropertyDesc ImageQualityDesc;

        //// Constructor
        public CameraModel(IntPtr camera)
        {
            this.lockCount = 0;
            this.camera = camera;
        }

        //// Get a camera object
        public IntPtr getCameraObject()
        {
            return this.camera;
        }


        //// -----------------------------------------------------------------
        //// Stock parameters ---------------------------------------------

        private void setAEMode(int value)
        {
            AEMode = value;
        }

        private void setTv(int value)
        {
            Tv = value;
        }

        private void setAv(int value)
        {
            Av = value;
        }

        private void setIso(int value)
        {
            Iso = value;
        }

        private void setMeteringMode(int value)
        {
            MeteringMode = value;
        }

        private void setExposureCompensation(int value)
        {
            ExposureCompensation = value;
        }

        private void setImageQuality(int value)
        {
            ImageQuality = value;
        }

        private void setModelName(string modelName)
        {
            _modelName = modelName;
        }

        //// -----------------------------------------------------------------
        //// Give parameters ---------------------------------------------

        private int getAEMode()
        {
            return AEMode;
        }

        private int getTv()
        {
            return Tv;
        }

        private int getAv()
        {
            return Av;
        }

        private int getIso()
        {
            return Iso;
        }

        private int getMeteringMode()
        {
            return MeteringMode;
        }

        private int getExposureCompensation()
        {
            return ExposureCompensation;
        }

        private int getImageQuality()
        {
            return ImageQuality;
        }

        //// -----------------------------------------------------------------
        //// Give available parameter lists ----------------------------------

        private CameraControllerTest.EDSDKTypes.EdsPropertyDesc getAEModeDesc()
        {
            return AEModeDesc;
        }

        private CameraControllerTest.EDSDKTypes.EdsPropertyDesc getAvDesc()
        {
            return AvDesc;
        }

        private CameraControllerTest.EDSDKTypes.EdsPropertyDesc getTvDesc()
        {
            return TvDesc;
        }

        private CameraControllerTest.EDSDKTypes.EdsPropertyDesc getIsoDesc()
        {
            return IsoDesc;
        }

        private CameraControllerTest.EDSDKTypes.EdsPropertyDesc getMeteringModeDesc()
        {
            return MeteringModeDesc;
        }

        private CameraControllerTest.EDSDKTypes.EdsPropertyDesc getExposureCompensationDesc()
        {
            return ExposureCompensationDesc;
        }

        private CameraControllerTest.EDSDKTypes.EdsPropertyDesc getImageQualityDesc()
        {
            return ImageQualityDesc;
        }


        //// -----------------------------------------------------------------
        //// Stock available parameter lists ---------------------------------

        private void setAEModeDesc(CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc)
        {
            AEModeDesc = desc;
        }

        private void setAvDesc(CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc)
        {
            AvDesc = desc;
        }

        private void setTvDesc(CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc)
        {
            TvDesc = desc;
        }

        private void setIsoDesc(CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc)
        {
            IsoDesc = desc;
        }

        private void setMeteringModeDesc(CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc)
        {
            MeteringModeDesc = desc;
        }

        private void setExposureCompensationDesc(CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc)
        {
            ExposureCompensationDesc = desc;
        }

        private void setImageQualityDesc(CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc)
        {
            ImageQualityDesc = desc;
        }


        //// Set a property <UInt32>
        public void setPropertyUInt32(int propertyID, int value)
        {
            switch (propertyID)
            {
                case EDSDKTypes.kEdsPropID_AEModeSelect:
                    setAEMode(value);
                    break;
                case EDSDKTypes.kEdsPropID_Tv:
                    setTv(value);
                    break;
                case EDSDKTypes.kEdsPropID_Av:
                    setAv(value);
                    break;
                case EDSDKTypes.kEdsPropID_ISOSpeed:
                    setIso(value);
                    break;
                case EDSDKTypes.kEdsPropID_MeteringMode:
                    setMeteringMode(value);
                    break;
                case EDSDKTypes.kEdsPropID_ExposureCompensation:
                    setExposureCompensation(value);
                    break;
                case EDSDKTypes.kEdsPropID_ImageQuality:
                    setImageQuality(value);
                    break;
            }
        }


        //// Get a property <UInt32>
        public int getPropertyUInt32(int propertyID)
        {
            int value = unchecked((int)0xffffffff);
            switch (propertyID)
            {
                case EDSDKTypes.kEdsPropID_AEModeSelect:
                    value = getAEMode();
                    break;
                case EDSDKTypes.kEdsPropID_Tv:
                    value = getTv();
                    break;
                case EDSDKTypes.kEdsPropID_Av:
                    value = getAv();
                    break;
                case EDSDKTypes.kEdsPropID_ISOSpeed:
                    value = getIso();
                    break;
                case EDSDKTypes.kEdsPropID_MeteringMode:
                    value = getMeteringMode();
                    break;
                case EDSDKTypes.kEdsPropID_ExposureCompensation:
                    value = getExposureCompensation();
                    break;
                case EDSDKTypes.kEdsPropID_ImageQuality:
                    value = getImageQuality();
                    break;
            }
            return value;

        }


        //// Get a property <String>
        public void getPropertyString(int propertyID, ref string str)
        {
            switch (propertyID)
            {
                case EDSDKTypes.kEdsPropID_ProductName:
                    str = _modelName;
                    break;
            }
        }


        //// Set a property <String>
        public void setPropertyString(int propertyID, string str)
        {
            switch (propertyID)
            {
                case EDSDKTypes.kEdsPropID_ProductName:
                    _modelName = str;
                    break;
            }
        }


        //// Set an available parameter list.
        public void setPropertyDesc(int propertyID, CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc)
        {
            switch (propertyID)
            {
                case EDSDKTypes.kEdsPropID_AEModeSelect:
                    setAEModeDesc(desc);
                    break;
                case EDSDKTypes.kEdsPropID_Tv:
                    setTvDesc(desc);
                    break;
                case EDSDKTypes.kEdsPropID_Av:
                    setAvDesc(desc);
                    break;
                case EDSDKTypes.kEdsPropID_ISOSpeed:
                    setIsoDesc(desc);
                    break;
                case EDSDKTypes.kEdsPropID_MeteringMode:
                    setMeteringModeDesc(desc);
                    break;
                case EDSDKTypes.kEdsPropID_ExposureCompensation:
                    setExposureCompensationDesc(desc);
                    break;
                case EDSDKTypes.kEdsPropID_ImageQuality:
                    setImageQualityDesc(desc);
                    break;
            }
        }

        //// Get an available parameter list.
        public CameraControllerTest.EDSDKTypes.EdsPropertyDesc getPropertyDesc(int propertyID)
        {
            CameraControllerTest.EDSDKTypes.EdsPropertyDesc desc = new EDSDKTypes.EdsPropertyDesc();
            switch (propertyID)
            {
                case EDSDKTypes.kEdsPropID_AEModeSelect:
                    desc = getAEModeDesc();
                    break;
                case EDSDKTypes.kEdsPropID_Tv:
                    desc = getTvDesc();
                    break;
                case EDSDKTypes.kEdsPropID_Av:
                    desc = getAvDesc();
                    break;
                case EDSDKTypes.kEdsPropID_ISOSpeed:
                    desc = getIsoDesc();
                    break;
                case EDSDKTypes.kEdsPropID_MeteringMode:
                    desc = getMeteringModeDesc();
                    break;
                case EDSDKTypes.kEdsPropID_ExposureCompensation:
                    desc = getExposureCompensationDesc();
                    break;
                case EDSDKTypes.kEdsPropID_ImageQuality:
                    desc = getImageQualityDesc();
                    break;
            }
            return desc;
        }

        //// Check camera accessing flag.
        //// Connected camera is not a legacy one, this method will be called.
        public virtual bool isLegacy()
        {
            return false;
        }

    }

}