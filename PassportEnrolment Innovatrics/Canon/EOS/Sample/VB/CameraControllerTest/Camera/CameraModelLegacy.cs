﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
///******************************************************************************
//*                                                                             *
//*   PROJECT : EOS Digital Software Development Kit EDSDK                      *
//*      NAME : CameraModelLegacy.vb                                            *
//*                                                                             *
//*   Description: This is the Sample code to show the usage of EDSDK.          *
//*                                                                             *
//*                                                                             *
//*******************************************************************************
//*                                                                             *
//*   Written and developed by Camera Design Dept.53                            *
//*   Copyright Canon Inc. 2006 All Rights Reserved                             *
//*                                                                             *
//*******************************************************************************
//*   File Update Information:                                                  *
//*     DATE      Identify    Comment                                           *
//*   -----------------------------------------------------------------------   *
//*   06-03-22    F-001        create first version.                            *
//*                                                                             *
//******************************************************************************/

namespace CameraControllerTest
{

    public class CameraModelLegacy : CameraModel
    {

        ////Constructor
        public CameraModelLegacy(IntPtr camera)
            : base(camera)
        {
        }


        public override bool isLegacy()
        {

            return true;

        }

    }

}